---
title: "導入和分享聖經經文集合"
linkTitle: "聖經詩句集"
weight: 9
description: >
  從公開的聖經記憶經文集合中受益，並與他人分享您自己的聖經經文集合。
---

> 經文集合是已發布的標籤。

### 尋找聖經記憶經文的集合 {#find}

從左側導航抽屜選擇「集合」，並使用<span class="material-icons">search</span>
搜索功能來尋找您正在尋找的內容。點擊一個集合卡片以查看更多詳情，並在右上角點擊<span class="material-icons">download</span>
下載圖標以將集合的經文導入您的賬戶。集合名稱的標籤使新經文與您的其他經文區分開來。

### 發布和分享經文的集合

從左側導航抽屜選擇「集合」，然後點擊「發布」按鈕（<span class="material-icons">publish</span>
右上角的上傳圖標）以查看所有附加到經文的標籤的列表。一個<span class="material-icons">public_off</span>
劃掉的世界圖標意味著，該標籤及其經文對其他用戶不可見。

點擊一個標籤並在彈出的表單中提供您的集合描述。要中止或取消先前發布的集合的發布，請在表單底部點擊劃掉的世界圖標。所有發布細節被刪除，並且經文對其他用戶隱藏。當您準備好發布時，點擊表單底部的突出顯示的<span class="material-icons">
public</span>世界圖標。所有發布細節被保存，並且集合將出現在其他用戶的搜索結果中。

### 在您的網站中嵌入您的經文集合

由於記念我6也以網絡應用程序的形式提供，您可以在您的網站中嵌入一個集合或集合的一個經文。您的網站訪問者將能夠在您的網站上直接使用記念我的遊戲和閃卡。您需要做的就是在您的html代碼中添加一個iframe元素，帶有您在web.remem.me上的自己的經文集合的地址，例如，

```
<iframe
src="https://web.remem.me/collections/78112369558678"
style="border:none;"
width="320" height="480"
title="我們的記憶經文集合">
</iframe>
```

這將導致如下：

{{< app 78112369558678 >}}
