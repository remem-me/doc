---
title: "學習和複習聖經記憶經文"
linkTitle: "學習和複習"
weight: 6
description: >
  記念我 的遊戲和智能複習系統幫助您記住經文並保持記憶。
---
### 學習過程 {#process}
記憶經文在「新」、「到期」和「已知的」框之間移動。  
您可以通過點擊活動標籤 <img src="/images/triangle.svg"> 來更改框的排序方式。

|框:|<span class="material-icons">inbox</span><br>新|&rarr;|<span class="material-icons">check_box_outline_blank</span><br>到期|&rarr;|<span class="material-icons">check_box</span><br>已知的|
 |:-|:-:|:-:|:-:|:-:|:-:|
|動作:|1. **學習**|2. 確認|3. **複習**<br>**經文**|4. 回憶|5. **複習**<br>**參考**|
|按鈕:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|<img src="/images/cards_outline.svg">|<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">|

1. **學習**新的經文。
2. 確認一個經文將其移至「到期」框。
3. **複習經文**。
4. 成功回憶一個經文將其移至「已知的」框。
5. **複習參考**隨機選擇的經文。

### 學習 {#studying}
添加經文後，您可以使用不同類型的遊戲來學習。要開始學習，請點擊該經文，然後點擊學習符號 <span class="material-icons">school</span>。

<span class="material-icons">wb_cloudy</span> **混淆**: 隱藏一些單詞，並嘗試從記憶中填寫經文中的缺失單詞。點擊被混淆的單詞以顯示它。

<span class="material-icons">extension</span> **拼圖**: 通過點擊正確的單詞來構建經文。

<span class="material-icons">subject</span> **排列**: 通過點擊左側的圖標（在底部工具欄上）來顯示空行或首字母。通過點擊 <span class="material-icons">plus_one</span> (+單詞) 或 <span class="material-icons">playlist_add</span> (+行) 來進行進一步。在顯示它之前試著回憶單詞或行。

<span class="material-icons">keyboard</span> **打字**: 通過輸入每個單詞的第一個字母來構建經文。

將經文確認到記憶後，將卡片向右滑動，以將其移至「到期」列表。從現在開始，該經文將參與複習過程。

### 複習經文 {#passages}
選擇框 "**到期**"。如果您想更改經文的順序，請再次點擊「到期」。有三種開始複習的方式：

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> outlined flashcard" >}}
開始多個複習的快閃卡。
{{< /card >}}
{{< card header="點擊一個經文" >}}
開始單個快閃卡的複習。
{{< /card >}}
{{< card header="長按一個經文" >}}
直接在通道邊打開卡片。
{{< /card >}}
{{< /cardpane >}}

當參考和主題顯示時，從記憶中背誦經文，然後翻轉卡片檢查您是否正確。如果您喜歡逐行背誦經文（關於添加和編輯經文斷行的詳細信息，請參見 [添加和編輯經文](/zh-tw/docs/verses)），請為每行右下角的 <span class="material-icons">rule</span> 圖標進行點擊。

如果您需要一點幫助開始，長按卡片以獲得提示。（您可以通過向下滑動來跳過一張卡片。）

如果您正確記得了經文，請向右滑動它以將其移至「已知的」框。這將使該經文的等級提高一級（直到帳戶設置中的複習限制被達到）。如果不太正確，請向左滑動。這將重置該經文的等級為 0。這聽起來可能有些令人震驚，但對於經文的複習頻率非常重要。等級低的經文會更頻繁地出現進行複習。這有助於在經文被遺忘之前加強您對經文的記憶。間隔重複確保您經常重複挑戰性的經文，但也不會忘記熟悉的經文。

{{% alert title="間隔重複" color="secondary" %}}
當大腦以遞增間隔召回所學項目時，記憶效率最高。 記念我 使用指數算法計算間隔（與 Pimsleur 類似）。如果您的帳戶複習頻率設置為 "正常"，您將在 1、2、4、8、16... 天後複習您的經文。算法的另一組成部分是在失敗的複習後將經文的等級重置為 0（與 Leitner 類似）。這確保困難的經文比簡單的經文更頻繁地進行複習。
{{% /alert %}}

### 複習參考 {#references}
如果您想僅複習今天審核的參考，請在開始之前按一下標籤 "[今天審核](/zh-tw/docs/labels/#filter)"。選擇標籤 "**已知的**"，將其排序設置為 "隨機"，並在底部點擊具有 <img src="/images/cards_filled.svg"> 填充的快閃卡圖標的按鈕。

記念我 洗牌並呈現其參考。試著從記憶中背誦參考，然後通過點擊卡片來翻轉它來檢查您是否正確。如果是，請將卡片向右滑動。如果不是，請將其向左滑動。應用程序將在複習會話期間將卡片返回給您，直到您記住它。

您可以通過向下滑動來跳過一張卡片。與複習經文不同，應用程序不會跟踪您的複習進度。這種複習模式更像是您應該偶爾進行的測驗，以刷新您對參考的知識。

{{% alert title="帳戶設置" color="secondary" %}}
- **每日目標**: 每天要審查的字數。
- **複習閃卡數量**: 每次段落複習會話的最大閃卡數量。
- **反向閃卡數量**：每個參考複習會話的最大快閃卡數量。
- **學習引用**：將參考包含在學習和複習經文中。
- **審查頻率**：經文將多久出現進行複習。
- **間隔極限**：下一次複習的最大天數。間接限制可達到的等級數量。
  {{% /alert %}}


