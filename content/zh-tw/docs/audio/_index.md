---
title: "音頻功能"
linkTitle: "音頻功能"
weight: 12
description: >
  記念我可以為您朗讀經文或錄製您的背誦並回放給您。
---

> 反覆聽經文支持您的記憶過程。在複習一段經文時錄製您的聲音可以加強您對它的了解。

### 聽一盒經文 {#listen-box}

要開始聽經文，選擇您想聽的盒子。
聽您的"新"盒子中的經文讓您熟悉它們。
聽您的"已知的"盒子中的經文可以刷新您的知識。
按下<span class="material-icons">play_arrow</span>播放按鈕開始聽。
使用底部的按鈕，您可以暫停、停止或跳過一段經文。

您可以讓記念我重複所有經文或單個經文，或者使用控制按鈕旁的設置按鈕
(<span class="material-icons">settings</span> 齒輪)来减慢速度。

如果您想在段落后包含参考信息或按主题听经文，请在“账户”中激活相应的设置。

### 从当前经文开始 
面对一张抽认卡时，您可以通过按右上角的<span class="material-icons">play_arrow</span>播放按钮开始播放。
返回到经文列表后，当前经文仍然标记为播放或暂停。如果您现在点击列表的播放按钮，应用程序将从列表中那个位置开始朗读。

{{% alert title="文本转语音设置" color="secondary" %}}
<span class="material-icons">speed</span> 语速  
<span class="material-icons">pause_presentation</span> 暂停（秒）  
<span class="material-icons">av_timer</span> 睡眠計時器（以分鐘為單位）  
<span class="material-icons">playlist_play</span> 开启：播放整个列表 / 关闭：仅播放当前经文  
<span class="material-icons">repeat</span> 重复列表（或经文）  
{{% /alert %}}

### 在学习经文时倾听 {#listen-study}
在学习游戏中（参见学习），
记念我会朗读您揭示的每个单词或句子。您可以使用游戏中的设置菜单
(<span class="material-icons">settings</span> 齿轮) 来关闭语音。

### 录制并回放您自己的背诵 {#record}
您的“到期”盒子中的抽认卡（参见复习经文）在底部有一个
<span class="material-icons">mic</span> 麦克风按钮。
点击它来录制您的经文背诵。再次按下按钮或翻转卡片会停止录音。
揭示经文后，播放开始，使您更容易检查您的背诵是否正确。

### 更改声音

{{% alert color="secondary" %}}
您可以在帳戶設置中的*詩句的語言*和*參考資料的語言*中更改語音的**語言**。
{{% /alert %}}

记念我使用您设备上的文本转语音引擎。您可以在以下位置找到更改声音的说明

* [iPhone/iPad帮助](https://support.apple.com/zh-tw/guide/iphone/iph96b214f0/ios#iph938159887)
转到设置 > 辅助功能 > 朗读内容 > 语音：选择一个语音和方言，下载并设置为默认。
* [Android帮助](https://support.google.com/accessibility/android/answer/6006983?hl=zh-tw)

对于Android设备，有多个文本转语音引擎可用，例如：

* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Google语音服务](https://play.google.com/store/apps/details?id=com.google.android.tts)

在視窗(Windows)或蘋果電腦(macOS)上使用網頁應用程式時，您可以在記念我的語音設定中更改語音。您可能需要先在設備上安裝語音數據。
