---
title: "捐款"
linkTitle: "捐款"
weight: 25
description: >
  記念我是由Poimena發行的，這是一個非營利組織，致力於提供促進靈性成長的免費服務。
---

### 創始人

Poimena由Rev. Peter Schaffluetzel和Rev. Regula Studer Schaffluetzel創立。

### 地址

Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Switzerland

### 銀行轉賬

請使用下方資訊進行銀行轉賬捐款。
轉賬信息中請加入您的電子郵件地址，以便我們表示感謝。
| | |
|-------------------|-------------------------|
| 銀行名稱:         | Raiffeisen, Switzerland |
| 受益人:           | Poimena |
| Swift代碼 (BIC):  | RAIFCH22XXX |
| 賬戶IBAN:         | CH74 8080 8006 2918 4731 8 |

### PayPal/信用卡

如果您偏好使用PayPal或信用卡進行捐款，請使用下方按鈕。
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="NHMVF5RBNH3TE" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - 更安全、更簡單的線上支付方式！" alt="透過PayPal按鈕捐款" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>

