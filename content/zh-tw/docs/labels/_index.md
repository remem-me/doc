---
title: "通過標籤分組和過濾"
linkTitle: "按標籤分組"
weight: 7
description: >
  選擇和取消選擇標籤允許您隱藏、顯示和過濾經文，根據您的喜好。
---

> 快速過濾：在右側的 <span class="material-icons-outlined">visibility</span> 菜單中點擊一個標籤，只顯示該標籤的經文。長按它以排除它。

### 創建或編輯標籤 {#create}
從左側的主菜單中選擇“編輯標籤”，以創建、編輯或刪除標籤。
最近刪除的標籤可以從[垃圾箱](https://web.remem.me/labels/bin)在線恢復。

### 將標籤附加到經文 {#attach}
通過點擊它們的徽章（左側的圓角方塊）選擇一個或多個經文。點擊頂部上下文菜單中的<span class="material-icons">label</span>標籤圖標。檢查您想要附加到選定經文的標籤，取消檢查您想要從選定經文中移除的標籤，然後點擊按鈕保存更改（<span class="material-icons">save</span>
磁盘图标）。

### 通过标签过滤经文 {#filter}
标签分组的经文可见性在右侧的标签抽屉中设置。您可以通过点击或长按它们来改变标签/组的状态。图标有以下含义。

<span class="material-icons-outlined">visibility_off</span> **划掉的眼睛**：带有此标签的经文总是隐藏的。

<span class="material-icons-outlined">visibility</span> **轮廓的眼睛**：除非某些标签处于独占模式，否则带有此标签的经文可见。

<span class="material-icons">visibility</span> **填充的眼睛**：带有此标签的经文可见，所有其他经文隐藏。

一些自动过滤器和过滤设置可用。

<span class="material-icons-outlined">visibility</span> **今日复习**：此过滤器过滤今天复习的经文（取决于其他过滤设置）

<span class="material-icons-outlined">apps</span> **所有经文**：点击此选项会更改标签模式，以便所有经文都可见。

<span class="material-icons-outlined">label_off</span> **未标记的经文**：点击此选项会更改标签模式，以便只有未标记的经文可见。

### 一览表中的所有经文 {#all}
在某些情况下，将所有经文视为一个列表可能会有所帮助，例如，导出您的完整收藏到文件（见下文）。您可以通过在右侧的<span class="material-icons-outlined">visibility</span>可见性抽屉中切换<span class="material-icons-outlined">layers</span>盒子关闭并选择自动过滤器<span class="material-icons">apps</span>“所有经文”来实现这一点。

### 将经文作为文件（CSV）导出/导入 {#export}
打開文件菜單（<span class="material-symbols-outlined">file_open</span>三個點，僅限移動應用）在右側的可見性欄 <span class="material-icons-outlined">visibility</span>，並選擇「導出到文件」。這將創建一個新的CSV格式文件，其中包含當前顯示的詩句。您可以與其他應用共享導出的文件，以保存它或發送給其他人。 CSV文件可以用電子表格程序打開。

通过选择“从文件导入”选项，您可以从存储在您的设备上的文件中加载经文。 
