---
title: "用戶和帳戶"
linkTitle: "用戶和帳戶"
weight: 4
description: >
  註冊為用戶並為您的經文創建一個帳戶。
---

### 註冊和登錄用戶 {#register}

在 [web.remem.me](https://web.remem.me) 上免費註冊。提交註冊表單後，您將
收到一封電子郵件，要求您確認您的註冊。

完成註冊後，您可以點擊右上角的 **登錄** 按鈕來登錄應用。

{{% alert title="更改您的密碼" color="secondary" %}}
要更改密碼，請在登錄對話框中點擊“重置密碼”。
{{% /alert %}}

### 多個經文帳戶 {#multiple}

您的經文帳戶會在您註冊用戶時自動創建。語言、帳戶名稱、審查偏好等會儲存在您帳戶的設置中。作為用戶，您可以添加額外的帳戶，例如，為您的孩子或者用第二語言記住經文。對於大多數用戶來說，一個帳戶就足夠了。您不能在帳戶之間轉移經文。為了組織您的經文，請使用[標籤](/docs/labels)。

要添加新帳戶，請點擊左側菜單頭部的 <span class="material-icons">arrow_drop_down</span> 三角形，並從菜單中選擇“創建新帳戶”。

### 從之前版本導入帳戶 {#previous}

創建新的用戶帳戶（通過您的電子郵件地址識別）後，您可以附加來自之前 記念我
版本的經文帳戶。點擊左側菜單頭部的 <span class="material-icons">arrow_drop_down</span>
三角形，並從菜單中選擇“添加現有帳戶”。輸入舊帳戶的名稱和密碼，然後點擊“登錄”。您可以像使用任何其他 記念我 6 經文帳戶一樣使用附加的帳戶。

### 同步多個設備 {#sync}

具有離線功能的設備在左側導航抽屜中當前帳戶名稱旁邊有一個 <span class="material-icons">refresh</span>
圓形箭頭圖標。如果圖標不可見（例如，在網頁瀏覽器中），則經文僅在線上儲存。經文和學習進度會自動同步。點擊圓形箭頭圖標以加載在不同設備上新編輯的數據。如果您需要強制您的設備檢索並發送所有數據，請長按圖標。

{{% alert title="同步按鈕" color="secondary" %}}
<span class="material-icons">refresh</span> **一個箭頭**形成圓圈：可能在其他設備上的更改。點擊以檢索它們。  
<span class="material-icons">sync</span> **兩個箭頭**形成圓圈：此設備上的本地更改。點擊以發送它們。  
<span class="material-icons">sync_problem</span> **圓圈中的驚嘆號**：同步失敗。點擊以再次嘗試。
{{% /alert %}}

### 編輯和刪除經文帳戶 {#edit-account}

要編輯或刪除當前選擇的帳戶，請從左側的主導航選擇“帳戶”。在編輯後點擊左上角按鈕（<span class="material-icons">save</span>
磁盤符號）以保存帳戶。
點擊右上角的 <span class="material-icons">delete</span>
垃圾桶圖標刪除帳戶。已刪除的帳戶會被儲存在[垃圾桶](https://web.remem.me/account-bin)中三個月，並且可以從那裡恢復。

### 編輯和刪除用戶訪問 {#edit-user}

如果您想要編輯或刪除您的用戶註冊，切換到帳戶導航（<span class="material-icons">
arrow_drop_down</span> 左側菜單頭部的三角形）並選擇“用戶”。
要編輯您的電子郵件地址，輸入新地址和當前密碼，然後點擊“保存”。
要完全刪除您對 remem.me
的用戶訪問，首先需要刪除所有經文帳戶。然後，輸入當前密碼並點擊用戶對話框的 <span class="material-icons">delete</span>
垃圾桶圖標。

{{% alert title="注意" color="warning" %}}
刪除您對 remem.me 的訪問是不可逆的。您的所有數據將立即且永久從所有數據庫中移除。
{{% /alert %}}

### 僅限裝置上模式 {#on-device-only}

您可以使用記念我而不連接到 remem.me 雲端服務。在此模式下，所有資料僅儲存在您的裝置上。

- 資料保留在您的裝置上
- 無法使用雲端備份
- 無法發布經文集

{{% alert title="重要" color="warning" %}}
如果您在僅限裝置上模式下失去對裝置上應用程式的存取權，您的經文將無法恢復。
{{% /alert %}}
要啟用僅限裝置上模式，請在註冊對話框底部選擇「僅限裝置上」（在點擊「開始」後）。

要退出僅限裝置上模式：

1. 打開選單
2. 點擊帳戶名稱以切換到帳戶選單
3. 選擇「登出」

要將本地經文轉移到線上帳戶，請將您的經文[匯出並匯入](docs/labels/#export)為檔案。

{{% alert title="注意" color="secondary" %}}
與連接雲端的帳戶相比，僅限裝置上模式提供有限的功能。在選擇您偏好的操作模式時，請考慮隱私和功能之間的利弊。
{{% /alert %}}


