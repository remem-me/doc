---
title: "Translating App and Documentation"
linkTitle: "Translating"
type: docs
weight: 5
aliases:
  - /translate
description: >
  Help translate Remember Me and its documentation to your language.
---

### Translating an Open Source Project
- Create an account on [gitlab.com](https://gitlab.com) (if you haven't got an account yet) and [fork the project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) from the develop branch.
- Create the translated file or edit existing ones. You can create and edit files on gitlab.com directly.
- Finally, create a [merge request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream) to the original develop branch, so a project maintainer can merge your work into the project.

### Files to Translate
Remember Me consists of three projects. All three projects have files that need to be translated into the user's language. 

#### rm_app: 
the client application (the "app") that runs in a web browser or on a mobile device
- [en.json](https://gitlab.com/remem-me/app/-/blob/develop/assets/l10n/en.json) &rarr;  [de.json](https://gitlab.com/remem-me/app/-/blob/develop/assets/l10n/de.json)  
e.g.   "Verses.range": "#1 to #2" &rarr; "Verses.range": "#1 bis #2"
- [en_books.json](https://gitlab.com/remem-me/app/-/blob/develop/assets/l10n/en_books.json) &rarr; [de_books.json](https://gitlab.com/remem-me/app/-/blob/develop/assets/l10n/de_books.json)  
e.g. "qoh(elet)?": ["ECC", "Qohelet"] &rarr; "pr(e?d?|ediger)": ["ECC", "Prediger"]  
You can skip the first part if you are not familiar with [Regex](https://en.wikipedia.org/wiki/Regular_expression).  
The 3-letter code does not need translation.

#### rm_server: 
the replication server that manages user access and verse accounts
- [access/django.po](https://gitlab.com/remem-me/server/-/blob/develop/access/locale/de/LC_MESSAGES/django.po)  
e.g. msgid "User Activation" &rarr; msgstr "Benutzeraktivierung"
- [main/django.po](https://gitlab.com/remem-me/server/-/blob/develop/main/locale/de/LC_MESSAGES/django.po)  
e.g. msgid "reference language" &rarr; msgstr "Stellenangaben-Sprache"

#### rm_documentation: 
the app's online documentation
- the language configuration in [config.toml](https://gitlab.com/remem-me/doc/-/blob/develop/config.toml)  
e.g. languageName = "English" &rarr; languageName = "Deutsch"
- the [landing page](https://gitlab.com/remem-me/doc/-/blob/develop/content/en/_index.html)  
Its content is also used for app store descriptions.
- optional: everything else in [content/en](https://gitlab.com/remem-me/doc/-/tree/develop/content/en)  
_index.md: e.g. title: "Five Steps to Remember" &rarr; title: "Fünf Schritte zur Erinnerung"