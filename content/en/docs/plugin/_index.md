---
title: "Website Plugin"
linkTitle: "Website Plugin"
weight: 15
images: 
  - "/images/embedded-memorization-game.png"
      
description: >
  Embed your Bible memory verses as games, quizzes, and images into your church or homeschool website. Your visitors can engage in Bible memorization activities without leaving your web page.   
---

#### Feature a Verse as a Bible Memory Game

In order to make a Scripture memory verse publicly available, add it to a public collection of verses
(see [Publish a Collection of Bible Scriptures](/docs/collections/#publish)). Then, search for your collection on
https://web.remem.me/collections. Open the verse within your public collection in your web browser, click on the 
<span class="material-icons">school</span> study button of the flashcard in order to open the verse in study mode, 
and copy its link address from your browser's address field, e.g.
https://web.remem.me/collections/274543364943455/276225869771279/study.

Embed the following HTML snippet in your website's HTML code, replace the link with your own link, and adjust the other
settings if you like.

```
<h4>Practice this week's memory verse</h4>
<iframe 
    src="https://web.remem.me/collections/274543364943455/276225869771279/study" 
    style="border:none;" 
    width="360" height="330"> 
</iframe>
```

The result of the code above looks like this:

<h4>Practice this week's memory verse</h4>
<iframe
src="https://web.remem.me/collections/274543364943455/276225869771279/study"
style="border:none;"
width="360" height="330">
</iframe>

Your website visitors can start with the plugin's memorization activities on your website right away.

### Feature a Flashcard or a Collection of Verses

Instead of using the study link address as the source of the `<iframe>` tag, you can as well use a verse's URL when 
it is in flashcard mode, or you can use the share link of a public collection of verses. Your users can interact with 
your Bible memory flashcard or your memory verse collection right on your website.