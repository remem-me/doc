---
title: "Bible Verse Collections"
linkTitle: "Bible Verse Collections"
weight: 9
aliases:
  - /bg/docs/collections
  - /cs/docs/collections
  - /hu/docs/collections
  - /it/docs/collections
  - /no/docs/collections
  - /pl/docs/collections
  - /ro/docs/collections
  - /sv/docs/collections
  - /ur/docs/collections
  - /docs/importing-and-sharing-collections
  - /how-rm-works/collections
description: >
  Benefit from public Bible memory verse collections and share your own Scripture collections with others. 
---

> A verse collection is a published label. 

{{< columns >}}

### Find a Collection of Bible Memory Verses {#find}
Select "Collections" from the left navigation drawer and use the <span class="material-icons">search</span> search 
function to find what you are looking for. Tap on a collection card to see more details, and tap the 
<span class="material-icons">download</span> download icon in the top right corner in order to import the collection's 
verses into your account. A label with the collection's name makes the new verses distinguishable from your other verses. 

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/qIaBuX9c590?si=KAUUp-fuyR3cY8TN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Publish a Collection of Bible Scriptures {#publish}
Select "Collections" from the left navigation drawer and tap on the "Publish" button (<span class="material-icons">
publish</span> upload icon in the top right corner) in order to see a list of all labels that are attached to verses. 
A <span class="material-icons">public_off</span> crossed out world icon means, the label and its verses are not visible 
to other users. 

Tap on a label and provide a description (a minimum of 10 characters required) of your collection in the form that pops up. To abort or to unpublish a 
previously published collection, tap the crossed out world icon at the bottom of the form. All publication details are 
deleted and the verses hidden from other users. When you are ready to publish, tap the highlighted 
<span class="material-icons">public</span> world icon at the bottom of the form. All publication details are saved and 
the collection will appear in other users' search results.

### Share Public Bible Memory Verses {#share}
Select "Collections" from the left navigation drawer and tap on the "Publish" button (<span class="material-icons">
publish</span> upload icon in the top right corner) in order to see a list of your collections. In order to share one
of your Bible memory verse decks, tap on its <span class="material-icons">share</span> share icon. 

If you want to share a single verse of a public collection, open the verse from within your collection, open its 
extension menu (<span class="material-icons">more_vert</span> three dots), and select 
"Share Verse". If your bible study group members have got Remember Me installed on their mobile device, tapping on the 
link will open the verse right in the app. Otherwise they will be directed to the Bible memory verse on the 
[web app](https://web.remem.me).
