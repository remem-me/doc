---
title: "Filter by Labels"
linkTitle: "Filter by Labels"
weight: 7
aliases:
  - /docs/grouping-by-labels
  - /how-rm-works/manage-verses  
  - /how-rm-works-ios/labels-ios  
  - /how-rm-works/sets
  - /bg/docs/labels
  - /cs/docs/labels
  - /hu/docs/labels
  - /it/docs/labels
  - /no/docs/labels
  - /pl/docs/labels
  - /ro/docs/labels
  - /sv/docs/labels
  - /ur/docs/labels
description: >
  Selecting and deselecting labels allows you to hide, show, and filter verses as you please.

---

> Quick filtering: Tap on a label in the <span class="material-icons-outlined">visibility</span> menu on the right to
> display its verses exclusively. Press long on it to exclude it.

### Create or Edit a Label {#create}

Select "Edit labels" from the main menu on the left in order to create, edit, or delete a label.
Recently deleted labels can be restored online from the [waste bin](https://web.remem.me/labels/bin).

{{< columns >}}

### Attach Labels to Verses {#attach}

Select one or multiple verses by tapping on their badge (rounded square on the left). Tap on the
<span class="material-icons">label</span> label icon of the context menu on top. Check labels you want to attach to the
selected verses, uncheck labels you want to remove from the selected verses, and tap the button to save the changes
(<span class="material-icons">save</span>disk icon).

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/kYhpPZAMeh4?si=LtScpn7-qCNFJsiu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Filter Verses by Label {#filter}

The visibility of the verses grouped by labels is set in the labels' drawer on the right. You can change the state of
the labels/groups by tapping or pressing on them. The icons have the following meanings.

<span class="material-icons-outlined">visibility_off</span> **Crossed out eye**: Verses with this label are always
hidden.

<span class="material-icons-outlined">visibility</span> **Outlined eye**: Verses with this label are visible unless
some labels are in exclusive mode.

<span class="material-icons">visibility</span> **Filled eye**: Verses with this label are visible, and all other verses
are hidden.

Some automatic filters and filter settings are available.

<span class="material-icons-outlined">visibility</span> **Reviewed today**: This filter filters verses reviewed today
(depending on other filter settings)

<span class="material-icons-outlined">apps</span> **All verses**: Tapping this option changes the labels' modes so that
all verses are visible.

<span class="material-icons-outlined">label_off</span> **Unlabelled verses**: Tapping this option changes the labels'
modes so that only unlabelled verses are visible.

### All Verses in One List {#all}

In some situations it may be helpful to view all verses in one list, e.g. for exporting your complete collection to a
file (see below). You can achieve this in the <span class="material-icons-outlined">visibility</span> visibility drawer
on the right by switching <span class="material-icons-outlined">layers</span> boxes off and selecting the automatic
filter <span class="material-icons">apps</span> "All verses".  
(<a href="https://www.youtube.com/watch?v=9s0KIbw4eCg" target="_blank">Tutorial: ONE COLUMN for all Bible verses</a>)

### Export/Import Verses as File (CSV) {#export}
Open the file menu <span class="material-symbols-outlined">file_open</span> (mobile app only) in the visibility
bar <span class="material-icons-outlined">visibility</span> on the right side and select "Export to File". This will
create a new file in CSV format with the currently displayed verses. You can share the exported file with other apps to
save it or send it to someone else. CSV files can be opened with spreadsheet programs.

By choosing the option "Import from file" you can load verses from a file stored on your device. The file must be a CVS file in the same format and stored in a local folder. (For Android, store the importing file in a local folder other than the Download folder.)

