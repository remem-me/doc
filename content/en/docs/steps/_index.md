---
title: "5 Steps to Remember: Learning Bible Verses By Heart"
linkTitle: "5 Steps to Remember"
weight: 2
aliases:
  - /docs/5-steps-to-remember
  - /how-rm-works
  - /how-rm-works-ios
  - /bg/docs/steps
description: >
  Memorize Bible verses with Remember Me. Apply the basic principles of Scripture memorization to your daily life.
---

<iframe width="256" height="456" src="https://www.youtube.com/embed/Gd0DTn_nNCk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 16px;"></iframe>

### 1 Add a new Bible memory verse.

The Bible memory app Remember Me offers various options to save a Bible memory text to your device. You can

- enter [any text](/docs/verses/#add) manually
- retrieve a verse from a variety of [online Bibles](/docs/verses/#online-bible)
- download user curated [Bible memory verse collections](/docs/collections/#find)

### 2 Commit a Bible verse to memory.

[Studying](/docs/study/#studying) a Bible verse becomes fun if you apply a variety of methods.

- [Listen](/docs/audio/#listen-box) to the verse
- Hide random words
- Make it a puzzle
- Reveal only first letters or blank spaces
- Practice typing the first letter of each word

### 3 Use topics and images.

Not everybody is good with numbers - and you don't need to be to memorize with a system. Add a [topic](/docs/labels/) or
an [image](/docs/verses/#image) to your Bible memory verse and it will help you pick out the right Bible verse from your
memory.

### 4 Review and reinforce memorized Bible verses.

Once you have committed a memory verse, it shows up for review in the "Due"
box. Start a series of [Bible flashcards](/docs/study/#passages) to review the
verses. [Record yourself](/docs/audio/#record) reciting the Scripture memory verse, flip the card, and check if you got it right.

Spaced Repetition makes sure you review newly memorized Bible verses often, but don't forget well known Bible memory
verses either.

### 5 Redeem the time.

Make the most of every opportunity. The best Bible memory apps work because you can take the two minutes during which
you are brushing your teeth to memorize Scripture. Keep Remember Me with you and use it during those natural pause
moments of life.
