---
title: "Audio Features"
linkTitle: "Audio Features"
weight: 12
aliases:
  - /bg/docs/audio
  - /cs/docs/audio
  - /hu/docs/audio
  - /it/docs/audio
  - /no/docs/audio
  - /pl/docs/audio
  - /ro/docs/audio
  - /sv/docs/audio
  - /ur/docs/audio
  - /docs/audio-features
  - /how-rm-works-ios/audio-ios
  - /how-rm-works/audio
description: >
  Remember Me can read verses to you or record your recitation and play it back to you.
---

> Repeatedly listening to verses supports your memorizing process. Recording your voice while reviewing a verse
> strengthens your knowledge of it.

### Listen to a Box of Verses {#listen-box}

In order to start listening to verses, select the box you want to listen to.
Listening to verses in your box "New" makes you familiar with them.
Listening to verses in your box "Known" refreshes your knowledge.
Press the <span class="material-icons">play_arrow</span> play button to start listening.
Using the buttons at the bottom you can pause, stop, or skip a verse.

You can have Remember Me repeat all verses or a single verse or slow it down using the settings button
(<span class="material-icons">settings</span> cog wheel) next to the control buttons.

If you would like to include the reference after the passage or to listen to verses by topic,
activate the corresponding setting in "Account".

### Start with the Current Verse {#listen-verse}

With a flashcard in front of you, you can start playback by pressing the
<span class="material-icons">play_arrow</span> play button in the top right corner.
After returning to the verse list, the current verse is still marked as playing or paused.
If you tap on the list's play button now, the app starts reading out from that position in the list.

{{% alert title="Text-to-Speech Settings" color="secondary" %}}
<span class="material-icons">speed</span> Speech rate  
<span class="material-icons">pause_presentation</span> Pause (in seconds)  
<span class="material-icons">av_timer</span> Sleep timer (in minutes)  
<span class="material-icons">playlist_play</span> *on*: Play the entire list / *off*: Play the current verse only  
<span class="material-icons">repeat</span> Repeat the list (or verse)
{{% /alert %}}

### Listen while Studying a Verse {#listen-study}

During the study games (see [Studying](/docs/study/#studying)),
Remember Me reads each word or sentence you reveal. You can turn off speech using the settings menu
(<span class="material-icons">settings</span> cog wheel) in the game.

### Record and Play Back Your Own Recitation {#record}

The flashcards in your box "Due"  (see [Reviewing Passages](/docs/study/#passages)) sport a
<span class="material-icons">mic</span> microphone button at the bottom.
Tap it to record your recitation of the passage. Pressing the button again or flipping the card stops the recording.
After revealing the passage, playback starts and makes it easier for you to check if your recitation was correct.

### Change the Voice {#voice}
<a id="change-the-voice"></a>
{{% alert color="secondary" %}}
You can change the **language** of the voice in your account settings *Language of the verses* and *Language of the
references*.
{{% /alert %}}

Remember Me uses your device's text-to-speech engine. You can find instructions about changing the voice on

* [Help for iPhone/iPad](https://support.apple.com/en-mt/guide/iphone/iph96b214f0/ios#iph938159887)  
  Go to Settings > Accessibility > Spoken Content > Voices: Choose a voice (except Siri, because Apple does not allow
  3rd party access to Siri voices), download it, and set it as default.
* [Help for Android](https://support.google.com/accessibility/android/answer/6006983)

For Android devices there are multiple text-to-speech engines available, e.g.

* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Speech Services by Google](https://play.google.com/store/apps/details?id=com.google.android.tts)

When using the web app on Windows or macOS, you can change the voice in Remember Me's speech settings. You may need to
install voice data on your device first.