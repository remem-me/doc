---
title: "Donations"
linkTitle: "Donations"
weight: 25
description: >
  Remember Me is published by Poimena, a non-profit organization that provides free services dedicated to spiritual growth. 
---
### Founders
Poimena was founded by Rev. Peter Schaffluetzel and Rev. Regula Studer Schaffluetzel.

### Address
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Switzerland

### Bank Transfer
Use the information below to make a donation by bank transfer.
Please add your e-mail address to the transfer message so we can thank you.
|                   |                         |
|-------------------|-------------------------|
| Bank name:        | Raiffeisen, Switzerland |
| Beneficiary:      | Poimena                 |
| Swift code (BIC): | RAIFCH22XXX             |
| Account IBAN:     | CH74 8080 8006 2918 4731 8 |

### PayPal/credit card
If you prefer PayPal or credit card to make a donation, please use the button below.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="NHMVF5RBNH3TE" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>

