
---
title: "How to Use Remember Me"
description: "Information about managing, memorizing, and sharing Bible verses with the Bible memorization app Remember Me."
linkTitle: "Manual"
weight: 20
menu:
  main:
    weight: 20
aliases:
  - /docs
  - /bg/docs
  - /uk/docs
---

