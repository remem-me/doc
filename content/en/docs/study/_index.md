---
title: "Study & Review Verses"
linkTitle: "Study & Review"
description: Remember Me's games and intelligent review system help you to memorize Scripture and retain it.
weight: 6
aliases:
  - /bg/docs/study
  - /cs/docs/study
  - /hu/docs/study
  - /it/docs/study
  - /no/docs/study
  - /pl/docs/study
  - /ro/docs/study
  - /sv/docs/study
  - /ur/docs/study
  - /docs/studying-and-reviewing
  - /how-rm-works/practice
  - /how-rm-works/flashcards
  - /how-rm-works/lists
  - /how-rm-works-ios/review-frequency-ios
---

### The Learning Process {#process}

|Boxes:|<span class="material-icons">inbox</span><br>New|&rarr;|<span class="material-icons">check_box_outline_blank</span><br>Due|&rarr;|<span class="material-icons">check_box</span><br>Known|
 |:-|:-:|:-:|:-:|:-:|:-:|
|Actions:|1. **Study**|2. Commit|3. **Review**<br>**passages**|4. Recall|5. **Review**<br>**references**|
|Buttons:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|<img src="/images/cards_outline.svg">|<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">|

{{< columns >}}

1. **Study** new verses.
2. Committing a verse moves it to box "Due".
3. **Review passages** of verses.
4. Successfully recalling a verse moves it to box "Known".
5. **Review references** of randomly selected verses.

Memory verses move between the boxes "New", "Due", and "Known".  
You can change the sort order of a box by tapping on the active tab <img src="/images/triangle.svg">.  
(<a href="https://www.youtube.com/watch?v=cL21UZcR5qo" target="_blank">Tutorial: How to change the ORDER LIST</a>)

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/TjC8Nh2DQ5k?si=dsDzNSHKHvHcfj1k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Studying {#studying}
After adding a verse, it can be studied using different kinds of games. To start studying, tap on the verse and on the 
<span class="material-icons">school</span> study symbol.  

{{< columns >}}

<span class="material-icons">wb_cloudy</span> **Obfuscate**: Hide some words and try to recite the verse by filling in 
the missing words from your memory. Tap on an obfuscated word in order to reveal it.  

<span class="material-icons">extension</span> **Puzzle**: Build the verse by tapping on the correct word.  

<span class="material-icons">subject</span> **Line Up**: Display empty lines or first letters by tapping on the icon on 
the left (on the bottom bar). Advance by tapping on <span class="material-icons">plus_one</span> (+word) or 
<span class="material-icons">playlist_add</span> (+line). Try to recite the word or line before you reveal it.  

<span class="material-icons">keyboard</span> **Typing**: Build the verse by typing the first letter of each word.   

After committing the verse to memory swipe the flashcard to the right in order to move it to the "Due" list. From now 
on, the verse takes part in the reviewing process. 

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/A0IrhPQvxa0?si=KFpnYMTYycTAmQAz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Reviewing Passages {#passages}
Select the box "**Due**". If you would like to change the order of the verses, tap on "Due" again. There are 3 ways to start a review:

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> outlined flashcard" >}}
Starts a review of multiple flashcards.
{{< /card >}}
{{< card header="Tapping on a verse" >}}
Starts a review of a single flashcard.
{{< /card >}}
{{< card header="Pressing long on a verse" >}}
Opens the flashcard on the passage side directly.
{{< /card >}}
{{< /cardpane >}}

When reference and topic are displayed, recite the verse from memory, then flip the card to check if you were correct. 
If you prefer reciting the verse line by line (for editing line breaks see 
[Adding and Editing Verses](/docs/verses)), tap the <span class="material-icons">rule</span> icon in
the bottom right corner for each line. 

{{< columns >}}

If you need a little help to get started, press long on the card to get a hint. (You can skip a card by swiping it down.)  

If you remembered the verse correctly, swipe it to the right in order to move it to the "Known" box. This will increase 
the verse's level by one (until the review limit in the account's settings is reached). If it was not quite correct, 
swipe it to the left. This will reset the verse's level to 0. This might sound a bit shocking, but it is very important 
for the review frequency of the verse. Verses with low levels show up for review more often. This helps to reinforce 
your memory of a verse before it gets forgotten. Spaced Repetition ensures that you repeat challenging passages 
frequently, but also don't forget familiar passages. 

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/9m7yXw80NYQ?si=tt4JPs2K8BiiDa6X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

{{% alert title="Spaced Repetition" color="secondary" %}}
Memorisation is most efficient when the brain recalls the learned items in increasing intervals. Remember Me uses an 
exponential algorithm to calculate the intervals (similar to Pimsleur). If your account's review frequency is set to 
"Normal", you will review your verses after 1, 2, 4, 8, 16... days. The other component of the algorithm is resetting 
a verse's level to 0 after a failed review (similar to Leitner). This makes sure that difficult passages are reviewed 
more often than easy ones. 
{{% /alert %}}

### Reviewing References {#references}
If you want to review the references of the passages reviewed today only, press on the label 
"[Reviewed today](/docs/labels/#filter)", before you start. Select the tab "**Known**", set its 
sort order to "Random", and tap on the button with the <img src="/images/cards_filled.svg"> filled flashcard icon at 
the bottom. 

{{< columns >}}

Remember Me shuffles the cards and presents their passages. Try to recite the reference from memory, and flip the card 
by tapping it to check if you are correct. If you are, swipe the card to the right. If you aren't, swipe it to the left. 
The app will get the card back to you during the review session until you remember it. 

You can skip a card by swiping it down. Unlike reviewing passages, the app does not keep track of your reviewing progress. 
This mode of reviewing is more like a quiz you should take once in a while to refresh your knowledge of the references. 

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/lqsiZp35X2M?si=qMAYXeANnlVvggwR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Account Settings {#settings}

{{% alert color="secondary" %}}
- **Daily Bible Memory Goal**: Number of words to be reviewed per day.
- **Number of Review Flashcards**: Maxmimum number of flashcards per passage review session.[^1]
- **Number of Inverse Flashcards**: Maxmimum number of flashcards per reference review session.[^1]
- **Learn with Reference**: Include the reference in studying and reviewing verses.
- **Review Frequency**: How often the verses will show up for review.
- **Interval Limit**: Maximum number of days until to the next review. Limits indirectly the number of levels that can be achieved.
[^1]: When you tap on the flashcard symbol at the bottom of the screen, Remember Me takes a batch of this size and presents it to you for review.
{{% /alert %}}

| Learn with Reference | Learning by Topic | Flashcard Front | Flashcard Hint | Flashcard Back | Audio Playback |
|---|---|---|---|---|---|
| OFF | OFF | reference, topic, labels | beginning | passage | reference, passage |
| ON | OFF | reference | topic, labels, beginning | passage, reference | reference, passage, reference |
| OFF | ON | topic, reference, labels | beginning | passage | topic, reference, passage |
| ON | ON | topic | reference, labels, beginning | passage, reference | topic, reference, passage, reference |
{.td-initial .table-styled}




