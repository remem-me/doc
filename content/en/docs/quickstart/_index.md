---
title: "Quick Start"
linkTitle: "Quick Start"
weight: 3
description: >
  A step-by-step guide to memorizing Bible verses with Remember Me.
---

##### Contents {#contents}
[How to **Register** with Remember Me (Free)](#register) <br>
[How to **Modify Account Settings**](#account) <br>
[How to **Quickly Add a Verse** to Remember Me](#add) <br>
[How to **Open** a Verse Card](#open) <br>
[How to **Edit** a Verse](#edit) <br>
[How to **Delete** a Verse](#delete) <br>
[How to **Add a Label** to a Verse](#label) <br>
[How to **Sort** Verses in a Section](#sort) <br>
[How to **Filter** Verses](#filter) <br>
[How to **Move** Verses between Sections](#move) <br>
[How to **Play** Verses](#play) <br>
[How to **Open Study Mode**](#study) <br>
[How to **Import Collections**](#collection) <br>
[How to **Publish** Your Verse Collection](#publish) <br>
[How to **Export** Verses](#export) <br>
[How to **Change the Text Size**](#text_size) <br>
[How to **Sync** between Devices](#sync) <br>
[How to **Add Accounts**](#add_account) <br>
[How to **Switch** between Accounts](#switch) <br>
[Bible **Book Abbreviations**](#books) <br><br>

Notes: 
* Remember Me is still in development, so what you see on your device may differ slightly from the images in this user guide. 
* To <mark>show tooltips</mark> on the app,
	- <mark>Long-press the icon</mark> (Android/iPhone app)
	- <mark>Hover over the icon</mark> (Web version) 
* When something is not working as supposed to be, try the following: 
	- For Android/iPhone app: Restart the app or restart the phone
	- For the Web version: Refresh the browser, clear the browser cache, or remove the Remember Me’s cookie (remem.me) and log back in

### **How to Register with Remember Me** (Free) {#register}
To register as a Remember Me user:
1.	Go to [web.remem.me](https://web.remem.me).
2.	Tap (or click) **GET STARTED**. (Swipe up if you don't see the button.)
<br><img src="register_0.png" alt="register_0" width="300" style="padding: 7px 10px 10px"><br>
 
3.	Enter your email address and password. Tap **REGISTER** at the bottom.
4.	Tap (or click) the link on the activation email you’ll receive. 
5.	Tap **LOG IN** at the upper right. 
<br><img src="register_1.png" alt="register_1" width="275" style="padding: 7px 10px 10px">
* If you are not using an Android or iPhone app, tap <span class="material-icons-outlined">
home</span> at the top left on the “Registration Completed” page or access [web.remem.me](https://web.remem.me), where you’ll see **LOG IN** at the top.
<br><img src="register_2.png" alt="register_2" width="250" style="padding: 7px 10px 7px"><br>
 6.	Enter your email address and password, and tap **LOG IN** at the bottom.<br>
See ["Register and Log In a User"](/docs/accounts/#register) for more information.<br>
[\[Back to Top\]](#contents)

### **How to Modify Account Settings** {#account}
1.	Tap <span class="material-icons-outlined">menu</span> to show the main navigation bar on the left (if not shown already).
<br><img src="account_1.png" alt="account_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Select **Account** with your account name (usually the left part of @ of your email address).
<br><img src="account_2.png" alt="account_2" width="270" style="padding: 7px 10px 10px"><br>
 
3.	Select a language of verses and references, and other settings as needed.
<br><img src="account_3.png" alt="account_3" width="270" style="padding: 7px 10px 10px"><br>
If you turn on **Include Reference in Learning**, the Book name, the chapter and verse number(s) appear under the verse text on the flashcard. 
<br><img src="account_4.png" alt="account_4" width="260" style="vertical-align: top; padding: 7px 10px 10px">
<img src="account_5.png" alt="account_5" width="220" style= "vertical-align: top; padding: 7px 10px 10px"><br>
      
4.	Tap <span class="material-icons">save</span> on the upper left to save the account settings. 
<br><img src="account_6.png" alt="account_6" width="250" style="padding: 7px 10px 10px"><br>
 
See ["Users and Accounts"](/docs/accounts/) for more information.
[\[Back to Top\]](#contents)

### **How to Quickly Add a Verse to Remember Me** {#add}
You can copy any verses from the Bible websites to Remember Me with a few taps.
1.	Tap <span class="material-icons">menu</span> to show the main navigation bar on the left (if not shown already).
<br><img src="add_1.png" alt="add_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Tap **My Verses**.
<br><img src="add_2.png" alt="add_2" width="250" style="padding: 7px 10px 10px"><br>
 
3.	In the **NEW** section, tap <span class="material-icons">add_circle</span> at the bottom. 
<br><img src="add_3-1.png" alt="add_3-1" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="add_3-2.png" alt="add_3-2" width="200" style= "vertical-align: top; padding: 7px 10px 10px"><br>
         
4.	Tap <span class="material-icons">arrow_drop_down</span> on the **Source** box and select a Bible version from the dropdown list. Instead, you can type a three-letter Bible abbreviation in all capitals (e.g., ESV).
<br><img src="add_4.png" alt="add_4" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="add_5.png" alt="add_5" width="250" style= "vertical-align: top; padding: 7px 10px 10px">
* If you want to select Bibles in other languages, we suggest you create separate accounts for each language. (See [“How to Add Accounts”](#add_account))<br>
5.	Type a Book name in the **Reference** box at the top. Once the name is recognized, the complete Book name (e.g., “Genesis”) appears below the Reference box. Tap (or press the Enter key) the Book name. Usually, the Book name is recognized after you start typing the first few words. 
(Refer to ["Bible Book Abbreviations"](#books))
<br><img src="add_6.png" alt="add_6" width="250" style="padding: 7px 10px 10px"><br>
    
6.	Enter a chapter number, followed by a colon and verse number(s) (e.g., Genesis 1:1-3). 
<br><img src="add_7.png" alt="add_7" width="250" style="padding: 7px 10px 10px"><br>
 
7.	As soon as you enter a chapter number, the orange Bible button <span class="material-icons">menu_book</span> appears at the top right. Tap the orange Bible button.
<br><img src="add_8.png" alt="add_8" width="250" style="padding: 7px 10px 10px"><br>
  
8.	If you want to include the verse number(s), slide the toggle button <span class="material-icons">format_list_numbered</span> <span class="material-icons">toggle_off</span> to the right.
<br><img src="add_9.png" alt="add_9" width="250" style="padding: 7px 10px 10px"><br>
 
9.	You’ll be directed to a Bible website with the chapter/verses of the Bible version you selected. Tap the red paste button <span class="material-icons">content_paste</span> at the bottom right to copy the text.
<br><img src="add_10.png" alt="add_10" width="200" style="padding: 7px 10px 10px"><br>
 
10.	The verse has now been copied to the Passage section on the app. Edit the text as needed.
<br><img src="add_11.png" alt="add_11" width="225" style="padding: 7px 10px 10px"><br>
 
11.	You can add a topic name if you like. 
<br><img src="add_12.png" alt="add_12" width="250" style="padding: 7px 10px 10px"><br>
 
12.	You can also add an image to each of your verse cards. If you tap the image icon <span class="material-icons">image</span> on the right in the **Image URL** box, you can insert a photo image from Unsplash.com. Enter a keyword of images you want in the search box. Tap an image you want to insert.
<br><img src="add_13.png" alt="add_13" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="add_14.png" alt="add_14" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
     
13.	The URL of the Unsplash image is inserted in the **Image URL** box. Instead, you can type any other image URL of your choice.
<br><img src="add_15.png" alt="add_15" width="250" style="padding: 7px 10px 10px"><br>
 
14. Tap <span class="material-icons">save</span> at the top left to save the verse card.
<br><img src="add_16.png" alt="add_16" width="250" style="padding: 7px 10px 10px"><br>
 
15.	The verse has now been added to the **NEW** section.
<br><img src="add_17.png" alt="add_17" width="250" style="padding: 7px 10px 10px"><br>
 
See ["Managing Bible Memory Verses"](/docs/verses/) for more information.
[\[Back to Top\]](#contents)

### **How to Open a Verse Card** {#open}

1.	To open the flashcard's front side, tap the verse's right part (not the badge). If you want to open the back side of the card directly, long-press this part.
<br><img src="open_1.png" alt="open_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	The reference (Book name and chapter/verse numbers) side of the card opens in the case of the NEW section. The topic and label names may also show depending on the settings.
<br><img src="open_2.png" alt="open_2" width="200" style="padding: 7px 10px 10px"><br>
   
3.	To flip the card, tap the card (or tap <span class="material-icons">flip</span> at the bottom right). The verse side appears (in the case of the NEW section). Tap the card again to flip the card.
<br><img src="open_3.png" alt="open_3" width="220" style="padding: 7px 10px 10px"><br>
  
[\[Back to Top\]](#contents)

### ****How to Edit a Verse**** {#edit}
Tap <span class="material-icons-outlined">more_vert</span> (Show menu) at the top right corner of either side of the flashcard. Select **Edit**.
<br><img src="edit_1.png" alt="edit_1" width="220" style="vertical-align: top; padding: 7px 10px 10px">
<img src="edit_2.png" alt="edit_2" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
    
See [“How to Quickly Add a Verse on Remember Me”](#add) for how to edit a verse.<br>
[\[Back to Top\]](#contents)

### **How to Delete a Verse** {#delete}
1.	Tap the badge of the verse in the list view to select a verse.
<br><img src="delete_1.png" alt="delete_1" width="230" style="padding: 7px 10px 10px"><br>
 
2.	The badge changes to a checkmark when selected. If you want to select all verses in the section, tap <span class="material-icons">select_all</span> (Select all). To deselect, tap the badge again.
Tap <span class="material-icons-outlined">more_vert</span> (Show Menu) at the top right corner.
<br><img src="delete_2.png" alt="delete_2" width="270" style="padding: 7px 10px 10px"><br>
     
3.	Tap **Delete**. 
(Each section has a different menu, but **Delete** is available in all sections.)
<br><img src="delete_3.png" alt="delete_3" width="275" style="padding: 7px 10px 10px"><br>

[\[Back to Top\]](#contents)

### **How to Add a Label to a Verse** {#label}
A level allows you to group verses and share them in public as your verse collection.
1.	Tap the badge of a verse to select a verse.
Tap <span class="material-icons">label</span> (Add/remove labels).
<br><img src="label_1.png" alt="label_1" width="270" style="padding: 7px 10px 10px"><br>
 Alternatively, while either side of the flashcard is open, tap <span class="material-icons-outlined">more_vert</span> (Show menu) at the top right. Select **Labels**.
<br><img src="label_2.png" alt="label_2" width="220" style="vertical-align: top; padding: 7px 10px 10px">
<img src="label_3.png" alt="label_3" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
    
2.	To choose an existing label, select the checkbox.
<br><img src="label_4.png" alt="label_4" width="230" style="padding: 7px 10px 10px"><br>
 
3.	To create a new label, tap <img src="new_label_24dp_000000.svg"> (Add a label).
<br><img src="label_5.png" alt="label_5" width="230" style="padding: 7px 10px 10px"><br>
 
4.	Type a label name. Tap <span class="material-icons">save</span> at the bottom to save the label.
<br><img src="label_6.png" alt="label_6" width="220" style="padding: 7px 10px 10px"><br>
 
5.	Select the checkbox and tap <span class="material-icons">save</span> to save.
<br><img src="label_7.png" alt="label_7" width="230" style="padding: 7px 10px 10px"><br>
 
6.	The label name is shown at the bottom right of the verse and the bottom of the card.
<br><img src="label_8.png" alt="label_8" width="280" style="vertical-align: top; padding: 7px 10px 10px">
<img src="label_9.png" alt="label_9" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
      
See ["Grouping and Filtering by Labels"](/docs/labels/) for more information.
[\[Back to Top\]](#contents)

### **How to Sort Verses in a Section** {#sort}
1.	Tap the current section tab. To find out which section you are in now, look for the triangle button <img src="/images/triangle.svg"> and an orange underline on the section tab. 
<br><img src="sort_1.png" alt="sort_1" width="250" style="padding: 7px 10px 10px"><br>  
2.	Sort verses in the section by your desired order.
<br><img src="sort_2.png" alt="sort_2" width="250" style="padding: 7px 10px 10px"><br>
[\[Back to Top\]](#contents)

### **How to Filter Verses** {#filter}
1.	Tap <span class="material-icons-outlined">visibility</span> (Show/hide labels) on the upper right.
<br><img src="filter_1.png" alt="filter_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Filtering options appear. Under this standard menu, the labels you created will also be listed so you can filter by label.
<br><img src="filter_2.png" alt="filter_2" width="250" style="padding: 7px 10px 10px"><br>
 
3.	When **Boxes** is **On**, the verses are shown in separate sections (New, Due, and Known). 
<br><img src="filter_3.png" alt="filter_3" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="filter_4.png" alt="filter_4" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
     
4.	 When **Boxes** is **Off**, all the verses are shown together in one section.
<br><img src="filter_5.png" alt="filter_5" width="275" style="vertical-align: top; padding: 7px 10px 10px">
<img src="filter_6.png" alt="filter_6" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br> 
      
See ["Filter Verses by Label"](/docs/labels/#filter) for more information.
[\[Back to Top\]](#contents)

### **How to Move Verses between Sections** {#move}
Swipe the verse to move it to the section to the right while the verse side of the card is open. 
<br><img src="move_1.png" alt="move_1" width="210" style="padding: 7px 10px 10px"><br>
If you want to undo the action right after that, tap <span class="material-icons">undo</span>  to undo the action.
<br><img src="move_8.png" alt="move_8" width="250" style="vertical-align: top; padding: 7px 10px 10px ">
<img src="move_9.png" alt="move_9" width="250" style= "vertical-align: top; padding: 7px 10px 10px "><br>
You can also display options to move the verse(s) between sections from <span class="material-icons-outlined">more_vert</span> (Show Menu) while the card(s) is selected in the list view.
* To move a verse from the **DUE** section:
1.	In the **DUE** section, tap the badge of the verse to select it. (Tap again to deselect it.) 
<br><img src="move_2.png" alt="move_2" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Tap <span class="material-icons-outlined">more_vert</span> at the top right corner. 
<br><img src="move_3.png" alt="move_3" width="250" style="padding: 7px 10px 10px"><br>
 
3.	To move the verse(s) back to **NEW**, tap **Move to ‘New.’**
To move the verse(s) to **KNOWN**, tap **Remembered**.
<br><img src="move_4.png" alt="move_4" width="270" style="padding: 7px 10px 10px">
 
* To move a verse from the **KNOWN** section:
1.	In the **KNOWN** section, tap the badge of the verse to select it. (Tap again to deselect it.)
<br><img src="move_5.png" alt="move_5" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Tap <span class="material-icons-outlined">more_vert</span> at the top right corner.
<br><img src="move_6.png" alt="move_6" width="250" style="padding: 7px 10px 10px"><br>
 
3.	You can move the verse(s) back to the **’NEW’** or **’DUE’** section.
<br><img src="move_7.png" alt="move_7" width="270" style="padding: 7px 10px 10px">
 
[\[Back to Top\]](#contents)

### **How to Play Verses** {#play}
To play a verse, tap <span class="material-icons">play_circle_filled</span> at the bottom of the list view or <span class="material-icons">play_arrow</span> at the upper right on the verse side of an individual card.
<br><img src="play_1.png" alt="play_2" width="230" style="vertical-align: top; padding: 7px 10px 10px">
<img src="play_2.png" alt="play_2" width="210" style= "vertical-align: top; padding: 7px 10px 10px"><br><br>
 Control buttons such as <span class="material-icons">pause</span> and <span class="material-icons">stop</span> appear while playing.
<br><img src="play_3.png" alt="play_3" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="play_4.png" alt="play_4" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
<img src="play_5.png" alt="play_5" width="250" style= "padding: 7px 10px 10px"><br><br>
Tap <span class="material-icons">video_settings</span> to adjust speech settings. 
<br><img src="play_6.png" alt="play_6" width="250" style="vertical-align: top; padding: 9px 10px 10px">
<img src="play_7.png" alt="play_7" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
 <br><img src="play_8.png" alt="play_8" width="310" style="padding: 7px 10px 10px"><br><br>
 
See ["Audio Features"](/docs/audio/) for more information.
[\[Back to Top\]](#contents)

### **How to Open Study Mode** {#study}
Tap <span class="material-icons">school</span> at the bottom left of the flashcard.
<br><img src="study_1.png" alt="study_1" width="250" style="padding: 7px 10px 10px"><br><br>
Select a study game you want to play.
<br><img src="study_2.png" alt="study_2" width="350" style="padding: 7px 10px 10px"><br>
<br><img src="study_2-1.png" alt="study_2-1" width="240" style="vertical-align: top; padding: 7px 10px 10px">
<img src="study_2-2.png" alt="study_2-2" width="240" style="vertical-align: top; padding: 7px 10px 10px">
<br><img src="study_2-3.png" alt="study_2-3" width="240" style="vertical-align: top; padding: 7px 10px 10px">
<img src="study_2-4.png" alt="study_2-4" width="240" style= "vertical-align: top; padding: 7px 10px 10px"><br><br>
Tap <span class="material-icons">settings</span> to adjust settings.
<br><img src="study_3.png" alt="study_3" width="250" style="padding: 7px 10px 10px">
<br><img src="study_4.png" alt="study_4" width="370" style="vertical-align: top; padding: 7px 10px 10px"><br><br>
     
See ["Studying and Reviewing Scripture Memory Verses"](/docs/study/) for more information.<br>
[\[Back to Top\]](#contents)

### **How to Import Collections** {#collection}
You can import verses published by others, and you can publish your verses.<br><br>
1.	Tap **Collections** on the main navigation bar on the left. 
<br><img src="collection_1.png" alt="collection_1" width="240" style="padding: 7px 10px 10px"><br>
     
2.	The listing of collections published by other users appears.
<br><img src="collection_2.png" alt="collection_2" width="340" style="padding: 7px 10px 13px"><br>
 
3.	Tap <span class="material-icons-outlined">search</span> to search for your desired theme. You can also sort the collections. Tap "Relevance" (default sort setting) to view sort options.
<br><img src="collection_3.png" alt="collection_3" width="240" style="padding: 7px 10px 10px"><br>
 
 4. Tap a collection to view the details. Tap <span class="material-icons">download</span>  to import the collection to My Verses in your current account.
 
See ["Importing and Sharing Bible Verse Collections"](/docs/collections/) for more information.<br>
[\[Back to Top\]](#contents)

### **How to Publish Your Verse Collection** {#publish}
To publish your verses as a collection, you need first to add labels to your verses to group verses. (See [“How to Add a Label to a Verse”](#label))<br><br> 
1.	Tap <span class="material-icons">publish</span> at the top.
<br><img src="publish_1.png" alt="publish_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Your label names (if any) appear as your collections. Tap a collection you want to publish.
<br><img src="publish_2.png" alt="publish_2" width="250" style="padding: 7px 10px 10px"><br>

3.	Enter the description. The minimum length is 10 characters. Tap <span class="material-icons">public</span> at the bottom right to publish.
<br><img src="publish_3.png" alt="publish_3" width="250" style="padding: 7px 10px 10px"><br>
 
4.	The icon in **My Collections** changes to <span class="material-icons">public</span> after publishing. 
Tap <span class="material-icons">share</span> if you want to share your collection with someone individually.
<br><img src="publish_4.png" alt="publish_4" width="400" style="padding: 7px 10px 13px"><br>
To unpublish, open the collection and tap <span class="material-icons">public_off</span> at the bottom.
<br><img src="publish_5.png" alt="publish_5" width="135" style="padding: 7px 10px 10px"><br>
 
See ["Publish a Collection of Bible Scriptures"](/docs/collections/#publish) for more information.
[\[Back to Top\]](#contents)

### **How to Export Verses** {#export}
You can export and import verses stored in Remember Me in CVS format.<br><br>
1.	Tap <span class="material-icons-outlined">visibility</span> at the top right corner.
<br><img src="export_1.png" alt="export_1" width="240" style="padding: 7px 10px 10px"><br>
 
2.	If you want to export all verses in all sections, Turn the <span class="material-icons-outlined">layers</span> **Boxes off** and select <span class="material-icons">apps</span> **All verses**.
<br><img src="export_2.png" alt="export_2" width="250" style="padding: 7px 10px 10px"><br>
 
3.	Tap <span class="material-symbols-outlined">file_open</span> on the upper right corner. (This menu is available on mobile apps only)
<br><img src="export_3(2).png" alt="export_3(2)" width="250" style="padding: 7px 10px 10px"><br>
 
4.	To export, tap **Export to file**.
<br><img src="export_4.png" alt="export_4" width="240" style="padding: 7px 10px 10px"><br>
 
5.	Select an app or a drive to export as a CVS file. 
* To import verses, tap **Import from file**. The file must be a CVS file in the same format and stored in a local folder. (For Android, store the importing file in a local folder <u>other than</u> the Download folder.)
[\[Back to Top\]](#contents)

### **How to Change the Text Size** {#text_size}
1.	Tap **Settings** on the main navigation bar. (If the navigation bar is not shown, tap <span class="material-icons">menu</span> on the upper left.)
<br><img src="text_1.png" alt="text_1" width="240" style="padding: 7px 10px 10px"><br>
 
2.	Tap **General Settings**.
<br><img src="text_2.png" alt="text_2" width="210" style="padding: 7px 10px 10px"><br>
 
3.	Adjust the text size in <span class="material-icons-outlined">text_fields</span>.
<br><img src="text_3.png" alt="text_3" width="260" style="padding: 7px 10px 10px"><br>
 
[\[Back to Top\]](#contents)

### **How to Sync between Devices** {#sync}
1.	Tap <span class="material-icons">menu</span> to open the main navigation bar (if not open yet).
<br><img src="sync_1.png" alt="sync_1" width="230" style="padding: 7px 10px 10px"><br>
 
2.	Tap <span class="material-icons-outlined">refresh</span> (or a similar icon) at the top of the main navigation bar. (Available on mobile apps only)
<br><img src="sync_2.png" alt="sync_2" width="250" style="padding: 7px 10px 10px"><br>
 
See ["Synchronising Multiple Devices"](/docs/accounts/#sync) for more information.
[\[Back to Top\]](#contents)

### **How to Add Accounts** {#add_account}
A user can have one or more accounts for different purposes.<br><br>

1.	Tap <span class="material-icons">menu</span> to show the main navigation bar on the left (if not shown already).
<br><img src="add_acct_1.png" alt="add_acct_1" width="230" style="padding: 7px 10px 10px"><br>
 
2.	Tap <span class="material-icons">arrow_drop_down</span> at the upper right.
<br><img src="add_acct_2.png" alt="add_acct_2" width="250" style="padding: 7px 10px 10px"><br>
 
3.	Note that the triangle is changed to <span class="material-icons-outlined">arrow_drop_up</span> in the account navigation bar.
<br><img src="add_acct_3.png" alt="add_acct_3" width="250" style="padding: 7px 10px 10px"><br>
 
4.	Tap **Create New Account**.
<br><img src="add_acct_4.png" alt="add_acct_4" width="230" style="padding: 7px 10px 10px"><br>

5.	Enter an account name.
<br><img src="add_acct_5.png" alt="add_acct_5" width="250" style="padding: 7px 10px 10px"><br>
 
6.	Select a language of verses and references, and other settings as needed.
7.	Tap <span class="material-icons">save</span> on the upper left to save the account settings. 
<br><img src="add_acct_6.png" alt="add_acct_6" width="250" style="padding: 7px 10px 10px"><br>
 
See ["Multiple Verse Accounts"](/docs/accounts/#multiple) for more information.
[\[Back to Top\]](#contents)

### **How to Switch between Accounts** {#switch}
1.	Tap <span class="material-icons">arrow_drop_down</span> on top right of the main navigation bar.
<br><img src="switch_acct_1.png" alt="switch_acct_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	Tap one of your accounts in the account navigation bar.
<br><img src="switch_acct_2.png" alt="switch_acct_2" width="250" style="padding: 7px 10px 10px"><br>
 
3.	Tap <span class="material-icons-outlined">arrow_drop_up</span> to return to the main navigation bar (if necessary).
<br><img src="switch_acct_3.png" alt="switch_acct_3" width="250" style="padding: 7px 10px 10px"><br>
 [\[Back to Top\]](#contents)<br><br>

### **Bible Book Abbreviations** {#books}
<img src="books_en_1.png" alt="books_en_1" width="590" style="padding: 7px 10px 10px"><br>
[\[Back to Top\]](#contents)
