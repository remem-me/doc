---
title: "Prayers & Devotions"
linkTitle: "Prayers & Devotions"
weight: 18
aliases:
  - /docs/managing-prayer-cards
description: >
  Remember Me's boxes help you to manage prayer cards and other texts you intend to read or listen to regularly.
---

### Create a Prayer Account
Create a separate account for texts that you want to read regularly rather than memorise. Since the references probably play a minor role, you can select the option "Learn by Topic" when setting up the account.  

### Adding Prayers/Devotions
You can add texts in the "New" box or import a collection of texts from "Collections". Some kind of numbering is useful as reference. For example, if you split up a collection of long prayers into individual prayer cards  - such as the books by Stormie Omartian - the references could be 1a, 1b, 1c, 2a, 2b etc. References are used to sort the cards. It is also helpful to add a topic to each prayer.

### Pray Daily
Sort the prayers by topic or by reference (alphabetically) by tapping the "New" tab a second time. It is best to say the prayers aloud and in your own dialect or words. This way, even preconceived prayers can turn into a  personal conversation with God. 
Write down impressions and thoughts that become important to you during prayer below the prayer text (see [Editing a verse](/docs/verses/#edit)). In this way, your prayer account also becomes your prayer diary and reflects part of your communication with God. 
Move the prayer after your daily devotion to the "Due" box to keep track of your prayer progress. The "Known" tray is not needed for prayers. 

### Reusing a Prayer Collection
If you want to start over with a prayer collection after you have completed it, select a prayer (tap on its badge on the left), then "select all", and move the whole collection back to the "New" box.

