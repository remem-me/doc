---
title: "What's New?"
linkTitle: "What's New?"
weight: 1
description: >
  Remember Me 6 takes Bible memorization to a new level.
---

{{% pageinfo %}}
If you are looking for information about previous Remember Me versions, please visit the [help pages for Remember Me 5 and older](https://v5.remem.me).

If you need to recover verses after updating please see [this user forum post](https://forum.remem.me/d/120-update-to-remember-me-6-issues/3).
{{% /pageinfo %}}

### Easier to Use
Remember Me 6 emphasizes the flashcard metaphor. Flashcards move between the boxes New, Due, and Known. A verse's flashcard is the starting point for studying and for reviewing it. The flashcard metaphor helps to understand the basic processes more intuitively.

### More Versatility
Power users with hundreds of Verses enjoy more ways to group and filter their verse decks using customizable labels. Visual users benefit from adding images to their verses. Multilingual users can listen to reference and passage in different languages. 

### Unified Experience
The new app version guarantees a unified user experience across all devices, because it uses the same software framework for all platforms. App and framework are open source.
