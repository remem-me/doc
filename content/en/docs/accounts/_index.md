---
title: "Users & Accounts"
linkTitle: "Users & Accounts"
weight: 4
aliases:
  - /docs/users-and-accounts
  - /how-rm-works/synchronize
  - /bg/docs/accounts
  - /cs/docs/accounts
  - /hu/docs/accounts
  - /it/docs/accounts
  - /no/docs/accounts
  - /pl/docs/accounts
  - /ro/docs/accounts
  - /sv/docs/accounts
  - /ur/docs/accounts
description: >
  Register as a user and create an account for your verses.
---

{{< columns >}}

### Register and Log In a User {#register}

On [web.remem.me](https://web.remem.me) you can register for free. After submitting the registration form, you will
receive an email asking you to confirm your registration.

After completing the registration, you can log in to the app by clicking the **Log In** button (top right).

### Changing Your Password

For changing your password click on "Reset Password" in the login dialog.

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/fdoO6fDSGPE?si=VaicPMEwOoNMzWyy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Multiple Verse Accounts {#multiple}

An account for your verses is created automatically with your user registration. Language, account name, review
preferences etc. are stored in your account's settings. As a user you can add additional accounts, e.g. for your
children or for memorizing verses in a second language. For most users, one account will be sufficient. You cannot
transfer verses between accounts. For organizing your verses, use [labels](/docs/labels).

In order to add a new account, click on the <span class="material-icons">arrow_drop_down</span> triangle in the header
of the menu on the left, and select "Create new account" from the menu.

### Import Accounts from Previous Versions {#previous}

After creating a new user account (identified by your email address), you can attach verse accounts from previous
Remember Me versions. Click on the <span class="material-icons">arrow_drop_down</span> triangle in the header of the
menu on the left, and select "Add existing account" from the menu. Enter name and password of the legacy account and
tap on "Log in". You can use the attached account just like any other Remember Me 6 verse account.

### Synchronising Multiple Devices {#sync}

Devices with offline capability sport a <span class="material-icons">refresh</span> round arrow icon next to the
current account's name in the navigation drawer on the left. If the icon is not visible (e.g. in a web browser), verses
are stored online only. Verses and study progress are synchronised automatically. Tap the round arrow icon to load data
newly edited on a different device. If you need to force your device to retrieve and send all data, press long on the
icon.

{{% alert title="Synchronization Button" color="secondary" %}}
<span class="material-icons">refresh</span> **one arrow** as a circle: Possible changes on other devices. Tap to
retrieve them.  
<span class="material-icons">sync</span> **two arrows** as a circle: Local changes on this device. Tap to send them.  
<span class="material-icons">sync_problem</span> **exclamation mark** in the circle: Synchronisation failed. Tap to try
again.
{{% /alert %}}

### Editing and Deleting Verse Accounts {#edit-account}

In order to edit or delete the currently selected account, select "Account" from the main navigation on the left. Tap on
the top left button (<span class="material-icons">save</span> disk symbol) in order to save the account after editing
it.
Tapping on the <span class="material-icons">delete</span> wastebin icon in the top right corner deletes the account.
A deleted account is stored in the [waste bin](https://web.remem.me/account-bin) for three months and can be restored
from there.

### Editing and Deleting User Access {#edit-user}

If you want to edit or delete your user registration, switch to account navigation (<span class="material-icons">
arrow_drop_down</span> triangle in the header of the menu on the left) and select "User".
For editing your email address, enter the new address and the current password and tap on "Save".
For deleting your user access to remem.me altogether, all verse accounts need to be deleted first. Then, enter the
current password and tap on the user dialog's <span class="material-icons">delete</span> wastebin icon.

{{% alert title="Attention" color="warning" %}}

- Deleting your access to remem.me cannot be undone. All your data will be removed from all databases immediately and
  permanently.
- A user and all the associated user data will be deleted from the system after 360 days of inactivity (no login). You
  will receive notifications every 120 days.
  {{% /alert %}}

### On-Device Only Mode {#on-device-only}

You can access Remember Me without connecting to the remem.me cloud service. In this mode, all data is stored exclusively on your
device.

- Data remains on your device
- No cloud backup available
- Publishing verse collections not possible

{{% alert title="Important" color="warning" %}}
If you lose access to the app on your device in On-Device Only Mode, your verses cannot be recovered.
{{% /alert %}}

To access On-Device Only Mode, select "On-Device Only" at the bottom of the registration dialog (after tapping on "Get Started").

To exit On-Device Only Mode:

1. Open the menu drawer
2. Tap on the account name to switch to the accounts menu
3. Select "Log out"

To transfer local verses to an online account, [export and import](docs/labels/#export) your verses as a file.

{{% alert title="Note" color="secondary" %}}
On-Device Only Mode offers limited functionality compared to cloud-connected accounts. Consider the trade-offs
between data privacy and feature accessibility when choosing your preferred mode of operation.
{{% /alert %}}


