---
title: "Add & Edit Verses"
linkTitle: "Add & Edit Verses"
weight: 5
aliases:
  - /bg/docs/verses
  - /cs/docs/verses
  - /hu/docs/verses
  - /it/docs/verses
  - /no/docs/verses
  - /pl/docs/verses
  - /ro/docs/verses
  - /sv/docs/verses
  - /ur/docs/verses
  - /how-rm-works/editor
  - /how-rm-works-ios/editor-ios
  - /docs/adding-and-editing-verses
description: >
  Retrieve Bible verses from online bibles or add and edit any other text for learning by heart. Postpone, delete, or restore Bible memory flashcards.
---

{{< columns >}}

### Adding a Bible Memory Verse {#add}
Select the tab "New" and press on the "+" button at the bottom of the screen to open the form.
Fill in reference and passage and tap on the top left button (<span class="material-icons">save</span> disk symbol) in 
order to save the verse. The verse is now listed in your inbox.  
- Other Bible versions may be added upon request if the content's web link is provided.
- If you want to select Bibles in other languages, we suggest you create separate accounts for each language. (See [Multiple Verse Accounts](/docs/accounts/#multiple))

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/hNBWnX11uUM?si=ipKTHnIjBggyWXNF" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Retrieving a Passage from an Online Bible {#online-bible}
When adding or editing a verse, tap the dropdown button <span class="material-icons">arrow_drop_down</span> 
of the field "Source" in order to open the list of available Bible versions, or type a Bible abbreviation (e.g. KJV) 
into the field "Source".

If Remember Me recognizes the reference (e.g. 1st Jn 3:16 or 1John 3:1-3) as a reference to a Bible passage and the 
source (e.g. ESV) as an online Bible version, it displays a button with a <span class="material-icons">menu_book</span> 
Bible symbol. An additional toggle button <span class="material-icons-outlined">format_list_numbered</span> allows to retrieve the passage with or without verse numbers.
If you tap the Scripture button, Remember Me opens the online Bible's website and offers you to 
<span class="material-icons">paste</span> paste the passage into the app. You can add additional line breaks or other 
modifications and save the verse (see above).  
(<a href="https://www.youtube.com/watch?v=JkkjqzaRj_c" target="_blank">Tutorial: Change BIBLE TRANSLATIONS in Remember Me App</a>)

### Getting a Verse from a Bible App {#bible-app}
Most mobile Bible apps allow to share verses with other apps. Mark the verse in the Bible app, select "Share", and 
choose Remember Me from the list of apps. Remember Me opens the verse editor and fills in reference, passage, and source
(if provided by the Bible app). Tap on the save button to add the verse to your box of new Bible memory verses.

### Editing a Scripture Memory Verse {#edit}
If you tap on a verse listed in one of the three boxes (New, Due, Known), it is displayed as a flash card. You can edit 
it by tapping on the pencil symbol in the top bar. 

You can break a passage into multiple study sections by adding line breaks.

#### Formatting Options
- \*italic\*
- \*\*bold\*\*
- \>blockquote

#### Verse Numbers
Verse numbers are wrapped in square brackets, e.g. \[1\], \[2\], \[3\]. 
If the account setting "Learn with reference" is on, verse numbers are included in audio and study games.

### Attaching an Image {#image}
When editing a verse (see above), you can attach an image that will be used as flashcard background for the verse. 
Basically, you can enter the internet address (URL) of any image available online in the field "Image URL" in the lower 
part of the screen. For convenience, there is an <span class="material-icons">image</span> image button on the right 
for searching images on Unsplash.com. Tap on the name to find out more about the photographer and on the image to insert 
its address into the form field. Only photos from Unsplash.com are included in public verse collections.  

Tap on the top left button (<span class="material-icons">save</span> disk symbol) in order to save the verse with the image attached.

{{< columns >}}

### Moving, Postponing, or Deleting Verses {#move}
Select verses by tapping on their badge (rounded square on the left), and open the menu on the right (three dots). It provides the options to move 
the selected verses to a different box, to postpone their due date, or to delete them.

<--->

<iframe width="256" height="460" src="https://www.youtube.com/embed/jXoTGMypU3o?si=-LMkpHy2NytbuoNU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 8px;"></iframe>

{{< /columns >}}

### Restore Deleted Verses {#restore}
Recently deleted Scripture memory verses can be restored online from the [waste bin](https://web.remem.me/bin). 