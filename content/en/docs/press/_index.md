---
title: "Press"
linkTitle: "Press"
weight: 30
description: >
  Press releases and materials
---

### Downloadable Images
* <a href="/images/screens-en.png" download>Features</a>
* <a href="/images/dl-screenshot-flashcard.png" download>Screenshot flashcard</a>
* <a href="/images/dl-screenshot-verse-box.png" download>Screenshot verse box</a>
* <a href="/images/dl-screenshot-game.png" download>Screenshot game</a>
* <a href="/images/dl-app-icon-1024.png" download>App icon</a>
* <a href="/images/dl-banner1920x1186.png" download>Banner</a>


### Remember Me 6.7: The Leading Bible Memorization App Reaches New Heights

**Zurich, Switzerland - November 29, 2024** - Poimena, a Swiss non-profit organization dedicated to spiritual growth, has
announced the release of Remember Me version 6.7, marking a significant milestone in the evolution of the world’s most
popular Bible memorization app. This latest update, which has been four years in the making, completes the process of
creating a seamless user experience across all platforms. Whether accessed on Android, iOS, or through a web browser,
Remember Me 6 offers a cohesive interface that ensures users can effortlessly continue their Scripture memorization
journey regardless of the device they use.

In a groundbreaking move, Remember Me is now completely open source and free on all platforms, with no ads or
limitations. This transition to an open-source model under the MIT License reflects Poimena’s commitment to making
spiritual growth tools accessible to everyone. The new version introduces two major features: user-selected flashcard
images for personalizing the memorization process with custom visuals, and versatile label management for improved
organization and categorization of verses.

With an impressive 2.2 million downloads across Google Play and Apple App Store, Remember Me has solidified its position
as the most widely used Bible memorization app globally. This milestone underscores the app’s effectiveness and user
appeal in supporting spiritual growth through Scripture retention. The app continues to break language barriers by
allowing users to memorize verses in 44 languages from an extensive library of 284 Bible translations, catering to a
global audience and making Scripture memorization accessible to people from various linguistic backgrounds.

Remember Me retains its core features that have made it a favorite among users. These include gamification elements such
as engaging word puzzles and gap tests to enhance motivation; scientifically-backed spaced repetition algorithms that
optimize review schedules for effective long-term retention; multi-platform synchronization for seamless data
replication across devices; voice recording and text-to-speech capabilities that allow users to record their recitations
or listen to verses using speech synthesis; and customizable flashcards for creating personalized study materials from
various Bible translations.

Poimena, the organization behind Remember Me, is a Switzerland-based non-profit dedicated to providing free services for
spiritual growth. Their commitment to accessibility is reflected in Remember Me’s availability as a free, open-source
application under the MIT License. For more information about Remember Me and to download the app,
visit their website [www.remem.me](https://www.remem.me).