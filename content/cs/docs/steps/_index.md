---
title: "Pět kroků k zapamatování"
linkTitle: "5 kroků k zapamatování"
weight: 2
description: >
  Memorování biblických veršů pomocí aplikace Remember Me. Uplatňujte základní principy učení se Bibli nazpaměť v každodenním životě.
---

### 1 Přidejte biblický verš.
Aplikace Remember Me nabízí několik možností ukládání biblických textů do zařízení. Můžete
- zadat libovolný text ručně
- načíst verš z různých verzí Bible
- stahovat sbírky biblických veršů od ostatních uživatelů

### 2 Zapamatujte si verš.
Zapamatovat si biblický verš je zábavné, pokud používáte různé metody.
- Poslechněte si ji
- Skrýt náhodná slova
- Vytvořte z něj skládačku
- Ukažte první písmena nebo řádky prázdných slov
- Napište první písmeno každého slova

### 3 Použijte motivy a obrázky.
Ne každý si umí dobře zapamatovat čísla a vy si je pamatovat nemusíte. Přidejte k verši motiv nebo obrázek, který vám pomůže vybavit si správný verš zpaměti.

### 4 Opakujte si zapamatované verše.
Jakmile si verš zapamatujete, objeví se v poli "Splatný", kde si jej můžete zkontrolovat. Spusťte sérii flash karet pro kontrolu. Řekněte nahlas biblický verš, otočte kartičku a zkontrolujte, zda jste si ho zapamatovali správně.

Rozložené opakování zajistí, že si budete často opakovat nově zapamatované verše, ale nezapomenete ani na dobře známé verše.

### 5 Využijte čas na maximum.
Využijte každý okamžik naplno. Nejlepší aplikace pro zapamatování bible fungují, protože k zapamatování můžete využít dvě minuty při čištění zubů. Mějte aplikaci u sebe a používejte ji během přirozených životních přestávek.
