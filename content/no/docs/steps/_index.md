---
title: "Fem trinn å huske"
linkTitle: "5 trinn å huske"
weight: 2
description: >
  Lære bibelvers med appen Remember Me. Bruk de grunnleggende prinsippene for bibelminne i ditt daglige liv.
---

### 1 Legg til et bibelvers.
Bibelappen Remember Me tilbyr flere alternativer for å lagre bibeltekster på enheten din. Kan
- skriv inn tekst manuelt
- hente et vers fra en rekke bibelversjoner
- last ned samlinger av bibelvers fra andre brukere

### 2 Lær et vers utenat.
Å memorere et bibelvers er morsomt hvis du bruker en rekke metoder.
- Hør det
- Skjul tilfeldige ord
- Gjør det til et puslespill
- Vis de første bokstavene eller linjene med ord tomme
- Skriv den første bokstaven i hvert ord

### 3 Bruk temaer og bilder.
Ikke alle er flinke til å huske tall, og du trenger ikke være det for å huske en tekst. Legg til et tema eller bilde til verset ditt, og det vil hjelpe deg å huske det riktige verset fra minnet.

### 4 Gjenta minneversene.
Når du har lært et vers utenat, vil det vises i boksen "Forfaller" for vurdering. Start en serie minnekort for gjennomgang. Si bibelverset for å memorere høyt, snu kortet og se om du husket det riktig.

Mellomromsrepetisjon sørger for at du ofte anmelder nylig memorerte vers, men ikke glem kjente vers heller.

### 5 Utnytt tiden.
Få mest mulig ut av hvert øyeblikk. De beste appene for å huske bibelen fungerer fordi du kan dra nytte av de to minuttene mens du pusser tennene for å huske. Hold appen med deg og bruk den under de naturlige pausene i livet.
