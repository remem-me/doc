---
title: "Fem steg att komma ihåg"
linkTitle: "5 steg att komma ihåg"
weight: 2
url: "/sv/docs/steg"
aliases:
  - /sv/docs/steps
description: >
  Att memorera bibelverser med appen Remember Me. Tillämpa de grundläggande principerna för att memorera Bibeln i ditt dagliga liv.
---

#### 1 Lägg till en bibelvers.
Bibelappen Remember Me erbjuder flera alternativ för att spara bibeltexter på din enhet. Du kan
- ange [vilken text](/docs/verses/#add) som helst manuellt
- hämta en vers från [olika bibelversioner](/docs/verses/#online-bible)
- ladda ner [samlingar av bibelverser](/docs/collections/#find) från andra användare.

#### 2 Lär dig en vers utantill.
Det är roligt att [memorera en bibelvers](/docs/study/#studying) om du använder olika metoder.
- [Lyssna](/docs/audio/#listen-box) på den
- Göm slumpmässiga ord
- Gör det till ett pussel
- Visa de första bokstäverna eller raderna i tomma ord.
- Skriv den första bokstaven i varje ord

#### 3 Använd teman och bilder.
Det är inte alla som är bra på att komma ihåg siffror, och det behöver du inte heller vara för att memorera en text. Lägg till ett [tema](/docs/labels/) eller en [bild](/docs/verses/#image) till versen så hjälper det dig att minnas den rätta versen.

#### 4 Upprepa utantillinlärda verser.
När du har memorerat en vers kommer den att visas i rutan "Förfaller" för granskning. [Starta en serie flashkort](/docs/study/#passages) för genomgång. [Säg bibelversen högt](/docs/audio/#record), vänd på kortet och kontrollera om du kom ihåg den rätt.

Spaced Repetition ser till att du ofta går igenom nyinlärda verser, men glömmer inte heller välkända verser.

#### 5 Utnyttja tiden på bästa sätt.
Gör det mesta av varje ögonblick. De bästa apparna för att memorera bibeln fungerar eftersom du kan använda de två minuterna när du borstar tänderna till att memorera. Ha appen med dig och använd den under de naturliga avbrotten i livet.
