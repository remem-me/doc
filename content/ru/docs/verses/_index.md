---
title: "Управление библейскими стихами для запоминания"
linkTitle: "Управление стихами"
weight: 5
aliases:
  - /uk/docs/verses
description: >
  Получение библейских стихов из онлайн-Библии или добавление и редактирование любого другого текста для запоминания. Отложить, удалить или восстановить учебные карточки Библии.
---

### Добавление стиха Библии на память
Выберите вкладку "Новые" и нажмите на кнопку "+" внизу экрана, чтобы открыть форму.
Заполните ссылку и текст и нажмите на кнопку сверху слева (<span class="material-icons">save</span> символ диска), чтобы
сохранить стих. Теперь стих будет перечислен в вашем входящем ящике.

### Получение стиха из онлайн-Библии
При добавлении или редактировании стиха нажмите на кнопку выпадающего списка <span class="material-icons">arrow_drop_down</span>
в поле "Источник", чтобы открыть список доступных версий Библии, или введите аббревиатуру Библии (например, KJV)
в поле "Источник".

Если Помни меня распознает ссылку (например, 1 Ин 3:16 или 1Иоанна 3:1-3) как ссылку на стих Библии и источник (например, ESV) как версию онлайн-Библии, оно отображает кнопку с символом <span class="material-icons">menu_book</span>
Библии. Дополнительная кнопка-переключатель <span class="material-icons-outlined">format_list_numbered</span> позволяет получить стих с номерами стихов или без них.
Если вы нажмете на кнопку Священного Писания, Помни меня откроет веб-сайт онлайн-Библии и предложит вам
<span class="material-icons">paste</span> вставить стих в приложение. Вы можете добавить дополнительные переносы строк или другие
модификации и сохранить стих (см. выше).

### Получение стиха из приложения Библии
Большинство мобильных приложений Библии позволяют делиться стихами с другими приложениями. Выделите стих в приложении Библии, выберите "Поделиться" и
выберите Помни меня из списка приложений. Помни меня откроет редактор стихов и заполнит ссылку, текст и источник
(если этопредоставлено приложением Библии). Нажмите на кнопку сохранения, чтобы добавить стих в ваш ящик новых стихов Библии на память.

### Редактирование стиха на память {#edit}
Если вы нажмете на стих, перечисленный в одном из трех ящиков (Новые, Должные, Запомненные), он будет отображаться в виде карточки-вспышки. Вы можете отредактировать его,
нажав на символ карандаша в верхней панели.

Вы можете разделить текст на несколько секций для изучения, добавив переносы строк.

#### Параметры форматирования
- \*курсив\*
- \*\*жирный\*\*
- \>блочная цитата

#### Номера стихов
Номера стихов заключаются в квадратные скобки, например, \[1\], \[2\], \[3\].
Если включена настройка учетной записи "Изучать ссылку", номера стихов будут включены в озвучивание и учебные игры.

### Прикрепление изображения
При редактировании стиха (см. выше) вы можете прикрепить изображение, которое будет использоваться в качестве фона карточки-вспышки для стиха.
В основном, вы можете ввести интернет-адрес (URL) любого изображения, доступного в Интернете, в поле "URL изображения" в нижней части экрана. Для удобства справа есть кнопка <span class="material-icons">image</span> изображения
для поиска изображений на Unsplash.com. Нажмите на имя, чтобы узнать больше о фотографе и на изображение, чтобы вставить его адрес в поле формы. Только фотографии с Unsplash.com включены в публичные коллекции стихов.

Нажмите на кнопку сверху слева (<span class="material-icons">save</span> символ диска), чтобы сохранить стих с прикрепленным изображением.

### Перемещение, отсрочка или удаление стихов
Выберите стихи, нажав на их значок (округлый квадрат слева), и откройте меню справа (три точки). Оно предоставляет опции для перемещения
выбранных стихов в другой ящик, отсрочки их даты выполнения или удаления.

### Восстановление удаленных стихов
Недавно удаленные стихи на память можно восстановить онлайн из [корзины](https://web.remem.me/bin).


