---
title: "Что нового в Помни Меня 6?"
linkTitle: "Что нового?"
weight: 1
description: >
  Помни Меня 6 поднимает запоминание Библии на новый уровень.
---

{{% pageinfo %}}
Если вы ищете информацию о предыдущих версиях Помни Меня, пожалуйста, посетите [страницы помощи для Помни Меня 5 и старше](https://v5.remem.me).

Если вам нужно восстановить стихи после обновления, пожалуйста, ознакомьтесь с [этим сообщением на пользовательском форуме](https://forum.remem.me/d/120-update-to-remember-me-6-issues/3).
{{% /pageinfo %}}

### Проще в использовании
Помни Меня 6 акцентирует внимание на метафоре карточек. Карточки перемещаются между коробками Новые, Должные и Запомненные. Карточка стиха является отправной точкой для изучения и повторения. Метафора карточек помогает интуитивно понимать основные процессы.

### Больше возможностей
Пользователи-профессионалы, имеющие сотни стихов, получают больше способов группировки и фильтрации своих колод стихов с использованием настраиваемых ярлыков. Визуальные пользователи выигрывают от добавления изображений к своим стихам. Многоязычные пользователи могут слушать ссылки и тексты на разных языках.

### Единый опыт
Новая версия приложения гарантирует единый пользовательский опыт на всех устройствах, так как использует одини тот же программный фреймворк для всех платформ. Приложение и фреймворк являются открытым исходным кодом.

