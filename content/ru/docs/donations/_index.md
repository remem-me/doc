---
title: "Пожертвования"
linkTitle: "Пожертвования"
weight: 25
description: >
  Помни меня публикуется некоммерческой организацией Поимена, которая предоставляет бесплатные услуги, посвященные духовному росту.
---
### Основатели
Поимена была основана Rev. Peter Schaffluetzel и Rev. Regula Studer Schaffluetzel.

### Адрес
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Switzerland

### Банковский перевод
Используйте информацию ниже для совершения пожертвования через банковский перевод.
Пожалуйста, добавьте ваш электронный адрес в сообщение перевода, чтобы мы могли вас поблагодарить.
|                   |                         |
|-------------------|-------------------------|
| Название банка:   | Raiffeisen, Switzerland |
| Бенефициар:       | Poimena                  |
| Swift код (BIC):  | RAIFCH22XXX             |
| Номер счета IBAN: | CH74 8080 8006 2918 4731 8 |

### PayPal/кредитная карта
Если вы предпочитаете делать пожертвования через PayPal или кредитную карту, пожалуйста, используйте кнопку ниже.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="NHMVF5RBNH3TE" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - Безопаснее и проще платить онлайн!" alt="Кнопка пожертвований через PayPal" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>

