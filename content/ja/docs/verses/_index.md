---
title: "聖句の追加と管理" 
linkTitle: "聖句の追加と管理" 
weight: 5 
description: >
  Remember Meでは、Webサイトからの聖句のコピー、あらゆるテキストの追加・編集、フラッシュカードの削除・復元、学習日程の変更などができます。
---

### 聖句の追加
メインナビゲーションから「マイ聖句カード」を選択し、「新規」セクション下部の<span class="material-icons">add_circle</span>をタップすると、「聖句を追加」ページが表示されます。聖書の書名・章節と聖句を入力して、左上の<span class="material-icons">save</span>をタップして保存します。保存した聖句は、「新規」セクションのリストに表示されます。  

### Webサイトから聖句をコピーする方法 {#copy}
聖句を追加するには、<span class="material-icons">add_circle</span>ボタンをタップして「聖句を追加」ページを開き、「訳名」ボックスの<span class="material-icons">arrow_drop_down</span>（三角ボタン）をタップして、ドロップダウンリストから訳名を1つ選択します。または、英字3文字の略称を大文字で入力します（例：新共同訳聖書の場合は「JAS」。[「日本語聖書の訳名略称リスト」](/ja/docs/quickstart/#versions)を参照）。<br><br>
- リストにない訳名については、公開されたWebリンクがあれば、リクエストに応じて追加が可能です。
- 英語など他の言語の訳名を使用したい場合は、別アカウントを作成して、アカウント設定で希望の言語を選択することを推奨します（[「アカウントを追加する」](/ja/docs/quickstart/#add_account)を参照）。<br>

次に、「書名・章節」ボックスに書名（新共同訳聖書の書名）を入力します。
システムが認識すると、「書名・章節」ボックスの下に完全な書名（例：創世記）が表示されるので、それを選択します。通常は、最初の1～3文字入力すると書名が認識されます（例：創世記の場合は「創」など。[「聖書の書名略称リスト」](/ja/docs/quickstart/#books)を参照）。<br>
書名に続けて章番号を入力し、コロン（:）で区切って節番号を入力します。複数の節の場合は、ハイフン（-）で区切ります（例：第1章の第1節～第3節の場合は「1:1-3」）。<br><br>
章番号を入力した時点で、オレンジ色の聖書ボタン<span class="material-icons">menu_book</span>が表示されます。コピーする聖句に節番号を含めたい場合は、右上の<span class="material-icons-outlined">format_list_numbered</span> <span class="material-icons">toggle_off</span>（数字が書かれたトグル）をオンに（右にスライド）します。<br>
オレンジ色の聖書ボタン<span class="material-icons">menu_book</span>をタップすると、聖書のWebサイトから、指定した聖句が表示されます。右下の赤い貼り付けボタン<span class="material-icons">paste</span>をタップすると、テキストがRemember Meにコピーされます。「聖句」フィールドにコピーされたテキストは、改行を加えるなど、必要に応じて編集できます。左上の保存ボタン<span class="material-icons">save</span>をタップして保存します。  
（[クイックスタートガイドの「Remember Meに聖句をすばやく追加する」](/ja/docs/quickstart/#add)を参照）

### 他の聖書アプリからの聖句を共有
ほとんどの聖書アプリは、聖句を他のアプリと共有できる機能を備えています。他のアプリで聖句を選択し、「共有」または<span class="material-icons-outlined">share</span>をタップし、表示されたアプリのリストからRemember Meを選択します。Remember Meの「聖句を追加」の編集画面が表示され、必要に応じて、書名・章節や聖句を編集できます。左上の保存ボタン<span class="material-icons">save</span>をクリックすると、「新規」セクションに聖句が追加されます。

### 追加した聖句の編集 {#edit}
各セクション（「新規」「復習」「記憶済み」）にリスト表示された、それぞれの聖句（左側のアイコンではない部分）をタップすると、フラッシュカードが表示されます。フラッシュカードの聖句を編集したい場合は、右上の<span class="material-icons-outlined">more_vert</span>メニューから「編集」を選択します。
（[クイックスタートガイドの「保存した聖句カードを編集する」](/ja/docs/quickstart/#edit)を参照）

聖句に改行を加えると、「学習ゲーム」モードの<span class="material-icons-outlined">subject</span>（改行ごとに表示）で、改行された行ごとに表示されるようになります。

#### フォーマットオプション
- \*イタリック\*
- \*\*太字\*\*
- \>引用

#### 節番号
節番号は角括弧で囲まれます。例：\[1\]、\[2\]、\[3\]。
アカウント設定で「聖句の下に書名・章節を表示」が有効になっている場合、節番号は音声読み上げや学習ゲームに含まれます。

### 聖句に画像を追加する
編集画面（上記参照）では、聖句ごとに画像を追加できます。追加した画像は、フラッシュカードの背景として表示されます。画面の中ほどにある「画像のURL」フィールドに画像があるWebページのアドレス（URL）を入力することで画像を追加できます。<br><br>

「画像のURL」フィールドの右側の<span class="material-icons">image</span> （画像ボタン）をタップすれば、Unsplash.comで公開されている画像を簡単に挿入できます。上部の検索フィールドにテキストを入力して、好みのテーマの画像を検索することもできます。各画像下部の「撮影者」をタップすると、その写真を撮影した人の情報が表示されます。画像をタップすると、その画像のURLが「画像のURL」フィールドに自動入力されます。この画像ボタンで表示できるのは、Unsplash.comの画像のみです（それ以外の画像については、URLを直接入力してください）。<br>
画像を追加したら、左上の<span class="material-icons">save</span>（保存）をクリックして、画像付きの聖句を保存します。<br>
（[「Remember Meに聖句をすばやく追加する」](/ja/docs/quickstart/#add)を参照）

### 聖句の移動、復習の延期、削除
各セクションのリスト表示画面で、聖句の左側のアイコン（四角い部分）をタップすると、聖句を選択できます（チェックマークが付きます）。この状態で、右上の<span class="material-icons-outlined">more_vert</span>メニューをタップします。このメニューでは、選択した聖句を別のセクションに移動したり、復習日を延期したり、聖句を削除したりできます。

### 削除した聖句の復元
削除して間もない聖句は、メインナビゲーションの「聖句の復元」の[ごみ箱](https://web.remem.me/bin)から復元できます。