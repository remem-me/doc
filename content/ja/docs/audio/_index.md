---
title: "音声機能" 
linkTitle: "音声機能" 
weight: 12 
description: >
  Remember Meには、聖句の音声読み上げ機能と自分の暗唱を録音・再生する機能があります。
---

> 聖句を繰り返し聴くことで、より効果的に暗記できます。また、復習時に自分の声を録音することで、理解度が深まります。

### セクション内の聖句を聴く
聴きたい聖句があるセクションを開きます。「新規」セクションで聖句を聴くと、それらの聖句になじみやすくなります。さらに、「記憶済み」セクションで聖句を聴くと、記憶を更新できます。下部の<span class="material-icons">play_circle_filled</span>（再生）ボタンをタップすると聖句の読み上げが始まります。画面下部には、一時停止、停止、次の聖句を再生、前の聖句を再生するボタンが表示されます。<br><br>

<span class="material-icons">video_settings</span>（読み上げ設定）では、リストのすべての聖句を再生する（オン）か、1つのみを再生する（オフ）オプションの選択や、繰り返し再生の設定、読み上げ速度と音量の変更ができます（各アイコンの説明は、モバイルアプリではアイコンを長押し、パソコンではマウスカーソルをアイコンに重ねると表示されます）。<br>
聖句を読み上げた後にも書名・章節を繰り返してほしい場合は、「アカウント」設定で「聖句の下に書名・章節を表示」をオンにします。

### 表示中の聖句の読み上げ
フラッシュカードの聖句が表示されている画面で、右上の<span class="material-icons">play\_arrow</span>（再生）ボタンをタップすると聖句の読み上げが始まります。再生中に聖句のリスト表示の画面に戻っても、再生中の聖句を一時停止したり、再生したりできます。リスト表示の画面では、再生中の聖句に続く聖句を順次読み上げていきます。<br>
（[クイックスタートガイドの「聖句の音声読み上げ」](/ja/docs/quickstart/#play)を参照）

{{% alert title="テキスト読み上げ音声設定" color="secondary" %}}<span class="material-icons">speed</span> 読み上げ速度  
<span class="material-icons">pause\_presentation</span> 次の聖句を読み上げるまでの休止（秒）  
<span class="material-icons">av_timer</span> スリープタイマー（分単位）  
<span class="material-icons">playlist\_play</span> オン：リスト内の聖句をすべて再生|オフ：現在の聖句のみを再生<br>
<span class="material-icons">repeat</span> リスト内の聖句すべて（または1つの聖句）を繰り返し再生{{% /alert %}}

### 聖句を学習中に再生
学習ゲームモード（[学習ゲーム](/docs/study/#studying)を参照）では、答えが表示された後、自動的に音声読み上げが始まります。この自動音声読み上げを無効にしたい場合は、学習ゲーム画面右上の<span class="material-icons">settings</span>（学習ゲーム設定）メニューで、「音声」をオフにします。

### 暗唱の録音と再生
「復習」セクション（[聖句の復習](/docs/study/#passages)を参照）のフラッシュカードの表側（書名・章節）で、画面下部にある<span class="material-icons">mic</span>（マイク）ボタンをタップすると、聖句を暗唱する自分の声を録音できます。マイクボタンを再タップするか、カードをめくると録音が停止します。カード裏の聖句が表示されるとすぐに録音が再生されるので、自分の暗唱が正しかったかどうかを確認できます。

### 読み上げ音声の変更

{{% alert color="secondary" %}}
アカウント設定の*聖句の言語*と*書名・章節の言語*で音声の**言語**を変更できます。
{{% /alert %}}

Remember Meは、デバイスのテキスト読み上げエンジンを使用します。読み上げ音声の変更手順については以下をご覧ください。

- [iPhone/iPadの場合](https://support.apple.com/ja-jp/guide/iphone/iph96b214f0/17.0/ios/17.0)  
「設定」＞「アクセシビリティ」＞「読み上げコンテンツ」＞「声」で、 声と方言を選択してダウンロードし、選択した声をデフォルトに設定します。
- [Androidの場合](https://support.google.com/accessibility/android/answer/6006983?hl=ja)
Androidデバイス用のテキスト読み上げエンジンは多数あります。以下はその一部の例です。

- [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts&hl=ja)
- [Cereproc (CerePlay) TTS](https://play.google.com/store/apps/details?id=com.cereproc.cerevoicesapp&hl=ja)
- [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT?langCd=ja)
- [Googleの音声認識サービス](https://play.google.com/store/apps/details?id=com.google.android.tts&hl=ja)

WindowsまたはmacOSでウェブアプリを使用する場合、Remember Meの音声設定で音声を変更できます。まず、デバイスに音声データをインストールする必要がある場合があります。
