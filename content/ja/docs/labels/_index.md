---
title: "ラベルごとのグループ化とフィルタリング" 
linkTitle: "ラベルでのグループ化" 
weight: 7 
description: >
  聖句に付けたラベルごとに聖句の表示・非表示やフィルタリングが自在に行えます。
---

> 素早くフィルタリング：リスト表示の状態で、右上の<span class="material-icons-outlined">visibility</span>メニューをタップしてラベル名を選択すると、そのラベルの付いた聖句のみが表示されます。ラベル名を長押しすると、そのラベルの付いた聖句のみが非表示になります。<br>

### ラベルの作成と編集 {#create}
右側のメインナビゲーションから、「ラベルを編集」を選択します。この画面下部の<span class="material-icons">add_circle</span>をタップするとラベルを作成、ラベル名の左のペンアイコンをタップするとラベル名を編集、右の削除アイコン<span class="material-icons">delete</span>をタップするとラベルを削除できます。削除して間もないラベルは、画面右上の<span class="material-icons">restore_from_trash</span>（ラベルを復元）をタップして開いた画面から復元できます。

### 聖句にラベルを追加 {#attach}
リスト表示の状態で、左側の四角いアイコン部分をタップして、聖句を1つまたは複数選択します。選択した聖句にはチェックマークがつきます。右上の<span class="material-icons">label</span>（ラベルの追加/削除）をタップします。ラベル名にチェックをつけるとラベルが追加され、チェックを外すとラベルが聖句から削除されます。<span class="material-icons">save</span>（保存）をタップして変更を保存します。<br>
（[クイックスタートガイドの「聖句カードにラベルを追加する」](/ja/docs/quickstart/#label)を参照）

### 聖句をラベルごとにフィルタリング {#filter}
右上の<span class="material-icons-outlined">visibility</span>（フィルタリング）では、さまざまなフィルタリング設定ができます。メニュー下部に表示されたラベル名をタップ（表示）または長押し（非表示）することで、ラベルごとにフィルタリングできます。各アイコンは次の状態を示します。

<span class="material-icons-outlined">visibility_off</span>（白目に斜線）：このラベルが付いた聖句は非表示になります。

<span class="material-icons-outlined">visibility</span>（白目）：このラベルが付いた聖句は、別のラベルが除外モードになっていない限り表示されます。

<span class="material-icons">visibility</span>（塗りつぶされた目）：このラベルが付いた聖句のみが表示され、他の聖句はすべて非表示になります。<br>

その他の自動フィルターとフィルタリング設定は次のとおりです。

<span class="material-icons-outlined">visibility</span> **今日復習した聖句**：その日に復習した聖句が表示されます（他のフィルター設定に影響されます）。

<span class="material-icons-outlined">apps</span> **すべての聖句を表示**：他のフィルタリング設定が解除され、すべての聖句が表示されます（デフォルトの設定）。

<span class="material-icons-outlined">label_off</span> **ラベルのない聖句を表示**：このオプションをタップすると、他のラベルモードが解除され、ラベルのない聖句のみが表示されます。

### すべてのセクションの聖句を一覧表示 {#all}
コレクションすべてをエクスポートしたいとき（次のセクションを参照）など、すべてのセクションの聖句を一覧表示にするには、右上の<span class="material-icons-outlined">visibility</span>メニューから<span class="material-icons-outlined">layers</span>（セクション別の表示）を「オフ」にし、<span class="material-icons">apps</span>（すべての聖句を表示）フィルターを選択します。<br>
（[クイックスタートガイドの「聖句カードをフィルタリングする」](/ja/docs/quickstart/#filter) を参照）

### 聖句をエクスポート／インポート（CSVファイル） {#export}
現在表示されている聖句すべてをCSVファイルとしてエクスポートすることができます（モバイルアプリでのみ対応）。<span class="material-icons-outlined">visibility</span>メニューを開き、左上の<span class="material-symbols-outlined">file_open</span>をタップし、「ファイルにエクスポート」を選択します。「共有」オプションから、他のアプリや送信したい友人などエクスポート先を選択します。CSVファイルは、スプレッドシートアプリ（ExcelやGoogleスプレッドシートなど）で表示できます。<br>([クイックスタートガイドの「聖句をエクスポートする」](/ja/docs/quickstart/#export)を参照)<br><br>

「ファイルからインポート」を選択すると、デバイスに保存されたCVSファイルをインポートすることもできます。
（注：同じ構成のCVSファイルのみ対応可。Androidの場合はダウンロードフォルダ以外のローカルフォルダに保存する必要があります。）