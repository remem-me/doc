---
title: "Remember Me クイックスタートガイド"
linkTitle: "クイックスタート"
weight: 3
description: >
  聖書暗記アプリ Remember Me の基本的な使い方をステップバイステップで説明します。
---

##### 目次 {#contents}
[Remember Meに**ユーザー登録**する（無料）](#register) <br>
[**アカウント設定**を変更する](#account) <br>
[Remember Meに**聖句をすばやく追加**する](#add) <br>
[聖句カードを**開く**](#open) <br>
[聖句カードを**編集**する](#edit) <br>
[聖句カードを**削除**する](#delete) <br>
[聖句カードに**ラベルを追加**する](#label) <br>
[聖句カードを**並べ替え**る](#sort) <br>
[聖句カードを**フィルタリング**する](#filter) <br>
[カードを別のセクションに**移動**する](#move) <br>
[聖句の**音声読み上げ**](#play) <br>
[**学習ゲーム**にアクセスする](#study) <br>
[**コレクション**をインポートする](#collection) <br>
[自分の聖句コレクションを**公開**する](#publish) <br>
[聖句を**エクスポート**する](#export) <br>
[アプリの**文字サイズ**を変更する](#font_size) <br>
[デバイス間で設定を**同期**する](#sync) <br>
[**アカウントを追加**する](#add_account) <br>
[別のアカウントに**切り替え**る](#switch) <br>
[**聖書の書名略称**リスト](#books) <br>
[**日本語聖書の訳名略称**リスト」](#versions) <br><br>

**注**：
* Remember Meアプリは、アップデートを繰り返しているため、このユーザーガイドの画像と実際のデバイスの表示が多少異なる場合があります。
* アプリでの<mark>各アイコンの説明（ツールチップ）</mark>は以下の方法で表示できます。
	* Android/iPhoneアプリの場合：<mark>アイコンを長押し</mark>する
	* Web版の場合：<mark>アイコンにマウスカーソルを重ねる</mark>（マウスオーバー）
* マニュアル通りにならないことがあったときは、以下を試してみてください。
	* Android/iPhoneアプリの場合：アプリを再起動する、またはスマホを再起動する
	* Web版の場合：ブラウザを更新（リロード）する、またはブラウザのキャッシュもしくはRemember MeのCookie（remem.me）を削除して、ログインし直す
	
### **Remember Meにユーザー登録する**（無料）{#register}
1.	Remember Meウェブサイト[web.remem.me](https://web.remem.me).にアクセスします。
2.	「**今すぐ始める**」をタップ（またはクリック）します（これが表示されない場合は、画面を上にスワイプしてください）。
<br><img src="j_register_0.png" alt="j_register_0" width="260" style="padding: 7px 10px 10px"><br>
3.	メールアドレスとパスワードを入力し、右下の「**登録**」をタップします。
4.	登録確認メールが届いたら、メール内のリンクをタップします。
5.	ログインするには、右上の「**ログイン**」をタップします。
<br><img src="j_register_1.png" alt="j_register_1" width="250" style="padding: 7px 10px 7px"><br>
 
* Android／iPhoneのアプリを使用していない場合は、「**登録が完了しました**」ページ左上の<span class="material-icons-outlined">home</span>をタップ、または[web.remem.me](https://web.remem.me)にアクセスして、右上の「**ログイン**」をタップします。
6.	登録したメールアドレスとパスワードを入力して、左下の「**ログイン**」をタップします。

詳細については、[「ユーザー登録とログイン」](/ja/docs/accounts/#register)を参照してください。
[\[目次に戻る\]](#contents)

### **アカウント設定を変更する** {#account}
1.	<span class="material-icons-outlined">menu</span>をタップして、画面左のメインナビゲーションを開きます（表示されていない場合）。
<br><img src="j_account_1.png" alt="j_account_1" width="250" style="padding: 7px 10px 10px"><br>
 
2.	「**アカウント**」を選択します（通常は、登録したメールアドレスの「@」の左側がアカウント名となります）。
<br><img src="j_account_2.png" alt="j_account_2" width="250" style="padding: 7px 10px 10px"><br>
 
3.	聖句や書名・章節の言語や、その他の設定を必要に応じて選択します。
<br><img src="j_account_3.png" alt="j_account_3" width="250" style="padding: 7px 10px 10px"><br>
 カードの聖句の下に書名と章節番号を表示したい場合は、「**聖句の下に書名・章節を表示**」をオンにします。
<br><img src="j_account_4.png" alt="j_account_4" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_account_5.png" alt="j_account_5" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>

4.	左上の<span class="material-icons">save</span>をタップして、アカウント設定を保存します。
<br><img src="j_account_6.png" alt="account_6" width="250" style="padding: 7px 10px 10px"><br>

詳細については、[「ユーザーとアカウント」](/ja/docs/accounts)を参照してください。
[\[目次に戻る\]](#contents)

### **Remember Meに聖句をすばやく追加する** {#add}
公開されている聖書のウェブサイトから、アプリ内に好きな聖句を簡単にコピーできます。<br><br>
1.	<span class="material-icons">menu</span>をタップして、メインナビゲーションを開きます（表示されていない場合）。
 <br><img src="j_add_0.png" alt="j_add_0" width="250" style="padding: 7px 10px 10px"><br>
2.	「**マイ聖句カード**」をタップ（またはクリック）します。
<br><img src="j_add_1.png" alt="j_add_1" width="250" style="padding: 7px 10px 10px"><br>
3.	「**新規**」セクションで下部の<span class="material-icons">add_circle</span>ボタンをタップすると、「**聖句を追加**」ページが表示されます。
 <br><img src="j_add_2.png" alt="j_add_2" width="190" style="padding: 7px 10px 10px"><br>
4.	「**訳名**」ボックスの<span class="material-icons">arrow_drop_down</span>をタップして、ドロップダウンリストから訳名を1つ選択します。
または、英字3文字の略称を大文字で入力します（例：新共同訳聖書の場合は「JAS」。[「日本語聖書の訳名略称リスト」](#versions)を参照）。
 <br><img src="j_add_3.png" alt="j_add_3" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_add_4.png" alt="j_add_4" width="250" style= "vertical-align: top; padding: 7px 10px 10px">
* 英語など他の言語の聖書を表示させたい場合は、言語ごとに別アカウントを作成して（[「アカウントを追加する」](#add_account)を参照）、アカウント設定ページの「**聖書の言語**」で希望の言語を選択することを推奨します。
5.	「**書名・章節**」ボックスに書名を入力します。書名が認識されると、「**書名・章節**」ボックスの下に書名が表示されるので、タップして（または「Enterキー」を押して）選択します。通常は、最初の1～3文字入力すると書名が認識されます（例：創世記の場合は「創」など。新共同訳聖書の書名で認識されます。[「聖書の書名略称リスト」](#books)を参照）。
 <br><img src="j_add_5.png" alt="j_add_5" width="250" style="horizontal-align: center; padding: 7px 10px 10px"><br>
6.	書名の後に章番号を入力し、コロン（:）で区切って節番号を入力します。複数の節の場合は、ハイフン（-）で区切ります（例：第1章第1節～第3節の場合は「1:1-3」）。全角数字とコロンで認識されることもありますが、半角の使用を推奨します。<br>
章番号を入力した段階で、オレンジ色の聖書ボタン<span class="material-icons">menu_book</span>が右上に表示されます。
<br><img src="j_add_6.png" alt="j_add_6" width="250" style="padding: 7px 10px 10px"><br>
7.	コピーする聖句に節番号を含めたい場合は、右上のトグルボタン<span class="material-icons">format_list_numbered</span> <span class="material-icons">toggle_off</span>をオンに（右にスライド）します。
 <br><img src="j_add_7.png" alt="j_add_7" width="250" style="padding: 7px 10px 10px"><br>
8.	オレンジ色の聖書ボタン<span class="material-icons">menu_book</span>をタップすると、聖書のウェブサイトにリダイレクトされ、指定した章節番号の聖句が表示されます。右下の赤い貼り付けボタン<span class="material-icons">content_paste</span>をタップすると、テキストがRemember Meにコピーされます。
 <br><img src="j_add_8.png" alt="j_add_8" width="250" style="padding: 7px 10px 10px"><br>
9.	「**聖句**」フィールドにコピーされたテキストは、自由に編集できます。
 <br><img src="j_add_9.png" alt="j_add_9" width="250" style="padding: 7px 10px 10px"><br>
10.	トピック名を入力したい場合は、「**トピック**」ボックスに入力します。
 <br><img src="j_add_10.png" alt="j_add_10" width="250" style="padding: 7px 10px 10px"><br>
11.	聖句カードに画像を追加したい場合は、「**画像のURL**」ボックス右端の画像アイコン<span class="material-icons">image</span>をタップすると、Unsplash.comのページがアプリ内で開きます。検索ボックスにキーワードを入力して探したいタイプの画像を表示させ、好みの画像をタップします。
 <br><img src="j_add_11.png" alt="j_add_11" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_add_12.png" alt="j_add_12" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
12.	「**画像のURL**」ボックスに、選択したUnsplash.comの画像のURLがコピーされます。代わりに、他のウェブサイトからの画像URLを直接入力することもできます。
<br><img src="j_add_13.png" alt="j_add_13" width="250" style="padding: 7px 10px 10px"><br>
13.	左上の保存ボタン<span class="material-icons">save</span>をタップして聖句を保存します。
<br><img src="j_add_14.png" alt="j_add_14" width="250" style="padding: 7px 10px 10px"><br>
14.	「**新規**」セクションに、保存した聖句が表示されます。
 <br><img src="j_add_15.png" alt="j_add_15" width="250" style="padding: 7px 10px 10px"><br>
詳細については、[「Webサイトから聖句をコピーする方法」](/ja/docs/verses/#copy)を参照してください。<br>
[\[目次に戻る\]](#contents)

### **聖句カードを開く** {#open}
1.	カードの表側を開くには、表示名の右側の部分（アイコンではない部分）をタップします。
（カードの裏側を直接開きたい場合は、この部分を長押しします。）
 <br><img src="j_open_1.png" alt="j_open_1" width="250" style="padding: 7px 10px 10px"><br>
2.	「新規」セクションの場合、聖句の書名・章節が表示されたカードの表側が開きます。
設定によっては、トピック名とラベル名も表示されます。
 <br><img src="j_open_2.png" alt="j_open_12" width="250" style="padding: 7px 10px 10px"><br>
3.	カードをタップ（または、右下の<span class="material-icons">flip</span>をタップ）すると、カードの裏側（「新規」せクションでは、聖句）が表示されます。再度カードをタップすると、表側が再表示されます。
 <br><img src="j_open_3.png" alt="j_open_3" width="250" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)

### **聖句カードを編集する** {#edit}
聖句カード（表裏どちらでも）の右上にある<span class="material-icons-outlined">more_vert</span>（メニューを表示）をタップして、「**編集**」を選択します。
 <br><img src="j_edit_1.png" alt="j_edit_1" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_edit_2.png" alt="j_edit_2" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
（編集方法については、[「Remember Meに聖句をすばやく追加する」](#add)を参照）<br>
[\[目次に戻る\]](#contents)

### **聖句カードを削除する** {#delete}
1.	リスト表示の状態で、聖句のアイコン（左の部分）をタップして、聖句を選択します。
 <br><img src="j_delete_1.png" alt="j_delete_1" width="250" style="padding: 7px 10px 10px"><br>
2.	選択すると、聖句のアイコンにチェックマークがつきます。セクション内にあるすべての聖句を選択したい場合は、<span class="material-icons">select_all</span>（すべて選択）をタップします。選択を解除する場合は、もう一度タップします。左上の<span class="material-icons-outlined">more_vert</span>（メニューを表示）をタップします。
 <br><img src="j_delete_2.png" alt="j_delete_2" width="310" style="padding: 7px 10px 10px"><br>
3.	「**削除**」を選択します。
 <br><img src="j_delete_3.png" alt="j_delete_3" width="250" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)

### **聖句カードにラベルを追加する** {#label}
聖句カードにラベルを追加すると、カードをグループ化でき、聖句コレクションとして公開できます。
1.	リスト表示の状態で、聖句のアイコン（左の部分）をタップして、聖句を選択します。
<span class="material-icons">label</span>（ラベルの追加/削除）をタップします。
<br><img src="j_label_1.png" alt="j_label_1" width="320" style="padding: 7px 10px 10px"><br>
または、カードを開いた状態（表裏どちらでも）で、右上の<span class="material-icons-outlined">more_vert</span>をタップして、「**ラベルを追加**」を選択します。
 <br><img src="j_label_2.png" alt="j_label_2" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_label_3.png" alt="j_label_3" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
2.	既存のラベルを追加する場合は、選択するラベルのチェックボックスをオンにします。
 <br><img src="j_label_4.png" alt="j_label_4" width="250" style="padding: 7px 10px 10px"><br>
3.	新規にラベルを作成したい場合は、<img src="new_label_24dp_000000.svg">（ラベルを追加）ボタンをタップします。
 <br><img src="j_label_5.png" alt="j_label_5" width="240" style="padding: 7px 10px 10px"><br>
4.	ラベル名を入力して、右下の<span class="material-icons">save</span>（保存）ボタンをタップします。
 <br><img src="j_label_6.png" alt="j_label_6" width="240" style="padding: 7px 10px 10px"><br>
5.	使用するラベルのチェックボックスをオンにします。
 <br><img src="j_label_7.png" alt="j_label_7" width="240" style="padding: 7px 10px 10px"><br>
ラベル名が聖句の右下とカードの一番下に表示されます。
 <br><img src="j_label_8.png" alt="j_label_8" width="280" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_label_9.png" alt="j_label_9" width="260" style= "vertical-align: top; padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)

### **聖句カードを並べ替える** {#sort}
1.	現在のセクションのタブをタップします。現在表示されているセクションには、<img src="/images/triangle.svg">とオレンジ色の下線が表示されます。
<br><img src="j_sort_1.png" alt="j_sort_1" width="250" style="padding: 7px 10px 10px"><br>
2.	並べ替えのオプションを選択します。
<br><img src="j_sort_2.png" alt="j_sort_2" width="250" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)

### **聖句カードをフィルタリングする** {#filter}
右上の<span class="material-icons-outlined">visibility</span>（フィルタリング）をタップします。
<br><img src="j_filter_1.png" alt="j_filter_1" width="250" style="padding: 7px 10px 10px"><br>
フィルタリングメニューが表示されます。この標準メニューの下には、作成したラベル名が表示されるので、ラベルごとにフィルタリングすることもできます。
<br><img src="j_filter_2.png" alt="j_filter_2" width="300" style="padding: 7px 10px 10px"><br><br>
「**セクション別の表示**」が「**オン**」（デフォルト設定）の状態では、セクション別（新規、復習、記憶済み）に聖句が表示されます。
 <br><img src="j_filter_3.png" alt="j_filter_3" width="300" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_filter_4.png" alt="j_filter_4" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br><br>
「**セクション別の表示**」を「**オフ**」にすると、1つのセクション内にすべての聖句が表示されます。
 <br><img src="j_filter_5.png" alt="j_filter_5" width="300" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_filter_6.png" alt="j_filter_6" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>

詳細については、[「ラベルごとのグループ化とフィルタリング」](/ja/docs/labels)を参照してください。<br>
[\[目次に戻る\]](#contents)

### **別のセクションにカードを移動する** {#move}
聖句が表示された状態でカードを右にスワイプすると、カードを右のセクションに移動できます。
<br><img src="j_move_1.png" alt="j_move_1" width="250" style="padding: 7px 10px 15px"><br>
移動した直後に移動を取り消したい場合は、<span class="material-icons">undo</span>（元に戻す）をタップすると元の状態に戻ります。
<br><img src="j_move_8.png" alt="j_move_8" width="250" style="vertical-align: top; padding: 7px 10px 10px ">
<img src="j_move_9.png" alt="j_move_9" width="250" style= "vertical-align: top; padding: 7px 10px 10px "><br>
また、リスト表示でカードが選択された状態で<span class="material-icons-outlined">more_vert</span>（メニューを表示）をタップすると、移動のオプションが表示されます。<br><br>
**「復習」セクションから移動する場合**<br>
「**復習**」セクションで、聖句のアイコン（左の部分）をタップします（選択を解除する場合は、もう一度タップします）。
<br><img src="j_move_2.png" alt="j_move_2" width="250" style="padding: 7px 10px 10px"><br>
右上の<span class="material-icons-outlined">more_vert</span>をタップします。
<br><img src="j_move_3.png" alt="j_move_3" width="250" style="padding: 7px 10px 10px"><br>
「**記憶済みに移動**」または「**新規に移動**」を選択します。
<br><img src="j_move_4.png" alt="j_move_4" width="250" style="padding: 7px 10px 10px"><br><br>

**「記憶済み」セクションから移動する場合**<br>
「**記憶済み**」セクションで、聖句のアイコン（左の部分）をタップします（選択を解除する場合は、もう一度タップします）。
<br><img src="j_move_5.png" alt="j_move_5" width="250" style="padding: 7px 10px 10px"><br>
右上の <span class="material-icons-outlined">more_vert</span>をタップします。
<br><img src="j_move_6.png" alt="j_move_6" width="250" style="padding: 7px 10px 10px"><br>
「**復習に移動**」または「**新規に移動**」を選択します。
<br><img src="j_move_7.png" alt="j_move_7" width="250" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)

### **聖句の音声読み上げ** {#play}
聖句のリスト表示で画面下の<span class="material-icons">play_circle_filled</span>をタップするか、カードの聖句が表示された状態で右上の<span class="material-icons">play_arrow</span>をタップすると聖句の読み上げが始まります。
<br><img src="j_play_1.png" alt="j_play_1" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_play_2.png" alt="j_play_2" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br><br>
再生中には、<span class="material-icons">pause</span>（一時停止）や<span class="material-icons">stop</span>（停止）ボタンなどが表示されます。
<br><img src="j_play_3.png" alt="j_play_3" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_play_4.png" alt="j_play_4" width="250" style= "vertical-align: top; padding: 7px 10px 10px"><br>
<img src="j_play_5.png" alt="j_play_5" width="250" style="padding: 7px 10px 10px"><br><br>
<span class="material-icons">video_settings</span>をタップすると読み上げ設定を変更できます。
<br><img src="j_play_6.png" alt="j_play_6" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_play_7.png" alt="j_play_7" width="250" style= "vertical-align: top; padding: 7px 10px 8px"><br>
<br><img src="j_play_8.png" alt="j_play_8" width="380" style="padding: 7px 10px 15px"><br>
詳細については、[「音声機能」](/ja/docs/audio)を参照してください。
[\[目次に戻る\]](#contents)

### **学習ゲームにアクセスする** {#study}
聖句カード開いた状態で、左下の<span class="material-icons">school</span>をタップします。
<br><img src="j_study_1.png" alt="j_study_1" width="250" style="padding: 7px 10px 10px"><br>
学習ゲームを1つ選択します。
* 注：「**ぼかし**」以外は、日本語ではうまく機能しないことがあります。
<br><img src="j_study_2.png" alt="j_study_2" width="370" style="padding: 7px 10px 10px"><br>
<br><img src="j_study_2-1.png" alt="j_study_2-1" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<img src="j_study_2-2.png" alt="j_study_2-2" width="250" style="vertical-align: top; padding: 7px 10px 10px">
<br><img src="j_study_2-3.png" alt="j_study_2-3" width="250" style="vertical-align: top; padding: 7px 10px 20px">
<img src="j_study_2-4.png" alt="j_study_2-4" width="250" style= "vertical-align: top; padding: 7px 10px 20px"><br>
学習ゲーム画面の右上の<span class="material-icons">settings</span>をタップすると、設定を変更できます。
<br><img src="j_study_3.png" alt="j_study_3" width="250" style="vertical-align: top; padding: 7px 10px 15px"><br>
<img src="j_study_4.png" alt="j_study_4" width="380" style= "vertical-align: top; padding: 7px 10px 10px"><br>
詳細については、[「聖句の暗記と復習」](/ja/docs/study)を参照してください。
[\[目次に戻る\]](#contents)

### **コレクションをインポートする** {#collection}
Remember Meでは、他のユーザーが公開した聖句コレクションをインポートしたり、自分の聖句コレクションを公開したりできます。<br><br>
1.	左のメインナビゲーションで「**コレクション**」をタップします。
（メインナビゲーションが表示されていない場合は、左上の<span class="material-icons">menu</span>をタップします。）
<br><img src="j_collection_1.png" alt="j_collection_1" width="230" style="padding: 7px 10px 10px"><br>
2.	他のユーザーが公開したコレクションの一覧が表示されます。他の言語のコレクションも表示できます。
<br><img src="j_collection_2.png" alt="j_collection_2" width="320" style="padding: 7px 10px 10px"><br>
3.	<span class="material-icons-outlined">search</span>をタップして、好みのテーマを検索したり、コレクションの一覧を並べ替えたり（デフォルトの「**おすすめ**」をタップ）できます。
<br><img src="j_collection_3.png" alt="j_collection_3" width="250" style="padding: 7px 10px 10px"><br>
4. 各コレクションをタップすると詳細が表示されます。上部の<span class="material-icons">download</span>（聖句をインポート）をタップすると、コレクションが、現在のアカウントの「マイ聖句カード」に追加されます。<br>
詳細については、[「聖句コレクションのインポートと共有」](/ja/docs/collections)を参照してください。<br>
[\[目次に戻る\]](#contents)

### **自分の聖句コレクションを公開する** {#publish}
自分の聖句をコレクションとして公開するには、まず、各聖句カードにラベルを追加する必要があります（[「聖句カードにラベルを追加する」](#label)を参照）。ラベルを追加した聖句は、ラベルごとにグループ化されます。<br><br>
1.	画面上部の<span class="material-icons">publish</span>をタップします。
<br><img src="j_publish_1.png" alt="j_publish_1" width="250" style="padding: 7px 10px 10px"><br>
2.	自分が作成したラベル名の一覧が表示されます。コレクションとして公開したいラベル名をタップします。
<br><img src="j_publish_2.png" alt="j_publish_2" width="250" style="padding: 7px 10px 10px"><br>
3.	「**説明**」セクションにコレクションの詳細を入力します。最低10文字入力する必要があります。詳細を入力したら、右下の赤い<span class="material-icons">public</span>をタップして公開します。
<br><img src="j_publish_3.png" alt="j_publish_3" width="250" style="padding: 7px 10px 10px"><br>
4.	公開すると、「**マイコレクション**」画面のアイコンが<span class="material-icons">public</span>に変わります。また、右の<span class="material-icons">share</span>をタップすると、個別に誰かと共有することもできます。
<br><img src="j_publish_4.png" alt="j_publish_4" width="390" style="padding: 7px 10px 15px"><br>
公開した聖句を非公開にしたい場合は、コレクションを開き、右下のグレーの<span class="material-icons">public_off</span>をタップします。
<br><img src="j_publish_5.png" alt="j_publish_5" width="130" style="padding: 7px 10px 10px"><br>
詳細については、[「自分の聖句コレクションの公開」](/ja/docs/collections/#publish)を参照してください。<br>
[\[目次に戻る\]](#contents)

### **聖句をエクスポートする** {#export}
Remember Meに保存した聖句は、CVS形式で一覧としてエクスポートしたり、インポートしたりもできます（モバイルデバイスでのみ対応）。<br><br>
1.	聖句のリスト表示の画面で、右上の<span class="material-icons-outlined">visibility</span>をタップします。
<br><img src="j_export_1.png" alt="j_export_1" width="250" style="padding: 7px 10px 10px"><br>
2.	すべてのせクションの聖句をエクスポートしたい場合は、「<span class="material-icons-outlined">layers</span> **セクション別の表示**」をタップして「**オフ**」にし、「<span class="material-icons">apps</span> **すべての聖句を表示**」をタップします。
<br><img src="j_export_2.png" alt="j_export_2" width="250" style="padding: 7px 10px 10px"><br>
3.	右上の<span class="material-symbols-outlined">file_open</span>をタップします（このメニューはモバイルアプリでのみ表示されます）。
<br><img src="j_export_3(2).png" alt="j_export_3(2)" width="250" style="padding: 7px 10px 10px"><br>
4.	エクスポートする場合は、「**ファイルにエクスポート**」を選択します。
<br><img src="j_export_4.png" alt="j_export_4" width="250" style="padding: 7px 10px 10px"><br>
5.	CVSファイルをエクスポートするアプリまたはドライブを選択します。
* 聖句リストをインポートする場合は、「**ファイルからインポート**」を選択します。インポートするファイルは、同じ構成で作成されたCVSファイルをローカルフォルダに保存します。（Androidの場合は、ダウンロードフォルダ<u>以外</u>のフォルダに保存してください。）<br>
[\[目次に戻る\]](#contents)

### **アプリの文字サイズを変更する** {#font_size}
1.	メインナビゲーションで「**設定**」をタップします（メインナビゲーションが表示されていない場合は、左上の<span class="material-icons">menu</span>をタップします）。
 <br><img src="j_text_1.png" alt="j_text_1" width="260" style="padding: 7px 10px 10px"><br>
2.	「**一般設定**」をタップします。
<br><img src="j_text_2.png" alt="j_text_2" width="270" style="padding: 7px 10px 10px"><br>
3.	<span class="material-icons-outlined">text_fields</span>で文字サイズを調整します。
 <br><img src="j_text_3.png" alt="j_text_3" width="270" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)

### **デバイス間で設定を同期する** {#sync}
<span class="material-icons">menu</span>をタップしてメインナビゲーションを開きます（すでに開いていない場合）。
<br><img src="j_sync_1.png" alt="j_sync_1" width="250" style="padding: 7px 10px 10px"><br>
メインナビゲーション上部の<span class="material-icons-outlined">refresh</span>（または類似のアイコン）をタップします（モバイルアプリでのみ対応）。
 <br><img src="j_sync_2.png" alt="j_sync_2" width="250" style="padding: 7px 10px 10px"><br>
詳細については、[「複数のデバイス間の同期」](/ja/docs/accounts/#sync)を参照してください。<br>
[\[目次に戻る\]](#contents)

### **アカウントを追加する** {#add_account}
登録ユーザーは、目的に応じて複数のアカウントを作成することもできます。<br><br>
1.	<span class="material-icons">menu</span>をタップして、画面左のメインナビゲーションを開きます（表示されていない場合）。
<br><img src="j_add_acct_1.png" alt="j_add_acct_1" width="250" style="padding: 7px 10px 10px"><br>
2.	右上の<span class="material-icons">arrow_drop_down</span>をタップします。
 <br><img src="j_add_acct_2.png" alt="j_add_acct_2" width="250" style="padding: 7px 10px 10px"><br>
3.	アカウントナビゲーションでは、三角ボタンが<span class="material-icons-outlined">arrow_drop_up</span>に変わります。
 <br><img src="j_add_acct_3.png" alt="j_add_acct_3" width="250" style="padding: 7px 10px 10px"><br>
4.	「**新しいアカウントを作成**」をタップします。
 <br><img src="j_add_acct_4.png" alt="j_add_acct_4" width="200" style="padding: 7px 10px 10px"><br>
5.	アカウント名を入力します。
<br><img src="j_add_acct_5.png" alt="j_add_acct_5" width="250" style="padding: 7px 10px 10px"><br>
6.	聖句や書名・章節の言語や、その他の設定を必要に応じて選択します。
7.	左上の<span class="material-icons">save</span>をタップして、アカウント設定を保存します。
<br><img src="j_add_acct_6.png" alt="j_add_acct_6" width="250" style="padding: 7px 10px 10px"><br>
詳細については、[「複数のアカウント」](/ja/docs/accounts/#multiple)を参照してください。
[\[目次に戻る\]](#contents)

### **別のアカウントに切り替える** {#switch}
1.	メインナビゲーションで<span class="material-icons">arrow_drop_down</span>をタップします。
<br><img src="j_switch_acct_1.png" alt="j_switch_acct_1" width="250" style="padding: 7px 10px 10px"><br>
2.	アカウントナビゲーションで、切り替えたいアカウントを1つ選択します。
<br><img src="j_switch_acct_2.png" alt="j_switch_acct_2" width="250" style="padding: 7px 10px 10px"><br>
3.	（必要に応じて）<span class="material-icons-outlined">arrow_drop_up</span>をタップしてメインナビゲーションに戻ります。
<br><img src="j_switch_acct_3.png" alt="j_switch_acct_3" width="250" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)<br><br>

### **聖書の書名略称リスト** {#books}
<img src="j_books_1.png" alt="j_books_1" width="900" style="padding: 7px 10px 10px"><br>
* 新約・旧約聖書の書名は「新共同訳聖書」より引用     [\[目次に戻る\]](#contents)

### **日本語聖書の訳名略称リスト** {#versions}
<img src="j_books_2.png" alt="j_books_2" width="310" style="padding: 7px 10px 10px"><br>
[\[目次に戻る\]](#contents)
