---
title: "寄付"  
linkTitle: "寄付"  
weight: 25  
description: >
  Remember Me は、スイスの非営利団体 Poimena によって開発されました。Poimena は、スピリチュアルな成長を目的とした無料のサービスを提供しています。
---

### 創設者
Poimena は、Peter Schaffluetzel 牧師と Regula Studer Schaffluetzel 牧師によって創設されました。

### 住所
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Switzerland

### 銀行振込（海外送金）
海外送金で寄付をいただける場合は、以下の情報をご利用ください。お礼メールをお送りしたいので、受取人宛メッセージ欄にご自分のメールアドレスを記入してください。
|                   |                         | 
|-------------------|-------------------------| 
|Bank name（金融機関名）：        | Raiffeisen, Switzerland | | Beneficiary（受取人氏名）：      | Poimena                 | 
| SWIFT（BIC）コード：| RAIFCH22XXX             | 
|IBAN（口座番号）：     | CH74 8080 8006 2918 4731 8 |

### PayPal（ペイパル）またはクレジットカード
PayPal またはクレジットカードで寄付をしていただける場合は、以下の「Donate」ボタンからお願いいたします。
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="NHMVF5RBNH3TE" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>
