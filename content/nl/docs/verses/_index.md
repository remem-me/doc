---
title: "Bijbelverzen beheren om te memoriseren"
linkTitle: "Verzen beheren"
weight: 5
url: "/nl/docs/verzen"
aliases:
  - /nl/docs/verses
description: >
  Bijbelverzen ophalen uit online bijbels of een andere tekst toevoegen en bewerken om uit het hoofd te leren. Uitstellen, verwijderen of herstellen van bijbelstudiekaarten.
---

### Een vers toevoegen {#toevoegen}

Selecteer het tabblad "Nieuw" en druk op de knop "+" onderaan het scherm om het formulier te openen.
Vul de referentie en het passage in en tik op de linkerbovenknop (<span class="material-icons">save</span> symbool
van de schijf) om het vers op te slaan. Het vers verschijnt nu in uw inbox.

### Een passage uit een online bijbel halen {#online-bijbel}

Bij het toevoegen of bewerken van een vers, tik op de uitklapknop <span class="material-icons">arrow_drop_down</span>
van het "Bron" veld om de lijst met beschikbare bijbelversies te openen, of typ een bijbelafkorting
(bijv. NBV) in het "Bron" veld.

Als Remember Me de referentie herkent (bijv. 1 Joh 3:16 of 1 Johannes 3:1-3) als een bijbelpassage
en de bron (bijv. NBV) als een online bijbelversie, wordt een knop weergegeven met een symbool
<span class="material-icons">menu_book</span> bijbel. Een uitklapknop <span class="material-icons-outlined">
format_list_numbered</span> maakt het mogelijk de passage op te halen met of zonder
versnummers.
Als u op de Schrift-knop drukt, opent Remember Me de website van de online bijbel en biedt u aan om te plakken
<span class="material-icons">paste</span> de passage in de applicatie. U kunt extra regelafbrekingen toevoegen of andere
aanpassingen maken en het vers opslaan (zie hierboven).

### Een vers kopiëren vanuit een bijbel-app {#bijbel-app}

De meeste bijbel-apps voor mobiele apparaten bieden de mogelijkheid om verzen te delen met andere apps. Markeer de
vers in de bijbel-app, selecteer "Delen" en selecteer Remember Me uit de lijst met apps. Remember Me
opent de vers-editor en vult de referentie, de tekst en de bron in (indien verstrekt door de bijbel-app).
Druk op de knop Opslaan om het vers toe te voegen aan uw verzameling nieuwe bijbelverzen.

### Een vers bewerken {#bewerken}

Als u een vers aanraakt dat in een van de drie vakken verschijnt (Nieuw, Actueel, Bekend), wordt het
weergegeven als een flitskaart.
U kunt het bewerken door op het potloodsymbool in de bovenste balk te tikken.

U kunt een passage opsplitsen in meerdere studiegedeelten door regelafbrekingen toe te voegen.

#### Opmaakopties
- \*cursief\*
- \*\*vet\*\*
- \>blokcitaat

#### Versnummers
Versnummers worden tussen vierkante haken geplaatst, bijv. \[1\], \[2\], \[3\].
Als de accountinstelling "Referentie bestuderen" is ingeschakeld, worden versnummers opgenomen in spraakaanduidingen en leerspellen.

### Een afbeelding toevoegen {#afbeelding}

Bij het bewerken van een vers (zie hierboven), kunt u een afbeelding toevoegen die als achtergrond voor de memokaart
wordt gebruikt
voor het vers. U kunt eenvoudigweg het internetadres (URL) van een beschikbare afbeelding online invoeren in
het "URL van afbeelding" veld onderaan het scherm. Voor uw gemak is er een knop
<span class="material-icons">image</span> afbeelding aan de rechterkant om afbeeldingen te zoeken op Unsplash.com. Tik
op de naam
om meer te weten te komen over de fotograaf en tik op de afbeelding om het afbeeldingsadres in het formulierveld in te
voegen. Alleen
foto's van Unsplash.com worden opgenomen in openbare versverzamelingen.

Tik op de linkerbovenknop (<span class="material-icons">save</span> schijfsymbool) om het vers op te slaan met
de bijgevoegde afbeelding.

### Verzen verplaatsen, uitstellen of verwijderen {#verplaatsen}

Selecteer de verzen door op hun badge te tikken (afgeronde vierkant links) en open het rechtermenu (drie punten). Dit
biedt de mogelijkheid om de geselecteerde verzen naar een ander vak te verplaatsen, hun uitgestelde datum te wijzigen of
ze te verwijderen.

### Verwijderde verzen herstellen

Recent verwijderde verzen kunnen online worden hersteld vanuit de [prullenbak](https://web.remem.me/bin).
