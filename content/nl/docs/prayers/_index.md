---
title: "Beheer van gebedskaarten en devoties"
linkTitle: "Gebeden en devoties"
weight: 18
url: "/nl/docs/gebeden"
aliases:
  - /nl/docs/prayers
description: >
  Remember Me-boxen helpen bij het beheren van gebedskaarten en andere teksten die u regelmatig wilt lezen of beluisteren.
---

### Maak een gebedsaccount aan

Maak een apart account aan voor teksten die u regelmatig wilt lezen in plaats van te memoriseren. Aangezien referenties
waarschijnlijk een minder belangrijke rol spelen, kunt u bij het instellen van het account de optie "Thematisch leren"
selecteren.

### Gebeden/devoties toevoegen

U kunt teksten toevoegen in het "Nieuw" vak of een collectie teksten importeren vanuit "Collecties". Het is handig om
enige vorm van nummering als referentie toe te voegen. Bijvoorbeeld, als u een lange collectie gebeden opdeelt in
afzonderlijke gebedskaarten - zoals de boeken van Stormie Omartian - kunnen de referenties 1a, 1b, 1c, 2a, 2b, etc.
zijn. Referenties worden gebruikt om de kaarten te classificeren. Het is ook handig om aan elke gebedskaart een thema
toe te voegen.

### Dagelijks bidden

Sorteer de gebeden op thema of referentie (alfabetisch) door nogmaals op het tabblad "Nieuw" te tikken. Het is het beste
om de gebeden hardop en in uw eigen dialect of woorden te zeggen. Op deze manier kunnen zelfs voorgekauwde gebeden een
persoonlijk gesprek met God worden. Schrijf de indrukken en gedachten die tijdens het gebed belangrijk voor u zijn onder
de tekst (zie [Vers bewerken](/nl/docs/verzen)). Zo wordt uw gebedsaccount ook uw gebedsdagboek en weerspiegelt het een deel van uw
communicatie met God. Verplaats het gebed na uw dagelijkse devotie naar het "Actueel" vak om uw voortgang in het
gebed bij te houden. Het "Bekend" vak is niet nodig voor de gebeden.

### Hergebruik van een gebedscollectie

Als u een collectie gebeden opnieuw wilt gebruiken nadat u deze hebt voltooid, selecteer dan een gebed (raak de badge
links aan), selecteer vervolgens "Alles selecteren" en verplaats de hele collectie opnieuw naar het "Nieuw" vak.

