---
title: "Groeperen en filteren op tags"
linkTitle: "Groeperen op tags"
weight: 7
url: "/nl/docs/tags"
aliases:
  - /nl/docs/labels
description: >
  Het selecteren en deselecteren van tags stelt u in staat om verzen te verbergen, weergeven en filteren zoals u wilt.
---

> Snelle filtering: Tik op een tag in het menu <span class="material-icons-outlined">visibility</span> aan de
> rechterkant om exclusief de bijbehorende verzen weer te geven. Houd het lang ingedrukt om het uit te sluiten.

### Een tag maken of bewerken {#maken}

Selecteer "Tags bewerken" in het hoofdmenu aan de linkerkant om een tag te maken, bewerken of verwijderen. Recent
verwijderde tags kunnen online worden hersteld uit de [prullenbak](https://web.remem.me/labels/bin).

### Tags toewijzen aan verzen {#toewijzen}

Selecteer één of meerdere verzen door op hun badge te tikken (ronde vierkantje links). Tik op het
tag-icoon <span class="material-icons">label</span> in het contextmenu bovenaan. Vink de tags aan die u aan de
geselecteerde verzen wilt toewijzen, schakel de tags uit die u van de geselecteerde verzen wilt verwijderen en tik op de
knop om de wijzigingen op te slaan (diskette-icoon <span class="material-icons">save</span>).

### Verzen filteren op tag {#filteren}

De zichtbaarheid van verzen gegroepeerd op tags wordt ingesteld in het tagpaneel aan de rechterkant (zie hierboven). U
kunt de status van tags/groepen wijzigen door erop te tikken of erop te drukken. De pictogrammen hebben de volgende
betekenissen:

<span class="material-icons-outlined">visibility_off</span> **Doorgestreepte oog**: verzen met deze tag zijn altijd
verborgen.

<span class="material-icons-outlined">visibility</span> **Omlijnd oog**: verzen met deze tag zijn zichtbaar tenzij
sommige tags in exclusieve modus staan.

<span class="material-icons">visibility</span> **Vol oog**: verzen met deze tag zijn zichtbaar en alle andere verzen
zijn verborgen.

Enkele automatische filters en filterinstellingen zijn beschikbaar:

<span class="material-icons-outlined">visibility</span> **Vandaag beoordeeld**: Deze filter filtert verzen die vandaag
zijn
herzien (afhankelijk van andere filterinstellingen).

<span class="material-icons-outlined">apps</span> **Alle verzen**: door deze optie aan te tikken, worden de modi van de
tags gewijzigd zodat alle verzen zichtbaar zijn.

<span class="material-icons-outlined">label_off</span> **Verzen zonder tag**: door deze optie aan te tikken, worden de
modi van de tags gewijzigd zodat alleen verzen zonder tag zichtbaar zijn.

### Alle verzen in één lijst {#allemaal}

In sommige situaties kan het handig zijn om alle verzen in dezelfde lijst te bekijken, bijv. om uw volledige verzameling
naar een bestand te exporteren (zie hieronder). U kunt dit bereiken in het zichtbaarheidsvenster aan de rechterkant door
de vakjes <span class="material-icons-outlined">layers</span> uit te schakelen en de automatische
filter <span class="material-icons">apps</span> "Alle verzen" te selecteren.

### Verzen exporteren/importeren als bestand (CSV) {#exporteren}

Open het bestandsmenu <span class="material-symbols-outlined">file_open</span> (alleen mobiele app) in de
zichtbaarheidsbalk <span class="material-icons-outlined">visibility</span> aan de rechterkant en selecteer "Exporteren
naar bestand". Hierdoor wordt een nieuw bestand in CSV-formaat gecreëerd met de momenteel weergegeven verzen. U kunt het
geëxporteerde bestand delen met andere apps om het op te slaan of naar iemand anders te sturen. CSV-bestanden kunnen
worden geopend met spreadsheetprogramma's.

Als u de optie "Importeren uit bestand" kiest, kunt u verzen laden vanuit een bestand dat op uw apparaat is opgeslagen.
