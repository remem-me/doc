---
title: "Wat is er nieuw in Remember Me 6?"
linkTitle: "Wat is er nieuw?"
weight: 1
url: "/nl/docs/versie"
aliases:
  - /nl/docs/version
description: >
  Remember Me 6 tilt het memoriseren van de Bijbel naar een hoger niveau.
---

{{% pageinfo %}}
Als u informatie zoekt over eerdere versies van Remember Me, bezoek dan
de [help pagina's van Remember Me 5 en eerdere versies](https://v5.remem.me).

Als u verzen moet herstellen na het bijwerken, zie dan [dit gebruikersforum bericht](https://forum.remem.me/d/120-update-to-remember-me-6-issues/3).
{{% /pageinfo %}}

### Gemakkelijker in gebruik

Remember Me 6 legt de nadruk op de metafoor van geheugenkaarten. De kaarten verplaatsen zich tussen de Nieuw, In
afwachting en Gememoriseerd vakken. De kaart van een vers is het startpunt om te bestuderen en te herzien. De metafoor
van de kaarten helpt om de basisprocessen intuïtiever te begrijpen.

### Meer veelzijdigheid

Gevorderde gebruikers met honderden verzen kunnen hun verzamelingen van verzen op meer manieren groeperen en filteren
door middel van aanpasbare labels. Visuele gebruikers profiteren van het toevoegen van afbeeldingen aan hun verzen.
Meertalige gebruikers kunnen de referentie en passage in verschillende talen beluisteren.

### Eén uniforme ervaring

De nieuwe versie van de app zorgt voor een uniforme gebruikerservaring op alle apparaten, omdat het dezelfde
softwareframework gebruikt voor alle platforms. De app en het softwareframework zijn open source.
