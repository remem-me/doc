---
title: "Collecties importeren en delen"
linkTitle: "Collecties van verzen"
weight: 9
url: "/nl/docs/collecties"
aliases:
  - /hoe-werkt-rm-functies/collecties
  - /nl/docs/collecties-importeren-en-delen
description: >
  Profiteer van openbare verscollecties en deel uw eigen collecties met anderen.
---

> Een verscollectie is een gepubliceerd label samen met zijn verzen.

### Een verscollectie vinden {#vinden}

Selecteer "Collecties" in het linkermenu van de app en gebruik de <span class="material-icons">search</span>
zoekfunctie om te vinden waar u naar op zoek bent. Tik op een collectiekaart om meer details te zien, en tik op
het <span class="material-icons">download</span> symbool in de balk rechtsboven om de verzen van de collectie naar uw
account te importeren. Een label met de naam van de collectie maakt de nieuwe verzen onderscheidend van uw andere
verzen.

### Een verscollectie publiceren en delen {#delen}

Selecteer "Collecties" in het linkermenu van de app en tik op de publicatieknop <span class="material-icons">
publish</span> (upload-pictogram rechtsboven) om een lijst te zien van alle labels die aan verzen zijn gekoppeld. Een
doorgestreept wereldsymbool <span class="material-icons">public_off</span> betekent dat het label en zijn verzen niet
zichtbaar zijn voor andere gebruikers.

Tik op een label en voer een beschrijving van uw collectie in het formulier in dat verschijnt. Om een publicatie te
annuleren of een eerder gepubliceerde collectie in te trekken, tikt u op het doorgestreepte wereldsymbool onderaan het
formulier. Alle details van de publicatie worden verwijderd en de verzen worden verborgen voor andere gebruikers.
Wanneer u klaar bent om te publiceren, tikt u op het gemarkeerde wereldsymbool <span class="material-icons">
public</span> onderaan het formulier. Alle details van de publicatie worden opgeslagen en de collectie verschijnt
vanaf nu in de zoekresultaten van andere gebruikers.

### Uw verscollectie insluiten op uw website

Omdat Remember Me 6 ook beschikbaar is als webapplicatie, kunt u een collectie of een vers van een collectie
insluiten op uw website. Bezoekers van uw website kunnen dan de games en leerkansen van Remember Me rechtstreeks op uw
website gebruiken. Het enige wat u hoeft te doen is een iframe-element met het adres van uw eigen verscollectie op
web.remem.me in uw HTML-code te plaatsen, bijvoorbeeld:
```
<iframe 
    src="https://web.remem.me/collections/407966087235984" 
    style="border:none;" 
    width="320" height="480" 
    title="Onze versverzameling">
</iframe>
```
wat resulteert in het volgende:

{{< app 407966087235984 >}}