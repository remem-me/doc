---
title: "Audiomogelijkheden"
linkTitle: "Audiomogelijkheden"
weight: 12
description: >
  Remember Me kan verzen voor u voorlezen of uw eigen voordracht opnemen en afspelen.
---

> Het herhaaldelijk beluisteren van verzen ondersteunt uw leerproces. Het opnemen van uw stem tijdens het voordragen versterkt uw kennis van het vers.

### Luisteren naar verzen uit vakken {#luisteren-vak}

Om te beginnen met het beluisteren van verzen, selecteert u het vak met de verzen die u wilt horen. Het beluisteren van verzen in uw "Nieuw" vak helpt u ermee vertrouwd te raken, terwijl het beluisteren van verzen in uw "Bekend" vak uw kennis opfrist. Druk op de <span class="material-icons">play_arrow</span> afspeelknop om te beginnen met luisteren. Met de knoppen onderaan kunt u een vers pauzeren, stoppen of overslaan.

U kunt Remember Me alle verzen of een enkel vers laten herhalen, of het afspelen vertragen via de instellingen (<span class="material-icons">settings</span> tandwiel) naast de bedieningsknoppen.

Als u de referentie na de bijbeltekst wilt toevoegen of de verzen per thema wilt beluisteren, activeert u de betreffende instelling onder "Account".

### Beginnen bij het huidige vers #{luisteren-vers}
Als u een leerkans voor u hebt liggen, kunt u het afspelen starten door op de knop <span class="material-icons">play_arrow</span> Afspelen in de rechterbovenhoek te drukken. Na terugkeer naar de lijst met verzen blijft het huidige vers nog steeds gemarkeerd als afspelend of gepauzeerd. Door nu op de afspeelknop van de lijst te tikken, begint de app met voorlezen vanaf deze positie in de lijst.

{{% alert title="Instellingen voor tekst-naar-spraak" color="secondary" %}}
<span class="material-icons">speed</span> Snelheid van spraak  
<span class="material-icons">pause_presentation</span> Pauze (in seconden)  
<span class="material-icons">av_timer</span> Innsovningstimer (i minutter)  
<span class="material-icons">playlist_play</span> *aan*: Leest de hele lijst voor / *uit*: Leest alleen het huidige vers voor  
<span class="material-icons">repeat</span> Herhaal de lijst (of het vers)
{{% /alert %}}

### Luisteren tijdens het leren van een vers {#luisteren-studie}
Tijdens een leerspel (zie [Studeren](/nl/docs/leren/#studeren)), leest Remember Me elk woord of elke zin voor die u onthult. U kunt de spraakuitvoer uitschakelen in het instellingenmenu (<span class="material-icons">settings</span> Tandwiel) tijdens het spel.

### Opnemen en afspelen van uw eigen voordracht {#opnemen}
De leerkansen in uw vak "Vervallen" (zie [Teksten herhalen](/nl/docs/leren/#teksten)) hebben onderaan een <span class="material-icons">mic</span> Microfoonsymbool. Tik erop om uw voordracht van het vers op te nemen. Door opnieuw op de knop te drukken of de kaart om te draaien, wordt de opname gestopt. Nadat de tekst van het vers is onthuld, wordt het afspelen gestart zodat u gemakkelijk kunt controleren of uw voordracht correct was.

### Stem wijzigen {#stem}

{{% alert color="secondary" %}}
U kunt de **taal** van de stem wijzigen in de instellingen van uw account *Taal van de verzen* en *Taal van de verwijzingen*.
{{% /alert %}}

Remember Me maakt gebruik van de tekst-naar-spraak-engine van uw apparaat. Instructies voor het wijzigen van de stem vindt u hier:
* [Help voor iPhone/iPad](https://support.apple.com/nl-nl/guide/iphone/iph96b214f0/ios#iph938159887)  
  Ga naar Instellingen > Toegankelijkheid > Gesproken inhoud > Stemmen: Selecteer een stem en dialect, download deze en stel deze in als standaard.
* [Help voor Android](https://support.google.com/accessibility/android/answer/6006983?hl=nl)

Voor Android-apparaten zijn meerdere tekst-naar-spraak-engines beschikbaar, zoals:
* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Speech Services van Google](https://play.google.com/store/apps/details?id=com.google.android.tts)

Bij gebruik van de web-app op Windows of macOS kunt u de stem wijzigen in de spraakinstellingen van Remember Me. Mogelijk moet u eerst spraakgegevens op uw apparaat installeren.



