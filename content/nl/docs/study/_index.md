---
title: "Studeren en herzien"
linkTitle: "Studeren en herzien"
weight: 6
url: "/nl/docs/leren"
aliases:
  - /nl/docs/study
description: >
  Spelletjes en een intelligent herhalingssysteem helpen je om te memoriseren en te onthouden.
---
### Het leerproces {#proces}
De te memoriseren verzen bewegen tussen de vakjes "Nieuw", "Actueel" en "Bekend".
U kunt de volgorde van een vakje wijzigen door op het actieve tabblad te tikken <img src="/images/triangle.svg">.

|Vakjes:|<span class="material-icons">inbox</span><br>Nieuw|&rarr;| <span class="material-icons">check_box_outline_blank</span><br>Actueel |&rarr;| <span class="material-icons">check_box</span><br>Bekend |
|:-|:-:|:-:|:----------------------------------------------------------------------:|:-:|:-------------------------------------------------------:|
|Acties:|1. **Studeren**|2. Toewijden|                     3. **Herzien**<br>**teksten**                      |4. Afspelen|           5. **Herzien**<br>**verwijzingen**            
|Knoppen:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|                 <img src="/images/cards_outline.svg">                  |<span class="material-icons">check</span>|          <img src="/images/cards_filled.svg">           

1. **Studeren** van nieuwe verzen.
2. Toewijding aan een vers verplaatst het naar het vakje "Actueel".
3. **Herzien van teksten** van verzen.
4. Correct afspelen van een vers verplaatst het naar het vakje "Bekend".
5. **Herzien van verwijzingen** van willekeurig geselecteerde verzen.

### Studeren {#studeren}
Nadat een vers aan het systeem is toegevoegd, kan het worden bestudeerd met verschillende soorten spellen.
Om te beginnen met studeren, tik op het vers en het <span class="material-icons">school</span> symbool voor studie.

<span class="material-icons">wb_cloudy</span> **Verhullen**: Verberg enkele woorden en probeer het vers op te zeggen
door de ontbrekende woorden vanuit uw geheugen aan te vullen. Tik op een verwarrend woord om het te onthullen.

<span class="material-icons">extension</span> **Puzzel**: bouw het vers door op het juiste woord te tikken.

<span class="material-icons">subject</span> **Uitlijnen**: toont lege regels of eerste letters door op het pictogram aan de
linkerkant te tikken (op de onderste balk). Ga verder door op <span class="material-icons">plus_one</span> (+woord) of
<span class="material-icons">playlist_add</span> (+regel) te tikken. Probeer het woord of de regel op te zeggen voordat u deze onthult.

<span class="material-icons">keyboard</span> **Schrijven**: bouw het vers door de eerste letter van elk woord te schrijven.

Nadat het vers is gememoriseerd, schuift u het tabblad naar rechts om het naar de lijst "Actueel" te verplaatsen. Vanaf
nu neemt het vers deel aan het herhalingsproces.

### Herzien van teksten {#teksten}
Selecteer het vakje "**Actueel**". Als u de volgorde van de verzen wilt wijzigen, tik dan nogmaals op "Actueel".
Er zijn 3 manieren om een herziening te starten:

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> omlijnde kaarten" >}}
Start een herziening van meerdere kaarten.
{{< /card >}}
{{< card header="Door op een vers te tikken" >}}
Start een herziening van slechts één kaart.
{{< /card >}}
{{< card header="Door op een vers te drukken" >}}
Opent de kaart aan de tekstzijde.
{{< /card >}}
{{< /cardpane >}}

Wanneer de referentie en het onderwerp worden weergegeven, zeg dan het vers uit het hoofd op en draai dan de kaart om te controleren
of u het juist had. Als u liever het vers regel voor regel opzegt (om regelovergangen te bewerken,
zie [Verzen toevoegen en bewerken](/nl/docs/verzen), tik dan op het pictogram <span class="material-icons">rule</span> in de
rechterbenedenhoek voor elke regel.

Als u een beetje hulp nodig heeft om te beginnen, houd dan de kaart ingedrukt om een hint te krijgen.
(U kunt een kaart overslaan door deze omlaag te schuiven.)

Als u het vers correct hebt onthouden, schuift u het naar rechts om het naar het vakje "Bekend" te verplaatsen. Hierdoor wordt
het niveau van het vers met 1 verhoogd (tot het herhalingslimiet in de accountinstellingen is bereikt). Als het niet helemaal correct was,
schuift u het naar links. Hierdoor wordt het niveau van het vers teruggezet naar 0. Dit kan een beetje ontmoedigend klinken, maar
het is erg belangrijk voor de herhalingsfrequentie van het vers. Verzen met lage niveaus worden vaker herhaald. Dit helpt bij het
versterken van het geheugen van een vers voordat het wordt vergeten.
Geleidelijke herhaling zorgt ervoor dat uitdagende teksten vaak worden herhaald, maar dat bekende teksten niet worden vergeten.

{{% alert title="Geleidelijke herhaling (Spaced Repetition)" color="secondary" %}}
Het geheugen is effectiever wanneer de hersenen zich herinneren wat ze hebben geleerd met steeds langere tussenpozen.
Remember Me gebruikt een exponentieel algoritme om de intervallen te berekenen (vergelijkbaar met Pimsleur). Als de herhalingsfrequentie
van uw account is ingesteld op "Normaal", zult u uw verzen herhalen na 1, 2, 4, 8, 16... dagen. Het andere
onderdeel van het algoritme is het terugzetten van het niveau van een vers naar 0 na een incorrecte herhaling (vergelijkbaar met Leitner).
Op deze manier worden moeilijke teksten vaker herhaald dan gemakkelijke teksten.  
{{% /alert %}}

### Herzien van verwijzingen {#referenties}
Als u alleen de referenties van de vandaag herziene teksten wilt herzien, tik dan op het label
"[Vandaag beoordeeld](/nl/docs/tags/#filteren)", voordat u begint. Selecteer het
vakje "**Bekend**", stel de volgorde in op "willekeurig" en tik op de knop met het pictogram <img src="/images/cards_filled.svg">
van volle kaarten onderaan.

Remember Me schudt de kaarten en presenteert uw teksten. Probeer de referentie uit het hoofd op te zeggen en draai dan de kaart om te
controleren of u het juist had. Als dat zo is, schuift u de kaart naar rechts. Als dat niet het geval is,
schuift u het naar links. De app zal de kaart tijdens de herhalingsessie teruggeven totdat u deze onthoudt.

U kunt een kaart overslaan door deze omlaag te schuiven. In tegenstelling tot de herziening van teksten, houdt de app geen
rekening met uw herhalingsvoortgang. Deze revisiemodus lijkt meer op een quiz die u af en toe moet doen om uw kennis van de referenties bij te werken.

{{% alert title="Accountinstellingen" color="secondary" %}}
- **Dagelijks doel**: Aantal te beoordelen woorden per dag.
- **Aantal herhalingsflashcards**: Maximaal aantal flashcards bij herhalen teksten.
- **Aantal omgekeerde flashcards**: Maximum aantal flashcards bij herhalen referenties.
- **Referentie bestuderen**: Referentie opnemen in het studeren en herzien van verzen.
- **Herzieningsfrequentie**: Hoe vaak de verzen verschijnen voor herziening.
- **Intervallimiet**: Maximum aantal dagen tot de volgende herhaling. Beperkt indirect het aantal niveaus dat kan worden bereikt.
  {{% /alert %}}


