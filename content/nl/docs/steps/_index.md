---
title: "Vijf stappen om te herinneren: Bijbelverzen onthouden"
linkTitle: "5 stappen om te herinneren"
weight: 2
url: "/nl/docs/steps"
description: >
  Leer hoe u Bijbelverzen kunt onthouden met de Remember Me-app. Pas de basisprincipes van het memoriseren van de Bijbel toe in uw dagelijks leven.
---

### 1 Voeg een Bijbelvers toe.
De bijbel-app Remember Me biedt verschillende mogelijkheden om Bijbelteksten op uw apparaat op te slaan. U kunt:
- een [willekeurige tekst](/nl/docs/verzen/#toevoegen) handmatig invoeren
- een vers ophalen uit verschillende [bijbelversies](/nl/docs/verzen/#online-bijbel)
- [collecties Bijbelverzen](/nl/docs/collecties/#vinden) downloaden van andere gebruikers

### 2 Onthoud een vers.
Het [studeren](/nl/docs/leren/#studeren) van een Bijbelvers is leuk als u verschillende methoden gebruikt:
- [Luister](/nl/docs/audio/#luisteren-vak) naar het vers
- Verberg willekeurige woorden
- Maak er een puzzel van
- Toon de eerste letters of lege regels
- Schrijf de eerste letter van elk woord

### 3 Gebruik thema's en afbeeldingen.
Niet iedereen is goed in het onthouden van getallen, en dat hoeft ook niet om een tekst uit het hoofd te leren. Voeg een [thema](/nl/docs/verzen/#bewerken) of [afbeelding](/nl/docs/verzen/#afbeelding) toe aan uw vers en het zal u helpen het juiste vers te onthouden.

### 4 Herhaal de gememoriseerde verzen.
Zodra u een vers uit het hoofd hebt geleerd, verschijnt het in het vakje "Actueel", zodat u het kunt [herhalen](/nl/docs/leren/#teksten). Start een serie flash-kaarten voor toetsing. [Zeg](/nl/docs/audio/#opnemen) het Bijbelse geheugenvers hardop, draai de kaart om en controleer of u het goed onthouden hebt. 

Spaced Repetition zorgt ervoor dat u regelmatig nieuw gememoriseerde verzen herziet, maar ook bekende verzen niet vergeet.

### 5 Benut de tijd optimaal.
Profiteer van elk moment. De Remember Me-app werkt omdat u de twee minuten tijdens het tandenpoetsen kunt gebruiken om te memoriseren. Houd de app bij u en gebruik hem tijdens die natuurlijke onderbrekingen in het leven.