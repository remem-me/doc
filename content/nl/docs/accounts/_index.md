---
title: "Gebruikers en accounts"
linkTitle: "Gebruikers en accounts"
weight: 4
url: "/nl/docs/accounts"
description: >
  Registreer u als gebruiker en maak een account aan voor uw verzen.
---

### Registreren en inloggen als gebruiker {#registeren}
Op [web.remem.me](https://web.remem.me) kunt u zich gratis registreren. Nadat u het registratieformulier hebt ingediend, ontvangt u een e-mail met het verzoek uw registratie te bevestigen.

Na registratie kunt u zich aanmelden bij de app door op de knop **Inloggen** (rechtsboven) te klikken.

{{% alert title="Uw wachtwoord wijzigen" color="secondary" %}}
Om uw wachtwoord te wijzigen, klik op "Wachtwoord opnieuw instellen" in het aanmeldvenster.
{{% /alert %}}

### Meerdere verzenaccounts {#meerdere}
Een account voor uw verzen wordt automatisch aangemaakt bij uw gebruikersregistratie. Taal, accountnaam, herhaalinstellingen, enz. worden ingesteld in de accountinstellingen. Als gebruiker kunt u extra accounts toevoegen, bijvoorbeeld voor uw kinderen of om verzen in een tweede taal te leren. Voor de meeste gebruikers zal één account voldoende zijn. Verzen kunnen niet worden verplaatst tussen accounts. Om verzen te organiseren, zijn [tags](/de/docs/schlagworte) beter geschikt.

Om een nieuw account toe te voegen, klikt u op het <span class="material-icons">arrow_drop_down</span> pijltje in het menu aan de linkerkant en selecteert u "Nieuw account maken" in het menu.

### Importeren van accounts uit eerdere versies {#eerdere}
Nadat u een nieuw gebruikersaccount (geïdentificeerd door uw e-mailadres) hebt aangemaakt, kunt u verzenaccounts uit eerdere Remember Me-versies toevoegen. Klik op het <span class="material-icons">arrow_drop_down</span> pijltje in de koptekst van het menu aan de linkerkant en selecteer "Bestaand account toevoegen" in het menu. Voer de naam en het wachtwoord van het oude account in en klik op "Inloggen". U kunt het bijgevoegde account gebruiken zoals elk ander Remember Me 6 verzenaccount.

### Synchronisatie tussen meerdere apparaten {#synchroniseren}
Op apparaten met offline-mogelijkheden is een rond pijlsymbool <span class="material-icons">refresh</span> naast de naam van het huidige account in de linker navigatiebalk te zien. Als het symbool niet zichtbaar is (bijvoorbeeld in een webbrowser), worden de verzen alleen online opgeslagen. Verzen en leervorderingen worden automatisch gesynchroniseerd. Klik op het ronde pijlsymbool om gewijzigde gegevens op een ander apparaat te laden. Als u uw apparaat wilt dwingen om alle gegevens op te halen en te verzenden, houdt u lang ingedrukt op het symbool.

{{% alert title="Synchronisatieknop" color="secondary" %}}
<span class="material-icons">refresh</span> **een pijl** in een cirkel: Mogelijke wijzigingen op andere apparaten. Klik erop om ze op te halen.  
<span class="material-icons">sync</span> **twee pijlen** in een cirkel: Lokale wijzigingen op dit apparaat. Klik erop om ze te verzenden.  
<span class="material-icons">sync_problem</span> **uitroepteken** in een cirkel: De synchronisatie is mislukt. Klik erop om het opnieuw te proberen.
{{% /alert %}}

### Bewerken en verwijderen van verzenaccounts {#account-bewerken}
Om het momenteel geselecteerde account te bewerken of te verwijderen, selecteert u "Account" in de hoofdnavigatie aan de linkerkant. Klik op het pictogram linksboven (diskette <span class="material-icons">save</span>) om het account na bewerking op te slaan. Om het account te verwijderen, klikt u op het prullenbakpictogram <span class="material-icons">delete</span> rechtsboven. Een verwijderd account wordt drie maanden lang opgeslagen in de [prullenbak](https://web.remem.me/account-bin) en kan van daaruit worden hersteld.

### Bewerken en verwijderen van gebruikers toegang {#gebruiker-bewerken}
Als u uw gebruikersaanmelding wilt bewerken of verwijderen, gaat u naar de accountnavigatie (<span class="material-icons">arrow_drop_down</span> pijltje in de koptekst van het menu aan de linkerkant) en selecteert u "Gebruiker". Om uw e-mailadres te wijzigen, voert u het nieuwe adres en het huidige wachtwoord in en klikt u op "Opslaan". Om uw gebruikerstoegang tot remem.me volledig te verwijderen, moeten eerst alle verzenaccounts worden verwijderd. Voer vervolgens het huidige wachtwoord in en klik op het prullenbakpictogram <span class="material-icons">delete</span> in het gebruikers dialoogvenster.

{{% alert title="Waarschuwing" color="warning" %}}
Het verwijderen van uw toegang tot remem.me kan niet ongedaan worden gemaakt. Al uw gegevens worden onmiddellijk en permanent verwijderd uit alle databases.
{{% /alert %}}

### Alleen op apparaat-modus {#alleen-op-apparaat}

U kunt Remember Me gebruiken zonder verbinding te maken met de remem.me-clouddienst. In deze modus worden alle gegevens uitsluitend op uw apparaat opgeslagen.

- Gegevens blijven op uw apparaat
- Geen cloudback-up beschikbaar
- Niet mogelijk om verzamelingen van verzen te publiceren

{{% alert title="Belangrijk" color="warning" %}}
Als u de toegang tot de app op uw apparaat verliest terwijl u in de Alleen op apparaat-modus bent, kunnen uw verzen niet worden hersteld.
{{% /alert %}}
Om de Alleen op apparaat-modus te activeren, selecteert u "Alleen op apparaat" onderaan het registratiedialoogvenster (na het tikken op "Begin")..

Om de Alleen op apparaat-modus te verlaten:

1. Open het menu
2. Tik op de accountnaam om naar het accountmenu te gaan
3. Selecteer "Uitloggen"

Om lokale verzen over te dragen naar een online account, [exporteert en importeert](docs/labels/#exporteren) u uw verzen als een bestand.

{{% alert title="Opmerking" color="secondary" %}}
De Alleen op apparaat-modus biedt beperkte functionaliteit in vergelijking met cloudverbonden accounts. Overweeg de voor- en nadelen tussen privacy en functionaliteit bij het kiezen van uw voorkeurswerkmodus.
{{% /alert %}}


