---
title: "Donaties"
linkTitle: "Donaties"
weight: 25
url: "/nl/docs/donaties"
aliases:
  - /nl/docs/donations
description: >
  Remember Me is een publicatie van Poimena, een non-profitorganisatie die gratis diensten biedt voor spirituele groei.
---
### Oprichters
Poimena werd opgericht door Rev. Peter Schaffluetzel en Rev. Regula Studer Schaffluetzel.

### Adres
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Zwitserland

### Bankoverschrijving
Gebruik de volgende informatie om een donatie per bankoverschrijving te doen.
Voeg uw e-mailadres toe aan het overschrijvingsbericht zodat we u kunnen bedanken.
|                     |                    |
|---------------------|--------------------|
| Naam van de bank:   | Raiffeisen, Zwitserland |
| Begunstigde:       | Poimena            |
| SWIFT-code (BIC): | RAIFCH22XXX        |
| IBAN-nummer:        | CH74 8080 8006 2918 4731 8 |

### PayPal/creditcard
Als u liever PayPal of een creditcard gebruikt om te doneren, gebruik dan de onderstaande knop.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="22E6HVFMD62HY" />
<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - De veiligere, gemakkelijkere manier om online te betalen!" alt="Doneren met PayPal-knop" />
<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
</form>
