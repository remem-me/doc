---
title: "Bedankt voor uw donatie!"
linkTitle: "Bedankt"
type: docs
toc_hide: true
hide_summary: true
description: >
  Hartelijk dank voor uw steun aan de ontwikkeling en verspreiding van Remember Me! Uw donatie is een zegen en een aanmoediging voor ons. God zegene u!
---

