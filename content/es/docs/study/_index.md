---
title: "Estudiar y repasar"
linkTitle: "Estudiar y repasar"
weight: 6
url: "/es/docs/estudiar"
aliases:
  - /es/docs/study
  - /es/docs/estudiando-y-revisando
description: >
  Los juegos y un sistema de repaso inteligente le ayudan a memorizar y retener.
---
### El proceso de aprendizaje {#proceso}
Los versículos para memorizar se mueven entre las cajas "Nuevo", "Pendiente" y "Memorizado".
Puede cambiar el orden de una caja tocando la pestaña activa <img src="/images/triangle.svg">.

|Cajas:|<span class="material-icons">inbox</span><br>Nuevo|&rarr;| <span class="material-icons">check_box_outline_blank</span><br>Pendiente |&rarr;| <span class="material-icons">check_box</span><br>Memorizado|
|:-|:-:|:-:|:------------------------------------------------------------------------:|:-:|:-:|
|Acciones:|1. **Estudiar**|2. Comprometerse|                       3. **Repasar**<br>**textos**                       |4. Reproducir|5. **Repasar**<br>**referencias**
|Botones:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|                  <img src="/images/cards_outline.svg">                   |<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">

1. **Estudiar** nuevos versos.
2. Comprometerse a un versículo lo mueve a la caja "Pendiente".
3. **Repasar textos** de versículos.
4. Reproducir correctamente un versículo lo mueve a la caja "Memorizado".
5. **Repasar referencias** de versículos seleccionados al azar.

### Estudiar {#estudiar}
Después de agregar un versículo al sistema, se puede estudiar usando diferentes tipos de juegos. 
Para comenzar a estudiar, toque el versículo y el símbolo <span class="material-icons">school</span> de estudio.

<span class="material-icons">wb_cloudy</span> **Ofuscar**: Oculte algunas palabras e intente recitar el versículo 
completando las palabras que faltan desde su memoria. Toque una palabra confusa para revelarla.

<span class="material-icons">extension</span> **Rompecabezas**: construye el versículo tocando la palabra correcta.

<span class="material-icons">subject</span> **Alinear**: muestra líneas vacías o primeras letras tocando el icono de la 
izquierda (en la barra inferior). Avance tocando <span class="material-icons">plus_one</span> (+palabra) o 
<span class="material-icons">playlist_add</span> (+línea). Trate de recitar la palabra o línea antes de revelarla.

<span class="material-icons">keyboard</span> **Escritura**: Construya el versículo escribiendo la primera letra de cada 
palabra.

Después de memorizar el versículo, deslice la ficha hacia la derecha para moverla a la lista "Pendiente". A partir de 
ahora, el versículo participa en el proceso de repaso.

### Repasar textos {#textos}
Seleccione la caja "**Pendiente**". Si desea cambiar el orden de los versículos, toque "Pendiente" otra vez.
Hay 3 formas de comenzar un repaso:

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> fichas delineadas" >}}
Inicia un repaso de varias fichas.
{{< /card >}}
{{< card header="Al tocar un versículo" >}}
Inicia un repaso de una sola ficha.
{{< /card >}}
{{< card header="Al presionar un versículo" >}}
Abre la ficha en el lado del texto.
{{< /card >}}
{{< /cardpane >}}

Cuando se muestren la referencia y el tema, recite el versículo de memoria, luego voltee la ficha para verificar 
si estaba en lo correcto. Si prefiere recitar el versículo línea por línea (para editar los saltos de línea, 
consulte [Agregar y editar versículos](/es/docs/versiculos)), toque el icono <span class="material-icons">rule</span> en la 
esquina inferior derecha para cada línea.

Si necesita un poco de ayuda para comenzar, mantenga presionada la ficha para obtener una pista. 
(Puede omitir una ficha deslizándola hacia abajo.)

Si recordó el versículo correctamente, deslícelo hacia la derecha para moverlo a la caja "Memorizado". Esto aumentará 
el nivel del versículo en 1 (hasta que se alcance el límite de repaso en la configuración de la cuenta). Si no fue del 
todo correcto, deslícelo hacia la izquierda. Esto restablecerá el nivel del versículo a 0. Esto puede sonar un poco 
impactante, pero es muy importante para la frecuencia de repaso del versículo. Los versículos con niveles bajos 
aparecen para repaso con más frecuencia. Esto ayuda a reforzar la memoria de un versículo antes de que se olvide. 
Repetición graduada asegura que repase textos desafiantes con frecuencia, pero tampoco olvide textos familiares.

{{% alert title="Repetición graduada (Spaced Repetition)" color="secondary" %}}
La memorización es más eficaz cuando el cerebro recuerda lo que ha aprendido a intervalos cada vez más largos. 
Remember Me utiliza un algoritmo exponencial para calcular los intervalos (similar a Pimsleur). Si la frecuencia de 
repaso de su cuenta está configurada como "Normal", repasará sus versos después de 1, 2, 4, 8, 16... días. El otro 
componente del algoritmo consiste en restablecer el nivel de un verso a 0 después de un repaso incorrecto (similar a 
Leitner). De este modo, se consigue que los textos difíciles se repitan más a menudo que los fáciles.  
{{% /alert %}}

### Repasar referencias {#referencias}
Si quiere repasar las referencias de los textos repasados hoy solamente, pulse la etiqueta 
"[Repasado hoy](/es/docs/etiquetas/#filtrar)", antes de empezar. Seleccione la 
caja "**Memorizado**", establezca su orden en "aleatorio" y toque el botón con el icono <img src="/images/cards_filled.svg"> 
de fichas lleno en la parte inferior. 

Remember Me baraja las fichas y presenta sus textos. Intente recitar la referencia de la memoria y dé la vuelta a la 
ficha tocándola para verificar si está en lo correcto. Si es así, deslice la ficha hacia la derecha. Si no es así, 
deslícelo hacia la izquierda. La aplicación le devolverá la ficha durante la sesión de repaso hasta que la recuerde.

Puede omitir una ficha deslizándola hacia abajo. A diferencia del repaso de textos, la aplicación no realiza un 
seguimiento de su progreso de repaso. Este modo de repaso se parece más a un cuestionario que debe realizar de vez 
en cuando para actualizar su conocimiento de las referencias.

{{% alert title="Configuración de la cuenta" color="secondary" %}}
- **Objetivo diario**: Número de palabras a repasar por día.
- **Número de fichas de repaso**: Número máximo de fichas por repaso de textos.
- **Número de fichas inversas**: Número máximo de fichas por repaso de referencias.
- **Aprender con referencia**: Incluir referencia en el estudio y repaso de versos.
- **Frecuencia de repaso**: Con qué frecuencia aparecerán los versículos para su repaso.
- **Límite de intervalo**: Número máximo de días hasta el próximo repaso. Limita indirectamente el número de niveles que se pueden alcanzar.
{{% /alert %}}



