---
title: "Política de privacidad"
linkTitle: "Política de privacidad"
weight: 21
url: "/es/docs/privacidad"
aliases:
  - /es/docs/privacy
  - /es/docs/política-de-privacidad
description: >
  Así es como protegemos su privacidad.
---

{{% pageinfo %}}
**POLÍTICA DE PRIVACIDAD**

**Última actualización: 25 de marzo de 2023**

Este aviso de privacidad para Poimena ('**organización**', '**nosotros**', o '**nuestro**'), describe cómo y por qué podemos recopilar, almacenar, utilizar y/o compartir ('**procesar**') su información cuando usted utiliza nuestros servicios ('**Servicios**'), como cuando usted:

* Visita nuestro sitio web en [https://web.remem.me](https://web.remem.me)

* Descarga y utiliza nuestra aplicación móvil (Remember Me. Bible Memory Joy)

**¿Preguntas o inquietudes?** La lectura de este aviso de privacidad le ayudará a comprender sus derechos y opciones en materia de privacidad. Si no está de acuerdo con nuestras políticas y prácticas, le rogamos que no utilice nuestros Servicios. Si aún así tiene alguna pregunta o inquietud, póngase en contacto con nosotros en support@remem.me.


**1\. ¿QUÉ INFORMACIÓN RECOPILAMOS?**

**Información personal que usted nos revela**

**_En breve:_** _Recogemos la información personal que usted nos proporciona._

Recopilamos la información personal que usted nos proporciona voluntariamente cuando se registra en los Servicios, expresa su interés en obtener información sobre nosotros o sobre nuestros productos y Servicios, cuando participa en actividades en los Servicios o, de otro modo, cuando se pone en contacto con nosotros.

**Información personal proporcionada por usted.** La información personal que recopilamos depende del contexto de sus interacciones con nosotros y con los Servicios, de las elecciones que haga y de los productos y funciones que utilice. La información personal que recopilamos puede incluir lo siguiente:

* direcciones de correo electrónico

* contraseñas

**Información sensible.** No procesamos información sensible.

Toda la información personal que nos proporcione debe ser verdadera, completa y precisa, y debe notificarnos cualquier cambio en dicha información personal.


**2\. ¿CÓMO PROCESAMOS SU INFORMACIÓN?**

**_En breve:_** _Procesamos su información para proporcionar, mejorar y administrar nuestros Servicios, comunicarnos con usted, para la seguridad y la prevención del fraude, y para cumplir con la ley. También podemos procesar su información para otros fines con su consentimiento._

**Procesamos su información personal por una variedad de razones, dependiendo de cómo usted interactúe con nuestros Servicios, incluyendo:**

* **Para facilitar la creación y autenticación de cuentas y gestionar de otro modo las cuentas de usuario.** Podemos procesar su información para que pueda crear e iniciar sesión en su cuenta, así como para mantener su cuenta en funcionamiento.

* **Para prestar y facilitar la prestación de servicios al usuario.** Podemos procesar su información para prestarle el servicio solicitado.

* **Para salvar o proteger el interés vital de un individuo.** Podemos procesar su información cuando sea necesario para salvar o proteger el interés vital de un individuo, como por ejemplo para prevenir daños.


**3\. ¿EN QUÉ BASES LEGALES NOS BASAMOS PARA PROCESAR SU INFORMACIÓN?**

**_En breve:_** _Sólo procesamos su información personal cuando creemos que es necesario y tenemos una razón legal válida (es decir, base legal) para hacerlo bajo la ley aplicable, como con su consentimiento, para cumplir con las leyes, para proporcionarle servicios para entrar en o cumplir con nuestras obligaciones contractuales, para proteger sus derechos, o para cumplir con nuestros intereses comerciales legítimos._

_**Si se encuentra en la UE o en el Reino Unido, esta sección se aplica a usted.**_

El Reglamento General de Protección de Datos (RGPD) y el RGPD del Reino Unido nos obligan a explicar las bases jurídicas válidas en las que nos basamos para procesar su información personal. Como tal, podemos basarnos en las siguientes bases legales para procesar su información personal:

* **Consentimiento.** Podemos procesar su información si usted nos ha dado permiso (es decir, consentimiento) para utilizar su información personal para un propósito específico. Puede retirar su consentimiento en cualquier momento.

* **Cumplimiento de un contrato.** Podemos procesar su información personal cuando creamos que es necesario para cumplir nuestras obligaciones contractuales con usted, incluyendo la prestación de nuestros Servicios o a petición suya antes de celebrar un contrato con usted.

* **Obligaciones legales.** Podemos procesar su información cuando creamos que es necesario para el cumplimiento de nuestras obligaciones legales, como cooperar con un cuerpo de seguridad o agencia reguladora, ejercer o defender nuestros derechos legales, o revelar su información como prueba en un litigio en el que estemos implicados.

* **Intereses vitales.** Podemos procesar su información cuando creamos que es necesario para proteger sus intereses vitales o los intereses vitales de un tercero, como situaciones que impliquen amenazas potenciales a la seguridad de cualquier persona.

**_Si se encuentra en Canadá, esta sección se aplica a usted._**

Podemos procesar su información si nos ha dado permiso específico (es decir, consentimiento expreso) para utilizar su información personal para un propósito específico, o en situaciones en las que su permiso puede inferirse (es decir, consentimiento implícito). Puede retirar su consentimiento en cualquier momento.

En algunos casos excepcionales, es posible que la legislación aplicable nos permita procesar su información sin su consentimiento, por ejemplo

* Si la recopilación es claramente en interés de un individuo y no se puede obtener el consentimiento de manera oportuna

* Para investigaciones y detección y prevención del fraude

* Para transacciones comerciales siempre que se cumplan determinadas condiciones

* Si figura en la declaración de un testigo y la recogida es necesaria para evaluar, tramitar o liquidar una reclamación al seguro

* Para identificar a personas heridas, enfermas o fallecidas y comunicarnos con los familiares más próximos

* Si tenemos motivos razonables para creer que una persona ha sido, es o puede ser víctima de abuso financiero

* Si es razonable esperar que la recopilación y el uso con consentimiento comprometa la disponibilidad o la exactitud de la información y la recopilación es razonable para fines relacionados con la investigación de un incumplimiento de un acuerdo o una contravención de las leyes de Canadá o de una provincia

* Si la divulgación es necesaria para cumplir con una citación, una orden judicial, una orden de un tribunal o las normas del tribunal relativas a la producción de registros

* Si fue producida por un individuo en el curso de su empleo, negocio o profesión y la recopilación es coherente con los fines para los que se produjo la información

* Si la recopilación tiene únicamente fines periodísticos, artísticos o literarios

* Si la información está a disposición del público y así lo especifica la normativa


**4\. ¿CUÁNDO Y CON QUIÉN COMPARTIMOS SU INFORMACIÓN PERSONAL?**

**_En breve:_** _Podemos compartir información en situaciones específicas descritas en esta sección y/o con los siguientes terceros._

Es posible que necesitemos compartir su información personal en las siguientes situaciones:

* **Transferencias de negocios.** Podemos compartir o transferir su información en relación con, o durante las negociaciones de, cualquier fusión, venta de activos de la organización, financiación o adquisición de la totalidad o una parte de nuestro negocio a otra organización.


**5\. ¿CUÁNTO TIEMPO CONSERVAMOS SU INFORMACIÓN?**

**_En breve:_** _Conservamos su información durante el tiempo necesario para cumplir con los propósitos descritos en este aviso de privacidad, a menos que la ley exija lo contrario._

Sólo conservaremos su información personal durante el tiempo que sea necesario para cumplir los fines establecidos en este aviso de privacidad, a menos que la ley exija o permita un periodo de conservación más largo (como requisitos fiscales, contables u otros requisitos legales). Ninguna de las finalidades contempladas en el presente aviso nos obligará a conservar sus datos personales durante más tiempo que el que los usuarios tengan una cuenta con nosotros.

Cuando no tengamos ninguna necesidad comercial legítima en curso de procesar su información personal, eliminaremos o anonimizaremos dicha información o, si esto no es posible (por ejemplo, porque su información personal se ha almacenado en archivos de copia de seguridad), almacenaremos de forma segura su información personal y la aislaremos de cualquier procesamiento posterior hasta que sea posible su eliminación.


**6\. ¿CÓMO MANTENEMOS SEGURA SU INFORMACIÓN?**

**_En breve:_** _Nuestro objetivo es proteger su información personal a través de un sistema de medidas de seguridad organizativas y técnicas._

Hemos implementado medidas de seguridad técnicas y organizativas adecuadas y razonables diseñadas para proteger la seguridad de cualquier información personal que procesemos. Sin embargo, a pesar de nuestras salvaguardas y esfuerzos por asegurar su información, no se puede garantizar que ninguna transmisión electrónica por Internet o tecnología de almacenamiento de información sea segura al 100%, por lo que no podemos prometer ni garantizar que los piratas informáticos, ciberdelincuentes u otros terceros no autorizados no puedan burlar nuestra seguridad y recopilar, acceder, robar o modificar indebidamente su información. Aunque haremos todo lo posible para proteger su información personal, la transmisión de información personal hacia y desde nuestros Servicios corre por su cuenta y riesgo. Sólo debe acceder a los Servicios dentro de un entorno seguro.


**7\. ¿CUÁLES SON SUS DERECHOS DE PRIVACIDAD?**

**_En breve:_** _En algunas regiones, como el Espacio Económico Europeo (EEE), el Reino Unido (RU) y Canadá, usted tiene derechos que le permiten un mayor acceso y control sobre su información personal. Puede revisar, modificar o cancelar su cuenta en cualquier momento._

En algunas regiones (como el EEE, el Reino Unido y Canadá), usted tiene ciertos derechos en virtud de las leyes de protección de datos aplicables. Estos pueden incluir el derecho (i) a solicitar acceso y obtener una copia de su información personal, (ii) a solicitar la rectificación o supresión; (iii) a restringir el tratamiento de su información personal; y (iv) si procede, a la portabilidad de los datos. En determinadas circunstancias, también puede tener derecho a oponerse al tratamiento de su información personal. Puede realizar dicha solicitud poniéndose en contacto con nosotros a través de los datos de contacto que se facilitan a continuación.

Consideraremos y actuaremos ante cualquier solicitud de acuerdo con las leyes de protección de datos aplicables.

Si se encuentra en el EEE o en el Reino Unido y cree que estamos procesando ilegalmente su información personal, también tiene derecho a presentar una reclamación ante la [autoridad de protección de datos del Estado miembro](https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm) o la [autoridad de protección de datos del Reino Unido](https://ico.org.uk/make-a-complaint/data-protection-complaints/data-protection-complaints/).

Si se encuentra en Suiza, puede ponerse en contacto con el [Comisionado Federal de Protección de Datos e Información](https://www.edoeb.admin.ch/edoeb/en/home.html).

**Retirada de su consentimiento:** Si nos basamos en su consentimiento para procesar su información personal, que puede ser un consentimiento expreso y/o implícito en función de la legislación aplicable, tiene derecho a retirar su consentimiento en cualquier momento. Puede retirar su consentimiento en cualquier momento poniéndose en contacto con nosotros a través de los datos de contacto facilitados a continuación o actualizando sus preferencias.

No obstante, tenga en cuenta que esto no afectará a la legalidad del tratamiento anterior a su retirada ni, cuando la legislación aplicable lo permita, afectará al tratamiento de su información personal realizado sobre la base de motivos legales de tratamiento distintos del consentimiento.

**Información de la cuenta**

Si en algún momento desea revisar o modificar la información de su cuenta o cancelarla, puede hacerlo:

* Acceder a la configuración de su cuenta y actualizar su cuenta de usuario.

Si solicita cancelar su cuenta, desactivaremos o eliminaremos su cuenta y su información de nuestras bases de datos activas. Sin embargo, podemos conservar alguna información en nuestros archivos para prevenir el fraude, solucionar problemas, ayudar en cualquier investigación, hacer cumplir nuestros términos legales y/o cumplir con los requisitos legales aplicables.

Si tiene preguntas o comentarios sobre sus derechos de privacidad, puede enviarnos un correo electrónico a support@remem.me.


**8\. CONTROLES DE LAS FUNCIONES DO-NOT-TRACK**.

La mayoría de los navegadores web y algunos sistemas operativos para móviles y aplicaciones para móviles incluyen una función o ajuste Do-Not-Track ("DNT") que puede activar para señalar su preferencia de privacidad para que no se controlen ni recopilen datos sobre sus actividades de navegación en línea. En este momento no se ha ultimado ninguna norma tecnológica uniforme para reconocer y aplicar las señales DNT. Por ello, actualmente no respondemos a las señales DNT del navegador ni a ningún otro mecanismo que comunique automáticamente su elección de no ser rastreado en línea. Si se adopta una norma para el rastreo en línea que debamos seguir en el futuro, le informaremos sobre dicha práctica en una versión revisada de este aviso de privacidad.


**9\. ¿TIENEN LOS RESIDENTES EN CALIFORNIA DERECHOS ESPECÍFICOS EN MATERIA DE PRIVACIDAD?**

**_En breve:_** _Sí, si usted es residente de California, se le otorgan derechos específicos con respecto al acceso a su información personal._

El artículo 1798.83 del Código Civil de California, también conocido como la ley "Shine The Light", permite a nuestros usuarios residentes en California solicitarnos y obtener, una vez al año y de forma gratuita, información sobre las categorías de información personal (si las hubiera) que hayamos revelado a terceros con fines de marketing directo y los nombres y direcciones de todos los terceros con los que hayamos compartido información personal en el año natural inmediatamente anterior. Si reside en California y desea realizar dicha solicitud, envíenosla por escrito utilizando la información de contacto que se indica a continuación.

Si es menor de 18 años, reside en California y tiene una cuenta registrada en los Servicios, tiene derecho a solicitar la eliminación de los datos no deseados que publique en los Servicios. Para solicitar la eliminación de dichos datos, póngase en contacto con nosotros utilizando la información de contacto que se proporciona a continuación e incluya la dirección de correo electrónico asociada a su cuenta y una declaración de que reside en California. Nos aseguraremos de que los datos no se muestren públicamente en los Servicios, pero tenga en cuenta que es posible que los datos no se eliminen de forma completa o exhaustiva de todos nuestros sistemas (por ejemplo, copias de seguridad, etc.).

**Aviso de privacidad de la CCPA**

El Código de Reglamentos de California define "residente" como

(1) todo individuo que se encuentre en el Estado de California con un propósito que no sea temporal o transitorio y

(2) todo individuo domiciliado en el Estado de California que se encuentre fuera del Estado de California con un propósito temporal o transitorio.

Todos los demás individuos se definen como 'no residentes'.

Si esta definición de 'residente' se aplica a usted, debemos respetar ciertos derechos y obligaciones relativos a su información personal.

**¿Qué categorías de información personal recogemos?**

Hemos recopilado las siguientes categorías de información personal en los últimos doce (12) meses:

| Categoría | Ejemplos | Recogida
|----------------|---|---|
| A. Identificadores | Datos de contacto, como nombre real, alias, dirección postal, número de teléfono fijo o móvil de contacto, identificador personal único, identificador en línea, dirección de protocolo de Internet, dirección de correo electrónico y nombre de la cuenta | SÍ
| B. Categorías de información personal enumeradas en el estatuto de registros de clientes de California | Nombre, información de contacto, educación, empleo, historial laboral e información financiera | NO
| C. Características de clasificación protegidas en virtud de la legislación californiana o federal | Sexo y fecha de nacimiento | NO
| D. Información comercial | Información sobre transacciones, historial de compras, detalles financieros e información sobre pagos | NO | E. Información biométrica
| E. Información biométrica | Huellas dactilares y de voz | NO | .
| F. Actividad en Internet u otras redes similares | Historial de navegación, historial de búsqueda, comportamiento en línea, datos sobre intereses e interacciones con nuestros y otros sitios web, aplicaciones, sistemas y anuncios | NO | G. Información de geolocalización
| G. Datos de geolocalización | Ubicación del dispositivo | NO |
| H. Información sonora, electrónica, visual, térmica, olfativa o similar | Imágenes y grabaciones de audio, vídeo o llamadas creadas en relación con nuestras actividades comerciales | NO |
| I. Información profesional o relacionada con el empleo | Datos de contacto de la empresa con el fin de prestarle nuestros Servicios a nivel empresarial o cargo, historial laboral y cualificaciones profesionales si solicita un empleo con nosotros | NO
| J. Información sobre educación | Registros de estudiantes e información del directorio | NO
| K. Inferencias extraídas de otra información personal | Inferencias extraídas de cualquiera de la información personal recopilada enumerada anteriormente para crear un perfil o resumen sobre, por ejemplo, las preferencias y características de un individuo | NO | 
| L. Información personal delicada | | NO |

Utilizaremos y conservaremos la información personal recopilada según sea necesario para prestar los Servicios o para:

* Categoría A - Mientras el usuario tenga una cuenta con nosotros.

También podemos recopilar otra información personal fuera de estas categorías a través de instancias en las que usted interactúa con nosotros en persona, en línea, o por teléfono o correo en el contexto de:

* Recibir ayuda a través de nuestros canales de atención al cliente;

* Participación en encuestas o concursos de clientes; y

* Facilitación en la prestación de nuestros Servicios y para responder a sus consultas.

**¿Cómo utilizamos y compartimos su información personal?**

Puede encontrar más información sobre nuestras prácticas de recopilación y compartición de datos en este aviso de privacidad.

Puede ponerse en contacto con nosotros por correo electrónico en la dirección support@remem.me, o consultando los datos de contacto que figuran en la parte inferior de este documento.

Si utiliza un agente autorizado para ejercer su derecho de exclusión voluntaria, podremos denegar la solicitud si el agente autorizado no presenta pruebas de que ha sido autorizado válidamente para actuar en su nombre.

**¿Se compartirá su información con alguien más?**

Podemos divulgar su información personal con nuestros proveedores de servicios en virtud de un contrato escrito entre nosotros y cada proveedor de servicios. Cada proveedor de servicios es una entidad con ánimo de lucro que procesa la información en nuestro nombre, siguiendo las mismas estrictas obligaciones de protección de la privacidad que exige la CCPA.

Podemos utilizar su información personal para nuestros propios fines comerciales, como por ejemplo para llevar a cabo investigaciones internas para el desarrollo tecnológico y la demostración. Esto no se considera "venta" de su información personal.

Poimena no ha revelado, vendido o compartido ninguna información personal a terceros con fines empresariales o comerciales en los doce (12) meses anteriores. Poimena no venderá ni compartirá en el futuro información personal perteneciente a visitantes del sitio web, usuarios y otros consumidores.

**Sus derechos con respecto a sus datos personales**

Derecho a solicitar la supresión de los datos - Solicitud de supresión

Puede solicitar la supresión de sus datos personales. Si nos solicita la supresión de sus datos personales, respetaremos su solicitud y eliminaremos sus datos personales, con sujeción a determinadas excepciones previstas por la ley, tales como (pero no limitadas a) el ejercicio por parte de otro consumidor de su derecho a la libertad de expresión, nuestros requisitos de cumplimiento derivados de una obligación legal, o cualquier tratamiento que pueda ser necesario para protegernos contra actividades ilegales.

Derecho a ser informado - Solicitud de información

Dependiendo de las circunstancias, usted tiene derecho a saber

* si recopilamos y utilizamos su información personal;

* las categorías de información personal que recopilamos;

* los fines para los que se utiliza la información personal recopilada;

* si vendemos o compartimos información personal con terceros;

* las categorías de información personal que vendemos, compartimos o divulgamos con fines comerciales;

* las categorías de terceros a los que se vendió, compartió o divulgó la información personal con un fin comercial;

* el propósito empresarial o comercial para recopilar, vender o compartir información personal; y

* las piezas específicas de información personal que recopilamos sobre usted.

De acuerdo con la legislación aplicable, no estamos obligados a proporcionar o eliminar información del consumidor que esté desidentificada en respuesta a una solicitud del consumidor o a volver a identificar datos individuales para verificar una solicitud del consumidor.

Derecho a la no discriminación por el ejercicio de los derechos de privacidad del consumidor

No le discriminaremos si ejerce sus derechos de privacidad.

Derecho a limitar el uso y la divulgación de datos personales sensibles

No procesamos la información personal sensible del consumidor.

Proceso de verificación

Al recibir su solicitud, necesitaremos verificar su identidad para determinar que usted es la misma persona sobre la que tenemos la información en nuestro sistema. Estos esfuerzos de verificación requieren que le pidamos que nos proporcione información para que podamos cotejarla con la información que nos ha proporcionado anteriormente. Por ejemplo, dependiendo del tipo de solicitud que presente, podemos pedirle que nos proporcione cierta información para poder cotejar la información que nos facilite con la que ya tenemos en nuestros archivos, o podemos ponernos en contacto con usted a través de un método de comunicación (por ejemplo, teléfono o correo electrónico) que nos haya facilitado previamente. También podemos utilizar otros métodos de verificación según las circunstancias.

Sólo utilizaremos la información personal proporcionada en su solicitud para verificar su identidad o autoridad para realizar la solicitud. En la medida de lo posible, evitaremos solicitarle información adicional con fines de verificación. Sin embargo, si no podemos verificar su identidad a partir de la información que ya conservamos, podremos solicitarle que nos proporcione información adicional con el fin de verificar su identidad y con fines de seguridad o de prevención del fraude. Eliminaremos dicha información adicional tan pronto como terminemos de verificarle.

Otros derechos de privacidad

* Puede oponerse al tratamiento de su información personal.

* Puede solicitar la corrección de sus datos personales si son incorrectos o han dejado de ser pertinentes, o pedir que se restrinja el tratamiento de la información.

* Puede designar a un agente autorizado para que presente una solicitud en virtud de la CCPA en su nombre. Podemos denegar una solicitud de un agente autorizado que no presente pruebas de que ha sido autorizado válidamente para actuar en su nombre de conformidad con la CCPA.

* Puede solicitar la exclusión voluntaria de la futura venta o cesión de su información personal a terceros. Tras recibir una solicitud de exclusión voluntaria, actuaremos en consecuencia tan pronto como sea posible, pero no más tarde de quince (15) días a partir de la fecha de presentación de la solicitud.

Para ejercer estos derechos, puede ponerse en contacto con nosotros por correo electrónico en la dirección support@remem.me, o consultando los datos de contacto que figuran al final de este documento. Si tiene alguna queja sobre la forma en que tratamos sus datos, nos gustaría conocerla.


**10\. ¿TIENEN LOS RESIDENTES EN VIRGINIA DERECHOS ESPECÍFICOS EN MATERIA DE PRIVACIDAD?**

**_En breve:_** _Sí, si usted es residente de Virginia, se le pueden conceder derechos específicos en relación con el acceso y el uso de su información personal._

**Aviso de privacidad de la CDPA de Virginia**

Según la Ley de Protección de Datos de los Consumidores de Virginia (CDPA)

Por "consumidor" se entiende una persona física residente en la Commonwealth que actúa únicamente en un contexto individual o doméstico. No incluye a una persona física que actúe en un contexto comercial o laboral.

'Datos personales' significa cualquier información vinculada o razonablemente vinculable a una persona física identificada o identificable. 'Datos personales' no incluye los datos desidentificados ni la información públicamente disponible.

'Venta de datos personales' significa el intercambio de datos personales a cambio de una contraprestación monetaria.

Si esta definición 'consumidor' se aplica a usted, debemos respetar ciertos derechos y obligaciones relativos a sus datos personales.

La información que recopilemos, utilicemos y divulguemos sobre usted variará en función de cómo interactúe con Poimena y nuestros Servicios.

Sus derechos con respecto a sus datos personales

* Derecho a ser informado de si estamos tratando o no sus datos personales

* Derecho a acceder a sus datos personales

* Derecho a corregir inexactitudes en sus datos personales

* Derecho a solicitar la supresión de sus datos personales

* Derecho a obtener una copia de los datos personales que compartió previamente con nosotros

* Derecho a oponerse al tratamiento de sus datos personales si se utilizan para publicidad dirigida, venta de datos personales o elaboración de perfiles para la toma de decisiones que produzcan efectos jurídicos o de importancia similar ("elaboración de perfiles")

Poimena no ha vendido datos personales a terceros con fines empresariales o comerciales. Poimena no venderá en el futuro datos personales pertenecientes a visitantes del sitio web, usuarios y otros consumidores.

Ejerza los derechos que le otorga la CDPA de Virginia

Puede encontrar más información sobre nuestras prácticas de recopilación e intercambio de datos en este aviso de privacidad.

Puede ponerse en contacto con nosotros por correo electrónico en la dirección support@remem.me o consultando los datos de contacto que figuran al final de este documento.

Si utiliza un agente autorizado para ejercer sus derechos, podremos denegar una solicitud si el agente autorizado no presenta pruebas de que ha sido autorizado válidamente para actuar en su nombre.

Proceso de verificación

Podemos solicitarle que proporcione la información adicional razonablemente necesaria para verificar su solicitud y la de su consumidor. Si presenta la solicitud a través de un agente autorizado, es posible que necesitemos recabar información adicional para verificar su identidad antes de tramitar su solicitud.

Una vez recibida su solicitud, le responderemos sin demora indebida, pero en todos los casos, dentro de los cuarenta y cinco (45) días siguientes a su recepción. El plazo de respuesta podrá ampliarse una vez en cuarenta y cinco (45) días adicionales cuando sea razonablemente necesario. Le informaremos de cualquier prórroga de este tipo dentro del plazo inicial de respuesta de 45 días, junto con el motivo de la prórroga.

Derecho de recurso

Si nos negamos a tomar medidas en relación con su solicitud, le informaremos de nuestra decisión y del razonamiento que la sustenta. Si desea apelar nuestra decisión, envíenos un correo electrónico a support@remem.me. En un plazo de sesenta (60) días a partir de la recepción de la apelación, le informaremos por escrito de cualquier medida tomada o no tomada en respuesta a la apelación, incluyendo una explicación por escrito de los motivos de las decisiones. Si su apelación es denegada, puede ponerse en contacto con el [Procurador General para presentar una queja](https://www.oag.state.va.us/consumer-protection/index.php/file-a-complaint).


**11\. ¿REALIZAMOS ACTUALIZACIONES DE ESTE AVISO?**

**_En breve:_** _Sí, actualizaremos este aviso según sea necesario para seguir cumpliendo con las leyes pertinentes._

Podemos actualizar este aviso de privacidad de vez en cuando. La versión actualizada se indicará mediante una fecha de "Revisado" y la versión actualizada entrará en vigor tan pronto como sea accesible. Si realizamos cambios materiales en este aviso de privacidad, podremos notificárselo mediante la publicación en un lugar destacado de un aviso de dichos cambios o enviándole directamente una notificación. Le recomendamos que revise este aviso de privacidad con frecuencia para estar informado de cómo protegemos su información.


**12\. ¿CÓMO PUEDE PONERSE EN CONTACTO CON NOSOTROS ACERCA DE ESTE AVISO?**.

Si tiene preguntas o comentarios sobre este aviso, puede enviarnos un correo electrónico a support@remem.me o por correo postal a:

Poimena  
Kirchstrasse 1  
CH-8497 Fischenthal ZH  
Suiza

**13\. ¿CÓMO PUEDE REVISAR, ACTUALIZAR O ELIMINAR LOS DATOS QUE RECOPILAMOS DE USTED?**

Tiene derecho a solicitar el acceso a los datos personales que recopilamos sobre usted, a modificarlos o a eliminarlos. Para solicitar la revisión, actualización o eliminación de su información personal, envíenos un correo electrónico a support@remem.me.

{{% /pageinfo %}}




