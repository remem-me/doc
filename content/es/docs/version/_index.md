---
title: "¿Qué hay de nuevo en Remember Me 6?"
linkTitle: "¿Qué hay de nuevo?"
weight: 1
url: "/es/docs/version"
description: >
  Remember Me 6 lleva la memorización de la Biblia a un nuevo nivel.
---

{{% pageinfo %}}
Si busca información sobre las versiones anteriores de Remember Me, visite las [páginas de ayuda de Remember Me 5 y anteriores](https://v5.remem.me).

Si necesita recuperar versículos después de la actualización, consulte [este mensaje del foro de usuarios](https://forum.remem.me/d/120-update-to-remember-me-6-issues/3).
{{% /pageinfo %}}

### Más fácil de usar
Remember Me 6 hace hincapié en la metáfora de las fichas de memoria. Las fichas se mueven entre las cajas de Nuevo, Pendiente y Memorizado. La ficha de un versículo es el punto de partida para estudiar y para revisar. La metáfora de las fichas ayuda a comprender los procesos básicos de forma más intuitiva.

### Más versatilidad
Los usuarios avanzados con cientos de versículos disfrutan de más formas de agrupar y filtrar sus colecciones de versículos mediante etiquetas personalizables. Los usuarios visuales se benefician de añadir imágenes a sus versos. Los usuarios multilingües pueden escuchar la referencia y el pasaje en diferentes idiomas.

### Experiencia unificada
La nueva versión de la aplicación garantiza una experiencia de usuario unificada en todos los dispositivos, porque utiliza el mismo marco de software para todas las plataformas. La aplicación y el marco de software son de código abierto.
