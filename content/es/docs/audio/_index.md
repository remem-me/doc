---
title: "Características de audio"
linkTitle: "Características de audio"
weight: 12
url: "/es/docs/audio"
aliases:
  - /es/docs/características-de-audio
description: >
  Remember Me puede leerle versos o grabar su recitación y reproducirla.
---

> Escuchar repetidamente los versos apoya su proceso de memorización. Grabar su voz mientras repasa un versículo 
> refuerza su conocimiento de si. 

### Escuchar una caja de versículos {#escuchar-caja}
Para empezar a escuchar versículos, seleccione la caja que desea escuchar. Escuchar los versos en su caja "Nuevo" hace 
que se familiarice con ellos. Escuchar los versículos de la caja "Memorizado" le permite refrescar sus conocimientos. 
Pulse el botón <span class="material-icons">play_arrow</span> de reproducción para empezar a escuchar. Con los botones 
de la parte inferior puede pausar, parar o saltarse un versículo. 

Puede hacer que Remember Me repita todos los versículos o un solo versículos, o que vaya más despacio usando el botón 
de configuración (rueda dentada) que está al lado de los botones de control. 

Si desea incluir la referencia después del pasaje o escuchar los versos por temas, active el ajuste correspondiente en 
"Cuenta".

### Comenzar en el versículo actual {#escuchar-versiculo}
Con una ficha de memoria frente a usted, puede iniciar la reproducción haciendo clic en el
botón <span class="material-icons">play_arrow</span> de reproducción en la esquina superior derecha.
Después de volver a la lista de versos, el versículo actual sigue marcado como en reproducción o en pausa.
Ahora, cuando toque el botón de reproducción en la lista, la aplicación comenzará a leer desde esa posición en la lista.

{{% alert title="Configuración de texto a voz" color="secondary" %}}
<span class="material-icons">speed</span> Velocidad del habla  
<span class="material-icons">pause_presentation</span> pausa (en segundos)  
<span class="material-icons">av_timer</span> Temporizador de sueño (en minutos)  
<span class="material-icons">playlist_play</span> *encendido*: lee la lista completa / *apagado*: lee solo el versículo actual  
<span class="material-icons">repeat</span> repetir la lista (o el versículo)
{{% /alert %}}

### Escuchar cuando se estudia un versículo {#escuchar-estudio}
Durante los juegos de estudio (ver [Estudiar](/es/docs/estudiar/#estudiar)), Remember Me lee cada 
palabra o frase que revela. Puede desactivar el habla mediante el menú de configuración (<span class="material-icons">
settings</span> rueda dentada) del juego. 

### Grabar y reproducir su propio recitado {#grabar}
Las fichas de su caja "Pendiente" (ver [Repasar textos](/es/docs/estudiar/#textos) tienen un botón <span class="material-icons">mic</span> de micrófono en la parte inferior. Púlselo para grabar su recitación del texto. Si se vuelve a pulsar el botón o se gira la ficha, se detiene la grabación. Después de revelar el pasaje, se inicia la reproducción y le facilita comprobar si su recitación ha sido correcta. 

### Cambiar la voz {#voz}

{{% alert color="secondary" %}}
Puede cambiar el **idioma** de la voz en la configuración de su cuenta *Idioma de los versículos* e *Idioma de las referencias*.
{{% /alert %}}

Remember Me utiliza el motor de texto a voz de su dispositivo. Puede encontrar instrucciones sobre cómo cambiar la voz en:
* [Ayuda para iPhone/iPad](https://support.apple.com/es-es/guide/iphone/iph96b214f0/ios#iph938159887)  
  Vaya a Configuración > Accesibilidad > Contenido Hablado > Voces: Elija una voz y dialecto, descárguelo y configúrelo como predeterminado.
* [Ayuda para Android](https://support.google.com/accessibility/android/answer/6006983?hl=es)

Para dispositivos Android, hay varias opciones de motores de texto a voz disponibles, como:
* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Speech Services de Google](https://play.google.com/store/apps/details?id=com.google.android.tts)

Al usar la aplicación web en Windows o macOS, puedes cambiar la voz en la configuración de voz de Remember Me. Es posible que primero debas instalar datos de voz en tu dispositivo.
