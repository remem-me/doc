---
title: "Importar y compartir colecciones"
linkTitle: "Colecciones de versículos"
weight: 9
url: "/es/docs/colecciones"
aliases:
  - /es/docs/collections
  - /es/docs/importando-y-compartiendo-colecciones
description: >
  Cree colecciones públicas de versículos y comparta sus propias colecciones con otros. 
---

> Una colección de versículos es una etiqueta pública.

### Encontrar una colección de versículos {#encontrar}
Seleccione "Colecciones" en la caja de navegación izquierda y utilice la función de <span class="material-icons">
search</span> búsqueda para encontrar lo que está buscando. Toque en una ficha de colección para ver más detalles, 
y toque el icono <span class="material-icons">download</span> de descarga en la esquina superior derecha para importar 
los versículos de la colección a su cuenta. Una etiqueta con el nombre de la colección hace que los nuevos versículos 
se distingan de sus otros versículos.

### Publicar y compartir una colección de versículos {#publicar}
Seleccione "Colecciones" en el cajón de navegación izquierdo y pulse el botón "Publicar" (icono 
<span class="material-icons">publish</span> de subida en la esquina superior derecha) para ver una lista de todas las 
etiquetas que se adjuntan a los versículos. Un icono <span class="material-icons">public_off</span> mundial tachado, 
significa que la etiqueta y sus versículos no son visibles para otros usuarios.

Toque una etiqueta y proporcione una descripción de su colección en el formulario que aparecerá en pantalla. Para 
cancelar o anular la publicación de una colección publicada anteriormente, toque el icono del mundo tachado en la parte 
inferior del formulario. Todos los detalles de la publicación se eliminarán y los versículos se ocultarán a otros 
usuarios. Cuando esté listo para publicar, toque el icono <span class="material-icons">public</span> del mundo 
resaltado en la parte inferior del formulario. Todos los detalles de la publicación se guardará y la colección 
aparecerá en los resultados de búsqueda de otros usuarios.

### Incrustar su colección de versos en su sitio web
Como Remember Me 6 también está disponible como aplicación web, puede incrustar una colección o un verso de una 
colección en su página web. Los visitantes de su sitio web podrán utilizar los juegos y las fichas de Remember Me 
directamente en su sitio web. Todo lo que tiene que hacer es añadir un elemento iframe a su código html con la 
dirección de su propia colección de versos en web.remem.me, por ejemplo
```
<iframe 
    src="https://web.remem.me/collections/279179164904161" 
    style="border:none;" 
    width="320" height="480" 
    title="Nuestra colección de versos">
</iframe>
```
lo que da como resultado esto:

{{< app 279179164904161 >}}