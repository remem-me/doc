---
title: "Usuarios y cuentas"
linkTitle: "Usuarios y cuentas"
weight: 4
url: "/es/docs/cuentas"
aliases:
  - /es/docs/accounts
  - /es/docs/usuarios-y-cuentas
description: >
  Regístrate como usuario y crea una cuenta para tus versículos.
---

### Registrar e iniciar sesión de usuario {#registrarse}
En [web.remem.me](https://web.remem.me) puede registrarse de forma gratuita. Después de enviar el formulario de registro, 
recibirá un correo electrónico pidiéndole que confirme su registro.

Después de completar el registro, puede iniciar sesión en la aplicación haciendo clic en el botón **Iniciar sesión** (arriba a la derecha).

{{% alert title="Cambiar su contraseña" color="secondary" %}}
Para cambiar su contraseña, haga clic en "Restablecer contraseña" en el cuadro de diálogo de inicio de sesión.
{{% /alert %}}

### Multiples cuentas de versículos {#multiples}
Una cuenta para sus versículos se crea automáticamente con su registro de usuario. El idioma, el nombre de la cuenta, 
las preferencias de repaso, etc. se almacenan en la configuración de su cuenta. Como usuario, puede agregar cuentas 
adicionales, p. Ej. para sus hijos o para memorizar versículos en un segundo idioma. Para la mayoría de los usuarios, 
una cuenta será suficiente. No puede transferir versículos entre cuentas. Para organizar sus versículos, use 
[etiquetas](/es/docs/etiquetas).

Para agregar una nueva cuenta, haga clic en el <span class="material-icons">arrow_drop_down</span> triángulo en el 
encabezado del menú de la izquierda y seleccione "Crear nueva cuenta" en el menú.

### Importar cuentas de versiones anteriores {#anteriores}
Después de crear una nueva cuenta de usuario (identificada por su dirección de correo electrónico), puede adjuntar 
cuentas de versiones anteriores de Remember Me. Pulse el <span class="material-icons">arrow_drop_down</span> triángulo 
en la cabecera del menú de la izquierda y seleccione "Agregar cuenta existente" en el menú. Introduce el nombre y la 
contraseña de la cuenta heredada y pulsa en "Iniciar sesión". Puede utilizar la cuenta adjunta como cualquier otra 
cuenta de versos de Remember Me 6.

### Sincronización de varios dispositivos {#sincronizacion}
Los dispositivos con capacidad offline muestran un icono <span class="material-icons">refresh</span> de flecha redonda 
junto al nombre de la cuenta actual en la navegación de la izquierda. Si el icono no está visible (p. ej., en un 
navegador web), los versos se almacenan sólo en línea. Los versos y el progreso del estudio se sincronizan 
automáticamente. Toque el icono de la flecha redonda para cargar los datos recién editados en otro dispositivo. Si 
necesita forzar su dispositivo para recuperar y enviar todos los datos, toque prolongadamente sobre el icono. 

{{% alert title="Botón de sincronización" color="secondary" %}}
<span class="material-icons">refresh</span> **una flecha** en forma de círculo: Posibles cambios en otros dispositivos. 
Toque para recuperarlos.  
<span class="material-icons">sync</span> **dos flechas** en forma de círculo: Cambios locales en este dispositivo. 
Toque para enviarlos.  
<span class="material-icons">sync_problem</span> **signo de exclamación** en el círculo: Sincronización fallida. Pulse 
para volver a intentarlo.
{{% /alert %}}

### Editar y eliminar cuentas de versículos {#editar-cuenta}
Para editar o eliminar la cuenta actualmente seleccionada, seleccione "Cuenta" en la navegación principal de la 
izquierda. Toque en el botón superior izquierdo (<span class="material-icons">save</span> símbolo de disco) para 
guardar la cuenta después de editarla.
Tocando el icono <span class="material-icons">delete</span> de la papelera en la esquina superior derecha se borra la 
cuenta. Una cuenta eliminada se almacena en la [papelera](https://web.remem.me/account-bin) durante tres meses y puede 
restaurarse desde allí.

### Editar y eliminar el acceso del usuario {#editar-usuario}
Si desea editar o eliminar su registro de usuario, pase a la navegación de la cuenta (<span class="material-icons">
arrow_drop_down</span> triángulo en la cabecera del menú de la izquierda) y seleccione "Usuario". 
Para editar su dirección de correo electrónico, introduzca la nueva dirección y la contraseña actual y pulse "Guardar".
Para eliminar su acceso de usuario a remem.me por completo, primero hay que eliminar todas las cuentas de versículos. 
A continuación, introduzca la contraseña actual y pulse el icono <span class="material-icons">delete</span> de la 
papelera del diálogo de usuario.  

{{% alert title="Atención" color="warning" %}}
La eliminación de su acceso a remem.me no se puede deshacer. Todos sus datos serán eliminados de todas las bases de datos de forma inmediata y permanente.
{{% /alert %}}

### Modo solo en dispositivo {#solo-en-dispositivo}

Puede utilizar Remember Me sin conectarse al servicio en la nube remem.me. En este modo, todos los datos se almacenan exclusivamente en su dispositivo.

- Los datos permanecen en su dispositivo
- No hay copia de seguridad en la nube disponible
- No es posible publicar colecciones de versos

{{% alert title="Importante" color="warning" %}}
Si pierde el acceso a la aplicación en su dispositivo mientras está en el modo solo en dispositivo, sus versos no podrán ser recuperados.
{{% /alert %}}
Para activar el modo solo en dispositivo, seleccione "Solo en dispositivo" en la parte inferior del diálogo de registro (después de tocar "Empiece ahora").

Para salir del modo solo en dispositivo:

1. Abra el menú
2. Toque el nombre de la cuenta para cambiar al menú de cuentas
3. Seleccione "Cerrar sesión"

Para transferir versos locales a una cuenta en línea, [exporte e importe](docs/labels/#exportar) sus versos como un archivo.

{{% alert title="Nota" color="secondary" %}}
El modo solo en dispositivo ofrece funcionalidad limitada en comparación con las cuentas conectadas a la nube. Considere las ventajas y desventajas entre privacidad y funcionalidad al elegir su modo de operación preferido.
{{% /alert %}}