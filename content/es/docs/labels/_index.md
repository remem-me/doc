---
title: "Agrupar y filtrar por etiquetas"
linkTitle: "Agrupar por etiquetas"
weight: 7
url: "/es/docs/etiquetas"
aliases:
  - /es/docs/labels
  - /es/docs/agrupar-por-etiquetas
description: >
  Seleccionar y deseleccionar etiquetas le permite ocultar, mostrar y filtrar versículos como desee.
---

> Filtrado rápido: Pulse sobra una etiqueta en el menú <span class="material-icons-outlined">visibility</span> de la
> derecha para mostrar exclusivamente sus versos. Presionelo prolongadamente para excluirlo.

### Crear o editar una etiqueta {#crear}

Seleccione "Editar etiquetas" en el menú principal de la izquierda para crear, editar o eliminar una etiqueta.
Las etiquetas eliminadas recientemente pueden restaurarse en línea desde la [papelera](https://web.remem.me/labels/bin).

### Adjuntar etiquetas a los versículos {#adjuntar}

Seleccione uno o varios versículos tocando su insignia (cuadrado redondeado a la izquierda).
Toque el icono <span class="material-icons">label</span> de etiqueta del menú contextual en la parte superior.
Marque las etiquetas que desee adjuntar a los versículos seleccionados, desmarque las etiquetas que desee eliminar de
los versículos seleccionados y toque el botón para guardar los cambios (icono <span class="material-icons">save</span>
de disco).

### Filtrar versículos por etiqueta {#filtrar}

La visibilidad de los versículos agrupados por etiquetas se establece en el cajón de etiquetas a la derecha
(ver arriba). Puede cambiar el estado de las etiquetas / grupos tocando o presionando sobre ellos. Los iconos tienen
los siguientes significados:

<span class="material-icons-outlined">visibility_off</span> **Ojo tachado**: los versículos con esta etiqueta
siempre están ocultos.

<span class="material-icons-outlined">visibility</span> **Ojo perfilado**: los versículos con esta etiqueta son
visibles a menos que algunas etiquetas estén en modo exclusivo.

<span class="material-icons">visibility</span> **Ojo lleno**: los versículos con esta etiqueta son visibles y
todos los demás versículos están ocultos.

Algunos filtros automáticos y configuraciones de filtro están disponibles:

<span class="material-icons-outlined">visibility</span> **Repasado hoy**: Este filtro filtra los versículos repasados
hoy (dependiendo de otros ajustes del filtro).

<span class="material-icons-outlined">apps</span> **Todos los versículos**: al tocar esta opción, se cambian los modos
de las etiquetas para que todos los versículos sean visibles.

<span class="material-icons-outlined">label_off</span> **Versículos sin etiqueta**: al tocar esta opción, se cambian
los modos de las etiquetas para que solo sean visibles los versículos sin etiqueta.

### Todos los versículos en una lista {#todos}

En algunas situaciones, puede ser útil ver todos los versículos en la misma lista, p. ej. para exportar su colección
completa a un archivo (ver más abajo). Puede lograr esto en el cajón <span class="material-icons-outlined">visibility
</span> de visibilidad a la derecha desactivando las cajas <span class="material-icons-outlined">layers</span> y
seleccionando el filtro automático <span class="material-icons">apps</span> "Todos los versículos".

### Exportar/importar versículos como archivo (CSV) {#exportar}

Abra el menú de archivo <span class="material-symbols-outlined">file_open</span> (solo en la aplicación móvil) en la barra de
visibilidad <span class="material-icons-outlined">visibility</span> en el lado derecho y seleccione "Exportar a
archivo". Esto creará un nuevo archivo en formato CSV con los versos actualmente mostrados. Puede compartir el archivo
exportado con otras aplicaciones para guardarlo o enviarlo a alguien más. Los archivos CSV se pueden abrir con programas
de hojas de cálculo.

Si elige la opción "Importar desde archivo" puede cargar versículos desde un archivo almacenado en su dispositivo. 


