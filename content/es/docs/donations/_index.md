---
title: "Donaciones"
linkTitle: "Donaciones"
weight: 25
url: "/es/docs/donaciones"
description: >
  Remember Me es una publicación de Poimena, una organización sin fines de lucro que brinda servicios gratuitos dedicados al crecimiento espiritual.
---
### Fundadores
Poimena fue fundada por Rev. Peter Schaffluetzel y Rev. Regula Studer Schaffluetzel.

### Dirección
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Suiza

### Transferencia bancaria
Utilice la siguiente información para hacer una donación por transferencia bancaria.
Agregue su dirección de correo electrónico al mensaje de transferencia para que podamos agradecerle.
|                     |                    |
|---------------------|--------------------|
| Nombre del banco:   | Raiffeisen, Suiza  |
| Beneficiario:       | Poimena            |
| Código SWIFT (BIC): | RAIFCH22XXX        |
| Cuenta IBAN:        | CH74 8080 8006 2918 4731 8 |

### PayPal/tarjeta de crédito
Si prefiere PayPal o la tarjeta de crédito para realizar una donación, utilice el botón siguiente.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="22E6HVFMD62HY" />
<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Botón Donar con PayPal" />
<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
</form>
