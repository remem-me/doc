---
title: "Gestión de fichas de oración y devociones"
linkTitle: "Oraciones y devociones"
weight: 18
url: "/es/docs/oraciones"
aliases:
  - /es/docs/gestion-de-fichas-de-oracion
description: >
  Las casillas de Remember Me se ayudan a gestionar las fichas de oración y otros textos que quiera leer o escuchar regularmente.
---

### Crear una cuenta de oraciones

Cree una cuenta separada para los textos que desee leer regularmente en lugar de memorizar. Dado que las referencias
probablemente desempeñan un papel menor, puede seleccionar la opción "Aprendizaje por temas" al configurar la cuenta.

### Añadir oraciones/devociones

Puede añadir textos en la casilla "Nuevo" o importar una colección de textos desde "Colecciones". Es útil algún tipo de
numeración como referencia. Por ejemplo, si divide una colección de oraciones largas en fichas de oración individuales -
como los libros de Stormie Omartian - las referencias podrían ser 1a, 1b, 1c, 2a, 2b, etc. Las referencias sirven para
clasificar las fichas. También es útil añadir un tema a cada oración.

### Rezar a diario

Ordene las oraciones por tema o por referencia (alfabéticamente) tocando la pestaña "Nuevo" una segunda vez. Lo mejor es
decir las oraciones en voz alta y en su propio dialecto o palabras. De este modo, incluso las oraciones preconcebidas
pueden convertirse en una conversación personal con Dios.
Anote las impresiones y los pensamientos que le resulten importantes durante la oración debajo del texto (ver [Editar
un versículo](/es/docs/versiculos/#editar)). Así su cuenta de oración se convierte también en su diario de oración y refleja parte de su comunicación con
Dios.
Mueva la oración después de su devoción diaria a la casilla "Pendiente" para llevar un registro de su progreso en la
oración. La casilla "Memorizado" no es necesaria para las oraciones.

### Reutilizar una colección de oraciones

Si quiere volver a empezar con una colección de oraciones después de haberla completado, seleccione una oración (toce su
insignia a la izquierda), luego "seleccionar todo", y mueva toda la colección de nuevo a la casilla "Nuevo".
