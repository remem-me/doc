---
title: "Gestionar versículos de la biblia para memorizar"
linkTitle: "Gestionar versículos"
weight: 5
url: "/es/docs/versiculos"
aliases:
  - /es/docs/verses
  - /es/docs/añadiendo-y-editando-versículos
description: >
  Recuperar versículos bíblicos de biblias en línea o añadir y editar cualquier otro texto para aprender de memoria. Posponer, eliminar o restaurar fichas de aprendizaje de la Biblia.
---

### Agregar un versículo {#agregar}

Seleccione la pestaña "Nuevo" y presione el botón "+" en la parte inferior de la pantalla para abrir el formulario.
Complete la referencia y el pasaje y toque el botón superior izquierdo (<span class="material-icons">save</span> símbolo
del disco) para guardar en el versículo. El versículo ahora aparece en su bandeja de entrada.

### Recuperar un pasaje de una Biblia en línea {#biblia-en-linea}

Al agregar o editar un verso, toque el botón desplegable <span class="material-icons">arrow_drop_down</span>
del campo "Fuente" para abrir la lista de versiones de la Biblia disponibles, o escriba una abreviatura de Biblia
(por ejemplo, RVC) en el campo "Fuente".

Si Remember Me reconoce la referencia (p. ej., 1 Jn 3:16 o 1 Juan 3:1-3) como una referencia a un pasaje de la Biblia
y la fuente (p. ej., NBV) como una versión bíblica en línea, muestra un botón con un símbolo
<span class="material-icons">menu_book</span> de biblia. Un botón desplegable <span class="material-icons-outlined">
format_list_numbered</span> permite recuperar el pasaje con o sin
números de versículo.
Si toca el botón Escritura, Remember Me abre el sitio web de la Biblia en línea y le ofrece pegar
<span class="material-icons">paste</span> el pasaje en la aplicación. Puede agregar saltos de línea adicionales u otras
modificaciones y guardar el versículo (ver arriba).

### Copiar un versículo de una aplicación bíblica {#aplicacion-biblica}

La mayoría de las aplicaciones móviles de la Biblia le permiten compartir versículos con otras aplicaciones. Resalte el
versículo en la aplicación de la Biblia, seleccione "Compartir" y seleccione Remember Me de la lista de apps. Remember
Me
abre el editor de versículos y rellena la referencia, el texto y la fuente (si la proporciona la aplicación Biblia).
Pulse el botón Guardar para añadir el versículo a su colección de nuevos versículos bíblicos.

### Editar un versículo {#editar}

Si toca un versículo que aparece en una de las tres cajas (Nuevo, Pendiente, Memorizado), se muestra como una ficha
flash. Puede editarlo tocando el símbolo del lápiz en la barra superior.

Puede dividir un pasaje en múltiples secciones de estudio agregando saltos de línea.

#### Opciones de formato

- \*cursiva\*
- \*\*negrita\*\*
- \>cita en bloque

#### Números de versículo

Los números de versículo se colocan entre corchetes, por ejemplo, \[1\], \[2\], \[3\].
Si la configuración de la cuenta "Aprender con referencia" está activada, los números de versículo se incluirán en la
lectura en voz alta y en los juegos de aprendizaje.

### Adjuntar una imagen {#imagen}

Al editar un verso (véase más arriba), puede adjuntar una imagen que se utilizará como fondo de la ficha de memoria
para el verso. Básicamente, puede introducir la dirección de Internet (URL) de cualquier imagen disponible en línea en
el campo "URL de imagen" en la parte inferior de la pantalla. Para mayor comodidad, hay un botón
<span class="material-icons">image</span> de imagen a la derecha para buscar imágenes en Unsplash.com. Toque el nombre
para saber más sobre el fotógrafo y la imagen para insertar la dirección de la imagen en el campo del formulario. Sólo
las fotos de Unsplash.com se incluyen en las colecciones de versos públicos.

Toque el botón superior izquierdo (<span class="material-icons">save</span> símbolo de disco) para guardar el verso con
la imagen adjunta.

### Mover, posponer o eliminar versos {#mover}

Seleccione los versos tocando su insignia (cuadrado redondeado a la izquierda), y abra el menú de la derecha (tres
puntos). Éste ofrece las opciones de mover
los versos seleccionados a una caja diferente, posponer su fecha pendiente o eliminarlos.

### Restaurar versos eliminados {#restaurar}

Los versos eliminados recientemente pueden restaurarse en línea desde la [papelera](https://web.remem.me/bin). 