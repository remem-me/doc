---
title: "¡Gracias por su donación!"
linkTitle: "Gracias"
type: docs
toc_hide: true
hide_summary: true
description: >
  Muchas gracias por apoyar el desarrollo y la difusión de Remember Me. Su donación es una bendición y un estímulo para nosotros. ¡Que Dios le bendiga!
---
