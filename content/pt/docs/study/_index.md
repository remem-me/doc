---
title: "Estudar e revisar"
linkTitle: "Estudar e revisar"
weight: 6
url: "/pt/docs/estudar"
aliases:
  - /pt/docs/estudo
description: >
  Jogos e um sistema de revisão inteligente ajudam na memorização e retenção.
---
### O processo de aprendizado {#processo}
Os versículos para memorização movem-se entre as caixas "Novo", "Pendente" e "Conhecido".
Você pode alterar a ordem de uma caixa tocando na aba ativa <img src="/images/triangle.svg">.

|Caixas:|<span class="material-icons">inbox</span><br>Novo|&rarr;| <span class="material-icons">check_box_outline_blank</span><br>Pendente |&rarr;| <span class="material-icons">check_box</span><br>Conhecido|
|:-|:-:|:-:|:------------------------------------------------------------------------:|:-:|:-:|
|Ações:|1. **Estudar**|2. Comprometer-se|                       3. **Revisar**<br>**textos**                       |4. Reproduzir|5. **Revisar**<br>**referências**
|Botões:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|                  <img src="/images/cards_outline.svg">                   |<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">

1. **Estudar** novos versículos.
2. Comprometer-se com um versículo o move para a caixa "Pendente".
3. **Revisar textos** dos versículos.
4. Reproduzir corretamente um versículo o move para a caixa "Conhecido".
5. **Revisar referências** de versículos selecionados aleatoriamente.

### Estudar {#estudar}
Depois de adicionar um versículo ao sistema, ele pode ser estudado usando diferentes tipos de jogos.
Para começar a estudar, toque no versículo e no ícone <span class="material-icons">school</span> de estudo.

<span class="material-icons">wb_cloudy</span> **Ofuscar**: Oculte algumas palavras e tente recitar o versículo
completando as palavras que faltam da memória. Toque em uma palavra confusa para revelá-la.

<span class="material-icons">extension</span> **Quebra-cabeça**: construa o versículo tocando na palavra correta.

<span class="material-icons">subject</span> **Alinhar**: mostra linhas vazias ou as primeiras letras tocando no ícone da
esquerda (na barra inferior). Avance tocando em <span class="material-icons">plus_one</span> (+palavra) ou
<span class="material-icons">playlist_add</span> (+linha). Tente recitar a palavra ou linha antes de revelá-la.

<span class="material-icons">keyboard</span> **Escrita**: Construa o versículo escrevendo a primeira letra de cada
palavra.

Depois de memorizar o versículo, deslize o cartão para a direita para movê-lo para a lista "Pendente". A partir de
agora, o versículo participa do processo de revisão.

### Revisar textos {#textos}
Selecione a caixa "**Pendente**". Se quiser alterar a ordem dos versículos, toque novamente em "Pendente".
Existem 3 formas de começar uma revisão:

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> cartões delineados" >}}
Inicia uma revisão de vários cartões.
{{< /card >}}
{{< card header="Ao tocar em um versículo" >}}
Inicia uma revisão de um único cartão.
{{< /card >}}
{{< card header="Ao pressionar em um versículo" >}}
Abre o cartão na lateral do texto.
{{< /card >}}
{{< /cardpane >}}

Quando a referência e o conteúdo forem exibidos, recite o versículo de memória e vire o cartão para verificar
se estava correto. Se preferir recitar o versículo linha por linha (para editar as quebras de linha,
consulte [Adicionar e editar versículos](/pt/docs/versiculos)), toque no ícone <span class="material-icons">rule</span> no
canto inferior direito de cada linha.

Se precisar de um pouco de ajuda para começar, mantenha pressionado o cartão para obter uma dica.
(Você pode pular um cartão deslizando-o para baixo.)

Se você lembrou o versículo corretamente, deslize-o para a direita para movê-lo para a caixa "Conhecido". Isso aumentará
o nível do versículo em 1 (até atingir o limite de revisão na configuração da conta). Se não estiver completamente
correto, deslize-o para a esquerda. Isso redefinirá o nível do versículo para 0. Isso pode parecer um pouco
surpreendente, mas é muito importante para a frequência de revisão do versículo. Versículos com níveis baixos
aparecem para revisão com mais frequência. Isso ajuda a reforçar a memória de um versículo antes que seja esquecido.
A repetição espaçada garante que você revise textos desafiadores com frequência, mas também não esqueça textos familiares.

{{% alert title="Repetição espaçada (Spaced Repetition)" color="secondary" %}}
A memorização é mais eficaz quando o cérebro lembra o que foi aprendido em intervalos cada vez maiores.
O Remember Me utiliza um algoritmo exponencial para calcular os intervalos (semelhante ao Pimsleur). Se a frequência de
revisão da sua conta estiver configurada como "Normal", você revisará seus versículos após 1, 2, 4, 8, 16... dias. O
outro componente do algoritmo é a redefinição do nível de um versículo para 0 após uma revisão incorreta (semelhante ao Leitner).
Dessa forma, é garantido que textos difíceis sejam revisados com mais frequência do que os fáceis.  
{{% /alert %}}

### Revisar referências {#referencias}
Se você quiser revisar apenas as referências dos textos revisados hoje, toque na etiqueta
"[Revisado hoje](/pt/docs/etiquetas/#filtrar)" antes de começar. Selecione a
caixa "**Conhecido**", defina a ordem como "aleatória" e toque no botão com o ícone <img src="/images/cards_filled.svg">
de cartões cheios na parte inferior.

O Remember Me embaralha os cartões e apresenta seus textos. Tente recitar a referência de memória e vire o cartão para verificar
se está correto. Se estiver, deslize o cartão para a direita. Se não estiver, deslize-o para a esquerda. O aplicativo apresentará
o cartão novamente durante a sessão de revisão até que você o lembre.

Você pode pular um cartão deslizando-o para baixo. Ao contrário da revisão de textos, o aplicativo não acompanha seu progresso
de revisão. Este modo de revisão se assemelha mais a um questionário que você deve fazer ocasionalmente para atualizar seu conhecimento das referências.

{{% alert title="Configuração da conta" color="secondary" %}}
- **Objectivo diário**: Número de palavras a serem revistas por dia.
- **Número de flashcards de revisão**: Número máximo de cartões por sessão de revisão de passagem.
- **Número de flashcards inversos**: Número máximo de cartões por revisão de referências.
- **Estudar referência**: Incluir referência no estudo e revisão de versículos.
- **Frequência da revisão**: Com que frequência os versículos serão apresentados para revisão.
- **Limite de intervalo**: Número máximo de dias até a próxima revisão. Isso limita indiretamente o número de níveis que podem ser alcançados.
  {{% /alert %}}
