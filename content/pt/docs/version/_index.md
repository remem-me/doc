---
title: "O que há de novo no Remember Me 6?"
linkTitle: "O que há de novo?"
weight: 1
url: "/pt/docs/versao"
description: >
  O Remember Me 6 leva a memorização da Bíblia a um novo nível.
---

{{% pageinfo %}}
Se você está procurando informações sobre versões anteriores do Remember Me, visite as [páginas de ajuda do Remember Me 5 e anteriores](https://v5.remem.me).

Se precisar recuperar versículos após a atualização, consulte [este post do fórum de usuários](https://forum.remem.me/d/120-update-to-remember-me-6-issues/3).
{{% /pageinfo %}}

### Mais fácil de usar
O Remember Me 6 enfatiza a metáfora das fichas de memória. As fichas movem-se entre as caixas de Novo, Pendente e Memorizado. A ficha de um versículo é o ponto de partida para estudar e revisar. A metáfora das fichas ajuda a compreender os processos básicos de forma mais intuitiva.

### Mais versatilidade
Usuários avançados com centenas de versículos desfrutam de mais formas de agrupar e filtrar suas coleções de versículos através de etiquetas personalizáveis. Usuários visuais se beneficiam ao adicionar imagens aos seus versos. Usuários multilíngues podem ouvir a referência e o trecho em diferentes idiomas.

### Experiência unificada
A nova versão do aplicativo garante uma experiência de usuário unificada em todos os dispositivos, pois utiliza o mesmo framework de software para todas as plataformas. O aplicativo e o framework de software são de código aberto.
