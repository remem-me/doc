---
title: "Gerenciar versículos da Bíblia para memorização"
linkTitle: "Gerenciar versículos"
weight: 5
url: "/pt/docs/versiculos"
aliases:
  - /pt/docs/verses
description: >
  Recuperar versículos bíblicos de bíblias online ou adicionar e editar qualquer outro texto para memorização. Adiar, excluir ou restaurar cartões de aprendizado da Bíblia.
---

### Adicionar um versículo {#adicionar}
Selecione a aba "Novo" e pressione o botão "+" na parte inferior da tela para abrir o formulário.
Preencha a referência e o trecho e toque no botão superior esquerdo (<span class="material-icons">save</span> símbolo de
disquete) para salvar o versículo. O versículo agora aparece em sua caixa de entrada.

### Recuperar um trecho de uma Bíblia online {#biblia-online}
Ao adicionar ou editar um verso, toque no botão suspenso <span class="material-icons">arrow_drop_down</span>
do campo "Fonte" para abrir a lista de versões da Bíblia disponíveis, ou digite uma abreviação de Bíblia
(por exemplo, ARC) no campo "Fonte".

Se o Remember Me reconhecer a referência (por exemplo, 1 Jo 3:16 ou 1 João 3:1-3) como uma referência a um trecho da Bíblia
e a fonte (por exemplo, NVT) como uma versão bíblica online, ele exibirá um botão com um símbolo
<span class="material-icons">menu_book</span> de Bíblia. Um botão suspenso <span class="material-icons-outlined">format_list_numbered</span> permite recuperar o trecho com ou sem
números de versículo.
Se você tocar no botão Escritura, o Remember Me abrirá o site da Bíblia online e oferecerá a opção de colar
<span class="material-icons">paste</span> o trecho na aplicação. Você pode adicionar quebras de linha adicionais ou outras
modificações e salvar o versículo (veja acima).

### Copiar um versículo de um aplicativo bíblico {#aplicativo-biblia}
A maioria dos aplicativos móveis da Bíblia permite compartilhar versículos com outros aplicativos. Destaque o
versículo no aplicativo da Bíblia, selecione "Compartilhar" e escolha Remember Me na lista de apps. O Remember Me
abrirá o editor de versículos e preencherá a referência, o texto e a fonte (se fornecida pelo aplicativo da Bíblia).
Pressione o botão Salvar para adicionar o versículo à sua coleção de novos versículos bíblicos.

### Editar um versículo {#editar}
Se você tocar em um versículo que aparece em uma das três caixas (Novo, Pendente, Conhecido), ele será exibido como um cartão
de memorização. Você pode editá-lo tocando no ícone de lápis na barra superior.

Você pode dividir uma passagem em múltiplas seções de estudo adicionando quebras de linha.

#### Opções de formatação
- \*itálico\*
- \*\*negrito\*\*
- \>citação em bloco

#### Números de versículos
Os números dos versículos são colocados entre colchetes, por exemplo, \[1\], \[2\], \[3\].
Se a configuração da conta "Estudar referência" estiver ativada, os números dos versículos serão incluídos na leitura em voz alta e nos jogos de memorização.

### Anexar uma imagem {#imagem}
Ao editar um verso (veja acima), você pode anexar uma imagem que será usada como fundo para o cartão de memorização
do verso. Basicamente, você pode inserir o endereço da internet (URL) de qualquer imagem disponível online no
campo "URL da imagem" na parte inferior da tela. Para maior comodidade, há um botão
<span class="material-icons">image</span> de imagem à direita para procurar imagens no Unsplash.com. Toque no nome
para saber mais sobre o fotógrafo e a imagem para inserir o endereço da imagem no campo do formulário. Somente
as fotos do Unsplash.com são incluídas nas coleções de versículos públicos.

Toque no botão superior esquerdo (<span class="material-icons">save</span> símbolo de disquete) para salvar o verso com
a imagem anexada.

### Mover, adiar ou excluir versículos {#mover}
Selecione os versículos tocando em sua insígnia (quadrado arredondado à esquerda) e abra o menu à direita (três pontos). Esse menu oferece opções para mover
os versículos selecionados para uma caixa diferente, adiar sua data pendente ou excluí-los.

### Restaurar versículos excluídos
Versículos excluídos recentemente podem ser restaurados online a partir da [lixeira](https://web.remem.me/bin). 
