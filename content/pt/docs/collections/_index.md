---
title: "Importar e compartilhar coleções"
linkTitle: "Coleções de versículos"
weight: 9
url: "/pt/docs/colecoes"
aliases:
- /pt/docs/collections
description: >
  Crie coleções públicas de versículos e compartilhe suas próprias coleções com outros.
---

> Uma coleção de versículos é uma etiqueta pública.

### Encontre uma coleção de versículos {#encontrar}
Selecione "Coleções" no menu de navegação à esquerda e use a função de pesquisa <span class="material-icons">
search</span> para encontrar o que está procurando. Toque em uma aba de coleção para ver mais detalhes
e toque no ícone <span class="material-icons">download</span> de download no canto superior direito para importar
os versículos da coleção para a sua conta. Uma etiqueta com o nome da coleção fará com que os novos versículos
se destaquem dos seus outros versículos.

### Publique e compartilhe uma coleção de versículos {#publicar}
Selecione "Coleções" no menu de navegação à esquerda e clique no botão "Publicar" (ícone
<span class="material-icons">publish</span> no canto superior direito) para ver uma lista de todas as
etiquetas que estão anexadas aos versículos. Um ícone <span class="material-icons">public_off</span> mundial com um traço,
significa que a etiqueta e seus versículos não são visíveis para outros usuários.

Toque em uma etiqueta e forneça uma descrição da sua coleção no formulário que aparecerá na tela. Para
cancelar ou despublicar uma coleção que tenha sido publicada anteriormente, toque no ícone do mundo com um traço na parte
inferior do formulário. Todos os detalhes da publicação serão removidos e os versículos serão ocultos para outros
usuários. Quando estiver pronto para publicar, toque no ícone <span class="material-icons">public</span> do mundo
destacado na parte inferior do formulário. Todos os detalhes da publicação serão salvos e a coleção
aparecerá nos resultados de pesquisa de outros usuários.

### Incorpore a sua coleção de versículos no seu site
Como o Remember Me 6 também está disponível como aplicação web, você pode incorporar uma coleção ou um verso de uma
coleção no seu site. Os visitantes do seu site poderão usar os jogos e as abas do Remember Me
diretamente no seu site. Tudo o que precisa fazer é adicionar um elemento iframe ao seu código html com o
endereço da sua própria coleção de versículos em web.remem.me, por exemplo

```
<iframe
src="https://web.remem.me/collections/78112261395199"
style="border:none;"
width="320" height="480"
title="Nossa coleção de versículos">
</iframe>
```
resultando em:

{{< app 78112261395199 >}}