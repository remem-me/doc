---
title: "Características de áudio"
linkTitle: "Características de áudio"
weight: 12
url: "/pt/docs/audio"
aliases:
  - /pt/docs/caracteristicas-de-audio
description: >
  Remember Me pode ler versos em voz alta ou gravar sua recitação e reproduzi-la.
---

> Ouvir versos repetidamente apoia o processo de memorização. Gravar sua voz enquanto revisa um verso reforça seu conhecimento sobre ele.

### Ouvir uma caixa de versículos {#escutar-caixa}
Para começar a ouvir versículos, selecione a caixa que deseja ouvir. Ouvir os versos na caixa "Novo" ajuda a se familiarizar com eles. Ouvir os versículos na caixa "Memorizado" permite refrescar seu conhecimento. Clique no botão <span class="material-icons">play_arrow</span> de reprodução para começar a ouvir. Com os botões na parte inferior, você pode pausar, parar ou pular um verso.

Você pode fazer com que o Remember Me repita todos os versículos ou apenas um verso, ou que vá mais devagar, usando o botão de configurações (roda dentada) que está ao lado dos botões de controle.

Se deseja incluir a referência após o trecho ou ouvir os versos por tópicos, ative a configuração correspondente em "Conta".

### Começar no verso atual {#escutar-verso}
Com um cartão de memória à sua frente, você pode iniciar a reprodução clicando no botão <span class="material-icons">play_arrow</span> de reprodução no canto superior direito. Depois de voltar à lista de versículos, o verso atual continuará marcado como em reprodução ou pausa. Agora, quando você tocar no botão de reprodução na lista, o aplicativo começará a ler a partir dessa posição na lista.

{{% alert title="Configuração de texto para fala" color="secondary" %}}
<span class="material-icons">speed</span> Velocidade de fala  
<span class="material-icons">pause_presentation</span> pausa (em segundos)  
<span class="material-icons">av_timer</span> Temporizador de sono (em minutos)  
<span class="material-icons">playlist_play</span> *ligado*: lê a lista completa / *desligado*: lê apenas o verso atual  
<span class="material-icons">repeat</span> repetir a lista (ou o verso)
{{% /alert %}}

### Ouvir enquanto estuda um verso {#escutar-estudar}
Durante os jogos de estudo (ver [Estudar](/pt/docs/estudar/#estudar)), o Remember Me lê cada palavra ou frase revelada. Você pode desativar a fala no menu de configuração (<span class="material-icons">settings</span> roda dentada) do jogo.

### Gravar e reproduzir sua própria recitação {#gravar}
Os cartões da sua caixa "Pendente" (ver [Revisar textos](/pt/docs/estudar/#textos)) têm um botão <span class="material-icons">mic</span> de microfone na parte inferior. Clique nele para gravar sua recitação do texto. Se você pressionar o botão novamente ou girar o cartão, a gravação será interrompida. Após revelar o trecho, a reprodução será iniciada, permitindo que você verifique se a sua recitação foi correta.

### Alterar a Voz

{{% alert color="secondary" %}}
Você pode alterar o **idioma** da voz nas configurações da sua conta *Idioma dos versículos* e *Idioma das referências*.
{{% /alert %}}

O Remember Me utiliza o mecanismo de texto para fala do seu dispositivo. Você pode encontrar instruções sobre como alterar a voz em:
* [Ajuda para iPhone/iPad](https://support.apple.com/pt-br/guide/iphone/iph96b214f0/ios#iph938159887)  
  Acesse Configurações > Acessibilidade > Conteúdo Falado > Vozes: Escolha uma voz e dialeto, faça o download e defina como padrão.
* [Ajuda para Android](https://support.google.com/accessibility/android/answer/6006983?hl=pt)

Para dispositivos Android, existem várias opções de mecanismos de texto para fala disponíveis, como:
* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Serviços de Fala do Google](https://play.google.com/store/apps/details?id=com.google.android.tts)

Ao usar o aplicativo web no Windows ou macOS, você pode alterar a voz nas configurações de fala do Remember Me. Pode ser necessário instalar dados de voz em seu dispositivo primeiro.

