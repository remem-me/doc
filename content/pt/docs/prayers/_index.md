---
title: "Gestão de fichas de oração e devoções"
linkTitle: "Orações e devoções"
weight: 18
url: "/pt/docs/oracoes"
aliases:
  - /pt/docs/prayers
  - /pt/docs/gestion-de-fichas-de-oracion
description: >
  As caixas Remember Me ajudam a gerenciar fichas de oração e outros textos que você deseja ler ou ouvir regularmente.
---

### Criar uma conta de orações
Crie uma conta separada para os textos que deseja ler regularmente em vez de memorizar. Como as referências provavelmente desempenham um papel menor, você pode selecionar a opção "Aprendizado por temas" ao configurar a conta.

### Adicionar orações/devoções
Você pode adicionar textos na caixa "Novo" ou importar uma coleção de textos de "Coleções". É útil algum tipo de numeração como referência. Por exemplo, se dividir uma coleção de orações longas em fichas de oração individuais - como os livros de Stormie Omartian - as referências poderiam ser 1a, 1b, 1c, 2a, 2b, etc. As referências ajudam a classificar as fichas. Também é útil adicionar um tema a cada oração.

### Orar diariamente
Ordene as orações por tema ou por referência (em ordem alfabética) tocando na guia "Novo" uma segunda vez. É melhor dizer as orações em voz alta e em seu próprio dialeto ou palavras. Dessa forma, até mesmo as orações predefinidas podem se tornar uma conversa pessoal com Deus.
Anote as impressões e pensamentos que considerar importantes durante a oração abaixo do texto (veja [Editar verso](/pt/docs/versiculos/#editar)). Assim, sua conta de oração também se torna seu diário de oração e reflete parte de sua comunicação com Deus.
Mova a oração após sua devoção diária para a caixa "Pendente" para acompanhar seu progresso na oração. A caixa "Memorizado" não é necessária para as orações.

### Reutilizar uma coleção de orações
Se quiser começar de novo com uma coleção de orações após tê-la completado, selecione uma oração (toque em sua insígnia à esquerda), depois "selecionar tudo" e mova toda a coleção de volta para a caixa "Novo".