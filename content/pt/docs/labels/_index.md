---
title: "Agrupar e filtrar por etiquetas"
linkTitle: "Agrupar por etiquetas"
weight: 7
url: "/pt/docs/etiquetas"
aliases:
  - /pt/docs/labels
description: >
  Selecionar e deselecionar etiquetas permite ocultar, mostrar e filtrar versículos conforme desejado.
---

> Filtragem rápida: Toque em uma etiqueta no menu <span class="material-icons-outlined">visibility</span> à direita
> para exibir exclusivamente seus versículos. Pressione-a por mais tempo para excluí-la.

### Criar ou editar uma etiqueta {#criar}

Selecione "Editar etiquetas" no menu principal à esquerda para criar, editar ou eliminar uma etiqueta.
As etiquetas recentemente eliminadas podem ser restauradas online a partir
da [lixeira](https://web.remem.me/labels/bin).

### Associar etiquetas aos versículos {#associar}

Selecione um ou vários versículos tocando em sua insígnia (quadrado arredondado à esquerda).
Toque no ícone de etiqueta <span class="material-icons">label</span> no menu contextual superior.
Marque as etiquetas que deseja associar aos versículos selecionados, desmarque as etiquetas que deseja remover
dos versículos selecionados e toque no botão para salvar as alterações (ícone <span class="material-icons">save</span>
de disco).

### Filtrar versículos por etiqueta {#filtrar}

A visibilidade dos versículos agrupados por etiquetas é definida no painel de etiquetas à direita
(veja acima). Você pode alterar o estado das etiquetas / grupos tocando ou pressionando sobre eles. Os ícones têm
os seguintes significados:

<span class="material-icons-outlined">visibility_off</span> **Olho marcado com X**: os versículos com esta etiqueta
sempre estão ocultos.

<span class="material-icons-outlined">visibility</span> **Olho contornado**: os versículos com esta etiqueta são
visíveis, a menos que algumas etiquetas estejam em modo exclusivo.

<span class="material-icons">visibility</span> **Olho preenchido**: os versículos com esta etiqueta são visíveis e
todos os outros versículos estão ocultos.

Alguns filtros automáticos e configurações de filtro estão disponíveis:

<span class="material-icons-outlined">visibility</span> **Revisados hoje**: Este filtro filtra os versículos revisados
hoje (dependendo de outras configurações de filtro).

<span class="material-icons-outlined">apps</span> **Todos os versículos**: tocando nesta opção, os modos das etiquetas
são alterados para que todos os versículos sejam visíveis.

<span class="material-icons-outlined">label_off</span> **Versículos sem etiqueta**: tocando nesta opção, os modos das
etiquetas são alterados para que apenas os versículos sem etiqueta sejam visíveis.

### Todos os versículos em uma lista {#todos}

Em algumas situações, pode ser útil ver todos os versículos na mesma lista, por exemplo, para exportar sua coleção
completa para um arquivo (veja abaixo). Você pode conseguir isso no painel <span class="material-icons-outlined">
visibility
</span> de visibilidade à direita desativando as caixas <span class="material-icons-outlined">layers</span> e
selecionando o filtro automático <span class="material-icons">apps</span> "Todos os versículos".

### Exportar/importar versículos como arquivo (CSV) {#exportar}

Abra o menu de arquivo <span class="material-symbols-outlined">file_open</span> (apenas no aplicativo móvel) na barra de
visibilidade <span class="material-icons-outlined">visibility</span> do lado direito e selecione "Exportar para
arquivo". Isso criará um novo arquivo no formato CSV com os versos atualmente exibidos. Você pode compartilhar o arquivo
exportado com outros aplicativos para salvá-lo ou enviá-lo a alguém. Arquivos CSV podem ser abertos com programas de
planilha.

Se escolher a opção "Importar de arquivo", você pode carregar versículos de um arquivo armazenado em seu dispositivo.
