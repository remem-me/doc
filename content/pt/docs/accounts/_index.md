---
title: "Usuários e contas"
linkTitle: "Usuários e contas"
weight: 4
url: "/pt/docs/contas"
aliases:
  - /pt/docs/accounts
description: >
  Registre-se como usuário e crie uma conta para seus versículos.
---

### Registar e iniciar sessão num utilizador {#registrar-se}
No [web.remem.me](https://web.remem.me), você pode se registrar gratuitamente. Após enviar o formulário de registro,
você receberá um e-mail pedindo para confirmar o seu registro.

Depois de completar o registro, você pode fazer o login na aplicação clicando no botão **Entrar** (canto superior direito).

{{% alert title="Alterando sua senha" color="secondary" %}}
Para alterar sua senha, clique em "Redefinir senha" na caixa de diálogo de login.
{{% /alert %}}

### Múltiplas contas de versículos {#multiplos}
Uma conta para os seus versículos é automaticamente criada junto com o seu registro de usuário. O idioma, o nome da
conta, as preferências de revisão, etc., são armazenados nas configurações da sua conta. Como usuário, você pode
adicionar contas adicionais, por exemplo, para seus filhos ou para memorizar versículos em um segundo idioma. Para a
maioria dos usuários, uma conta será suficiente. Você não pode transferir versículos entre contas. Para organizar
seus versículos, utilize [etiquetas](/pt/docs/etiquetas).

Para adicionar uma nova conta, clique na <span class="material-icons">arrow_drop_down</span> seta no cabeçalho do
menu à esquerda e selecione "Criar nova conta" no menu.

### Importar contas de versões anteriores {#anteriores}
Depois de criar uma nova conta de usuário (identificada pelo seu endereço de e-mail), você pode anexar contas de
versões anteriores do Remember Me. Clique na <span class="material-icons">arrow_drop_down</span> seta no cabeçalho
do menu à esquerda e selecione "Adicionar conta existente" no menu. Insira o nome e a senha da conta herdados e
clique em "Entrar". Você pode usar a conta anexada como qualquer outra conta de versículos do Remember Me 6.

### Sincronização em vários dispositivos {#sincronizacao}
Dispositivos com capacidade offline exibem um ícone <span class="material-icons">refresh</span> de seta circular ao
lado do nome da conta atual na navegação à esquerda. Se o ícone não estiver visível (por exemplo, em um navegador
web), os versículos são armazenados apenas online. Os versículos e o progresso do estudo são sincronizados
automaticamente. Toque no ícone da seta circular para carregar os dados recentemente editados em outro dispositivo.
Se você precisar forçar o seu dispositivo a recuperar e enviar todos os dados, toque e segure o ícone.

{{% alert title="Botão de sincronização" color="secondary" %}}
<span class="material-icons">refresh</span> **uma seta** em forma de círculo: Possíveis mudanças em outros dispositivos.
Toque para recuperá-las.
<span class="material-icons">sync</span> **duas setas** em forma de círculo: Mudanças locais neste dispositivo.
Toque para enviá-las.
<span class="material-icons">sync_problem</span> **sinal de exclamação** no círculo: Sincronização falhou. Toque
para tentar novamente.
{{% /alert %}}

### Editar e excluir contas de versículos {#editar-conta}
Para editar ou excluir a conta atualmente selecionada, selecione "Conta" na navegação principal à esquerda. Toque no
ícone superior esquerdo (<span class="material-icons">save</span> ícone de disco) para salvar a conta após editá-la.
Ao tocar no ícone <span class="material-icons">delete</span> da lixeira no canto superior direito, a conta será
apagada. Uma conta excluída é armazenada na [lixeira](https://web.remem.me/account-bin) por três meses e pode ser
restaurada a partir dela.

### Editar e excluir o acesso do usuário {#editar-usuario}
Se você quiser editar ou excluir seu registro de usuário, vá para a navegação da conta (<span class="material-icons">
arrow_drop_down</span> seta no cabeçalho do menu à esquerda) e selecione "Usuário".
Para editar seu endereço de e-mail, insira o novo endereço e a senha atual, e clique em "Salvar".
Para excluir o seu acesso de usuário ao remem.me completamente, você deve primeiro excluir todas as contas de
versículos. Em seguida, insira a senha atual e toque no ícone <span class="material-icons">delete</span> na caixa de
diálogo do usuário.

{{% alert title="Atenção" color="warning" %}}
A exclusão do seu acesso ao remem.me é irreversível. Todos os seus dados serão apagados de todos os bancos de dados
imediatamente e permanentemente.
{{% /alert %}}

### Modo apenas no dispositivo {#apenas-no-dispositivo}

Você pode usar o Remember Me sem se conectar ao serviço de nuvem remem.me. Neste modo, todos os dados são armazenados exclusivamente no seu dispositivo.

- Os dados permanecem no seu dispositivo
- Não há backup na nuvem disponível
- Não é possível publicar coleções de versículos

{{% alert title="Importante" color="warning" %}}
Se você perder o acesso ao aplicativo no seu dispositivo enquanto estiver no modo apenas no dispositivo, seus versículos não poderão ser recuperados.
{{% /alert %}}
Para ativar o modo apenas no dispositivo, selecione "Apenas no dispositivo" na parte inferior da caixa de diálogo de registro (após tocar em "Comece agora").

Para sair do modo apenas no dispositivo:

1. Abra o menu
2. Toque no nome da conta para mudar para o menu da conta
3. Selecione "Sair"

Para transferir versículos locais para uma conta online, [exporte e importe](docs/labels/#exportar) seus versículos como um arquivo.

{{% alert title="Nota" color="secondary" %}}
O modo apenas no dispositivo oferece funcionalidade limitada em comparação com contas conectadas à nuvem. Considere as vantagens e desvantagens entre privacidade e funcionalidade ao escolher seu modo de operação preferido.
{{% /alert %}}

