---
title: "Doações"
linkTitle: "Doações"
weight: 25
url: "/pt/docs/donaciones"
description: >
  Remember Me é uma publicação da Poimena, uma organização sem fins lucrativos que oferece serviços gratuitos dedicados ao crescimento espiritual.
---
### Fundadores
A Poimena foi fundada pelo Rev. Peter Schaffluetzel e Rev. Regula Studer Schaffluetzel.

### Endereço
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Suíça

### Transferência bancária
Utilize as seguintes informações para fazer uma doação por transferência bancária.
Adicione seu endereço de e-mail à mensagem de transferência para que possamos agradecer-lhe.
|                     |                    |
|---------------------|--------------------|
| Nome do banco:      | Raiffeisen, Suíça  |
| Beneficiário:       | Poimena            |
| Código SWIFT (BIC): | RAIFCH22XXX        |
| Conta IBAN:         | CH74 8080 8006 2918 4731 8 |

### PayPal/cartão de crédito
Se preferir PayPal ou cartão de crédito para fazer uma doação, utilize o botão abaixo.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="22E6HVFMD62HY" />
<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - A forma mais segura e fácil de pagar online!" alt="Botão Doar com PayPal" />
<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
</form>
