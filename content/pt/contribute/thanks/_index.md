---
title: "Obrigado pela sua doação!"
linkTitle: "Obrigado"
type: docs
toc_hide: true
hide_summary: true
description: >
  Muito obrigado por apoiar o desenvolvimento e a divulgação de Remember Me. Sua doação é uma bênção e um estímulo para nós. Que Deus o abençoe!
------
