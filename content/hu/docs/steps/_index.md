---
title: "Öt lépés az emlékezéshez"
linkTitle: "5 lépés az emlékezéshez"
weight: 2
description: >
  Memorizálj bibliaverseket a Remember Me alkalmazással. Alkalmazd a bibliai memorizálás alapelveit a mindennapi életében.
---

### 1 Adj hozzá egy bibliaverset.
A Remember Me Biblia alkalmazás számos lehetőséget kínál a bibliai szövegek készülékre történő mentésére. Lehetőséged van
- beírni bármilyen szöveget kézzel
- bemásolni egy verset a Biblia különféle változataiból
- Bibliavers-gyűjtemény letöltésére más felhasználóktól.

### 2 Tanulj meg egy verset.
Egy igevers memorizálása szórakoztató, ha különféle módszereket használsz.
- Hallgasd meg
- Takarj le bizonyos szavakat
- Változtasd kirakóvá
- Csak a szó kezdőbetűjét vagy hosszát láthatod
- Írd le minden szó első betűjét!

### 3 Használj témákat és képeket.
Nem mindenki tud jól megjegyezni számokat, és nem is kell ahhoz, hogy megtanulj igeverseket. Adj hozzá egy témát vagy képet a vershez, ami segít felidézni a helyes bibliaverset.

### 4 Ismételd át a már megtanult verseket.
Miután megtanultál egy verset, megjelenik a „Függőben” mappában felülvizsgálatra. Indítson el egy sor flash kártyát felülvizsgálatra. Mondd el hangosan a Biblia emlékversét, fordítsd meg a kártyát, és ellenőrizd, hogy jól emlékszel-e rá.

Az időközönként végzett ismétlés biztosítja, hogy gyakran átnézd az újonnan megtanult igeverseket, de ne felejtsd el a jól ismert verseket sem.

### 5 Hozd ki a legtöbbet az idődből.
Hozd ki a legtöbbet minden pillanatból. A legjobb biblia-memorizáló alkalmazások azért működnek, mert a fogmosás két percét igevers-tanulásra használhatod. Tartsd magadnál az alkalmazást, és használd az élet természetes szüneteiben.
