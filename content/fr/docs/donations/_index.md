---
title: "Dons"
linkTitle: "Dons"
weight: 25
url: "/fr/docs/dons"
aliases:
  - /fr/docs/donations
description: >
  Remember Me est une publication de Poimena, une organisation à but non lucratif offrant des services gratuits dédiés à la croissance spirituelle.
---
### Fondateurs
Poimena a été fondée par le Rév. Peter Schaffluetzel et le Rév. Regula Studer Schaffluetzel.

### Adresse
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Suisse

### Virement bancaire
Utilisez les informations suivantes pour faire un don par virement bancaire.
Ajoutez votre adresse e-mail au message de virement afin que nous puissions vous remercier.
|                     |                    |
|---------------------|--------------------|
| Nom de la banque:   | Raiffeisen, Suisse  |
| Bénéficiaire:       | Poimena            |
| Code SWIFT (BIC): | RAIFCH22XXX        |
| IBAN:        | CH74 8080 8006 2918 4731 8 |

### PayPal/carte de crédit
Si vous préférez utiliser PayPal ou une carte de crédit pour faire un don, utilisez le bouton ci-dessous.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="22E6HVFMD62HY" />
<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - Le moyen le plus sûr et le plus facile de payer en ligne !" alt="Bouton Donner avec PayPal" />
<img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
</form>