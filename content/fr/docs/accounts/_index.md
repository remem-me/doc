---
title: "Utilisateurs et comptes"
linkTitle: "Utilisateurs et comptes"
weight: 4
url: "/fr/docs/comptes"
aliases:
  - /fr/docs/accounts
description: >
  Enregistrez-vous en tant qu'utilisateur et créez un compte pour vos versets.
---

### Enregistrement et connexion des utilisateurs {#register}

Sur [web.remem.me](https://web.remem.me), vous pouvez vous inscrire gratuitement. Après avoir soumis le formulaire
d'inscription, vous recevrez un e-mail vous demandant de confirmer votre inscription.

Après avoir terminé votre inscription, vous pouvez vous connecter à l'application en cliquant sur le bouton
**Connexion** (en haut à droite).

{{% alert title="Changer votre mot de passe" color="secondary" %}}
Pour changer votre mot de passe, cliquez sur "Réinitialiser le mot de passe" dans la boîte de dialogue de connexion.
{{% /alert %}}

### Plusieurs comptes de versets {#plusieurs}

Un compte pour vos versets est automatiquement créé lors de votre inscription en tant qu'utilisateur. La langue, le nom
du compte,
les paramètres de répétition, etc. sont définis dans les paramètres du compte. En tant qu'utilisateur, vous pouvez
ajouter des comptes supplémentaires,
par exemple pour vos enfants ou pour apprendre des versets dans une deuxième langue. Pour la plupart des utilisateurs,
un seul compte suffira. Les versets ne peuvent pas être déplacés entre les comptes. Pour organiser les versets,
les [étiquettes](/fr/docs/etiquettes) sont plus appropriées.

Pour ajouter un nouveau compte, cliquez sur la flèche <span class="material-icons">arrow_drop_down</span> dans l'en-tête
du menu situé à gauche, et sélectionnez "Créer un nouveau compte" dans le menu.

### Importer des comptes depuis des versions antérieures {#antérieures}

Après avoir créé un nouveau compte utilisateur (identifié par votre adresse e-mail), vous pouvez ajouter des comptes de
versets provenant de
versions antérieures de Remember Me. Cliquez sur la flèche <span class="material-icons">arrow_drop_down</span> dans
l'en-tête du menu situé à gauche et sélectionnez "Ajouter un compte existant" dans le menu. Entrez le nom et le mot de
passe du vieux compte et cliquez sur "Connexion". Vous pouvez utiliser le compte attaché comme tout autre compte de
versets Remember Me 6.

### Synchronisation entre plusieurs appareils {#synchroniser}

Sur les appareils dotés de la capacité hors ligne, un symbole de flèche ronde <span class="material-icons">
refresh</span> à côté du nom du compte actuel est visible dans la navigation de gauche. Si le symbole n'est pas
visible (par exemple dans un navigateur Web), les versets sont uniquement sauvegardés en ligne. Les versets et le
progrès d'apprentissage sont automatiquement synchronisés. Cliquez sur le symbole de flèche ronde pour charger les
données modifiées sur un autre appareil. Si vous souhaitez forcer votre appareil à récupérer et à envoyer toutes les
données, appuyez longuement sur le symbole.

{{% alert title="Bouton de synchronisation" color="secondary" %}}
<span class="material-icons">refresh</span> **une flèche** dans un cercle: Modifications possibles sur d'autres
appareils. Cliquez dessus pour les récupérer.  
<span class="material-icons">sync</span> **deux flèches** dans un cercle: Modifications locales sur cet appareil.
Cliquez dessus pour les envoyer.  
<span class="material-icons">sync_problem</span> **point d'exclamation** dans un cercle: La synchronisation a échoué.
Cliquez dessus pour réessayer.
{{% /alert %}}

### Modifier et supprimer des comptes de versets {#modifier-compte}

Pour modifier ou supprimer le compte actuellement sélectionné, sélectionnez "Compte" dans la navigation principale à
gauche. Cliquez sur l'icône en haut à gauche (disquette <span class="material-icons">save</span>) pour sauvegarder le
compte après modification.
Pour supprimer le compte, cliquez sur l'icône de corbeille <span class="material-icons">delete</span> en haut à droite.
Un compte supprimé est stocké dans la [corbeille](https://web.remem.me/account-bin) pendant trois mois et peut être
restauré à partir de là.

### Modifier et supprimer l'accès utilisateur {#modifier-utilisateur}

Si vous souhaitez modifier ou supprimer votre connexion utilisateur, accédez à la navigation du
compte (<span class="material-icons">arrow_drop_down</span> flèche dans l'en-tête du menu situé à gauche) et
sélectionnez "Utilisateur".
Pour changer votre adresse e-mail, entrez la nouvelle adresse et le mot de passe actuel, puis cliquez sur "Enregistrer".
Pour supprimer complètement votre accès utilisateur à remem.me, vous devez d'abord supprimer tous les comptes de
versets. Ensuite, entrez le mot de passe actuel et cliquez sur l'icône de corbeille <span class="material-icons">
delete</span> dans la boîte de dialogue utilisateur.

{{% alert title="Attention" color="warning" %}}
La suppression de votre accès à remem.me est irréversible. Toutes vos données seront immédiatement et définitivement
supprimées de toutes les bases de données.
{{% /alert %}}

### Mode sur appareil uniquement {#sur-appareil-uniquement}

Vous pouvez utiliser Remember Me sans vous connecter au service cloud remem.me. Dans ce mode, toutes les données sont stockées exclusivement sur votre appareil.

- Les données restent sur votre appareil
- Pas de sauvegarde cloud disponible
- Impossible de publier des collections de versets

{{% alert title="Important" color="warning" %}}
Si vous perdez l'accès à l'application sur votre appareil en mode sur appareil uniquement, vos versets ne pourront pas être récupérés.
{{% /alert %}}
Pour activer le mode sur appareil uniquement, sélectionnez "Sur appareil uniquement" en bas de la boîte de dialogue d'inscription (après avoir appuyé sur "C'est parti!").

Pour quitter le mode sur appareil uniquement :

1. Ouvrez le menu
2. Appuyez sur le nom du compte pour passer au menu du compte
3. Sélectionnez "Déconnexion"

Pour transférer des versets locaux vers un compte en ligne, [exportez et importez](docs/labels/#exporter) vos versets sous forme de fichier.

{{% alert title="Note" color="secondary" %}}
Le mode sur appareil uniquement offre une fonctionnalité limitée par rapport aux comptes connectés au cloud. Considérez les avantages et les inconvénients entre la confidentialité et la fonctionnalité lors du choix de votre mode de fonctionnement préféré.
{{% /alert %}}
