---
title: "Quoi de neuf dans Remember Me 6?"
linkTitle: "Quoi de neuf?"
weight: 1
url: "/fr/docs/version"
description: >
  Remember Me 6 élève la mémorisation de la Bible à un niveau supérieur.
---

{{% pageinfo %}}
Si vous recherchez des informations sur les versions précédentes de Remember Me, visitez
les [pages d'aide de Remember Me 5 et antérieures](https://v5.remem.me).

Si vous avez besoin de récupérer des versets après la mise à jour, veuillez consulter [ce message du forum utilisateur](https://forum.remem.me/d/123-version-update-verse-list-disappeared/8).
{{% /pageinfo %}}

### Plus facile à utiliser

Remember Me 6 met l'accent sur la métaphore des cartes mémoire. Les cartes se déplacent entre les cases Nouveau, En
attente et Mémorisé. La carte d'un verset est le point de départ pour étudier et réviser. La métaphore des cartes aide à
comprendre de manière plus intuitive les processus de base.

### Plus de polyvalence

Les utilisateurs avancés avec des centaines de versets apprécient davantage de façons de regrouper et filtrer leurs
collections de versets grâce à des étiquettes personnalisables. Les utilisateurs visuels bénéficient de l'ajout d'images
à leurs versets. Les utilisateurs multilingues peuvent écouter la référence et le passage dans différentes langues.

### Expérience unifiée

La nouvelle version de l'application garantit une expérience utilisateur unifiée sur tous les appareils, car elle
utilise le même framework logiciel pour toutes les plateformes. L'application et le framework logiciel sont open source.
