---
title: "Étudier et réviser"
linkTitle: "Étudier et réviser"
weight: 6
url: "/fr/docs/apprendre"
aliases:
  - "/fr/docs/study"
description: >
  Les jeux et un système de révision intelligent vous aident à mémoriser et à retenir.
---
### Le processus d'apprentissage {#processus}
Les versets à mémoriser se déplacent entre les boîtes "Nouveau", "Échu" et "Rappelé".
Vous pouvez changer l'ordre d'une boîte en tapotant sur l'onglet actif <img src="/images/triangle.svg">.

|Boîtes:|<span class="material-icons">inbox</span><br>Nouveau|&rarr;| <span class="material-icons">check_box_outline_blank</span><br>Échu |&rarr;| <span class="material-icons">check_box</span><br>Rappelé|
|:-|:-:|:-:|:-------------------------------------------------------------------:|:-:|:-:|
|Actions:|1. **Étudier**|2. Appliquer|                 3. **Réviser**<br>**les passages**                  |4. Reproduire|5. **Réviser**<br>**les références**
|Boutons:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|                <img src="/images/cards_outline.svg">                |<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">

1. **Étudier** de nouveaux versets.
2. Appliquer un verset le déplace dans la boîte "Échu".
3. **Réviser les passages** des versets.
4. Reproduire correctement un verset le déplace dans la boîte "Rappelé".
5. **Réviser les références** des versets sélectionnés au hasard.

### Étudier {#etudier}
Après avoir ajouté un verset au système, il peut être étudié en utilisant différents types de jeux.
Pour commencer à étudier, touchez le verset et le symbole <span class="material-icons">school</span> d'étude.

<span class="material-icons">wb_cloudy</span> **Obscurcir**: Cachez certains mots et essayez de réciter le verset
en complétant les mots manquants à partir de votre mémoire. Touchez un mot confus pour le révéler.

<span class="material-icons">extension</span> **Puzzle**: construisez le verset en touchant le mot correct.

<span class="material-icons">subject</span> **Aligner**: affiche des lignes vides ou les premières lettres en touchant l'icône
à gauche (dans la barre inférieure). Avancez en touchant <span class="material-icons">plus_one</span> (+mot) ou
<span class="material-icons">playlist_add</span> (+ligne). Essayez de réciter le mot ou la ligne avant de le révéler.

<span class="material-icons">keyboard</span> **Écriture**: Construisez le verset en écrivant la première lettre de chaque mot.

Après avoir mémorisé le verset, faites glisser l'onglet vers la droite pour le déplacer vers la liste "En attente". Désormais, le verset participe
au processus de révision.

### Réviser les passages {#passages}
Sélectionnez la boîte "**Échu**". Si vous souhaitez modifier l'ordre des versets, touchez à nouveau "Échu".
Il existe 3 façons de commencer une révision:

{{<cardpane>}}
{{<card header="<img src='/images/cards_outline.svg'> cartes contour">}}
Démarrez une révision de plusieurs cartes.
{{</card>}}
{{<card header="En touchant un verset">}}
Démarrez une révision d'une seule carte.
{{</card>}}
{{<card header="En appuyant sur un verset">}}
Ouvre la carte côté passage.
{{</card>}}
{{</cardpane>}}

Lorsque la référence et le sujet sont affichés, récitez le verset de mémoire, puis retournez la carte pour vérifier
si vous aviez raison. Si vous préférez réciter le verset ligne par ligne (pour modifier les sauts de ligne,
consultez [Ajout et édition de versets](/fr/docs/versets)), touchez l'icône <span class="material-icons">rule</span> dans le
coin inférieur droit pour chaque ligne.

Si vous avez besoin d'un peu d'aide pour commencer, maintenez la carte enfoncée pour obtenir un indice.
(Vous pouvez ignorer une carte en la faisant glisser vers le bas.)

Si vous avez correctement mémorisé le verset, faites glisser vers la droite pour le déplacer vers la boîte "Rappelé". Cela augmentera
le niveau du verset de 1 (jusqu'à ce que le limite de révision dans les paramètres du compte soit atteinte). Si ce n'était pas tout à fait correct,
faites glisser vers la gauche. Cela réinitialisera le niveau du verset à 0. Cela peut sembler un peu décourageant, mais
c'est très important pour la fréquence de révision du verset. Les versets avec des niveaux bas sont révisés plus fréquemment. Cela aide à
renforcer la mémoire d'un verset avant qu'il ne soit oublié.
La répétition espacée garantit que les textes difficiles sont révisés fréquemment, mais que les textes familiers ne sont pas oubliés.

{{% alert title="Répétition espacée (Spaced Repetition)" color="secondary" %}}
La mémorisation est plus efficace lorsque le cerveau se souvient de ce qu'il a appris à des intervalles de plus en plus longs.
Remember Me utilise un algorithme exponentiel pour calculer les intervalles (similaire à Pimsleur). Si la fréquence de révision de votre compte est réglée sur "Normale",
vous réviserez vos versets après 1, 2, 4, 8, 16... jours. L'autre composante de l'algorithme consiste à réinitialiser le niveau d'un verset à 0 après une révision incorrecte (similaire à Leitner).
Ainsi, les textes difficiles sont révisés plus souvent que les textes faciles.  
{{% /alert %}}

### Réviser les références {#references}
Si vous voulez réviser uniquement les références des passages révisés aujourd'hui, appuyez sur l'étiquette
"[Révisés aujourd'hui](/fr/docs/etiquettes/#filtrer)", avant de commencer. Sélectionnez la
boîte "**Rappelé**", définissez son ordre sur "aléatoire" et appuyez sur le bouton avec l'icône <img src="/images/cards_filled.svg">
de cartes pleines en bas.

Remember Me mélange les cartes et présente vos textes. Essayez de réciter la référence de mémoire et retournez la
carte en la touchant pour vérifier si vous avez raison. Si c'est le cas, faites glisser la carte vers la droite. Si ce n'est pas le cas,
faites glisser vers la gauche. L'application vous représentera la carte pendant la session de révision jusqu'à ce que vous vous en souveniez.

Vous pouvez ignorer une carte en la faisant glisser vers le bas. Contrairement à la révision de passages, l'application ne suit pas
votre progression de révision. Ce mode de révision ressemble plus à un questionnaire que vous devez faire de temps en temps pour mettre à jour votre connaissance des références.

{{% alert title="Paramètres du compte" color="secondary" %}}
- **Objectif quotidien**: Nombre de mots à réviser par jour.
- **Nombre de cartes de révision**: Nombre maximum de cartes par révision de passage.
- **Nombre de cartes inversées**: Nombre maximal de cartes par révision de références.
- **Étudier référence**: Inclure la référence dans l'étude et la révision des versets.
- **Fréquence de révision**: À quelle fréquence les versets apparaîtront-ils pour révision.
- **Limite d'intervalle**: Nombre maximum de jours jusqu'à la prochaine révision. Limite indirecte du nombre de niveaux pouvant être atteints.
  {{% /alert %}}




