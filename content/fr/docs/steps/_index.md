---
title: "Cinq pas à retenir: Mémoriser des textes bibliques"
linkTitle: "5 pas à retenir"
weight: 2
url: "/fr/docs/steps"
description: >
  Mémoriser des versets bibliques avec l'application Remember Me. Appliquez les principes de base de la mémorisation de la Bible dans votre vie quotidienne.
---

### 1 Ajouter un verset biblique.
L'application biblique Remember Me offre plusieurs options pour sauvegarder des textes bibliques sur votre appareil. Vous pouvez
- [saisir manuellement](/fr/docs/versets/#ajouter) n'importe quel texte 
- récupérer un verset dans une variété de versions de la [Bible en ligne](/fr/docs/versets/#bible-en-ligne)
- télécharger des [collections de versets bibliques](/fr/docs/collections/#trouver) d'autres utilisateurs.

### 2 Mémoriser un verset.
[Étudier un verset biblique](/fr/docs/apprendre/#etudier) est amusant si vous utilisez diverses méthodes.
- [Écoutez-le](/fr/docs/audio/#ecouter-boite)
- Cacher des mots aléatoires
- Faire-en un puzzle
- Montrer les premières lettres ou lignes des mots vides.
- Écriver la première lettre de chaque mot

### 3 Utiliser des thèmes et des images.
Tout le monde n'est pas doué pour se souvenir des chiffres, et vous ne devez pas l'être pour mémoriser un texte. Ajoutez un [thème](/fr/docs/etiquettes) ou une [image](/fr/docs/versets/#image) à votre verset et cela vous aidera à vous rappeler le bon verset de mémoire.

### 4 Répéter les versets mémorisés.
Une fois que vous avez mémorisé un verset, il apparaît dans la boîte "Échu" pour être revu. Commencez une série de cartes flash pour la [révision](/fr/docs/apprendre/#passages). Dites le verset biblique à mémoriser à [haute voix](/fr/docs/audio/#enregistrer), retournez la carte et vérifiez si vous vous en êtes souvenu correctement.

La répétition espacée vous permet de revoir souvent les versets nouvellement mémorisés, mais n'oubliez pas non plus les versets connus.

### 5 Tirez le meilleur parti de votre temps.
Tirez le meilleur parti de chaque instant. Les meilleures applications de mémorisation de la bible fonctionnent parce que vous pouvez utiliser les deux minutes pendant que vous vous brossez les dents pour mémoriser. Gardez l'application avec vous et utilisez-la pendant ces pauses naturelles de la vie.
