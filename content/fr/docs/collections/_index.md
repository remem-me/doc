---
title: "Importer et partager des collections"
linkTitle: "Collections de versets"
weight: 9
url: "/fr/docs/collections"
aliases:
  - /fr/docs/collections
  - /comment-rm-fonctionne/collections
  - /fr/docs/importer-et-partager-des-collections
description: >
  Profitez des collections de versets publiques et partagez vos propres collections avec d'autres.
---

> Une collection de versets est une étiquette publiée avec ses versets.

### Trouver une collection de versets {#trouver}

Sélectionnez "Collections" dans le menu de l'application à gauche et utilisez la fonction de
recherche <span class="material-icons">search</span> pour trouver ce que vous recherchez. Appuyez sur une carte de
collection pour voir plus de détails, et appuyez sur l'icône de téléchargement <span class="material-icons">
download</span> dans la barre en haut à droite pour importer les versets de la collection dans votre compte. Une étiquette
portant le nom de la collection rend les nouveaux versets distincts de vos autres versets.

### Publier et partager une collection de versets {#publier}

Sélectionnez "Collections" dans le menu de l'application à gauche et appuyez sur le bouton de
publication <span class="material-icons">publish</span> (icône de téléchargement en haut à droite) pour voir une liste
de tous les étiquettes attachées aux versets. Une icône de monde barrée <span class="material-icons">public_off</span>
signifie que l'étiquette et ses versets ne sont pas visibles pour les autres utilisateurs.

Appuyez sur une étiquette et saisissez une description de votre collection dans le formulaire qui apparaît. Pour annuler ou
retirer une collection précédemment publiée, appuyez sur l'icône de monde barrée en bas du formulaire. Tous les détails
de la publication seront supprimés et les versets seront cachés aux autres utilisateurs. Lorsque vous êtes prêt à
publier, appuyez sur l'icône de monde mise en évidence <span class="material-icons">public</span> en bas du formulaire.
Tous les détails de la publication seront enregistrés et la collection apparaîtra désormais dans les résultats de
recherche des autres utilisateurs.

### Intégrer votre collection de versets sur votre site web

Puisque Remember Me 6 est également disponible en tant qu'application web, vous pouvez intégrer une collection ou un
verset d'une collection sur votre site web. Les visiteurs de votre site web pourront alors utiliser les jeux et les
cartes d'apprentissage de Remember Me directement sur votre site web. Tout ce que vous avez à faire est d'insérer un
élément iframe avec l'adresse de votre propre collection de versets sur web.remem.me dans votre code HTML, par exemple:
```
<iframe 
    src="https://web.remem.me/collections/432943556799288" 
    style="border:none;" 
    width="320" height="480" 
    title="Notre collection de versets">
</iframe>
```
ce qui donne le résultat suivant:

{{< app 432943556799288 >}}