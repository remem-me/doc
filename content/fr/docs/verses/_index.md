---
title: "Gérer les versets de la Bible à mémoriser"
linkTitle: "Gérer les versets"
weight: 5
url: "/fr/docs/versets"
aliases:
  - /fr/docs/verses
description: >
  Récupérer des versets bibliques de Bibles en ligne ou ajouter et modifier tout autre texte pour la mémorisation. Reporter, supprimer ou restaurer des fiches de mémorisation de la Bible.
---

### Ajouter un verset {#ajouter}

Sélectionnez l'onglet "Nouveau" et appuyez sur le bouton "+" en bas de l'écran pour ouvrir le formulaire.
Remplissez la référence et le passage et appuyez sur le bouton en haut à gauche (<span class="material-icons">
save</span> symbole du disque) pour enregistrer le verset. Le verset apparaît maintenant dans votre boîte de réception.

### Récupérer un passage d'une Bible en ligne {#bible-en-ligne}

Lors de l'ajout ou de la modification d'un verset, appuyez sur le bouton déroulant <span class="material-icons">
arrow_drop_down</span>
du champ "Source" pour ouvrir la liste des versions de la Bible disponibles, ou saisissez une abréviation de la Bible
(p. ex., NBS) dans le champ "Source".

Si Remember Me reconnaît la référence (par exemple, 1 Jn 3:16 ou 1 Jean 3:1-3) comme une référence à un passage de la
Bible
et la source (par exemple, BDS) comme une version biblique en ligne, il affiche un bouton avec un symbole
<span class="material-icons">menu_book</span> bible. Un bouton déroulant <span class="material-icons-outlined">
format_list_numbered</span> permet de récupérer le passage avec ou sans
numéros de verset.
En appuyant sur le bouton Écriture, Remember Me ouvre le site Web de la Bible en ligne et vous propose de coller
<span class="material-icons">paste</span> le passage dans l'application. Vous pouvez ajouter des sauts de ligne
supplémentaires ou d'autres
modifications et enregistrer le verset (voir ci-dessus).

### Copier un verset à partir d'une application biblique {#appli-biblique}

La plupart des applications bibliques mobiles vous permettent de partager des versets avec d'autres applications. Mettez
en surbrillance le
verset dans l'application biblique, sélectionnez "Partager" et choisissez Remember Me dans la liste des applications.
Remember Me
ouvre l'éditeur de versets et remplit la référence, le texte et la source (si fournie par l'application biblique).
Appuyez sur le bouton Enregistrer pour ajouter le verset à votre collection de nouveaux versets bibliques.

### Éditer un verset {#editer}

Si vous appuyez sur un verset qui apparaît dans l'une des trois cases (Nouveau, Échu, Rappelé), il s'affiche
comme une carte flash. Vous pouvez l'éditer en appuyant sur le symbole du crayon dans la barre supérieure.

Vous pouvez diviser un passage en plusieurs sections d'étude en ajoutant des sauts de ligne.

#### Options de formatage

- \*italique\*
- \*\*gras\*\*
- \>citation

#### Numéros de verset

Les numéros de verset sont placés entre crochets, par exemple \[1\], \[2\], \[3\].
Si le paramètre de compte "Étudier référence" est activé, les numéros de verset sont inclus dans la synthèse vocale et
les jeux d'apprentissage.

### Joindre une image {#image}

Lors de l'édition d'un verset (voir ci-dessus), vous pouvez joindre une image qui sera utilisée comme fond pour la carte
mémoire
pour le verset. Vous pouvez simplement entrer l'adresse Internet (URL) d'une image disponible en ligne dans
le champ "URL de l'image" en bas de l'écran. Pour votre commodité, il y a un bouton
<span class="material-icons">image</span> image sur la droite pour rechercher des images sur Unsplash.com. Appuyez sur
le nom
pour en savoir plus sur le photographe et appuyez sur l'image pour insérer l'adresse de l'image dans le champ du
formulaire. Seulement
les photos de Unsplash.com sont incluses dans les collections de versets publics.

Appuyez sur le bouton en haut à gauche (<span class="material-icons">save</span> symbole du disque) pour enregistrer le
verset avec
l'image jointe.

### Déplacer, reporter ou supprimer des versets {#deplacer}

Sélectionnez les versets en touchant leur badge (carré arrondi à gauche), et ouvrez le menu de droite (trois points).
Celui-ci offre la possibilité de déplacer les versets sélectionnés vers une autre boîte, de reporter leur date d'attente
ou de les supprimer.

### Restaurer les versets supprimés {#restaurer}

Les versets récemment supprimés peuvent être restaurés en ligne à partir de la [corbeille](https://web.remem.me/bin).
