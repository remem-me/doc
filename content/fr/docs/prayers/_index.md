---
title: "Gestion des cartes de prière et des dévotions"
linkTitle: "Prières et dévotions"
weight: 18
url: "/fr/docs/prieres"
aliases:
  - /fr/docs/prayers
description: >
  Les cases Remember Me aident à gérer les cartes de prière et autres textes que vous souhaitez lire ou écouter régulièrement.
---

### Créer un compte de prières

Créez un compte séparé pour les textes que vous souhaitez lire régulièrement plutôt que de les mémoriser. Comme les
références jouent probablement un rôle mineur, vous pouvez sélectionner l'option "Apprentissage par thèmes" lors de la
configuration du compte.

### Ajouter des prières/dévotions

Vous pouvez ajouter des textes dans la case "Nouveau" ou importer une collection de textes depuis "Collections". Il est
utile d'ajouter une sorte de numérotation comme référence. Par exemple, si vous divisez une longue collection de prières
en cartes de prière individuelles - comme les livres de Stormie Omartian - les références pourraient être 1a, 1b, 1c,
2a, 2b, etc. Les références servent à classer les cartes. Il est également utile d'ajouter un thème à chaque prière.

### Prier quotidiennement

Triez les prières par thème ou par référence (par ordre alphabétique) en touchant à nouveau l'onglet "Nouveau". Il est
préférable de dire les prières à haute voix et dans votre propre dialecte ou mots. De cette façon, même les prières
préconçues peuvent devenir une conversation personnelle avec Dieu. Notez les impressions et les pensées qui vous
semblent importantes pendant la prière sous le texte (voir [Modifier le verset](/fr/docs/versets/#editer)). Ainsi, votre compte de prière devient
également votre journal de prière et reflète une partie de votre communication avec Dieu. Déplacez la prière après votre
dévotion quotidienne vers la case "Échu" pour suivre vos progrès dans la prière. La case "Rappelé" n'est pas
nécessaire pour les prières.

### Réutiliser une collection de prières

Si vous souhaitez recommencer avec une collection de prières après l'avoir terminée, sélectionnez une prière (appuyez
sur son badge à gauche), puis "Sélectionner tout", et déplacez toute la collection à nouveau dans la case "Nouveau".
