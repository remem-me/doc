---
title: "Fonctions audio"
linkTitle: "Fonctions audio"
weight: 12
description: >
  Remember Me peut vous lire des versets ou enregistrer votre propre récitation et vous la jouer.
---

> Écouter des versets de manière répétée soutient votre processus d'apprentissage. Enregistrer votre voix pendant la récitation renforce votre connaissance du verset.

### Écouter des versets d'une boîte {#ecouter-boite}
Pour commencer à écouter des versets, sélectionnez la boîte contenant les versets que vous souhaitez écouter. En écoutant des versets dans la boîte "Nouveau", vous vous familiariserez avec eux. Écouter des versets dans la boîte "Connu" actualise votre connaissance. Appuyez sur le bouton <span class="material-icons">play_arrow</span> Lecture pour commencer la lecture. Utilisez les boutons en bas pour mettre en pause, arrêter ou passer un verset.

Vous pouvez demander à Remember Me de répéter tous les versets ou un seul verset, ou ralentir la lecture dans les paramètres (<span class="material-icons">settings</span> Engrenage) à côté des boutons de contrôle.

Si vous souhaitez ajouter la référence après le texte biblique ou écouter les versets par thème, activez le paramètre correspondant sous "Compte".

### Commencer par le verset actuel {#ecouter-verset}
Si vous avez une carte d'apprentissage devant vous, vous pouvez démarrer la lecture en appuyant sur le bouton <span class="material-icons">play_arrow</span> Lecture dans le coin supérieur droit. Après être retourné à la liste des versets, le verset actuel est toujours marqué comme en lecture ou en pause. En appuyant maintenant sur le bouton Lecture de la liste, l'application commence à lire à partir de cette position dans la liste.

{{% alert title="Paramètres de synthèse vocale" color="secondary" %}}
<span class="material-icons">speed</span> Vitesse de parole  
<span class="material-icons">pause_presentation</span> Pause (en secondes)  
<span class="material-icons">av_timer</span> Minuterie de sommeil (en minutes)  
<span class="material-icons">playlist_play</span> *activé*: Lit toute la liste / *désactivé*: Lit seulement le verset actuel  
<span class="material-icons">repeat</span> Répéter la liste (ou le verset)
{{% /alert %}}

### Écouter en apprenant un verset {#ecouter-apprendre}
Pendant un jeu d'apprentissage (voir [Apprendre](/fr/docs/apprendre/#apprendre)), Remember Me lit chaque mot ou phrase que vous révélez. Vous pouvez désactiver la sortie vocale dans le menu des paramètres (<span class="material-icons">settings</span> Engrenage) pendant le jeu.

### Enregistrer et jouer votre propre récitation {#enregistrer}
Les cartes d'apprentissage dans votre boîte "Échu" (voir [Répéter les passages](/fr/docs/apprendre/#passages)) ont une icône de microphone <span class="material-icons">mic</span> en bas. Appuyez dessus pour enregistrer votre récitation du verset. En appuyant à nouveau sur le bouton ou en retournant la carte, l'enregistrement s'arrête. Après avoir révélé le texte du verset, la lecture démarre pour vous permettre de vérifier facilement si votre récitation était correcte.

### Changer de voix {#voix}

{{% alert color="secondary" %}}
Vous pouvez changer la **langue** de la voix dans les paramètres de votre compte *Langue des versets* et *Langue des références*.
{{% /alert %}}

Remember Me utilise le moteur de synthèse vocale de votre appareil. Vous pouvez trouver des instructions pour changer de voix ici:
* [Aide pour iPhone/iPad](https://support.apple.com/fr-fr/guide/iphone/iph96b214f0/ios#iph938159887)  
  Allez dans Réglages > Accessibilité > Contenu parlé > Voix: Sélectionnez une voix et un dialecte, téléchargez-les et définissez-les comme standards.
* [Aide pour Android](https://support.google.com/accessibility/android/answer/6006983?hl=fr)

Pour les appareils Android, plusieurs moteurs de synthèse vocale sont disponibles, tels que:
* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Services de synthèse vocale de Google](https://play.google.com/store/apps/details?id=com.google.android.tts)

Lors de l'utilisation de l'application web sur Windows ou macOS, vous pouvez modifier la voix dans les paramètres vocaux de Remember Me. Il se peut que vous deviez d'abord installer des données vocales sur votre appareil.



