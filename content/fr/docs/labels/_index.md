---
title: "Regrouper et filtrer par étiquettes"
linkTitle: "Regrouper par étiquettes"
weight: 7
url: "/fr/docs/etiquettes"
aliases:
  - /fr/docs/labels
description: >
  Sélectionner et désélectionner des étiquettes vous permet de masquer, afficher et filtrer des versets comme vous le souhaitez.
---

> Filtrage rapide: Appuyez sur une étiquette dans le menu <span class="material-icons-outlined">visibility</span> à
> droite pour afficher exclusivement ses versets. Maintenez-le enfoncé pour l'exclure.

### Créer ou modifier une étiquette {#creer}

Sélectionnez "Modifier les étiquettes" dans le menu principal de gauche pour créer, modifier ou supprimer une étiquette.
Les étiquettes récemment supprimées peuvent être restaurées en ligne à partir de
la [corbeille](https://web.remem.me/labels/bin).

### Attacher des étiquettes aux versets {#attacher}

Sélectionnez un ou plusieurs versets en touchant leur badge (carré arrondi à gauche). Touchez
l'icône <span class="material-icons">label</span> d'étiquette dans le menu contextuel en haut. Cochez les étiquettes que
vous souhaitez attacher aux versets sélectionnés, décochez les étiquettes que vous souhaitez supprimer des versets
sélectionnés et appuyez sur le bouton pour enregistrer les modifications (icône de
disquette <span class="material-icons">save</span>).

### Filtrer les versets par étiquette {#filtrer}

La visibilité des versets regroupés par étiquettes est définie dans le tiroir d'étiquettes à droite (voir ci-dessus).
Vous pouvez changer l'état des étiquettes/groupes en les touchant ou en les appuyant. Les icônes ont les significations
suivantes:

<span class="material-icons-outlined">visibility_off</span> **Œil barré**: les versets avec cette étiquette sont
toujours masqués.

<span class="material-icons-outlined">visibility</span> **Œil profilé**: les versets avec cette étiquette sont visibles
sauf si certaines étiquettes sont en mode exclusif.

<span class="material-icons">visibility</span> **Œil plein**: les versets avec cette étiquette sont visibles et tous
les autres versets sont masqués.

Quelques filtres automatiques et paramètres de filtre sont disponibles:

<span class="material-icons-outlined">visibility</span> **Revu aujourd'hui**: Ce filtre filtre les versets revus
aujourd'hui (en fonction d'autres paramètres de filtre).

<span class="material-icons-outlined">apps</span> **Tous les versets**: en sélectionnant cette option, les modes des
étiquettes sont modifiés pour que tous les versets soient visibles.

<span class="material-icons-outlined">label_off</span> **Versets sans étiquette**: en sélectionnant cette option, les
modes des étiquettes sont modifiés pour que seuls les versets sans étiquette soient visibles.

### Tous les versets dans une liste {#tous}

Dans certaines situations, il peut être utile de voir tous les versets dans la même liste, par exemple pour exporter
votre collection complète vers un fichier (voir ci-dessous). Vous pouvez y parvenir dans le
tiroir <span class="material-icons-outlined">visibility</span> de visibilité à droite en désactivant les
cases <span class="material-icons-outlined">layers</span> et en sélectionnant le filtre
automatique <span class="material-icons">apps</span> "Tous les versets".

### Exporter/importer les versets sous forme de fichier (CSV) {#exporter}

Ouvrez le menu fichier <span class="material-symbols-outlined">file_open</span> (uniquement dans l'application mobile) dans
la barre de visibilité <span class="material-icons-outlined">visibility</span> sur le côté droit et sélectionnez "
Exporter vers fichier". Cela créera un nouveau fichier au format CSV avec les vers actuellement affichés. Vous pouvez
partager le fichier exporté avec d'autres applications pour le sauvegarder ou l'envoyer à quelqu'un d'autre. Les
fichiers CSV peuvent être ouverts avec des programmes de feuilles de calcul.

Si vous choisissez l'option "Importer à partir d'un fichier", vous pouvez charger des versets à partir d'un fichier
stocké sur votre appareil.

