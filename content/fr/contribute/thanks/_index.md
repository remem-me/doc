---
title: "Merci pour votre don!"
linkTitle: "Merci"
type: docs
toc_hide: true
hide_summary: true
description: >
  Un grand merci pour votre soutien au développement et à la diffusion de Remember Me! Votre don est une bénédiction et un encouragement pour nous. Que Dieu vous bénisse!
---

