---
title: "感谢您的捐助！"
linkTitle: "感谢"
type: docs
toc_hide: true
hide_summary: true
description: >
  感谢您支持「纪念我」的开发和推广！您的捐助对我们来说是一份祝福和鼓励。愿上帝保佑您！
---
