---
title: "用户和帐户"
linkTitle: "用户和帐户"
weight: 4
description: >
  注册为用户并为您的经文创建一个帐户。
---

### 注册和登录用户 {#register}

在 [web.remem.me](https://web.remem.me) 上免费注册。提交注册表单后，您将
收到一封电子邮件，要求您确认您的注册。

完成注册后，您可以点击右上角的 **登录** 按钮来登录应用。

{{% alert title="更改您的密码" color="secondary" %}}
要更改密码，请在登录对话框中点击“重置密码”。
{{% /alert %}}

### 多个经文帐户 {#multiple}

您的经文帐户会在您注册用户时自动创建。语言、帐户名称、审查偏好等会储存在您帐户的设置中。作为用户，您可以添加额外的帐户，例如，为您的孩子或者用第二语言记住经文。对于大多数用户来说，一个帐户就足够了。您不能在帐户之间转移经文。为了组织您的经文，请使用[标签](/docs/labels)。

要添加新帐户，请点击左侧菜单头部的 <span class="material-icons">arrow_drop_down</span> 三角形，并从菜单中选择“创建新帐户”。

### 从之前版本导入帐户 {#previous}

创建新的用户帐户（通过您的电子邮件地址识别）后，您可以附加来自之前 記念我
版本的经文帐户。点击左侧菜单头部的 <span class="material-icons">arrow_drop_down</span>
三角形，并从菜单中选择“添加现有帐户”。输入旧帐户的名称和密码，然后点击“登录”。您可以像使用任何其他 記念我 6 经文帐户一样使用附加的帐户。

### 同步多个设备 {#sync}

具有离线功能的设备在左侧导航抽屉中当前帐户名称旁边有一个 <span class="material-icons">refresh</span>
圆形箭头图标。如果图标不可见（例如，在网页浏览器中），则经文仅在线上储存。经文和学习进度会自动同步。点击圆形箭头图标以加载在不同设备上新编辑的数据。如果您需要强制您的设备检索并发送所有数据，请长按图标。

{{% alert title="同步按钮" color="secondary" %}}
<span class="material-icons">refresh</span> **一个箭头**形成圆圈：可能在其他设备上的更改。点击以检索它们。  
<span class="material-icons">sync</span> **两个箭头**形成圆圈：此设备上的本地更改。点击以发送它们。  
<span class="material-icons">sync_problem</span> **圆圈中的惊叹号**：同步失败。点击以再次尝试。
{{% /alert %}}

### 编辑和删除经文帐户 {#edit-account}

要编辑或删除当前选择的帐户，请从左侧的主导航选择“帐户”。在编辑后点击左上角按钮（<span class="material-icons">save</span>
磁盘符号）以保存帐户。
点击右上角的 <span class="material-icons">delete</span>
垃圾桶图标删除帐户。已删除的帐户会被储存在[垃圾桶](https://web.remem.me/account-bin)中三个月，并且可以从那里恢复。

### 编辑和删除用户访问 {#edit-user}

如果您想要编辑或删除您的用户注册，切换到帐户导航（<span class="material-icons">
arrow_drop_down</span> 左侧菜单头部的三角形）并选择“用户”。
要编辑您的电子邮件地址，输入新地址和当前密码，然后点击“保存”。
要完全删除您对 remem.me
的用户访问，首先需要删除所有经文帐户。然后，输入当前密码并点击用户对话框的 <span class="material-icons">delete</span>
垃圾桶图标。

{{% alert title="注意" color="warning" %}}
删除您对 remem.me 的访问是不可逆的。您的所有数据将立即且永久从所有数据库中移除。
{{% /alert %}}

### 仅限装置上模式 {#on-device-only}

您可以使用記念我而不连接到 remem.me 云端服务。在此模式下，所有资料仅储存在您的装置上。

- 资料保留在您的装置上
- 无法使用云端备份
- 无法发布经文集

{{% alert title="重要" color="warning" %}}
如果您在仅限装置上模式下失去对装置上应用程式的存取权，您的经文将无法恢复。
{{% /alert %}}
要启用仅限装置上模式，请在注册对话框底部选择「仅限装置上」（在点击「开始」后）。

要退出仅限装置上模式：

1. 打开菜单
2. 点击帐户名称以切换到帐户菜单
3. 选择「登出」

要将本地经文转移到在线帐户，请将您的经文[汇出并汇入](docs/labels/#export)为文件。

{{% alert title="注意" color="secondary" %}}
与连接云端的帐户相比，仅限装置上模式提供有限的功能。在选择您偏好的操作模式时，请考虑隐私和功能之间的利弊。
{{% /alert %}}



