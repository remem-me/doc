---
title: "背诵圣经的五个步骤"
linkTitle: "背诵的5个步骤"
weight: 2
description: >
  使用“记念我”应用程序，运用简单原理每天背诵圣经。
---

### 1 添加新的圣经经文

“记念我”应用程序提供多种方式将圣经经文保存到你的设备上。你可以：
- [手动输入](/zh-cn/docs/verses/#add)任何经文
- 从多种[在线圣经版本](/zh-cn/docs/verses/#online-bible)中提取经文
- 下载用户策划的[圣经经文集](/zh-cn/docs/collections/#find)

### 2 记忆圣经经文

运用各种方法[学习](/zh-cn/docs/study/#studying)圣经经文会变得有趣。你可以：
- [聆听](/zh-cn/docs/audio/#listen-box)经文
- 随机隐藏字词
- 将经文变成拼图
- 逐字或逐行显示经文
- 通过输入练习经文

### 3 使用主题和图片

不是每个人都擅长记忆数字，而且你也不需要。为你的圣经经文添加[主题](/zh-cn/docs/labels/)或[图片](/zh-cn/docs/verses/#image)，这将帮助你从记忆中提取正确的经文。

### 4 复习和强化已记忆的经文

当你记住一节经文后，它会出现在“到期”栏目中。开始一系列[圣经记忆卡](/zh-cn/docs/study/#passages)来复习这些经文。[录制自己](/zh-cn/docs/audio/#record)背诵经文的声音，翻转卡片，检查是否正确。

间隔重复法确保你经常复习新记住的经文，同时也不会忘记熟悉的经文。

### 5 善用时间

把握每个机会。最好的圣经记忆应用程序之所以有效，是因为你可以利用刷牙的两分钟来背诵经文。随身携带“记住我”应用程序，在生活中的自然停顿时刻使用它。