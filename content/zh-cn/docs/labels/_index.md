---
title: "通过标签分组和过滤"
linkTitle: "按标签分组"
weight: 7
description: >
  选择和取消选择标签允许您隐藏、显示和过滤经文，根据您的喜好。
---

> 快速过滤：在右侧的 <span class="material-icons-outlined">visibility</span> 菜单中点击一个标签，只显示该标签的经文。长按它以排除它。

### 创建或编辑标签 {#create}
从左侧的主菜单中选择“编辑标签”，以创建、编辑或删除标签。
最近删除的标签可以从[回收站](https://web.remem.me/labels/bin)在线恢复。

### 将标签附加到经文 {#attach}
通过点击它们的徽章（左侧的圆角方块）选择一个或多个经文。点击顶部上下文菜单中的<span class="material-icons">label</span>标签图标。检查您想要附加到所选经文的标签，取消检查您想要从所选经文中移除的标签，然后点击按钮保存更改（<span class="material-icons">save</span>
磁盘图标）。

### 通过标签过滤经文 {#filter}
标签分组的经文可见性在右侧的标签抽屉中设置。您可以通过点击或长按它们来改变标签/组的状态。图标有以下含义。

<span class="material-icons-outlined">visibility_off</span> **划掉的眼睛**：带有此标签的经文总是隐藏的。

<span class="material-icons-outlined">visibility</span> **轮廓的眼睛**：除非某些标签处于独占模式，否则带有此标签的经文可见。

<span class="material-icons">visibility</span> **填充的眼睛**：带有此标签的经文可见，所有其他经文隐藏。

一些自动过滤器和过滤设置可用。

<span class="material-icons-outlined">visibility</span> **今日复习**：此过滤器过滤今天复习的经文（取决于其他过滤设置）

<span class="material-icons-outlined">apps</span> **所有经文**：点击此选项会更改标签模式，以便所有经文都可见。

<span class="material-icons-outlined">label_off</span> **未标记的经文**：点击此选项会更改标签模式，以便只有未标记的经文可见。

### 一览表中的所有经文 {#all}
在某些情况下，将所有经文视为一个列表可能会有所帮助，例如，导出您的完整收藏到文件（见下文）。您可以通过在右侧的<span class="material-icons-outlined">visibility</span>可见性抽屉中切换<span class="material-icons-outlined">layers</span>盒子关闭并选择自动过滤器<span class="material-icons">apps</span>“所有经文”来实现这一点。

### 将经文作为文件（CSV）导出/导入 {#export}
打开文件菜单（<span class="material-symbols-outlined">file_open</span>三个点，仅限移动应用）在右侧的可见性栏 <span class="material-icons-outlined">visibility</span>，并选择“导出到文件”。这将创建一个新的CSV格式文件，其中包含当前显示的诗句。您可以与其他应用共享导出的文件，以保存它或发送给他人。 CSV文件可以用电子表格程序打开。

通过选择“从文件导入”选项，您可以从存储在您的设备上的文件中加载经文。 
