---
title: "捐款"
linkTitle: "捐款"
weight: 25
description: >
  纪念我是由Poimena发行的，这是一家非营利组织，致力于提供促进灵性成长的免费服务。
---

### 创始人

Poimena由Rev. Peter Schaffluetzel和Rev. Regula Studer Schaffluetzel创立。

### 地址

Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Switzerland

### 银行转账

请使用以下信息进行银行转账捐款。
转账信息中请加入您的电子邮件地址，以便我们表示感谢。
| | |
|-------------------|-------------------------|
| 银行名称:         | Raiffeisen, Switzerland |
| 收款人:           | Poimena |
| Swift代码 (BIC):  | RAIFCH22XXX |
| 账户IBAN:         | CH74 8080 8006 2918 4731 8 |

### PayPal/信用卡

如果您偏好使用PayPal或信用卡进行捐款，请使用以下按钮。
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="NHMVF5RBNH3TE" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - 更安全、更简单的线上支付方式！" alt="通过PayPal按钮捐款" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>


