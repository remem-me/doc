---
title: "音频功能"
linkTitle: "音频功能"
weight: 12
description: >
  记念我可以为您朗读经文或录制您的背诵并回放给您。
---

> 反复听经文支持您的记忆过程。在复习一段经文时录制您的声音可以加强您对它的理解。

### 听一盒经文 {#listen-box}

要开始听经文，选择您想听的盒子。
听您的"新"盒子中的经文让您熟悉它们。
听您的"已知的"盒子中的经文可以刷新您的知识。
按下<span class="material-icons">play_arrow</span>播放按钮开始听。
使用底部的按钮，您可以暂停、停止或跳过一段经文。

您可以让记念我重复所有经文或单个经文，或者使用控制按钮旁的设置按钮
(<span class="material-icons">settings</span> 齿轮)来减慢速度。

如果您想在段落后包含参考信息或按主题听经文，请在“账户”中激活相应的设置。

### 从当前经文开始 
面对一张抽认卡时，您可以通过按右上角的<span class="material-icons">play_arrow</span>播放按钮开始播放。
返回到经文列表后，当前经文仍然标记为播放或暂停。如果您现在点击列表的播放按钮，应用程序将从列表中那个位置开始朗读。

{{% alert title="文本转语音设置" color="secondary" %}}
<span class="material-icons">speed</span> 语速  
<span class="material-icons">pause_presentation</span> 暂停（秒）  
<span class="material-icons">av_timer</span> 睡眠计时器（以分钟为单位）  
<span class="material-icons">playlist_play</span> 开启：播放整个列表 / 关闭：仅播放当前经文  
<span class="material-icons">repeat</span> 重复列表（或经文）  
{{% /alert %}}

### 在学习经文时倾听 {#listen-study}
在学习游戏中（参见学习），
记念我会朗读您揭示的每个单词或句子。您可以使用游戏中的设置菜单
(<span class="material-icons">settings</span> 齿轮) 来关闭语音。

### 录制并回放您自己的背诵 {#record}
您的“到期”盒子中的抽认卡（参见复习经文）在底部有一个
<span class="material-icons">mic</span> 麦克风按钮。
点击它来录制您的经文背诵。再次按下按钮或翻转卡片会停止录音。
揭示经文后，播放开始，使您更容易检查您的背诵是否正确。

### 更改声音
记念我使用您设备上的文本转语音引擎。您可以在以下位置找到更改声音的说明

* [iPhone/iPad帮助](https://support.apple.com/zh-cn/guide/iphone/iph96b214f0/ios#iph938159887)
转到设置 > 辅助功能 > 朗读内容 > 语音：选择一个语音和方言，下载并设置为默认。
* [Android帮助](https://support.google.com/accessibility/android/answer/6006983?hl=zh-cn)

对于Android设备，有多个文本转语音引擎可用，例如：

* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Google语音服务](https://play.google.com/store/apps/details?id=com.google.android.tts)

在Windows或MacOS上使用网页应用程序时，您可以在记念我的语音设置中更改语音。您可能需要先在设备上安装语音数据。

