---
title: "学习和复习圣经记忆经文"
linkTitle: "学习和复习"
weight: 6
description: >
  记念我的游戏和智能复习系统帮助您记住经文并保持记忆。
---
### 学习过程 {#process}
记忆经文在「新」、「到期」和「已知的」框之间移动。
您可以通过点击活动标签 <img src="/images/triangle.svg"> 来更改框的排序方式。

|框:|<span class="material-icons">inbox</span><br>新|&rarr;|<span class="material-icons">check_box_outline_blank</span><br>到期|&rarr;|<span class="material-icons">check_box</span><br>已知的|
 |:-|:-:|:-:|:-:|:-:|:-:|
|动作:|1. **学习**|2. 确认|3. **复习**<br>**经文**|4. 回忆|5. **复习**<br>**参考**|
|按钮:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|<img src="/images/cards_outline.svg">|<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">|

1. **学习**新的经文。
2. 确认一个经文将其移至「到期」框。
3. **复习经文**。
4. 成功回忆一个经文将其移至「已知的」框。
5. **复习参考**随机选择的经文。

### 学习 {#studying}
添加经文后，您可以使用不同类型的游戏来学习。要开始学习，请点击该经文，然后点击学习符号 <span class="material-icons">school</span>。

<span class="material-icons">wb_cloudy</span> **混淆**: 隐藏一些单词，并尝试从记忆中填写经文中的缺失单词。点击被混淆的单词以显示它。

<span class="material-icons">extension</span> **拼图**: 通过点击正确的单词来构建经文。

<span class="material-icons">subject</span> **排列**: 通过点击左侧的图标（在底部工具栏上）来显示空行或首字母。通过点击 <span class="material-icons">plus_one</span> (+单词) 或 <span class="material-icons">playlist_add</span> (+行) 来进行进一步。在显示它之前试着回忆单词或行。

<span class="material-icons">keyboard</span> **打字**: 通过输入每个单词的第一个字母来构建经文。

将经文确认到记忆后，将卡片向右滑动，以将其移至「到期」列表。从现在开始，该经文将参与复习过程。

### 复习经文 {#passages}
选择框 "**到期**"。如果您想更改经文的顺序，请再次点击「到期」。有三种开始复习的方式：

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> outlined flashcard" >}}
开始多个复习的快闪卡。
{{< /card >}}
{{< card header="点击一个经文" >}}
开始单个快闪卡的复习。
{{< /card >}}
{{< card header="长按一个经文" >}}
直接在通道边打开卡片。
{{< /card >}}
{{< /cardpane >}}

当参考和主题显示时，从记忆中背诵经文，然后翻转卡片检查您是否正确。如果您喜欢逐行背诵经文（关于添加和编辑经文断行的详细信息，请参见 [添加和编辑经文](/zh-cn/docs/verses)），请为每行右下角的 <span class="material-icons">rule</span> 图标进行点击。

如果您需要一点帮助开始，长按卡片以获得提示。（您可以通过向下滑动来跳过一张卡片。）

如果您正确记得了经文，请向右滑动它以将其移至「已知的」框。这将使该经文的等级提高一级（直到账户设置中的复习限制被达到）。如果不太正确，请向左滑动。这将重置该经文的等级为 0。这听起来可能有些令人震惊，但对于经文的复习频率非常重要。等级低的经文会更频繁地出现进行复习。这有助于在经文被遗忘之前加强您对经文的记忆。间隔重复确保您经常重复挑战性的经文，但也不会忘记熟悉的经文。

{{% alert title="间隔重复" color="secondary" %}}
当大脑以递增间隔召回所学项目时，记忆效率最高。 记念我 使用指数算法计算间隔（与 Pimsleur 类似）。如果您的账户复习频率设置为 "正常"，您将在 1、2、4、8、16... 天后复习您的经文。算法的另一组成部分是在失败的复习后将经文的等级重置为 0（与 Leitner 类似）。这确保困难的经文比简单的经文更频繁地进行复习。
{{% /alert %}}

### 复习参考 {#references}
如果您想仅复习今天审核的参考，请在开始之前按一下标签 "[今天审核](/zh-cn/docs/labels/#filter)"。选择标签 "**已知的**"，将其排序设置为 "随机"，并在底部点击具有 <img src="/images/cards_filled.svg"> 填充的快闪卡图标的按钮。

记念我 洗牌并呈现其参考。试着从记忆中背诵参考，然后通过点击卡片来翻转它来检查您是否正确。如果是，请将卡片向右滑动。如果不是，请将其向左滑动。应用程序将在复习会话期间将卡片返回给您，直到您记住它。

您可以通过向下滑动来跳过一张卡片。与复习经文不同，应用程序不会跟踪您的复习进度。这种复习模式更像是您应该偶尔进行的测验，以刷新您对参考的知识。

{{% alert title="账户设置" color="secondary" %}}
- **每日目标**: 每天要审查的字数。
- **复习闪卡数量**: 每次段落复习会话的最大闪卡数量。
- **反向闪卡数量**：每个参考复习会话的最大快闪卡数量。
- **学习引用**：将参考包含在学习和复习经文中。
- **审查频率**：经文将多久出现进行复习。
- **间隔极限**：下一次复习的最大天数。间接限制可达到的等级数量。
  {{% /alert %}}


