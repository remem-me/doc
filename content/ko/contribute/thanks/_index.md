---
title: "기부해 주셔서 감사합니다!"
linkTitle: "감사합니다"
type: docs
toc_hide: true
hide_summary: true
description: >
  리멤버 미의 개발과 보급을 지원해 주셔서 진심으로 감사드립니다! 여러분의 기부는 저희에게 큰 축복과 격려가 됩니다. 하나님이 여러분을 축복하시길 바랍니다!
---

