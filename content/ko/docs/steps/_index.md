---
title: "기억할 다섯 가지 단계"
linkTitle: "기억할 5단계"
weight: 2
description: >
  리멤버 미를 사용하여 성경 구절을 암기하세요. 성경을 암기하는 기본 원칙을 일상 생활에 적용해 보세요.
---
<iframe width="256" height="456" src="https://www.youtube.com/embed/Gh3LgSgcgZo?si=j0Tk5gRc_KcwCrQA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen style="box-sizing: content-box; border-left: 2px solid rgba(1,1,1,0.3); border-right: 2px solid rgba(1,1,1,0.3); border-bottom: 2px solid rgba(1,1,1,0.3); border-radius: 16px;"></iframe>

### 1 성경 구절 추가하기.
리멤버 미 성경 앱은 기기에 성경 구절을 저장할 수 있는 여러 가지 옵션을 제공합니다. 
- 텍스트를 직접 입력하기
- 다양한 성경 번역본에서 구절 가져오기
- 다른 사용자의 성경 구절 모음 다운로드하기

### 2 성경 구절 암기하기.
여러 가지 방법을 사용하면 성경 구절을 암기하는 것이 재미있습니다.
- 구절을 들으세요.
- 무작위로 단어 숨기기
- 퍼즐로 만들기
- 단어의 첫 글자나 줄만 보여주기
- 각 단어의 첫 글자를 쓰기

### 3 주제 및 이미지 사용하기.
모든 사람이 숫자를 잘 기억하지는 못하지만, 텍스트를 암기하는 데 있어 그것은 꼭 필요하지 않습니다. 구절에 주제나 이미지를 추가하면 올바른 구절을 기억하는 데 도움이 됩니다.

### 4 암기한 구절 복습하기.
구절을 암기한 후에는 "예정된" 상자에 표시되어 복습할 수 있습니다. 복습을 위해 메모리 카드를 시작하세요. 암기하려는 성경 구절을 큰 소리로 말하고 카드를 뒤집어 제대로 기억했는지 확인하세요.

간격 반복은 새로 암기한 구절을 자주 복습하도록 하며, 이미 잘 알고 있는 구절도 잊지 않도록 도와줍니다.

### 5 시간을 잘 활용하기.
매 순간을 최대한 활용하세요. 성경 암기에 효과적인 앱은 양치질하는 동안 2분을 활용하여 암기할 수 있기 때문에 유용합니다. 앱을 항상 소지하고, 일상에서 자연스럽게 생기는 휴식 시간 동안 사용하세요.  