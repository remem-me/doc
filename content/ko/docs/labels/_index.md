---
title: "라벨별로 그룹화 및 필터링"
linkTitle: "라벨별 그룹화"
weight: 7
description: >
  라벨을 선택하거나 선택 해제하여 원하는 대로 구절을 숨기거나, 표시하거나, 필터할 수 있습니다.
---

> 빠른 필터링: 오른쪽의 <span class="material-icons-outlined">visibility</span> 메뉴에서 라벨을 탭하여 해당 라벨의 구절만 표시합니다. 길게 눌러서 제외할 수 있습니다.

### 라벨 생성 또는 편집 {#create}

왼쪽 메인 메뉴에서 "라벨 편집"을 선택하여 라벨을 생성, 편집, 삭제할 수 있습니다.
최근 삭제된 라벨은 [휴지통](https://web.remem.me/labels/bin)에서 온라인으로 복원할 수 있습니다.

### 구절에 라벨 첨부 {#attach}

구절의 배지(왼쪽의 둥근 사각형)를 탭하여 하나 이상의 구절을 선택합니다. 상단의 컨텍스트 메뉴에서 <span class="material-icons">label</span> 라벨 아이콘을 탭합니다. 선택한 구절에
첨부할 라벨을 선택하고, 선택한 구절에서 제거할 라벨의 선택을 해제한 다음, 변경 사항을 저장하기 위해 버튼을 탭합니다(<span class="material-icons">save</span>디스크 아이콘).

### 라벨별로 구절 필터링 {#filter}

라벨별로 그룹화된 구절의 가시성은 오른쪽의 라벨 서랍에서 설정됩니다. 라벨/그룹의 상태를 탭하거나 눌러서 변경할 수 있습니다. 아이콘의 의미는 다음과 같습니다.

<span class="material-icons-outlined">visibility_off</span> **가로줄이 그어진 눈**: 이 라벨이 있는 구절은 항상 숨겨집니다.

<span class="material-icons-outlined">visibility</span> **윤곽선이 있는 눈**: 이 라벨이 있는 구절은 일부 라벨이 독점 모드에 있지 않는 한 보입니다.

<span class="material-icons">visibility</span> **채워진 눈**: 이 라벨이 있는 구절은 보이며, 다른 모든 구절은 숨겨집니다.

일부 자동 필터와 필터 설정이 가능합니다.

<span class="material-icons-outlined">visibility</span> **오늘 리뷰한 구절**: 이 필터는 오늘 리뷰한 구절을 필터링합니다(다른 필터 설정에 따라 다름)

<span class="material-icons-outlined">apps</span> **모든 구절**: 이 옵션을 탭하면 모든 구절이 보이도록 라벨의 모드가 변경됩니다.

<span class="material-icons-outlined">label_off</span> **라벨이 없는 구절**: 이 옵션을 탭하면 라벨이 없는 구절만 보이도록 라벨의 모드가 변경됩니다.

### 하나의 목록에서 모든 구절 보기 {#all}

어떤 상황에서는 하나의 목록에서 모든 구절을 보는 것이 도움이 될 수 있습니다. 예를 들어, 전체 컬렉션을 파일로 내보내기 위해서입니다(아래 참조).
오른쪽의 <span class="material-icons-outlined">visibility</span> 가시성 서랍에서 <span class="material-icons-outlined">
layers</span> 상자를 끄고 자동 필터 <span class="material-icons">apps</span> "모든 구절"을 선택함으로써 이를 달성할 수 있습니다.

### 파일(CSV)로 구절 내보내기/가져오기 {#export}

파일 메뉴를 열어주세요 <span class="material-symbols-outlined">file_open</span> (모바일 앱 전용) 오른쪽에 있는 가시성
바 <span class="material-icons-outlined">visibility</span>에서 "파일로 내보내기"를 선택하세요. 이렇게 하면 현재 표시된 구절이 포함된 새 CSV 형식 파일이 생성됩니다.
내보낸 파일을 다른 앱과 공유하여 저장하거나 다른 사람에게 보낼 수 있습니다. CSV 파일은 스프레드시트 프로그램으로 열 수 있습니다.

"파일에서 가져오기" 옵션을 선택하면 기기에 저장된 파일에서 구절을 가져올 수 있습니다.
