---
title: "기부"
linkTitle: "기부"
weight: 25
description: >
  리멤버 미는 정신적 성장을 전념하는 무료 서비스를 제공하는 비영리 조직인 포이메나에 의해 출판되었습니다.
---
### 창립자
포이메나는 Rev. Peter Schaffluetzel과 Rev. Regula Studer Schaffluetzel에 의해 창립되었습니다.

### 주소
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Switzerland

### 은행 송금
아래 정보를 사용하여 은행 송금으로 기부하십시오.
송금 메시지에 이메일 주소를 추가하여 저희가 감사의 인사를 드릴 수 있도록 해주세요.
|                   |                         |
|-------------------|-------------------------|
| 은행 이름:        | Raiffeisen, Switzerland |
| 수취인:           | Poimena                 |
| 스위프트 코드 (BIC): | RAIFCH22XXX             |
| 계좌 IBAN:        | CH74 8080 8006 2918 4731 8 |

### PayPal/신용카드
PayPal이나 신용카드로 기부를 선호하시면 아래 버튼을 사용해주세요.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="NHMVF5RBNH3TE" />
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - 온라인에서 더 안전하고 쉽게 지불하는 방법!" alt="PayPal 버튼으로 기부하기" />
<img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
</form>


