---
title: "오디오 기능"
linkTitle: "오디오 기능"
weight: 12
description: >
  리멤버 미는 당신에게 구절을 읽어주거나 당신의 암송을 녹음하여 다시 재생할 수 있습니다.
---

> 구절을 반복해서 듣는 것은 암기 과정을 지원합니다. 구절을 검토하면서 목소리를 녹음하는 것은 그것에 대한 지식을 강화합니다.

### 구절 상자 듣기 {#listen-box}

구절을 듣기 시작하려면, 듣고 싶은 상자를 선택하세요.
"신규" 상자의 구절을 듣는 것은 그것들에 익숙해지게 합니다.
"기억" 상자의 구절을 듣는 것은 당신의 지식을 새롭게 합니다.
<span class="material-icons">play_arrow</span> 재생 버튼을 눌러 듣기를 시작하세요.
아래쪽의 버튼을 사용하여 구절을 일시 정지, 정지, 또는 건너뛸 수 있습니다.

모든 구절이나 단일 구절을 반복하게 하거나 속도를 늦추려면 제어 버튼 옆의 설정 버튼
(<span class="material-icons">settings</span> 톱니바퀴)을 사용하세요.

구절 뒤에 참조를 포함시키거나 주제별로 구절을 듣고 싶다면, "계정"에서 해당 설정을 활성화하세요.

### 현재 구절로 시작하기 {#listen-verse}

당신 앞에 플래시카드가 있을 때, 오른쪽 상단의 <span class="material-icons">play_arrow</span> 재생 버튼을 눌러 재생을 시작할 수 있습니다.
구절 목록으로 돌아간 후에도 현재 구절은 재생 중이거나 일시 정지된 것으로 표시됩니다.
이제 목록의 재생 버튼을 탭하면, 앱은 그 위치에서 목록을 읽기 시작합니다.

{{% alert title="음성-텍스트 설정" color="secondary" %}}
<span class="material-icons">speed</span> 말하기 속도  
<span class="material-icons">pause_presentation</span> 일시 정지 (초 단위)  
<span class="material-icons">av_timer</span> 취침 타이머 (분 단위)  
<span class="material-icons">playlist_play</span> *켜짐*: 전체 목록 재생 / *꺼짐*: 현재 구절만 재생  
<span class="material-icons">repeat</span> 목록(또는 구절) 반복
{{% /alert %}}

### 구절 공부하면서 듣기 {#listen-study}

공부 게임 중에 (참조 [공부하기](/docs/study/#studying)),
리멤버 미는 당신이 드러낸 각 단어나 문장을 읽어줍니다. 설정 메뉴에서 음성을 끌 수 있습니다
(<span class="material-icons">settings</span> 톱니바퀴) 게임 중에.

### 자신의 암송 녹음 및 재생하기 {#record}

"기한" 상자의 플래시카드에는 (참조 [구절 복습하기](/docs/study/#passages)) 하단에
<span class="material-icons">mic</span> 마이크 버튼이 있습니다.
그것을 탭하여 구절의 암송을 녹음하세요. 버튼을 다시 누르거나 카드를 뒤집으면 녹음이 중지됩니다.
구절을 드러낸 후, 재생이 시작되어 당신의 암송이 정확했는지 확인하기 쉽게 해줍니다.

### 목소리 변경하기 {#voice}

{{% alert color="secondary" %}}
계정 설정의 *구절 언어* 및 *참조 언어*에서 음성의 **언어**를 변경할 수 있습니다.
{{% /alert %}}

리멤버 미는 당신의 기기의 텍스트-음성 엔진을 사용합니다. 목소리를 변경하는 방법에 대한 지침은 다음에서 찾을 수 있습니다.

* [아이폰/아이패드 도움말](https://support.apple.com/ko-kr/guide/iphone/iph96b214f0/ios#iph938159887)  
  설정 > 접근성 > 말하기 내용 > 목소리로 이동: 목소리와 방언을 선택하고, 다운로드한 다음, 기본값으로 설정합니다.
* [안드로이드 도움말](https://support.google.com/accessibility/android/answer/6006983?hl=ko)

안드로이드 기기의 경우, 다양한 텍스트-음성 엔진이 사용 가능합니다. 예를 들어,

* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [삼성 TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Google의 음성 서비스](https://play.google.com/store/apps/details?id=com.google.android.tts)

윈도우(Windows) 또는 맥오에스(macOS)에서 웹 앱을 사용할 때 리멤버 미의 음성 설정에서 음성을 변경할 수 있습니다. 먼저 기기에 음성 데이터를 설치해야 할 수도 있습니다.

