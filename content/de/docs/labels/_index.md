---
title: "Nach Schlagworten gruppieren und filtern"
linkTitle: "Schlagworte & Filtern"
weight: 7
url: "/de/docs/schlagworte"
aliases:
  - /de/docs/labels
  - /de/docs/nach-kategorien-gruppieren
  - /de/docs/nach-labels-gruppieren
  - /de/docs/kategorien
  - /wie-rm-funktioniert/sets
description: >
  Das Wählen und Abwählen von Schlagworten erlaubt, Verse nach Belieben ein-, auszublenden und zu filtern. 
---

> Schnelles Filtern: Tippen Sie auf ein Schlagwort im <span class="material-icons-outlined">visibility</span> Menü
> rechts, um ausschliesslich deren Verse anzuzeigen. Drücken Sie lange darauf, um sie auszuschliessen.

### Ein Schlagwort erstellen oder bearbeiten {#erstellen}

Wählen Sie "Schlagworte bearbeiten" aus dem Hauptmenü links, um ein Schlagwort zu erstellen, zu bearbeiten oder zu
löschen.
Kürzlich gelöschte Schlagworte können online aus dem [Papierkorb](https://web.remem.me/labels/bin) wiederhergestellt
werden.

### Schlagworte an Verse heften {#anheften}

Wählen Sie einen oder mehrere Verse, indem Sie auf ihre Plakette tippen (abgerundetes Quadrat links).
Tippen Sie auf das <span class="material-icons">label</span> Schlagwort-Symbol im Kontextmenü oben. Setzen Sie Häkchen
bei Schlagworten, die Sie anfügen, und entfernen Sie Häkchen bei Schlagworten, die Sie von den gewählten Versen lösen
möchten. Tippen Sie auf die Schaltfläche, um die Änderungen zu speichern (<span class="material-icons">save</span>
Disketten-Symbol).

### Verse nach Schlagwort filtern {#filtern}

Die Sichtbarkeit von mit Schlagworten gruppierten Versen wird im Schlagwortmenü (siehe oben) gesetzt. Sie können den
Zustand der Schlagworte/Gruppen ändern, indem Sie auf sie tippen oder drücken. Die Symbole haben folgende Bedeutungen:

<span class="material-icons-outlined">visibility_off</span> **Durchgestrichenes Auge**: Verse mit diesem Schlagwort
werden immer ausgeblendet.

<span class="material-icons-outlined">visibility</span> **Umrandetes Auge**: Verse mit diesem Schlagwort werden
eingeblendet, falls kein anderes Schlagwort exklusiv angezeigt wird.

<span class="material-icons">visibility</span> **Gefülltes Auge**: Verse mit diesem Schlagwort werden eingeblendet,
während alle anderen Verse ausgeblendete werden.

Ein paar automatische Filter und Filter-Einstellungen sind verfügbar:

<span class="material-icons-outlined">visibility</span> **Heute repetiert**: Dieser Filter filtert heute repetierte
Verse (abhängig von anderen Filtereinstellungen)

<span class="material-icons-outlined">apps</span> **Alle Verse**: Tippen auf diese Option ändert die Zustände der
Schlagworte so, dass alle Verse eingeblendet werden.

<span class="material-icons-outlined">label_off</span> **Verse ohne Schlagwort**: Tippen auf diese Option ändert die
Zustände der Schlagworte so, dass nur Verse ohne Schlagwort eingeblendet werden.

### Alle Verse in derselben Liste {#alle}

In manchen Situationen kann es hilfreich sein, alle Verse in einer einzigen Liste anzuzeigen, z. B. zum Exportieren
Ihrer kompletten Sammlung in eine Datei (siehe unten). Sie können dies in der <span class="material-icons-outlined">
visibility</span> Sichtbarkeitsleiste auf der rechten Seite erreichen, indem Sie die
<span class="material-icons-outlined">layers</span> Fächer ausschalten und den automatischen Filter
<span class="material-icons">apps</span> "Alle Verse" anwählen.

### Verse als Datei exportieren/importieren (CSV) {#exportieren}

Öffnen Sie das Dateimenü <span class="material-symbols-outlined">file_open</span> (nur in der mobilen App) in
der <span class="material-icons-outlined">visibility</span> Sichtbarkeitsleiste auf der rechten Seite und wählen Sie "In
Datei exportieren". Dadurch wird eine neue Datei im CSV-Format mit den aktuell angezeigten
Versen erstellt. Sie können die exportierte Datei mit anderen Apps teilen, um sie zu speichern oder an jemand anderen
zu senden. CSV-Dateien können mit Tabellenkalkulationsprogrammen geöffnet werden.

Mit der Option "Aus Datei importieren" können Sie Verse aus einer auf Ihrem Gerät gespeicherten Datei laden. 