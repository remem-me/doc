---
title: "Gebetskarten und Andachten verwalten"
linkTitle: "Gebetskarten & Andachten"
weight: 18
url: "/de/docs/gebete"
aliases:
  - /de/docs/gebetskarten-verwalten
description: >
  Die Fächer von Remember Me helfen beim Verwalten von Gebetsvorlagen und anderen Texten, die Sie regelmässig lesen oder hören wollen.
---

### Ein Gebetskonto einrichten

Erstellen Sie für Texte, die Sie nicht auswendig lernen, sondern regelmässig lesen möchten, ein eigenes Konto. Da die
Stellenangabe vermutlich eine untergeordnete Rolle spielt, können Sie beim Einrichten des Kontos die Option "Nach Thema
lernen" auswählen.

### Gebete/Andachten hinzufügen

Sie können im Fach "Neu" selber Texte hinzufügen oder eine Sammlung von Texten unter "Sammlungen" importieren. Als
Stellenangabe bietet sich eine Art von Nummerierung an. Wenn Sie z.B. eine Sammlung von längeren Gebeten - wie die
Bücher von Stormie Omartian - in einzelne Gebetskarten aufteilen, könnte die Stellenangaben 1a, 1b, 1c, 2a, 2b etc.
lauten. Stellenangaben dienen der Sortierung der Karten. Hilfreich ist zudem, wenn Sie zu jedem Gebet ein Thema angeben.

### Täglich beten

Sortieren Sie die Gebete nach Thema oder nach Stellenangabe (alphabetisch), indem Sie ein zweites Mal auf den Reiter "
Neu" tippen. Am besten sprechen Sie die Gebete laut und in ihrem eigenen Dialekt oder mit ihren eigenen Worten. So
werden auch vorgefasste Gebet zu ihrem persönlichen Gespräch mit Gott.
Notieren Sie Eindrücke und Gedanken, die Ihnen während des Betens wichtig werden, unterhalb des Gebetstextes (siehe [Vers
bearbeiten](/de/docs/verse/#bearbeiten)). So wird Ihr Gebetskonto auch zu Ihrem Gebetstagebuch und widerspiegelt einen Teil Ihrer Kommunikation mit
Gott.
Verschieben Sie das Gebet nach ihrer täglichen Andacht ins Fach "Fällig", um den Überblick über Ihren Gebetsfortschritt
zu behalten. Das Fach "Erinnert" wird für Gebete nicht benötigt.

### Eine Gebetssammlung wiederverwenden

Wenn Sie mit einer Gebetssammlung nach deren Vollendung wieder von vorne beginnen möchten, wählen Sie ein Gebet aus (auf
seine Plakette links tippen), dann "alle auswählen", und verschieben Sie die ganze Sammlung wieder ins Fach "Neu".





