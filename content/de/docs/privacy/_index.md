---
title: "Datenschutzrichtlinie"
linkTitle: "Datenschutzrichtlinie"
weight: 21
url: "/de/docs/datenschutz"
aliases:
  - /de/docs/privacy
  - /de/docs/datenschutzrichtlinie
description: >
  So schützen wir Ihre Privatsphäre.
---

{{% pageinfo %}}
**DATENSCHUTZBESTIMMUNGEN**

**Letztes Update 25. März 2023**

Diese Datenschutzerklärung für Poimena ("**Organisation**", "**wir**", "**uns**" oder "**unser**") beschreibt, wie und warum wir Ihre Daten erheben, speichern, nutzen und/oder weitergeben ("**Verarbeitung**"), wenn Sie unsere Dienstleistungen ("**Dienstleistungen**") nutzen, z. B. wenn Sie:

* unsere Website unter [https://web.remem.me](https://web.remem.me) besuchen

* unsere mobile Anwendung herunterladen und nutzen (Remember Me. Bible Memory Joy)

**Fragen oder Bedenken?** Die Lektüre dieser Datenschutzerklärung wird Ihnen helfen, Ihre Datenschutzrechte und Wahlmöglichkeiten zu verstehen. Wenn Sie mit unseren Richtlinien und Praktiken nicht einverstanden sind, nutzen Sie unsere Dienste bitte nicht. Sollten Sie noch Fragen oder Bedenken haben, kontaktieren Sie uns bitte unter support@remem.me.


**1\. WELCHE INFORMATIONEN SAMMELN WIR?**

**Persönliche Informationen, die Sie uns zur Verfügung stellen**

**_Kurz gefasst:_** _Wir sammeln personenbezogene Daten, die Sie uns zur Verfügung stellen._

Wir erfassen personenbezogene Daten, die Sie uns freiwillig zur Verfügung stellen, wenn Sie sich bei den Diensten registrieren, ein Interesse an Informationen über uns oder unsere Produkte und Dienste bekunden, wenn Sie an Aktivitäten in den Diensten teilnehmen oder wenn Sie uns anderweitig kontaktieren.

**Von Ihnen bereitgestellte personenbezogene Daten** Die von uns erfassten personenbezogenen Daten hängen vom Kontext Ihrer Interaktionen mit uns und den Diensten, den von Ihnen getroffenen Entscheidungen und den von Ihnen verwendeten Produkten und Funktionen ab. Zu den personenbezogenen Daten, die wir erfassen, können die folgenden gehören:

* E-Mail-Adressen

* Passwörter

**Sensible Daten.** Wir verarbeiten keine sensiblen Daten.

Alle personenbezogenen Daten, die Sie uns zur Verfügung stellen, müssen wahrheitsgemäß, vollständig und genau sein, und Sie müssen uns über alle Änderungen dieser personenbezogenen Daten informieren.


**2\. WIE VERARBEITEN WIR IHRE DATEN?**

**_Kurz gefasst:_** _Wir verarbeiten Ihre Daten, um unsere Dienste bereitzustellen, zu verbessern und zu verwalten, um mit Ihnen zu kommunizieren, um Sicherheit und Betrug vorzubeugen und um Gesetze einzuhalten. Mit Ihrer Zustimmung können wir Ihre Daten auch für andere Zwecke verarbeiten._

**Wir verarbeiten Ihre personenbezogenen Daten aus einer Vielzahl von Gründen, je nachdem, wie Sie mit unseren Diensten interagieren, einschließlich:**

* **Um die Erstellung eines Kontos und die Authentifizierung zu erleichtern und Benutzerkonten anderweitig zu verwalten.** Wir können Ihre Daten verarbeiten, damit Sie Ihr Konto erstellen und sich bei ihm anmelden können, sowie um Ihr Konto in Ordnung zu halten.

* **Um Dienstleistungen für den Nutzer zu erbringen und deren Erbringung zu erleichtern.** Wir können Ihre Daten verarbeiten, um Ihnen die angeforderte Dienstleistung zur Verfügung zu stellen.

**Um lebenswichtige Interessen einer Person zu wahren oder zu schützen.** Wir können Ihre Daten verarbeiten, wenn dies notwendig ist, um lebenswichtige Interessen einer Person zu wahren oder zu schützen, z.B. um Schaden abzuwenden.


**3\. AUF WELCHE RECHTSGRUNDLAGEN STÜTZEN WIR UNS BEI DER VERARBEITUNG IHRER DATEN?**

**_Kurz gefasst:_** _Wir verarbeiten Ihre personenbezogenen Daten nur, wenn wir glauben, dass es notwendig ist und wir einen gültigen rechtlichen Grund (d.h. eine Rechtsgrundlage) dafür haben, dies unter geltendem Recht zu tun, wie z.B. mit Ihrer Zustimmung, um Gesetze einzuhalten, um Ihnen Dienstleistungen zu erbringen, um unsere vertraglichen Verpflichtungen einzugehen oder zu erfüllen, um Ihre Rechte zu schützen oder um unsere legitimen Geschäftsinteressen zu erfüllen._

Wenn Sie sich in der EU oder in Großbritannien befinden, gilt dieser Abschnitt für Sie.**_

Die General Data Protection Regulation (GDPR) und die UK GDPR verlangen von uns, die gültigen Rechtsgrundlagen zu erläutern, auf die wir uns stützen, um Ihre persönlichen Daten zu verarbeiten. So können wir uns auf die folgenden Rechtsgrundlagen stützen, um Ihre personenbezogenen Daten zu verarbeiten:

* **Zustimmung.** Wir können Ihre Daten verarbeiten, wenn Sie uns die Erlaubnis (d.h. die Zustimmung) gegeben haben, Ihre personenbezogenen Daten für einen bestimmten Zweck zu verwenden. Sie können Ihre Zustimmung jederzeit widerrufen.

* **Vertragserfüllung.** Wir können Ihre personenbezogenen Daten verarbeiten, wenn wir glauben, dass dies notwendig ist, um unsere vertraglichen Verpflichtungen Ihnen gegenüber zu erfüllen, einschließlich der Bereitstellung unserer Dienstleistungen oder auf Ihren Wunsch hin, bevor wir einen Vertrag mit Ihnen abschließen.

**Rechtliche Verpflichtungen.** Wir können Ihre Daten verarbeiten, wenn wir glauben, dass dies für die Erfüllung unserer rechtlichen Verpflichtungen notwendig ist, z.B. um mit einer Strafverfolgungsbehörde oder einer Aufsichtsbehörde zu kooperieren, unsere gesetzlichen Rechte auszuüben oder zu verteidigen oder Ihre Daten als Beweismittel in einem Rechtsstreit, an dem wir beteiligt sind, offenzulegen.

**Vitale Interessen.** Wir können Ihre Daten verarbeiten, wenn wir glauben, dass dies notwendig ist, um Ihre vitalen Interessen oder die vitalen Interessen eines Dritten zu schützen, wie z.B. in Situationen, die eine potentielle Bedrohung für die Sicherheit einer Person darstellen.

**_Wenn Sie sich in Kanada befinden, gilt dieser Abschnitt für Sie._**

Wir können Ihre Daten verarbeiten, wenn Sie uns die ausdrückliche Erlaubnis erteilt haben, Ihre persönlichen Daten für einen bestimmten Zweck zu verwenden (d.h. ausdrückliche Zustimmung), oder in Situationen, in denen Ihre Zustimmung abgeleitet werden kann (d.h. stillschweigende Zustimmung). Sie können Ihre Zustimmung jederzeit widerrufen.

In einigen Ausnahmefällen kann es uns nach geltendem Recht rechtlich gestattet sein, Ihre Daten ohne Ihre Zustimmung zu verarbeiten, z.B:

* Wenn die Erhebung eindeutig im Interesse einer Person liegt und die Zustimmung nicht rechtzeitig eingeholt werden kann

* Für Ermittlungen und zur Aufdeckung und Verhinderung von Betrug

* Für geschäftliche Transaktionen, sofern bestimmte Bedingungen erfüllt sind

* Wenn die Daten in einer Zeugenaussage enthalten sind und die Erhebung zur Beurteilung, Bearbeitung oder Regulierung eines Versicherungsanspruchs erforderlich ist

* Zur Identifizierung von verletzten, kranken oder verstorbenen Personen und zur Kommunikation mit den nächsten Angehörigen

* Wenn wir begründeten Anlass zu der Annahme haben, dass eine Person Opfer von finanziellem Missbrauch war, ist oder sein könnte

* Wenn vernünftigerweise zu erwarten ist, dass die Erhebung und Verwendung mit Zustimmung die Verfügbarkeit oder die Richtigkeit der Informationen beeinträchtigen würde und die Erhebung für Zwecke im Zusammenhang mit der Untersuchung eines Vertragsbruchs oder eines Verstoßes gegen die Gesetze Kanadas oder einer Provinz angemessen ist

* Wenn die Offenlegung erforderlich ist, um einer Vorladung, einem Durchsuchungsbefehl, einem Gerichtsbeschluss oder den Regeln des Gerichts in Bezug auf die Vorlage von Unterlagen nachzukommen

* Wenn sie von einer Person im Rahmen ihrer Beschäftigung, ihres Geschäfts oder ihres Berufs erstellt wurden und die Sammlung mit den Zwecken, für die die Informationen erstellt wurden, übereinstimmt

* Wenn die Sammlung ausschließlich zu journalistischen, künstlerischen oder literarischen Zwecken erfolgt.

* Wenn die Informationen öffentlich zugänglich sind und in den Vorschriften festgelegt sind


**4\. WANN UND MIT WEM GEBEN WIR IHRE PERSÖNLICHEN DATEN WEITER?**

**_Kurz gefasst:_** _Wir können Informationen in bestimmten, in diesem Abschnitt beschriebenen Situationen und/oder mit den folgenden Dritten teilen._

Es kann sein, dass wir Ihre persönlichen Daten in den folgenden Situationen weitergeben müssen:

* **Geschäftsübertragungen.** Wir können Ihre Daten in Verbindung mit oder während der Verhandlungen über eine Fusion, einen Verkauf von Organisationsvermögen, eine Finanzierung oder eine Übernahme unseres gesamten oder eines Teils unseres Geschäfts durch eine andere Organisation weitergeben oder übertragen.


**5\. WIE LANGE BEWAHREN WIR IHRE DATEN AUF?**

**_Kurz gefasst:_** _Wir bewahren Ihre Daten so lange auf, wie es für die Erfüllung der in dieser Datenschutzerklärung genannten Zwecke erforderlich ist, es sei denn, dies ist gesetzlich vorgeschrieben._

Wir werden Ihre personenbezogenen Daten nur so lange aufbewahren, wie es für die in dieser Datenschutzerklärung genannten Zwecke erforderlich ist, es sei denn, eine längere Aufbewahrungsfrist ist gesetzlich vorgeschrieben oder zulässig (z.B. aus steuerlichen, buchhalterischen oder anderen rechtlichen Gründen). Für keinen der in dieser Mitteilung genannten Zwecke ist es erforderlich, dass wir Ihre personenbezogenen Daten länger aufbewahren als für den Zeitraum, in dem die Nutzer ein Konto bei uns haben.

Wenn wir keine weitere legitime geschäftliche Notwendigkeit haben, Ihre persönlichen Daten zu verarbeiten, werden wir diese Daten entweder löschen oder anonymisieren, oder, falls dies nicht möglich ist (z.B. weil Ihre persönlichen Daten in Backup-Archiven gespeichert wurden), werden wir Ihre persönlichen Daten sicher aufbewahren und von jeder weiteren Verarbeitung isolieren, bis eine Löschung möglich ist.


**6\. WIE BEWAHREN WIR IHRE DATEN SICHER AUF?**

**_Kurz gefasst:_** _Wir sind bestrebt, Ihre persönlichen Daten durch ein System von organisatorischen und technischen Sicherheitsmaßnahmen zu schützen._

Wir haben geeignete und angemessene technische und organisatorische Sicherheitsmaßnahmen getroffen, um die Sicherheit der von uns verarbeiteten personenbezogenen Daten zu schützen. Trotz unserer Sicherheitsvorkehrungen und Bemühungen, Ihre Daten zu schützen, kann jedoch nicht garantiert werden, dass die elektronische Übertragung über das Internet oder die Informationsspeicherung zu 100 % sicher ist. Daher können wir nicht versprechen oder garantieren, dass Hacker, Cyberkriminelle oder andere unbefugte Dritte nicht in der Lage sein werden, unsere Sicherheitsvorkehrungen zu überwinden und Ihre Daten unrechtmäßig zu sammeln, darauf zuzugreifen, sie zu stehlen oder zu verändern. Obwohl wir unser Bestes tun, um Ihre persönlichen Daten zu schützen, erfolgt die Übertragung von persönlichen Daten zu und von unseren Diensten auf Ihr eigenes Risiko. Sie sollten nur innerhalb einer sicheren Umgebung auf die Dienste zugreifen.


**7\. WAS SIND IHRE DATENSCHUTZRECHTE?**

**_Kurz gefasst:_** _In einigen Regionen, wie z.B. dem Europäischen Wirtschaftsraum (EWR), dem Vereinigten Königreich (UK) und Kanada, haben Sie Rechte, die Ihnen mehr Zugang zu und Kontrolle über Ihre persönlichen Daten ermöglichen. Sie können Ihr Konto jederzeit überprüfen, ändern oder kündigen._

In einigen Regionen (wie dem EWR, dem Vereinigten Königreich und Kanada) haben Sie nach den geltenden Datenschutzgesetzen bestimmte Rechte. Dazu gehört das Recht, (i) Zugang zu Ihren personenbezogenen Daten zu verlangen und eine Kopie davon zu erhalten, (ii) die Berichtigung oder Löschung zu verlangen, (iii) die Verarbeitung Ihrer personenbezogenen Daten einzuschränken und (iv) gegebenenfalls die Datenübertragbarkeit zu verlangen. Unter bestimmten Umständen haben Sie auch das Recht, der Verarbeitung Ihrer personenbezogenen Daten zu widersprechen. Sie können einen solchen Antrag stellen, indem Sie sich über die unten angegebenen Kontaktdaten an uns wenden.

Wir werden jeden Antrag in Übereinstimmung mit den geltenden Datenschutzgesetzen prüfen und entsprechend handeln.

Wenn Sie sich im EWR oder im Vereinigten Königreich befinden und der Meinung sind, dass wir Ihre personenbezogenen Daten unrechtmäßig verarbeiten, haben Sie außerdem das Recht, sich bei Ihrer [Datenschutzbehörde des Mitgliedstaats](https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm) oder [Datenschutzbehörde des Vereinigten Königreichs](https://ico.org.uk/make-a-complaint/data-protection-complaints/data-protection-complaints/) zu beschweren.

Wenn Sie sich in der Schweiz befinden, können Sie sich an den [Eidgenössischen Datenschutz- und Öffentlichkeitsbeauftragten](https://www.edoeb.admin.ch/edoeb/de/home.html) wenden.

**Widerruf Ihrer Zustimmung:** Wenn wir uns auf Ihre Zustimmung zur Verarbeitung Ihrer personenbezogenen Daten verlassen, die je nach geltendem Recht eine ausdrückliche und/oder stillschweigende Zustimmung sein kann, haben Sie das Recht, Ihre Zustimmung jederzeit zu widerrufen. Sie können Ihre Zustimmung jederzeit widerrufen, indem Sie uns über die unten angegebenen Kontaktdaten kontaktieren oder Ihre Einstellungen aktualisieren.

Bitte beachten Sie jedoch, dass dies weder die Rechtmäßigkeit der Verarbeitung vor dem Widerruf berührt, noch, sofern dies nach geltendem Recht zulässig ist, die Verarbeitung Ihrer personenbezogenen Daten, die auf der Grundlage anderer rechtmäßiger Verarbeitungsgründe als der Einwilligung erfolgt.

**Kontoinformationen**

Wenn Sie zu irgendeinem Zeitpunkt die Informationen in Ihrem Konto überprüfen oder ändern oder Ihr Konto kündigen möchten, können Sie dies tun:

* Melden Sie sich bei Ihren Kontoeinstellungen an und aktualisieren Sie Ihr Benutzerkonto.

Auf Ihren Wunsch hin, Ihr Konto zu löschen, werden wir Ihr Konto und Ihre Informationen aus unseren aktiven Datenbanken deaktivieren oder löschen. Es kann jedoch sein, dass wir einige Informationen in unseren Dateien aufbewahren, um Betrug zu verhindern, Probleme zu beheben, bei Untersuchungen zu helfen, unsere rechtlichen Bestimmungen durchzusetzen und/oder geltende rechtliche Anforderungen zu erfüllen.

Wenn Sie Fragen oder Anmerkungen zu Ihren Datenschutzrechten haben, können Sie uns eine E-Mail an support@remem.me senden.


**8\. KONTROLLEN FÜR "DO-NOT-TRACK"-FUNKTIONEN**

Die meisten Webbrowser und einige mobile Betriebssysteme und Anwendungen enthalten eine Do-Not-Track (DNT)-Funktion oder -Einstellung, die Sie aktivieren können, um zu signalisieren, dass Sie keine Daten über Ihre Online-Browsing-Aktivitäten überwachen und sammeln lassen möchten. Zum gegenwärtigen Zeitpunkt ist noch kein einheitlicher Technologiestandard für die Erkennung und Umsetzung von DNT-Signalen festgelegt worden. Daher reagieren wir derzeit nicht auf DNT-Browsersignale oder andere Mechanismen, die automatisch mitteilen, dass Sie nicht online verfolgt werden möchten. Sollte ein Standard für das Online-Tracking verabschiedet werden, den wir in Zukunft befolgen müssen, werden wir Sie in einer überarbeiteten Version dieses Datenschutzhinweises über diese Praxis informieren.


**9\. HABEN EINWOHNER KALIFORNIENS BESONDERE DATENSCHUTZRECHTE?**

**_Kurz gefasst:_** _Ja, wenn Sie in Kalifornien ansässig sind, haben Sie bestimmte Rechte in Bezug auf den Zugang zu Ihren persönlichen Daten._

California Civil Code Section 1798.83, auch bekannt als 'Shine The Light'-Gesetz, erlaubt es unseren Nutzern, die in Kalifornien ansässig sind, einmal im Jahr kostenlos Informationen über die Kategorien personenbezogener Daten (falls vorhanden), die wir zu Direktmarketingzwecken an Dritte weitergegeben haben, sowie die Namen und Adressen aller Dritten, mit denen wir im unmittelbar vorangegangenen Kalenderjahr personenbezogene Daten ausgetauscht haben, anzufordern und von uns zu erhalten. Wenn Sie in Kalifornien ansässig sind und eine solche Anfrage stellen möchten, richten Sie diese bitte schriftlich an uns, indem Sie die unten angegebenen Kontaktinformationen verwenden.

Wenn Sie unter 18 Jahre alt sind, Ihren Wohnsitz in Kalifornien haben und über ein registriertes Konto bei den Diensten verfügen, haben Sie das Recht, die Entfernung von unerwünschten Daten zu verlangen, die Sie öffentlich in den Diensten veröffentlichen. Um die Entfernung solcher Daten zu beantragen, kontaktieren Sie uns bitte über die unten angegebenen Kontaktinformationen und geben Sie die mit Ihrem Konto verknüpfte E-Mail-Adresse sowie eine Erklärung an, dass Sie in Kalifornien wohnen. Wir werden dafür sorgen, dass die Daten nicht öffentlich in den Diensten angezeigt werden. Bitte beachten Sie jedoch, dass die Daten möglicherweise nicht vollständig oder umfassend aus allen unseren Systemen (z.B. Backups usw.) entfernt werden.

**CCPA Datenschutzhinweis**

Das kalifornische Gesetzbuch (California Code of Regulations) definiert einen 'Einwohner' als:

(1) jede Person, die sich im Bundesstaat Kalifornien nicht nur vorübergehend oder vorübergehend aufhält und

(2) jede Person, die ihren Wohnsitz im Staat Kalifornien hat und sich zu einem vorübergehenden oder vorübergehenden Zweck außerhalb des Staates Kalifornien aufhält.

Alle anderen Personen werden als 'Nicht-Einwohner' definiert.

Wenn diese Definition von 'ansässig' auf Sie zutrifft, müssen wir bestimmte Rechte und Pflichten in Bezug auf Ihre persönlichen Daten einhalten.

**Welche Kategorien von personenbezogenen Daten sammeln wir?**

Wir haben in den vergangenen zwölf (12) Monaten die folgenden Kategorien personenbezogener Daten erhoben:

| Kategorie | Beispiele | Gesammelt |
|----------------|---|-----------|
| A. Identifikatoren | Kontaktdaten, wie z. B. richtiger Name, Alias, Postanschrift, Telefon- oder Mobilfunknummer, eindeutige persönliche Kennung, Online-Kennung, Internet-Protokoll-Adresse, E-Mail-Adresse und Kontoname | JA        |
| B. Persönliche Datenkategorien, die im kalifornischen Gesetz über Kundendatensätze aufgeführt sind | Name, Kontaktinformationen, Ausbildung, Beschäftigung, Beschäftigungsgeschichte und finanzielle Informationen | NEIN      |
| C. Geschützte Klassifizierungsmerkmale nach kalifornischem oder Bundesrecht | Geschlecht und Geburtsdatum | NEIN      |
| D. Kommerzielle Informationen | Transaktionsinformationen, Kaufhistorie, finanzielle Details und Zahlungsinformationen | NEIN      |
| E. Biometrische Informationen | Fingerabdrücke und Stimmabdrücke | NEIN      |
| F. Internet- oder ähnliche Netzwerkaktivitäten | Browserverlauf, Suchverlauf, Online-Verhalten, Interessendaten und Interaktionen mit unseren und anderen Websites, Anwendungen, Systemen und Werbung | NEIN      |
| G. Geolokalisierungsdaten | Gerätestandort | NEIN      |
| H. Audio-, elektronische, visuelle, thermische, olfaktorische oder ähnliche Informationen | Bilder und Audio-, Video- oder Anrufaufzeichnungen, die im Zusammenhang mit unseren Geschäftsaktivitäten erstellt werden | NEIN      |
| I. Berufliche oder beschäftigungsbezogene Informationen | Geschäftliche Kontaktdaten, um Ihnen unsere Dienste auf geschäftlicher Ebene zur Verfügung zu stellen, oder Berufsbezeichnung, beruflicher Werdegang und berufliche Qualifikationen, wenn Sie sich bei uns um eine Stelle bewerben | NEIN      |
| J. Bildungsinformationen | Studentenunterlagen und Verzeichnisinformationen | NEIN      |
| K. Rückschlüsse aus anderen persönlichen Informationen | Rückschlüsse aus den oben aufgeführten gesammelten persönlichen Informationen, um ein Profil oder eine Zusammenfassung zu erstellen, z. B. über die Vorlieben und Eigenschaften einer Person | NEIN      |
| L. Sensible persönliche Daten | | NEIN      |

Wir werden die gesammelten persönlichen Daten so lange verwenden und aufbewahren, wie es für die Bereitstellung der Dienste oder für die:

* Kategorie A - Solange der Benutzer ein Konto bei uns hat

Wir können auch andere personenbezogene Daten außerhalb dieser Kategorien erheben, wenn Sie persönlich, online, telefonisch oder per Post mit uns in Kontakt treten:

* Sie erhalten Hilfe über unsere Kundensupport-Kanäle;

* Teilnahme an Kundenumfragen oder Wettbewerben; und

* Erleichterung bei der Bereitstellung unserer Dienstleistungen und zur Beantwortung Ihrer Anfragen.

**Wie verwenden und teilen wir Ihre persönlichen Daten?**

Weitere Informationen über unsere Praktiken der Datenerfassung und -weitergabe finden Sie in dieser Datenschutzerklärung.

Sie können uns per E-Mail unter support@remem.me oder über die Kontaktinformationen am Ende dieses Dokuments erreichen.

Wenn Sie einen Bevollmächtigten einsetzen, um Ihr Widerspruchsrecht auszuüben, können wir einen Antrag ablehnen, wenn der Bevollmächtigte nicht nachweist, dass er gültig bevollmächtigt wurde, in Ihrem Namen zu handeln.

**Werden Ihre Daten an andere Personen weitergegeben?**

Wir können Ihre persönlichen Daten gemäß einem schriftlichen Vertrag zwischen uns und dem jeweiligen Dienstleister an unsere Dienstleister weitergeben. Bei jedem Dienstleister handelt es sich um ein gewinnorientiertes Unternehmen, das die Daten in unserem Auftrag verarbeitet und dabei die gleichen strengen Datenschutzverpflichtungen erfüllt, wie sie vom CCPA vorgeschrieben sind.

Wir können Ihre persönlichen Daten für unsere eigenen Geschäftszwecke verwenden, z.B. für interne Forschung zur technologischen Entwicklung und Demonstration. Dies wird nicht als "Verkauf" Ihrer persönlichen Daten betrachtet.

Poimena hat in den vorangegangenen zwölf (12) Monaten keine persönlichen Daten zu geschäftlichen oder kommerziellen Zwecken an Dritte weitergegeben, verkauft oder weitergegeben. Poimena wird auch in Zukunft keine persönlichen Daten von Website-Besuchern, Benutzern und anderen Verbrauchern verkaufen oder weitergeben.

**Ihre Rechte in Bezug auf Ihre persönlichen Daten**

Recht, die Löschung der Daten zu verlangen - Antrag auf Löschung

Sie können die Löschung Ihrer persönlichen Daten beantragen. Wenn Sie uns um die Löschung Ihrer personenbezogenen Daten bitten, werden wir Ihrer Bitte nachkommen und Ihre personenbezogenen Daten löschen, vorbehaltlich bestimmter, gesetzlich vorgesehener Ausnahmen, wie z.B. (aber nicht beschränkt auf) die Ausübung des Rechts auf freie Meinungsäußerung durch einen anderen Verbraucher, die Erfüllung unserer Anforderungen, die sich aus einer gesetzlichen Verpflichtung ergeben, oder eine Verarbeitung, die zum Schutz vor illegalen Aktivitäten erforderlich sein kann.

Recht auf Information - Antrag auf Auskunft

Je nach den Umständen haben Sie ein Recht darauf, zu erfahren:

* ob wir Ihre persönlichen Daten sammeln und verwenden;

* welche Kategorien von personenbezogenen Daten wir sammeln;

* die Zwecke, für die die gesammelten persönlichen Daten verwendet werden;

* ob wir personenbezogene Daten an Dritte verkaufen oder weitergeben;

* die Kategorien der personenbezogenen Daten, die wir für einen Geschäftszweck verkauft, weitergegeben oder offengelegt haben;

* die Kategorien von Dritten, an die die personenbezogenen Daten zu einem geschäftlichen Zweck verkauft, weitergegeben oder offengelegt wurden;

* den geschäftlichen oder kommerziellen Zweck für das Sammeln, den Verkauf oder die Weitergabe personenbezogener Daten; und

* die spezifischen persönlichen Daten, die wir über Sie gesammelt haben.

In Übereinstimmung mit geltendem Recht sind wir nicht verpflichtet, Verbraucherdaten, die auf Anfrage eines Verbrauchers de-identifiziert wurden, zur Verfügung zu stellen oder zu löschen oder einzelne Daten erneut zu identifizieren, um eine Verbraucheranfrage zu verifizieren.

Recht auf Nicht-Diskriminierung bei der Ausübung der Datenschutzrechte eines Verbrauchers

Wir werden Sie nicht diskriminieren, wenn Sie Ihre Datenschutzrechte wahrnehmen.

Recht auf Einschränkung der Nutzung und Offenlegung sensibler persönlicher Daten

Wir verarbeiten keine sensiblen persönlichen Daten von Verbrauchern.

Überprüfungsverfahren

Nachdem wir Ihre Anfrage erhalten haben, müssen wir Ihre Identität überprüfen, um festzustellen, ob Sie die gleiche Person sind, über die wir die Informationen in unserem System haben. Diese Überprüfung erfordert, dass wir Sie um Informationen bitten, damit wir sie mit den Informationen abgleichen können, die Sie uns zuvor gegeben haben. Je nach Art der Anfrage, die Sie stellen, bitten wir Sie beispielsweise um bestimmte Informationen, damit wir die von Ihnen bereitgestellten Informationen mit den Informationen abgleichen können, die wir bereits gespeichert haben, oder wir kontaktieren Sie über eine Kommunikationsmethode (z.B. Telefon oder E-Mail), die Sie uns bereits mitgeteilt haben. Wir können auch andere Überprüfungsmethoden verwenden, wenn die Umstände dies erfordern.

Wir werden die in Ihrer Anfrage angegebenen persönlichen Daten nur dazu verwenden, Ihre Identität oder Ihre Berechtigung zur Anfrage zu überprüfen. Soweit es möglich ist, werden wir es vermeiden, zusätzliche Informationen von Ihnen für die Zwecke der Überprüfung anzufordern. Wenn wir Ihre Identität jedoch nicht anhand der von uns bereits gespeicherten Informationen überprüfen können, können wir Sie um zusätzliche Informationen bitten, um Ihre Identität zu überprüfen und um Sicherheit zu gewährleisten oder Betrug vorzubeugen. Wir werden solche zusätzlich zur Verfügung gestellten Informationen löschen, sobald wir die Überprüfung Ihrer Identität abgeschlossen haben.

Andere Datenschutzrechte

* Sie können der Verarbeitung Ihrer persönlichen Daten widersprechen.

* Sie können die Berichtigung Ihrer personenbezogenen Daten verlangen, wenn diese unrichtig oder nicht mehr relevant sind, oder eine Einschränkung der Verarbeitung der Informationen verlangen.

* Sie können einen Bevollmächtigten benennen, der in Ihrem Namen einen Antrag nach dem CCPA stellt. Wir können einen Antrag eines Bevollmächtigten ablehnen, der nicht den Nachweis erbringt, dass er in Ihrem Namen gemäß dem CCPA gültig bevollmächtigt wurde.

* Sie können beantragen, dass Ihre persönlichen Daten in Zukunft nicht mehr an Dritte verkauft oder weitergegeben werden. Nach Erhalt einer Opt-Out-Anfrage werden wir der Anfrage so schnell wie möglich nachkommen, jedoch nicht später als fünfzehn (15) Tage ab dem Datum der Anfrage.

Um diese Rechte auszuüben, können Sie uns per E-Mail unter support@remem.me oder über die Kontaktdaten am Ende dieses Dokuments kontaktieren. Wenn Sie eine Beschwerde darüber haben, wie wir Ihre Daten behandeln, würden wir gerne von Ihnen hören.


**10\. HABEN EINWOHNER VON VIRGINIA BESONDERE DATENSCHUTZRECHTE?**

**_Kurz gefasst:_** _Ja, wenn Sie in Virginia ansässig sind, haben Sie unter Umständen besondere Rechte in Bezug auf den Zugriff auf Ihre persönlichen Daten und deren Verwendung._

**Virginia CDPA Datenschutzhinweis**

Gemäß dem Virginia Consumer Data Protection Act (CDPA):

Der Begriff "Verbraucher" bezeichnet eine natürliche Person, die ihren Wohnsitz im Commonwealth hat und nur als Einzelperson oder in einem Haushalt handelt. Dies schließt keine natürliche Person ein, die in einem kommerziellen oder arbeitsrechtlichen Kontext handelt.

Personenbezogene Daten" sind alle Informationen, die mit einer identifizierten oder identifizierbaren natürlichen Person verknüpft sind oder vernünftigerweise verknüpft werden können. Personenbezogene Daten" umfassen keine de-identifizierten Daten oder öffentlich zugängliche Informationen.

Verkauf personenbezogener Daten" bedeutet den Austausch personenbezogener Daten gegen eine finanzielle Gegenleistung.

Wenn diese Definition 'Verbraucher' auf Sie zutrifft, müssen wir bestimmte Rechte und Pflichten in Bezug auf Ihre persönlichen Daten einhalten.

Die Informationen, die wir über Sie sammeln, verwenden und weitergeben, hängen davon ab, wie Sie mit Poimena und unseren Dienstleistungen interagieren.

Ihre Rechte in Bezug auf Ihre persönlichen Daten

* Recht, darüber informiert zu werden, ob wir Ihre personenbezogenen Daten verarbeiten oder nicht

* Recht auf Zugang zu Ihren persönlichen Daten

* Recht auf Berichtigung von Ungenauigkeiten in Ihren personenbezogenen Daten

* Recht, die Löschung Ihrer persönlichen Daten zu verlangen

* Recht, eine Kopie der persönlichen Daten zu erhalten, die Sie uns zuvor mitgeteilt haben

* Recht auf Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten, wenn diese für gezielte Werbung, den Verkauf personenbezogener Daten oder das Profiling zur Unterstützung von Entscheidungen, die rechtliche oder ähnlich bedeutende Auswirkungen haben, verwendet werden ("Profiling")

Poimena hat keine personenbezogenen Daten zu geschäftlichen oder kommerziellen Zwecken an Dritte verkauft. Poimena wird auch in Zukunft keine personenbezogenen Daten von Website-Besuchern, Nutzern und anderen Verbrauchern verkaufen.

Nehmen Sie Ihre Rechte gemäß dem Virginia CDPA wahr

Weitere Informationen über unsere Praktiken der Datenerfassung und -weitergabe finden Sie in diesem Datenschutzhinweis.

Sie können uns per E-Mail unter support@remem.me oder über die Kontaktdaten am Ende dieses Dokuments kontaktieren.

Wenn Sie einen Bevollmächtigten mit der Wahrnehmung Ihrer Rechte beauftragen, können wir eine Anfrage ablehnen, wenn der Bevollmächtigte nicht den Nachweis erbringt, dass er gültig bevollmächtigt wurde, in Ihrem Namen zu handeln.

Überprüfungsverfahren

Wir können Sie um zusätzliche Informationen bitten, die zur Überprüfung Ihres Antrags und des Antrags Ihres Verbrauchers notwendig sind. Wenn Sie die Anfrage über einen bevollmächtigten Vertreter einreichen, müssen wir möglicherweise zusätzliche Informationen einholen, um Ihre Identität zu überprüfen, bevor wir Ihre Anfrage bearbeiten können.

Nach Erhalt Ihrer Anfrage werden wir Ihnen unverzüglich antworten, in jedem Fall aber innerhalb von fünfundvierzig (45) Tagen nach Erhalt. Die Antwortfrist kann einmalig um fünfundvierzig (45) zusätzliche Tage verlängert werden, wenn dies vernünftigerweise erforderlich ist. Wir werden Sie über eine solche Verlängerung innerhalb der ersten 45-tägigen Antwortfrist informieren und den Grund für die Verlängerung angeben.

Recht auf Widerspruch

Wenn wir es ablehnen, Ihre Anfrage zu bearbeiten, werden wir Sie über unsere Entscheidung und die Gründe dafür informieren. Wenn Sie Einspruch gegen unsere Entscheidung einlegen möchten, senden Sie uns bitte eine E-Mail an support@remem.me. Innerhalb von sechzig (60) Tagen nach Eingang eines Widerspruchs informieren wir Sie schriftlich über alle Maßnahmen, die wir als Reaktion auf den Widerspruch ergriffen oder nicht ergriffen haben, einschließlich einer schriftlichen Erklärung der Gründe für die Entscheidungen. Wenn Ihr Einspruch abgelehnt wird, können Sie sich an den [Generalstaatsanwalt wenden, um eine Beschwerde einzureichen](https://www.oag.state.va.us/consumer-protection/index.php/file-a-complaint).


**11\. MACHEN WIR AKTUALISIERUNGEN AN DIESER MITTEILUNG?**

**_Kurz gefasst:_** _Ja, wir aktualisieren diesen Hinweis bei Bedarf, um den einschlägigen Gesetzen zu entsprechen._

Wir können diesen Datenschutzhinweis von Zeit zu Zeit aktualisieren. Die aktualisierte Version wird durch ein aktualisiertes 'Revidiert'-Datum gekennzeichnet, und die aktualisierte Version wird wirksam, sobald sie zugänglich ist. Wenn wir wesentliche Änderungen an diesem Datenschutzhinweis vornehmen, können wir Sie entweder durch einen gut sichtbaren Hinweis auf diese Änderungen oder durch direkte Zusendung einer Benachrichtigung informieren. Wir empfehlen Ihnen, diesen Datenschutzhinweis regelmäßig zu lesen, um sich darüber zu informieren, wie wir Ihre Daten schützen.


**12\. WIE KÖNNEN SIE UNS BEZÜGLICH DIESES HINWEISES KONTAKTIEREN?**

Wenn Sie Fragen oder Anmerkungen zu dieser Mitteilung haben, können Sie uns eine E-Mail an support@remem.me oder per Post an:

Poimena  
Kirchstrasse 1  
CH-8497 Fischenthal ZH  
Schweiz

**13\. WIE KÖNNEN SIE DIE DATEN, DIE WIR VON IHNEN ERFASSEN, ÜBERPRÜFEN, AKTUALISIEREN ODER LÖSCHEN?**

Sie haben das Recht, Einsicht in die von uns gesammelten persönlichen Daten zu verlangen, diese Daten zu ändern oder zu löschen. Um die Überprüfung, Aktualisierung oder Löschung Ihrer persönlichen Daten zu beantragen, senden Sie uns bitte eine E-Mail an support@remem.me.

{{% /pageinfo %}}



