---
title: "Spenden"
linkTitle: "Spenden"
weight: 25
url: "/de/docs/spenden"
description: >
  Remember Me wird von Poimena herausgegeben, einem gemeinnützigen Verein, der kostenlose Dienste für geistliches Wachstum anbietet.
---
### Gründer
  Poimena wurde von Pfr. Peter Schafflützel und Pfr. Regula Studer Schafflützel gegründet.

### Adresse
Poimena  
Kirchstrasse 1  
8497 Fischenthal  
Schweiz

### Banküberweisung
Verwenden Sie die folgenden Informationen, um eine Spende per Banküberweisung zu tätigen.
Bitte fügen Sie der Überweisungsnachricht Ihre E-Mail-Adresse hinzu, damit wir uns bei Ihnen bedanken können.
|                   |                     |
|-------------------|---------------------|
| Bankname:         | Raiffeisen, Schweiz |
| Begünstigter:     | Poimena             |
| SWIFT-Code (BIC): | RAIFCH22XXX         |
| Konto-IBAN:       | CH74 8080 8006 2918 4731 8 |

### PayPal/Kreditkarte
Falls Sie PayPal oder Kreditkarte bevorzugen, verwenden Sie bitte die folgende Schaltfläche.
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="ZVV68XVMWPY4Q" />
<input type="image" src="https://www.paypalobjects.com/de_DE/CH/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Spenden mit dem PayPal-Button" />
<img alt="" border="0" src="https://www.paypal.com/de_CH/i/scr/pixel.gif" width="1" height="1" />
</form>

