---
title: "Sammlungen importieren und teilen"
linkTitle: "Bibelvers-Sammlungen"
weight: 9
url: "/de/docs/sammlungen"
aliases:
  - /de/docs/collections
  - /wie-rm-funktioniert/sammlungen
  - /de/docs/sammlungen-importieren-und-teilen
description: >
  Von öffentlichen Vers-Sammlungen profitieren und Ihre eigenen Sammlungen mit anderen teilen
---

> Eine Vers-Sammlung ist ein veröffentlichtes Schlagwort samt seinen Versen.

### Eine Vers-Sammlung finden {#finden}
Wählen Sie "Sammlungen" aus dem App-Menü links und benutzen Sie die <span class="material-icons">search</span> 
Such-Funktion, um zu finden, wonach Sie suchen. Tippen Sie auf eine Sammlungskarte, um mehr Details zu sehen, und 
tippen Sie auf das <span class="material-icons">download</span> Symbol zum Herunterladen im Balken oben rechts, um die 
Verse der Sammlung in Ihr Konto zu importieren. Ein Schlagwort mit dem Namen der Sammlung macht die neuen Verse von 
Ihren übrigen Versen unterscheidbar.

### Eine Vers-Sammlung veröffentlichen und teilen {#veroeffentlichen}
Wählen Sie "Sammlungen" aus dem App-Menü links und tippen Sie auf die Schaltfläche zum Veröffentlichen 
(<span class="material-icons">publish</span> Hochladen-Symbol oben rechts), um eine Liste mit allen Schlagworten zu 
sehen, die an Verse angefügt sind. Ein <span class="material-icons">public_off</span> durchgestrichenes Welt-Symbol 
bedeutet, dass das Schlagwort und seine Verse nicht für andere Benutzer sichtbar sind. 

Tippen Sie auf ein Schlagwort und geben Sie eine Beschreibung Ihrer Sammlung im Formular, das eingeblendet wird, ein. 
Um abzubrechen oder eine früher veröffentlichte Sammlung zurückzuziehen, tippen Sie auf das durchgestrichene Welt-Symbol 
unten im Formular. Alle Details zur Veröffentlichung werden gelöscht und die Verse vor anderen Benutzern verborgen. 
Wenn Sie zur Veröffentlichung bereit sind, tippen Sie auf das hervorgehobene <span class="material-icons">public</span> 
Welt-Symbol unten im Formular. Alle Details zur Veröffentlichung werden gespeichert und die Sammlung erscheint von nun 
an in den Suchresultaten anderer Benutzer. 

### Einbetten Ihrer Vers-Sammlung in Ihre Website
Da Remember Me 6 auch als Webanwendung verfügbar ist, können Sie eine Sammlung oder einen Vers einer Sammlung in Ihre 
Website einbetten. Die Besucher Ihrer Website können dann die Spiele und Lernkarten von Remember Me direkt auf Ihrer 
Website nutzen. Alles, was Sie tun müssen, ist, ein iframe-Element mit der Adresse Ihrer eigenen Vers-Sammlung auf 
web.remem.me in Ihren HTML-Code einzufügen, z.B.
```
<iframe 
    src="https://web.remem.me/collections/166817522140997" 
    style="border:none;" 
    width="320" height="480" 
    title="Unsere Vers-Sammlung">
</iframe>
```
was zu folgendem Ergebnis führt:

{{< app 166817522140997 >}}