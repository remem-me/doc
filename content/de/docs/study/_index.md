---
title: "Lernen und repetieren"
linkTitle: "Lernen und repetieren"
weight: 6
url: "/de/docs/lernen"
aliases:
  - /de/docs/study
  - /wie-rm-funktioniert/karteikarten
  - /wie-rm-funktioniert/repetieren
  - /de/docs/lernen-und-wiederholen
description: >
  Spiele und eine intelligentes Wiederholsystem helfen beim Auswendiglernen und Behalten
---

### Der Lernprozess {#prozess}
Lernverse bewegen sich zwischen den Fächern "Neu", "Fällig" und "Bekannt".
Sie können die Sortierreihenfolge eines Fachs ändern, indem Sie auf die aktive Registerkarte <img src="/images/triangle.svg"> tippen.

|Fächer:|<span class="material-icons">inbox</span><br>Neu|&rarr;|<span class="material-icons">check_box_outline_blank</span><br>Fällig|&rarr;|<span class="material-icons">check_box</span><br>Bekannt|
|:-|:-:|:-:|:-:|:-:|:-:|
|Aktionen:|1. **Lernen**|2. Übernehmen|3. **Texte**<br>**repetieren**|4. Wiedergeben|5. **Stellenangaben**<br>**repetieren**
|Schaltflächen:|<span class="material-icons">school</span>|<span class="material-icons">launch</span>|<img src="/images/cards_outline.svg">|<span class="material-icons">check</span>|<img src="/images/cards_filled.svg">

1. Neue Verse **lernen**.
2. Das Übernehmen eines Verses verschiebt ihn ins Fach "Fällig".
3. **Texte** von fälligen Versen **repetieren**.
4. Das erfolgreiche Wiedergeben eines Verses verschiebt ihn ins Fach „Bekannt“.
5. **Stellenangaben** von zufällig ausgewählten Versen **repetieren**

### Lernen {#lernspiele}
Nachdem Hinzufügen eines Verses kann er mit verschiedenen Spielen gelernt werden. 
Um mit dem Lernen zu beginnen, tippen Sie auf den Vers und auf das <span class="material-icons">school</span> Lern-Symbol.

<span class="material-icons">wb_cloudy</span> **Vernebeln**: Verbergen Sie einige Wörter und versuchen Sie, den Vers 
aufzusagen, indem Sie die fehlenden Wörter aus Ihrem Gedächtnis einfügen. Tippen Sie auf ein vernebeltes Wort, um es sichtbar zu machen.

<span class="material-icons">extension</span> **Puzzle**: Setzen Sie den Vers zusammen, indem Sie auf das korrekte Wort 
tippen.

<span class="material-icons">subject</span> **Aufreihen**: Zeigen Sie leere Linien oder Anfangsbuchstaben an, indem Sie 
auf das Symbol links im Balken unten tippen. Reihen Sie das nächste Wort oder die nächste Zeile auf, indem Sie auf <span class="material-icons">plus_one</span> "+Wort" oder <span class="material-icons">playlist_add</span> "+Zeile" tippen. Versuchen Sie, das Wort oder die Zeile aufzusagen, bevor Sie es aufdecken.

<span class="material-icons">keyboard</span> **Tippen**: Setzen Sie den Vers zusammen, indem Sie den Anfangsbuchstaben 
jedes Worts tippen. 

Wenn Sie den Vers auswendig kennen, wischen Sie die Lernkarte nach rechts, um den Vers ins Fach "Fällig" zu übernehmen. 
Der Vers nimmt von nun an am Wiederhol-Prozess teil.

### Texte repetieren {#texte}
Wählen Sie das Fach "**Fällig**". Falls Sie die Reihenfolge der Verse ändern möchten, tippen Sie nochmals auf "Fällig". 
Es gibt 3 Möglichkeiten, eine Repetition zu starten:

{{< cardpane >}}
{{< card header="<img src='/images/cards_outline.svg'> umrissene Lernkarten" >}}
Startet die Repetition mehrerer Lernkarten.
{{< /card >}}
{{< card header="Antippen eines Verses" >}}
Startet die Repetition einer einzelnen Lernkarte.
{{< /card >}}
{{< card header="Lange auf einen Vers drücken" >}}
Öffnet die Lernkarte direkt auf der Textseite.
{{< /card >}}
{{< /cardpane >}}

Wenn Stellen und Thema angezeigt werden, sagen Sie den Vers auswendig auf. Drehen Sie die Karte dann um, um zu prüfen, 
ob Sie den Vers korrekt aufgesagt haben. Falls sie vorziehen, den Vers Zeile für Zeile aufzusagen (um Zeilenumbrüche 
hinzuzufügen siehe [Verse hinzufügen und bearbeiten](/de/docs/verse)), tippen Sie für jede Zeile auf das 
<span class="material-icons">rule</span> Symbol unten rechts.

Wenn Sie ein wenig Hilfe zum Einstieg benötigen, drücken Sie lange auf die Karte, um einen Hinweis zu erhalten. 
(Sie können eine Karte überspringen, indem Sie sie nach unten wischen.)

Wenn Sie den Vers richtig in Erinnerung hatten, wischen Sie ihn nach rechts, um ihn ins Fach "Erinnert" zu verschieben. 
Das erhöht den Stufe des Verses um eins (bis die Wiederhollimite der Kontoeinstellungen erreicht ist). Falls er nicht 
ganz richtig war, wischen Sie ihn nach links. Das setzt den Stufe des Verses auf 0 zurück. Das klingt vielleicht ein 
bisschen schockierend, aber es ist sehr wichtig für die Wiederholfrequenz des Verses. Verse mit niedrigen Stufen 
erscheinen häufiger zur Wiederholung. Das hilft, Ihre Erinnerung an einen Vers zu verstärken bevor Sie ihn vergessen. 
Abgestuftes Repetieren stellt sicher, dass Sie herausfordernde Texte häufiger wiederholen aber auch vertraute Texte 
nicht vergessen.

{{% alert title="Abgestuftes Repetieren (Spaced Repetition)" color="secondary" %}}
Das Auswendiglernen ist am effizientesten, wenn das Gehirn das Gelernte in immer grösseren Abständen abruft. 
Remember Me verwendet einen exponentiellen Algorithmus zur Berechnung der Intervalle (ähnlich wie Pimsleur). Wenn die 
Repetierhäufigkeit für Ihr Konto auf "Normal" eingestellt ist, werden Sie Ihre Verse nach 1, 2, 4, 8, 16... Tagen 
repetieren. Die andere Komponente des Algorithmus ist das Zurücksetzen des Niveaus eines Verses auf 0 nach einer 
inkorrekten Repetition (ähnlich wie bei Leitner). Dadurch wird sichergestellt, dass schwierige Passagen häufiger 
repetiert werden als leichte. 
{{% /alert %}}

### Stellenangaben repetieren {#stellen}
Wenn Sie nur die Stellenangaben der heute repetierten Texte überprüfen möchten, drücken Sie auf das Label 
"[Heute repetiert](/de/docs/schlagworte/#filtern)", bevor Sie beginnen. Wählen Sie das Fach 
"**Erinnert**", setzen Sie dessen Sortierreihenfolge auf "zufällig" und tippen Sie auf die Schaltfläche mit dem 
<img src="/images/cards_filled.svg"> gefüllten Lernkarten-Symbol unten. 

Remember Me mischt die Karten und präsentiert deren Texte. Versuchen Sie, die Bibelstellenangabe aus dem Gedächtnis 
aufzusagen, und wenden Sie die Karte, indem Sie sie antippen, um zu schauen, ob Sie richtig liegen. Wenn Ihre Antwort 
korrekt ist, wischen Sie die Karte nach rechts. Wenn nicht, wischen Sie sie nach links. Die App wird die Karte während 
des Repetierdurchgangs solange wiederholen, bis Sie sich an sie erinnern. 

Sie können eine Karte überspringen, indem Sie sie nach unten wischen. Anders als beim Repetieren von Texten merkt die 
App sich Ihren Fortschritt nicht. Diese Art des Repetierens ist eher wie ein Quiz, das Sie hin und wieder absolvieren 
sollten, um Ihr Bibelstellen-Wissen aufzufrischen. 

{{% alert title="Konto-Einstellungen" color="secondary" %}}
- **Anzahl Repetitionslernkarten**: Maximale Anzahl Lernkarten beim Repetieren der Texte.
- **Anzahl umgekehrter Lernkarten**: Maximale Anzahl Lernkarten beim Repetieren der Stellenangaben.
- **Stellenangabe mitlernen**: Stellenangabe in Lernen und Repetieren einschliessen.
- **Repetierhäufigkeit**: Wie häufig die Verse zur Repetition erscheinen.
- **Intervallgrenze**: Maximale Anzahl Tage bis zur nächsten Repetition. Begrenzt indirekt die Anzahl der erreichbaren Fortschrittsstufen. 
{{% /alert %}}




