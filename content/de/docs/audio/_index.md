---
title: "Audio-Funktionen"
linkTitle: "Audio-Funktionen"
weight: 12
aliases:
  - /de/docs/audio-funktionen
  - /wie-rm-funktioniert/audio
description: >
  Remember Me kann Ihnen Verse vorlesen oder Ihr eigenes Vortragen aufzeichnen und Ihnen vorspielen.
---

> Das wiederholte Anhören von Versen unterstützt Ihren Lernprozess. Die Aufnahme Ihrer Stimme während des Vortragens 
> festigt Ihre Kenntnis des Verses. 

### Verse eines Fachs hören {#fach-hoeren}
Um mit dem Hören von Versen zu beginnen, wählen Sie das Fach mit den Versen, die Sie hören möchten. Wenn Sie sich 
Verse im Fach "Neu" anhören, werden Sie mit ihnen vertraut. Das Anhören von Versen im Fach "Bekannt" frischt Ihr Wissen 
auf. Drücken Sie die <span class="material-icons">play_arrow</span> Play-Taste, um mit der Wiedergabe zu beginnen. Mit den 
Tasten am unteren Rand können Sie das Vorlesen anhalten, stoppen oder einen Vers überspringen. 

Sie können Remember Me alle Verse oder einen einzelnen Vers wiederholen lassen oder die Wiedergabe in den Einstellungen 
(<span class="material-icons">settings</span> Zahnrad) neben den Steuertasten verlangsamen. 

Wenn Sie die Stellenangabe nach der dem Bibeltext einfügen oder die Verse nach Thema anhören möchten, aktivieren Sie 
die entsprechende Einstellung unter "Konto".

### Beim aktuellen Vers beginnen {#vers-hoeren}
Wenn Sie eine Lernkarte vor sich haben, können Sie die Wiedergabe starten, indem Sie auf den 
<span class="material-icons">play_arrow</span> Play-Knopf in der oberen rechten Ecke drücken.
Nach der Rückkehr zur Versliste ist der aktuelle Vers immer noch als wiedergebend oder pausiert markiert.
Wenn Sie jetzt auf den Play-Knopf der Liste tippen, beginnt die App mit dem Vorlesen ab dieser Position in der Liste.

{{% alert title="Text-to-Speech-Einstellungen" color="secondary" %}}
<span class="material-icons">speed</span> Sprechgeschwindigkeit  
<span class="material-icons">pause_presentation</span> Pause (in Sekunden)  
<span class="material-icons">av_timer</span> Einschlaf-Timer (in Minuten)  
<span class="material-icons">playlist_play</span> *ein*: Liest die gesamte Liste / *aus*: Liest nur den aktuellen Vers  
<span class="material-icons">repeat</span> Wiederhole die Liste (oder den Vers)
{{% /alert %}}

### Zuhören beim Lernen eines Verses {#lernspiel}
Während eines Lernspiels (siehe [Lernen](/de/docs/lernen/#lernen)) liest Remember Me jedes Wort oder jeden Satz vor, 
den Sie aufdecken. Sie können die Sprachausgabe über das Einstellungsmenü (<span class="material-icons">settings</span> 
Zahnrad) im Spiel ausschalten.  

### Aufnehmen und Abspielen des eigenen Vortragens {#aufnehmen}
Die Lernkarten in Ihrem Fach "Fällig" (siehe [Texte repetieren](/de/docs/lernen/#texte)) 
haben am unteren Rand eine <span class="material-icons">mic</span> Mikrofonschalftläche. Tippen Sie darauf, um Ihren 
Versvortrag aufzuzeichnen. Wenn Sie die Schaltfläche erneut drücken oder die Karte umdrehen, wird die Aufnahme gestoppt. 
Nach dem Aufdecken des Verstextes wird die Wiedergabe gestartet, so dass Sie leichter überprüfen können, ob Ihr Vortrag 
korrekt war.

### Die Stimme ändern {#stimme}
Remember Me verwendet die Text-to-Speech-Engine Ihres Geräts. Anleitungen zum Ändern der Stimme finden Sie hier:
* [Hilfe für iPhone/iPad](https://support.apple.com/de-de/guide/iphone/iph96b214f0/ios#iph938159887)  
  Gehen Sie zu Einstellungen > Barrierefreiheit > Gesprochener Inhalt > Stimmen: Wählen Sie eine Stimme und einen Dialekt aus, laden Sie sie herunter und setzen Sie sie als Standard.
* [Hilfe für Android](https://support.google.com/accessibility/android/answer/6006983?hl=de)

Für Android-Geräte stehen mehrere Text-to-Speech-Engines zur Verfügung, z. B.:
* [Acapela TTS](https://play.google.com/store/apps/details?id=com.acapelagroup.android.tts)
* [Cereproc TTS](https://play.google.com/store/apps/developer?id=CereProc+Text-to-Speech)
* [Samsung TTS](https://galaxystore.samsung.com/detail/com.samsung.SMT)
* [Speech Services von Google](https://play.google.com/store/apps/details?id=com.google.android.tts)

Bei der Nutzung der Web-App auf Windows oder macOS können Sie die Stimme in den Spracheinstellungen von Remember Me ändern. Möglicherweise müssen Sie zuerst Sprachdaten auf Ihrem Gerät installieren.

