---
title: "Fünf Schritte zur Erinnerung: Bibeltexte lernen"
linkTitle: "5 Schritte zur Erinnerung"
weight: 2
url: "/de/docs/schritte"
aliases:
  - /wie-rm-funktioniert
  - /de/docs/steps
  - /de/docs/5-schritte-zur-erinnerung
description: >
  Bibelverse auswendig lernen mit Remember Me. Wenden Sie die Grundprinzipien des Bibellernens auf Ihr tägliches Leben an.
---

### 1 Einen Vers hinzufügen
Remember Me bietet verschiedene Wege, um einen Lerntext auf Ihrem Gerät zu speichern. Sie können 
- einen beliebigen Text [manuell eingeben](/de/docs/verse/#hinzufuegen)
- einen Vers aus einer Vielzahl von [Bibelversionen](/de/docs/verse/#online-bibel) abrufen
- [Bibelvers-Sammlungen](/de/docs/sammlungen/#finden) von anderen Benutzern herunterladen

### 2 Sich einen Vers aneignen
Einen [Bibelvers auswendig zu lernen](/de/docs/lernen/#lernspiele) wird durch das Anwenden vielfältiger Methoden zum Vergnügen. 
- [Anhören](/de/docs/audio/#fach-hoeren)
- Einzelne Wörter verbergen
- Ein Puzzle daraus machen
- Anfangsbuchstaben oder leere Linien statt Wörtern anzeigen
- Den Anfangsbuchstaben jedes Wortes tippen

### 3 Themen und Bilder verwenden
Nicht alle sind gut mit Zahlen - und das brauchen Sie auch nicht zu sein, um Texte auswendig zu lernen. 
Fügen Sie Ihrem Vers ein [Thema](/de/docs/schlagworte) oder ein [Bild](/de/docs/verse/#bild) hinzu, und es wird Ihnen helfen, den rechten Vers aus dem Gedächtnis zu rufen.

### 4 Gelernte Verse wiederholen 
Wenn Sie sich einen Vers einmal angeeignet haben, erscheint er zur [Repetition](/de/docs/lernen/#texte) im Fach "Fällig". Starten Sie zum Repetieren eine Lernkartenserie. [Sagen Sie den Vers laut](/de/docs/audio/#aufnehmen) und prüfen Sie nach dem Umdrehen der Karte, ob Sie ihn gewusst haben.
Abgestuftes Repetieren (Spaced Repetition) stellt sicher, dass Sie neu gelernte Verse oft wiederholen, aber auch wohlbekannte Verse nicht vergessen.  

### 5 Die Zeit auskaufen 
Nutzen Sie jede Gelegenheit. Remember Me funktioniert, weil Sie die zwei Minuten während des Zähneputzens zum Auswendiglernen benutzen können. Haben Sie es immer bei sich und benutzen Sie es in diesen natürlichen Pausen des Lebens. 
