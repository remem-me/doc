---
title: "Benutzer und Konten"
linkTitle: "Benutzer und Konten"
weight: 4
url: "/de/docs/konten"
aliases:
  - /de/docs/accounts
  - /de/docs/benutzer-und-konten
description: >
  Sich als Benutzer registrieren und ein Konto für Ihre Verse erstellen
---

### Benutzer registrieren und anmelden {#registrieren}
Auf [web.remem.me](https://web.remem.me) können Sie sich unentgeltlich registrieren. Nachdem Sie das Registrierungsformular 
abgeschickt haben, erhalten Sie eine E-Mail mit der Aufforderung, Ihre Registrierung zu bestätigen. 

Nach erfolgter Registrierung können Sie sich mit der Schaltfläche **Anmelden** (oben rechts) in der App anmelden.

{{% alert title="Passwort ändern" color="secondary" %}}
Um Ihr Passwort zu ändern, klicken Sie im Anmeldefenster auf "Passwort zurücksetzen".
{{% /alert %}}

### Mehrere Verskonten {#mehrere}
Ein Konto für Ihre Verse wird automatisch mit Ihrer Benutzerregistrierung erstellt. Sprache, Kontoname, 
Repetier-Einstellungen etc. werden in den Konto-Einstellungen festgelegt. Als Benutzer können Sie zusätzliche Konten 
hinzufügen, z.B. für Ihre Kinder oder um Verse in einer zweiten Sprache zu lernen. Für die meisten Benutzer wird ein 
einzelnes Konto genügen. Verse können nicht zwischen Konten verschoben werden. Um Verse zu organisieren eignen sich 
[Schlagworte](/de/docs/schlagworte) besser.

Um ein neues Konto hinzuzufügen, tippen Sie auf das <span class="material-icons">arrow_drop_down</span> Dreieck im 
Kopfteil des Menüs auf der linken Seite, und wählen Sie "Neues Konto erstellen" aus dem Menü.

### Konten aus früheren Versionen importieren {#fruehere}
Nachdem Sie ein neues Benutzerkonto (identifiziert durch Ihre E-Mail-Adresse) erstellt haben, können Sie Verskonten aus 
früheren Remember-Me-Versionen hinzufügen. Klicken Sie auf das <span class="material-icons">arrow_drop_down</span> 
Dreieck in der Kopfzeile des Menüs auf der linken Seite und wählen Sie "Bestehendes Konto hinzufügen" aus dem Menü. 
Geben Sie Name und Passwort des alten Kontos ein und tippen Sie auf "Anmelden". Sie können das angehängte Konto wie 
jedes andere Remember Me 6 Vers-Konto verwenden.

### Mehrere Geräte synchronisieren {#synchronisieren}
Bei Geräten mit Offline-Fähigkeit ist ein <span class="material-icons">refresh</span> rundes Pfeilsymbol neben dem Namen 
des aktuellen Kontos in der linken Navigation zu sehen. Wenn das Symbol nicht sichtbar ist (z. B. in einem Webbrowser), 
werden die Verse nur online gespeichert. Verse und Lernfortschritt werden automatisch synchronisiert. Tippen Sie auf 
das runde Pfeilsymbol, um auf einem anderen Gerät bearbeitete Daten zu laden. Wenn Sie Ihr Gerät zwingen möchten, alle 
Daten abzurufen und zu senden, drücken Sie lange auf das Symbol.  

{{% alert title="Synchronisierungsschaltfläche" color="secondary" %}}
<span class="material-icons">refresh</span> **ein Pfeil** als Kreis: Mögliche Änderungen auf anderen Geräten. Tippen 
Sie darauf, um sie abzurufen.  
<span class="material-icons">sync</span> **zwei Pfeile** als Kreis: Lokale Änderungen auf diesem Gerät. Tippen Sie 
darauf, um sie zu senden.  
<span class="material-icons">sync_problem</span> **Ausrufezeichen** im Kreis: Die Synchronisierung ist fehlgeschlagen. 
Tippen Sie darauf, um es erneut zu versuchen.
{{% /alert %}}

### Bearbeiten und Löschen von Verskonten {#konto-bearbeiten}
Um das aktuell ausgewählte Konto zu bearbeiten oder zu löschen, wählen Sie "Konto" in der Hauptnavigation auf der linken 
Seite. Tippen Sie auf das Symbol oben links (Diskette <span class="material-icons">save</span>), um das Konto nach der Bearbeitung zu speichern.
Durch Tippen auf das <span class="material-icons">delete</span> Papierkorbsymbol in der oberen rechten Ecke wird das 
Konto gelöscht. Ein gelöschtes Konto wird drei Monate lang im [Papierkorb](https://web.remem.me/account-bin) gespeichert 
und kann von dort aus wiederhergestellt werden.

### Bearbeiten und Löschen des Benutzerzugangs {#benutzer-bearbeiten}
Wenn Sie Ihre Benutzeranmeldung bearbeiten oder löschen möchten, wechseln Sie in die Kontonavigation 
(<span class="material-icons">arrow_drop_down</span> Dreieck in der Kopfzeile des Menüs auf der linken Seite) und wählen 
Sie "Benutzer". 
Um Ihre E-Mail-Adresse zu ändern, geben Sie die neue Adresse und das aktuelle Passwort ein und tippen Sie auf 
"Speichern".
Um Ihren Benutzerzugang zu remem.me komplett zu löschen, müssen zunächst alle Verskonten gelöscht werden. Geben Sie dann 
das aktuelle Passwort ein und tippen Sie auf das <span class="material-icons">delete</span> Papierkorbsymbol im 
Benutzerdialog.  

{{% alert title="Achtung" color="warning" %}}
Die Löschung Ihres Zugangs zu remem.me kann nicht rückgängig gemacht werden. Alle Ihre Daten werden sofort und dauerhaft 
aus allen Datenbanken entfernt.
{{% /alert %}}

### Nur-auf-Gerät-Modus {#nur-auf-geraet}

Sie können Remember Me nutzen, ohne sich mit dem remem.me-Cloud-Dienst zu verbinden. In diesem Modus werden alle Daten ausschließlich auf Ihrem Gerät gespeichert.

- Daten bleiben auf Ihrem Gerät
- Keine Cloud-Sicherung verfügbar
- Veröffentlichung von Verssammlungen nicht möglich

{{% alert title="Wichtig" color="warning" %}}
Wenn Sie im Nur-auf-Gerät-Modus den Zugriff auf die App auf Ihrem Gerät verlieren, können Ihre Verse nicht wiederhergestellt werden.
{{% /alert %}}
Um den Nur-auf-Gerät-Modus zu aktivieren, wählen Sie "Nur auf Gerät" am unteren Rand des Registrierungsdialogs aus (nach Tippen auf "Anfangen").

Um den Nur-auf-Gerät-Modus zu verlassen:

1. Öffnen Sie das Menü
2. Tippen Sie auf den Kontonamen, um zum Kontomenü zu wechseln
3. Wählen Sie "Abmelden"

Um lokale Verse auf ein Online-Konto zu übertragen, [exportieren und importieren](docs/labels/#exportieren) Sie Ihre Verse als Datei.

{{% alert title="Hinweis" color="secondary" %}}
Der Nur-auf-Gerät-Modus bietet im Vergleich zu cloud-verbundenen Konten eingeschränkte Funktionalität. Berücksichtigen Sie die Vor- und Nachteile zwischen Datenschutz und Funktionsumfang bei der Wahl Ihres bevorzugten Betriebsmodus.
{{% /alert %}}
