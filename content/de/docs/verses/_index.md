---
title: "Bibelverse hinzufügen und bearbeiten"
linkTitle: "Verse hinzufügen & bearbeiten"
weight: 5
url: "/de/docs/verse"
aliases:
  - /de/docs/verses
  - /de/docs/verse-hinzufügen-und-bearbeiten
description: >
  Bibelstellen aus Online-Bibeln abrufen oder andere Texte zum Auswendiglernen hinzufügen und bearbeiten. Bibel-Lernkarten verschieben, löschen oder wiederherstellen.
---

### Einen Vers hinzufügen {#hinzufuegen}
Wählen Sie den Reiter "Neu" und tippen Sie auf die Schaltfläche "+" am unteren Ende der Anwendung, um das Formular zu 
öffnen. Geben Sie Bibelstelle (oder eine andere Referenz) ein und tippen Sie auf die Schaltfläche links oben 
(<span class="material-icons">save</span> Disketten-Symbol), um den Vers zu speichern. Der Vers wird nun in ihrem 
Eingangsfach aufgeführt. 

### Einen Text von einer Online-Bibel abrufen {#online-bibel}
Tippen Sie beim Hinzufügen oder Bearbeiten eines Verses auf das Dropdown-Symbol <span class="material-icons">
arrow_drop_down</span> des Felds "Quelle", um die Liste der verfügbaren Bibelversionen zu öffnen, oder geben Sie ein 
Bibelkürzel (z.B. LUT) in das Feld "Quelle" ein.

Wenn Remember Me die Bibelstellenangabe (z.B. 1. Joh 3,16 oder 1Johannes 3,1-3) und die Bibelversion (z.B. HFA) erkennt, 
wird eine Schaltfläche mit einem <span class="material-icons">menu_book</span> Bibel-Symbol angezeigt. Ein zusätzlicher 
Kippschalter <span class="material-icons-outlined">format_list_numbered</span> erlaubt, die Bibelstelle mit oder ohne Versnummern zu importieren. 
Wenn Sie auf die Bibel-Schaltfläche tippen, öffnet Remember Me die Webseite der Online-Bible und bietet Ihnen an, den 
Text in die App zu <span class="material-icons">paste</span> kopieren. Sie können zusätzliche Zeilenumbrüche oder andere 
Änderungen hinzufügen und den Vers Speichern (siehe oben).

### Einen Vers aus einer Bibel-App übernehmen {#bibel-app}
Die meisten mobilen Bibel-Apps ermöglichen das Teilen von Versen mit anderen Apps. Markieren Sie den Vers in der Bibel-App, 
wählen Sie "Teilen" aus und wählen Sie Remember Me aus der Liste der Apps aus. Remember Me öffnet den Verseditor und füllt die 
Stellenangabe, den Text und die Quelle (falls von der Bibel-App bereitgestellt) aus. Tippen Sie auf die Speichern-Schaltfläche, 
um den Vers zu Ihrer Sammlung neuer Bibelverse hinzuzufügen.

### Einen Vers bearbeiten {#bearbeiten}
Wenn Sie auf einen Vers in einem der drei Fächer (New, Fällig, Erinnert) tippen, wird er als Lernkarte angezeigt. Sie 
können Ihn bearbeiten, indem Sie auf das Bleistift-Symbol im Balken oben tippen.

Sie können einen Textabschnitt in mehrere Lernabschnitte unterteilen, indem Sie Zeilenumbrüche einfügen.

#### Formatierungsoptionen
- \*kursiv\*
- \*\*fett\*\*
- \>Blockzitat

#### Versnummern
Versnummern werden in eckige Klammern gesetzt, z.B. \[1\], \[2\], \[3\].
Wenn die Kontoeinstellung "Stellenangabe mitlernen" aktiviert ist, werden Versnummern in Sprachausgabe und Lernspielen einbezogen.

### Ein Bild anfügen {#bild}
Bei der Bearbeitung eines Verses (siehe oben) können Sie ein Bild anfügen, das als Lernkartenhintergrund für den Vers 
verwendet werden soll. Grundsätzlich können Sie die Internetadresse (URL) eines beliebigen online verfügbaren Bildes in 
das Feld "Bild-URL" im unteren Teil des Bildschirms eingeben. Zur Vereinfachung gibt es rechts eine 
<span class="material-icons">image</span> Schaltfläche für die Suche nach Bildern auf Unsplash.com. Tippen Sie auf den 
Namen, um mehr über den Fotografen zu erfahren, und auf das Bild, um die Bild-Adresse in das Formularfeld einzufügen. 
Nur Fotos von Unsplash.com werden in öffentliche Vers-Sammlungen aufgenommen. 

Tippen Sie auf die Schaltfläche oben links (<span class="material-icons">save</span> Diskettensymbol), um den Vers mit 
dem Bild zu speichern.

### Verse verschieben, aufschieben oder löschen {#verschieben}
Wählen Sie Verse aus, indem Sie ihre Plakette berühren (abgerundetes Quadrat links), und öffnen Sie das Menü auf der rechten Seite (drei Punkte). 
Dieses bietet die Optionen, die ausgewählten Verse in ein anderes Fach zu verschieben, ihr Fälligkeitsdatum 
aufzuschieben oder sie zu löschen.

### Gelöschte Verse wiederherstellen {#wiederherstellen}
Kürzlich gelöschte Verse können online aus dem [Papierkorb](https://web.remem.me/bin) wiederhergestellt werden. 