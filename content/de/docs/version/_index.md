---
title: "Was ist neu in Remember Me 6?"
linkTitle: "Was ist neu?"
url: "/de/docs/version"
weight: 1
description: >
  Remember Me 6 bringt das Auswendiglernen von Bibelversen auf ein neues Niveau.
---

{{% pageinfo %}}
Wenn Sie Informationen zu früheren Remember Me Versionen suchen, besuchen Sie bitte die [Hilfeseiten für Remember Me 5 und älter](https://v5.remem.me).

Wenn Sie nach dem Update Verse wiederherstellen müssen, lesen Sie bitte [diesen Benutzerforum-Beitrag](https://forum.remem.me/d/119-abgespeicherte-verse-sind-verschwunden/2).
{{% /pageinfo %}}

### Einfacher zu benutzen
Remember Me 6 betont die Karteikarten-Metapher. Die Karteikarten bewegen sich zwischen den Fächern Neu, Fällig und Bekannt. Die Karteikarte eines Verses ist der Ausgangspunkt für das Lernen und das Repetieren des Verses. Die Karteikarten-Metapher hilft, die grundlegenden Abläufe intuitiver zu verstehen.

### Mehr Vielseitigkeit
Power-User mit Hunderten von Versen haben mehr Möglichkeiten, ihre Verse zu gruppieren und mit Hilfe von anpassbaren Schlagworten zu filtern. Visuelle Benutzer profitieren vom Hinzufügen von Bildern zu ihren Versen. Mehrsprachige Benutzer können sich Stellenangaben und Verstexte in verschiedenen Sprachen anhören.

### Einheitliches Erlebnis
Die neue App-Version garantiert ein einheitliches Nutzererlebnis auf allen Geräten, da sie für alle Plattformen das gleiche Software-Framework verwendet. App und Framework sind Open Source.
