baseurl: https://www.remem.me
headers:
  - for: /*
    values:
      Access-Control-Allow-Origin: '*'
enableRobotsTXT: false
enableGitInfo: true
contentDir: content/en
workingDir: .
defaultContentLanguage: en
defaultContentLanguageInSubdir: false
enableMissingTranslationPlaceholders: true
disableKinds:
  - taxonomy
pygmentsCodeFences: true
pygmentsUseClasses: false
pygmentsUseClassic: false
pygmentsStyle: tango
permalinks:
  blog: /:section/:year/:month/:day/:slug/
blackfriday:
  angledQuotes: false
  hrefTargetBlank: true
  latexDashes: true
  plainIDAnchors: true
imaging:
  anchor: Smart
  quality: 75
  resampleFilter: CatmullRom
services:
  googleAnalytics:
    id: G-1R5BC2G6B1
staticDir:
  - static
languages:
  en:
    languageName: English
    weight: 1
    params:
      title: Remember Me. Bible Memory Joy
      description: Learn and retain Bible verses with the leading Bible app for memorisation.
      keywords:
        - 'Bible memory verses'
        - 'Bible memorize'
        - 'memorize Scripture'
        - 'Bible memorization'
        - 'Bible study'
        - 'Bible verse memorization'
        - 'Scripture memory'
        - 'memory verses'
        - 'Bible flashcards'
        - 'Bible memory'
        - 'Biblical app'
        - 'Bible memory app'
    menu:
      main:
        - name: User Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Press
          url: /docs/press/
          weight: 40
        - name: Memory Verses
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  de:
    contentDir: content/de
    languageName: Deutsch
    weight: 1
    params:
      title: Remember Me. Bibelverse lernen
      description: Bibelverse lernen und behalten mit der besten Bibel-App zum
        Auswendiglernen.
      keywords:
        - Bibelverse lernen
        - Bibel-App
        - Bibelstudium
        - Lutherbibel
        - Bibelverse auswendig lernen
        - Bibel-Lernkarten
        - Christliche App
        - Bibelverse Bilder
        - Bibelmeditation
        - Bibelvers des Tages
      privacy_policy: /de/docs/datenschutz
      images:
        - /images/screens-de.png
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Bibelverse zum Lernen
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  es:
    contentDir: content/es
    languageName: Español
    weight: 1
    params:
      title: Remember Me. Memorizar la Biblia
      description:
        Aprender y retener versículos de la Biblia con la mejor aplicación
        bíblica para memorizar.
      keywords:
        - Memorizar versículos bíblicos
        - Aplicación bíblica
        - Estudio bíblico
        - Reina Valera
        - Versículos de memoria
        - Tarjetas bíblicas
        - App cristiana
        - Imágenes de versículos
        - Meditación bíblica
        - Versículo del día
      privacy_policy: /es/docs/privacidad
      images:
        - /images/screens-es.png
    menu:
      main:
        - name: Foro
          url: https://forum.remem.me/
          weight: 25
        - name: Versículos para memorizar
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  cs:
    contentDir: content/cs
    languageName: Čeština
    params:
      title: Remember Me. Memorování Bible
      description:
        Naučte se a uchovejte biblické verše s nejlepší aplikací Bible pro
        zapamatování.
      keywords:
        - Memorování Bible
        - Biblická aplikace
        - Studium Bible
        - Slovo na cestu
        - Biblické verše
        - Biblické kartičky
        - Křesťanská aplikace
        - Obrázky biblických veršů
        - Biblická meditace
        - Verš dne
      privacy_policy: /cs/docs/privacy
    menu:
      main:
        - name: Fórum
          url: https://forum.remem.me/
          weight: 25
        - name: Verše k zapamatování
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  fr:
    contentDir: content/fr
    languageName: Français
    params:
      title: Remember Me. Mémoriser la Bible
      description:
        Apprendre et retenir des versets de la Bible avec la meilleure
        application biblique pour mémorisation.
      keywords:
        - mémorisation biblique
        - versets à mémoriser
        - cartes bibliques
        - mémoriser l'Écriture
        - images de versets bibliques
        - Louis Segond
        - jeux de mémorisation biblique
        - écouter des versets bibliques
        - méditation sur les versets bibliques
        - application chrétienne
        - application de mémorisation biblique
      privacy_policy: /fr/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Versets à mémoriser
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  hu:
    contentDir: content/hu
    languageName: Magyar
    params:
      title: Remember Me. Memorizáld a Bibliát
      description: Tanuljon és emlékezzen bibliai versekre a legjobb bibliai
        memorizálási alkalmazással.
      keywords:
        - Bibliai memorizálás
        - Biblia alkalmazás
        - Bibliatanulmányozás
        - Károli Gáspár Bibliája
        - Bibliai versek
        - Bibliai memóriakártyák
        - Keresztény alkalmazás
        - Bibliai versképek
        - Bibliai elmélkedés
        - Napi igevers
      privacy_policy: /hu/docs/privacy
    menu:
      main:
        - name: Fórum
          url: https://forum.remem.me/
          weight: 25
        - name: Megjegyzendő versek
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  it:
    contentDir: content/it
    languageName: Italiano
    params:
      title: Remember Me. Memorizza la Bibbia
      description:
        Impara e conserva i versetti della Bibbia con la migliore app della
        Bibbia per la memorizzazione.
      keywords:
        - Memorizzazione della Bibbia
        - App biblica
        - Studio biblico
        - Sacra Bibbia CEI
        - Versetti biblici
        - Flashcard bibliche
        - Applicazione cristiana
        - Immagini di versetti biblici
        - Meditazione biblica
        - Versetto del giorno
      privacy_policy: /it/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Versetti da memorizzare
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  ja:
    contentDir: content/ja
    languageName: 日本語
    params:
      title: 聖書暗記アプリ Remember Me
      description: 人気の聖書暗記アプリで聖句をゲーム感覚で暗記・暗唱して記憶にとどめよう。
      keywords:
      - 聖書暗記
      - 聖書アプリ
      - 聖書学習
      - 新共同訳聖書
      - 聖句暗唱
      - 聖書フラッシュカード
      - クリスチャンアプリ
      - 聖句画像
      - 聖書瞑想
      - 今日の聖句
      privacy_policy: /ja/docs/privacy
    menu:
      main:
        - name: フォーラム
          url: https://forum.remem.me/
          weight: 25
        - name: 暗記する聖句
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  ko:
    contentDir: content/ko
    languageName: 한국어
    params:
      title: 리멤버 미. 성경암송 도우미
      description: 암기용 최고의 성경 앱으로 성경 구절을 배우고 유지하세요.
      keywords:
        - 성경 암기
        - 암기 구절
        - 성경 플래시 카드
        - 성경 암기하기
        - 성경 구절 이미지
        - 개역개정
        - 암기 게임
        - 성경 구절 듣기
        - 성경 구절 명상
        - 기독교 앱
        - 성경 암기 앱
      privacy_policy: /ko/docs/privacy
    menu:
      main:
        - name: 포럼
          url: https://forum.remem.me/
          weight: 25
        - name: 암기할 성경 구절
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  nl:
    contentDir: content/nl
    languageName: Nederlands
    params:
      title: Remember Me. Bijbel onthouden
      description:
        Leer en bewaar Bijbelverzen met de beste Bijbel-app om uit het
        hoofd te leren.
      keywords:
        - Bijbel memoriseren
        - Bijbel-app
        - Bijbelstudie
        - BasisBijbel
        - Bijbelverzen
        - Bijbelse flashcards
        - Christelijke app
        - Bijbelvers afbeeldingen
        - Bijbelse meditatie
        - Bijbeltekst van de dag
      privacy_policy: /nl/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Bijbelverzen
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  'no':
    contentDir: content/no
    languageName: Norsk
    params:
      title: Remember Me. Lære Bibelen
      description: Lær og behold bibelvers med den beste bibelappen for memorering.
      keywords:
        - Bibelmemorering
        - Bibel-app
        - Bibelstudium
        - Bibelen - Guds Ord
        - Bibelvers
        - Bibelske flashcards
        - Kristen app
        - Bibelvers bilder
        - Bibelsk meditasjon
        - Dagens bibelvers
      privacy_policy: /no/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Bibelvers å huske
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  pl:
    contentDir: content/pl
    languageName: Polski
    params:
      title: Remember Me. Zapamiętać Biblię
      description: Dowiedz się i zachowaj wersety biblijne z najlepszą aplikacją
        biblijną do zapamiętywania.
      keywords:
        - Zapamiętywanie Biblii
        - Aplikacja biblijna
        - Studium biblijne
        - Biblia Tysiąclecia
        - Wersety biblijne
        - Fiszki biblijne
        - Aplikacja chrześcijańska
        - Obrazy wersetów biblijnych
        - Medytacja biblijna
        - Werset dnia
      privacy_policy: /pl/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Wersety do zapamiętania
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  pt:
    contentDir: content/pt
    languageName: Português
    params:
      title: Remember Me. Memorizar a Bíblia
      description:
        Aprender e lembrar versículos bíblicos com o melhor aplicativo
        bíblico para memorização
      keywords:
        - 'Memorização da Bíblia'
        - 'Versículos para memorizar'
        - 'Flashcards bíblicos'
        - 'Memorizar a Escritura'
        - 'Imagens de versículos bíblicos'
        - 'Almeida Revista e Atualizada'
        - 'Jogos de memorização bíblica'
        - 'Ouvir versículos bíblicos'
        - 'Meditação bíblica'
        - 'Aplicativo cristão'
        - 'Aplicativo de memorização bíblica'
      privacy_policy: /pt/docs/privacy
    menu:
      main:
        - name: Fórum
          url: https://forum.remem.me/
          weight: 25
        - name: Versículos para decorar
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  ro:
    contentDir: content/ro
    languageName: Românesc
    params:
      title: Remember Me. Memoreze Biblia
      description:
        Învățați și memorați versete din Biblie cu cele mai bune aplicații
        de memorare a Bibliei.
      keywords:
        - Memorarea Bibliei
        - Aplicație biblică
        - Studiu biblic
        - Biblia Dumitru Cornilescu
        - Versete biblice
        - Flashcarduri biblice
        - Aplicație creștină
        - Imagini cu versete biblice
        - Meditație biblică
        - Versetul zilei
      privacy_policy: /ro/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Versete de memorat
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  ru:
    contentDir: content/ru
    languageName: Русский
    params:
      title: Помни меня. Запомнить Библию
      description:
        Выучите и запомните библейские стихи с помощью лучшего приложения
        для запоминания Библии.
      keywords:
        - Запоминание Библии
        - Библейское приложение
        - Изучение Библии
        - Синодальный перевод
        - Библейские стихи
        - Библейские флэшкарты
        - Христианское приложение
        - Изображения библейских стихов
        - Библейская медитация
        - Стих дня
      privacy_policy: /ru/docs/privacy
    menu:
      main:
        - name: Форум
          url: https://forum.remem.me/
          weight: 25
        - name: Стихи для запоминания
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  sv:
    contentDir: content/sv
    languageName: Svenska
    params:
      title: Remember Me. Memorera Bibeln
      description: Lär dig och behåll bibelverser med den bästa bibelappen för memorering.
      keywords:
        - Bibelmemorering
        - Bibel-app
        - Bibelstudier
        - Bibel 2000
        - Bibelverser
        - Bibliska flashcards
        - Kristen app
        - Bibelvers bilder
        - Biblisk meditation
        - Dagens bibelord
      privacy_policy: /sv/docs/privacy
    menu:
      main:
        - name: Forum
          url: https://forum.remem.me/
          weight: 25
        - name: Bibelverser att minnas
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  zh-cn:
    contentDir: content/zh-cn
    languageName: 中文 (简体中文)
    params:
      title: 记念我。背诵圣经金句
      description: 用最好的圣经背诵应用程序学习和记忆圣经经文。
      keywords:
        - 圣经背诵
        - 圣经应用
        - 圣经学习
        - 和合本
        - 圣经经文
        - 圣经抽认卡
        - 基督教应用
        - 圣经金句图片
        - 圣经默想
        - 每日经文
      privacy_policy: /zh-cn/docs/privacy
    menu:
      main:
        - name: 论坛
          url: https://forum.remem.me/
          weight: 25
        - name: 背诵经文
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
  zh-tw:
    contentDir: content/zh-tw
    languageName: 中文 (繁體)
    params:
      title: 記念我。背誦聖經金句
      description: 用最好的聖經背誦應用程序學習和記憶聖經經文。
      keywords:
        - 聖經背誦
        - 聖經應用程式
        - 聖經研讀
        - 和合本
        - 聖經經文
        - 聖經記憶卡
        - 基督教應用
        - 聖經金句圖片
        - 聖經默想
        - 每日經文
      privacy_policy: /zh-tw/docs/privacy
    menu:
      main:
        - name: 論壇
          url: https://forum.remem.me/
          weight: 25
        - name: 背誦經文
          url: https://web.remem.me/collections
          weight: 30
        - weight: 10
          url: /
          pre: <i class='fa-solid fa-download'></i>
markup:
  goldmark:
    renderer:
      unsafe: true
    parser:
      attribute:
        block: true
  highlight:
    style: tango
outputs:
  section:
    - HTML
    - print
params:
  copyright: Poimena
  privacy_policy: /docs/privacy
  thumbnail: /images/logo-full.svg
  images:
    - /images/screens-en.png
  version_menu: Releases
  archived_version: false
  version: "6.3"
  url_latest_version: https://example.com
  github_repo: https://gitlab.com/remem-me/doc/-
  github_branch: translate
  offlineSearch: true
  prism_syntax_highlighting: false
  ui:
    sidebar_menu_compact: false
    breadcrumb_disable: false
    sidebar_search_disable: false
    navbar_logo: false
    footer_about_enable: false
    feedback:
      enable: false
      no: Sorry to hear that. Please <a
        href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we
        can improve</a>.
      yes: Glad to hear it! Please <a
        href="https://github.com/USERNAME/REPOSITORY/issues/new">tell us how we
        can improve</a>.
    readingtime:
      enable: false
  links:
    user:
      - name: Donate
        url: /docs/donations/
        icon: fa fa-heart
    developer:
      - name: Translate
        url: /contribute/translate
        icon: fab fa-gitlab
module:
  hugoVersion:
    extended: true
    min: 0.127.0
  imports:
    - path: github.com/google/docsy
      disable: false
